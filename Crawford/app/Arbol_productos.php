<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arbol_productos extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
		'json',	'nombre', 'administrador', 'productos_id',	'marca',
	];
}
