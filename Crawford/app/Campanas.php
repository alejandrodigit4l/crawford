<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campanas extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nombre', 'descripcion', 'ifreclamos', 'ifcomprapoliza', 'aseguradora', 'tipo_seguro', 'marca',
    ];
}
