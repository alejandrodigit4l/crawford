<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chats extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'message','state','sender','receptor'
    ];
}
