<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'clientid',
		'nombre',
		'lastname',
		'email',
		'cedula',
		'password',
		'tokenAPI',
		'celular',
		'fechanacimiento',
		'fechaCC',
		'marca',
		'tipo_seguro',
		'aseguradora',
		'imei',
		'serial',
		'modelo',
		'ciudad',
		'sexo',
		'nombre_ciudad',
		'if_terminos',
		'if_notificaciones',
		'if_correo',
		'if_politicas',
		'estado',
		'fecha_ingreso',
		'dispositivo',
		'app',
		'so',
		'version_app',
        'sitio_creacion'
    ];

    protected $hidden = [
        'password'
    ];

    public function marca_cliente(){
    	return $this->belongsTo(marcas::class, 'marca', 'id');
    }

    public function scopeId($query, $id){
        if ($id) {
            return $query->where('id', 'LIKE', '%'.$id.'%');
        }
    }

    public function scopeMarca($query, $marca){
    	if ($marca) {
    		return $query->where('marca',$marca);
    	}
    }

    public function scopeNombre($query, $nombre){
    	if ($nombre) {
    		return $query->where('nombre', 'LIKE', '%'.$nombre.'%');
    	}
    }

    public function scopeLastname($query, $lastname){
    	if ($lastname) {
    		return $query->where('lastname', 'LIKE', '%'.$lastname.'%');
    	}
    }

    public function scopeEmail($query, $email){
    	if ($email) {
    		return $query->where('email', 'LIKE', '%'.$email.'%');
    	}
    }

    public function scopeCedula($query, $cedula){
    	if ($cedula) {
    		return $query->where('cedula', 'LIKE', '%'.$cedula.'%');
    	}
    }

    public function scopeCelular($query, $celular){
    	if ($celular) {
    		return $query->where('celular', 'LIKE', '%'.$celular.'%');
    	}
    }

}
