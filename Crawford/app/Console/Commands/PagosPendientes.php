<?php

namespace App\Console\Commands;

use App\Polizas;
use Illuminate\Console\Command;
use PayU;
use SupportedLanguages;
use Environment;
use PayUParameters;
use PayUPayments;
use PayUCountries;
use PayUReports;
use Mpdf\Mpdf;
use Mail;
use Auth;

class PagosPendientes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pago:pendiente';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $polizas = Polizas::whereRaw(' transaction_id IS NOT NULL and estado_poliza = 7 and response_code = "PENDING" ')->get();
        // $polizas = Polizas::all();
        $var = $polizas->count();

        PayU::$apiKey = "149389173be";
        PayU::$apiLogin = "2G93S6avzdzTj66";
        PayU::$merchantId = "109157";
        $accountId = "644720";

        // PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
        // PayU::$apiLogin = "pRRXKOl8ikMmt9u";
        // PayU::$merchantId = "508029";
        PayU::$language = SupportedLanguages::ES;
        PayU::$isTest = false;
        $reference = "Product_".rand(100, 100000);
        // Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi"); 
        Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi"); 
        

        // $archivo = fopen('prueba_poliza','w');
        // fwrite($archivo, $polizas);

        // return $polizas;

        foreach ($polizas as $poliza) {
            $parameters = array(PayUParameters::TRANSACTION_ID => $poliza->transaction_id,PayUParameters::ACCOUNT_ID => $accountId,);
            // $parameters = array(PayUParameters::TRANSACTION_ID => $poliza->transaction_id,PayUParameters::ACCOUNT_ID => '512321',);
            $response = PayUReports::getTransactionResponse($parameters);
            if ($response->state == 'APPROVED') {
            // if ($poliza->response_code == 'APPROVED') {
                $poliza->estado_poliza = 1;
                $poliza->fecha_vigencia = date('Y-m-d');
                $poliza->response_code = 'APPROVED';
                $poliza->state = 'NOTIFICADO';
                $poliza->save();
                
                $this->getGenerar($poliza->id);
                
            }elseif ($response->state == 'REJECTED') {
                $poliza->estado_poliza = 2;
                $poliza->response_code = 'REJECTED';
                $poliza->state = 'RECHAZADO';
                $poliza->save();
            }
        
        }

    }

    public function getGenerar($id)
    {
        error_reporting(0);

        // $data = Polizas::where('id', $id)
        // ->where('estado_poliza', 7)
        // ->with('productos','clientes')->first();

        $data = Polizas::where('id',$id)->first();

        $consecutivo = 3447 + $data->id;
        
        if( $data != null ){

            if( $data->productos->categoria != 'beneficio' ){
                
                $vista = array(
                    'iva' => ($data->productos->valor * 0.19),
                    'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
                    'precio' => $data->productos->valor,
                    'imei' => $data->imei,
                    'cubrimiento'=> $data->productos->cubrimiento,
                    'modelo'=> $data->productos->modelo,
                    'fecha'=> $data->fecha_vigencia,
                    'nombre'=> $data->clientes->nombre,
                    'apellido'=> $data->clientes->lastname,
                    'cedula'=> $data->clientes->cedula,
                    'telefono'=> $data->clientes->celular,
                    'ciudad'=> $data->clientes->ciudad,
                    'consecutivo' => $consecutivo,
                    'titulo' => $data->productos->titulo,
                    'descripcion' => $data->productos->descripcion,
                );
                $html = view('crawford.voluntariosSamsung',['data'=>$vista])->render();
                
                
            }else{
                $vista = array(
                    'iva' => ($data->productos->valor * 0.19),
                    'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
                    'precio' => $data->productos->valor,
                    'imei' => $data->imei,
                    'cubrimiento'=> $data->productos->cubrimiento,
                    'modelo'=> $data->productos->modelo,
                    'consecutivo' => $consecutivo
                );
                            
                $html = view('crawford.polizaSamsung',['data'=>$vista])->render();
            }


                $namefile = 'Poliza_'.$data->imei.'-'.date('y-m-d').'.pdf';
                $mpdf = new Mpdf();
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->WriteHTML($html);
                $path = public_path()."/polizasActivas/";
                $url = url('polizasActivas/'.$namefile);
                // $data->ruta_poliza = $path.$namefile;
                $data->url_poliza = $url;
                $data->state = 'notificado';
                $data->save();
                $fileName =$path.$namefile;
                $r = $mpdf->Output($path.$namefile, "F");

            $attachments = [
                public_path().'/condiciones/CONDICIONES PARTICULARES SAMSUNG MOBILECARE MANDATORIO- V GENERAL v final.pdf',
                public_path().'/condiciones/CONDICIONES_GENERALES.pdf',
                public_path().'/polizasActivas/'.$namefile
            ];

                Mail::send('mails/mail_samsung', 
                            [
                                'titulo' => 'Poliza adquirida equipo samsung',
                                'email' => $data->email,
                                'mensaje'=> 'En este mensaje '
                            ],
                                
                                function ($m) use ($data, $attachments)
                {   
                    foreach( $attachments as $adjunto ){
                        $m->attach($adjunto);
                    }
                    // $m->attach($fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($data->email, $data->name_user)->bcc('polizas@crawfordaffinitycolombia.com','Notificaciones Crawford')->subject('Notificación Seguros Mundial!');
                }); 
                return $data;

        }else{
            return "no hay clientes";
        }

    }

}
