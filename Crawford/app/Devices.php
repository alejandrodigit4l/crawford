<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'deviceid','registrationid','clientid','platform','cliente'
    ];
}
