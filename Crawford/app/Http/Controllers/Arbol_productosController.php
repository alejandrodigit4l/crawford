<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Arbol_productos;
use App\Usuarios;
use App\marcas;
use App\Productos;

class Arbol_productosController extends Controller
{
    //'json',	'nombre', 'administrador', 'productos_id',	'marca',
    /**
	*  respuesta todos las Arbol_productos
	*
	*  GET url
	*/
    public function all(){
    	$list = Arbol_productos::all();
    	foreach ($list as $arbol_productos) {
    		$arbol_productos->administrador = Usuarios::find($arbol_productos->administrador);
    		$arbol_productos->marca = marcas::find($arbol_productos->marca);
    		$arbol_productos->productos_id = Productos::find($arbol_productos->productos_id);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Arbol_productos
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Arbol_productos::where('id',$id)->get();
    	foreach ($list as $arbol_productos) {
    		$arbol_productos->administrador = Usuarios::find($arbol_productos->administrador);
    		$arbol_productos->marca = marcas::find($arbol_productos->marca);
    		$arbol_productos->productos_id = Productos::find($arbol_productos->productos_id);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Arbol_productos
	*  
	*  POST url
	*  @var json
	*  @var nombre
	*  @var administrador
	*  @var productos_id
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('administrador')!='' || request('marca')!=''){ //estan los datos completos
            $list = Arbol_productos::create(request(['json',	'nombre', 'administrador', 'productos_id',	'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'arbol_productos guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Arbol_productos
	*
	*  POST url
	*  @var id
	*  @var json
	*  @var nombre
	*  @var administrador
	*  @var productos_id
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('administrador')!='' || request('marca')!=''){ //estan los datos completos
            $arbol_productos = Arbol_productos::find(request('id'));
            $arbol_productos->json		= request('json');
			$arbol_productos->nombre		= request('nombre');
			$arbol_productos->administrador		= request('administrador');
			$arbol_productos->productos_id		= request('productos_id');
			$arbol_productos->marca		= request('marca');			
            $arbol_productos->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'arbol_productos Actualizada id:'.$arbol_productos->id,
                'body' => $arbol_productos
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Arbol_productos
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$arbol_productos = Arbol_productos::find($id);
        $arbol_productos->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'arbol_productos Eliminada id:'.$arbol_productos->id,
                'body' => $arbol_productos
            ]);
    }

    /**
	*  view index Arbol_productos
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Arbol_productos::all();
    	$marcas 		= marcas::all();
    	$usuarios 		= Usuarios::all();
    	foreach ($list as $arbol_productos) {
    		$arbol_productos->administrador = Usuarios::find($arbol_productos->administrador);
    		$arbol_productos->marca = marcas::find($arbol_productos->marca);
    		$arbol_productos->productos_id = Productos::find($arbol_productos->productos_id);
    	}
    	return view('arbol_productos', array(
    		'arbol_productos' => $list,
            'marcas' => $marcas,
            'usuarios' => $usuarios
    	));
    }

    /**
	*  view Filtros de  Arbol_productos
	*
	*  GET url 
	*/
    public function filtros($filtro){
        $marcas         = marcas::all();
    	$list = Arbol_productos::where('titulo', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $arbol_productos) {
    		$arbol_productos->administrador = Usuarios::find($arbol_productos->administrador);
    		$arbol_productos->marca = marcas::find($arbol_productos->marca);
    		$arbol_productos->productos_id = Productos::find($arbol_productos->productos_id);
    	}
    	return view('arbol_productos', array(
    		'arbol_productos' => $list,
            'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Arbol_productos
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Arbol_productos::all();
		return Excel::create('Arbol_productos', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
