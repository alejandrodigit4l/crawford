<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;

use Excel;

class AseguradorasController extends Controller
{
    //
    /**
	*  respuesta todos las Aseguradoras
	*
	*  GET url
	*/
    public function all(){
    	$list = Aseguradoras::all();
    	foreach ($list as $aseguradora) {
    		$aseguradora->marca       = marcas::find($aseguradora->marca);
            $aseguradora->tipo_seguro = Tipo_seguros::find($aseguradora->tipo_seguro);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Aseguradoras
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Aseguradoras::where('id',$id)->get();
    	foreach ($list as $aseguradora) {
    		$aseguradora->marca       = marcas::find($aseguradora->marca);
            $aseguradora->tipo_seguro = Tipo_seguros::find($aseguradora->tipo_seguro);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Aseguradoras
	*
	*  POST url
	*  @var nombre
	*  @var descripcion
	*  @var tipo_seguro
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!='' || request('tipo_seguro')!=''){ //estan los datos completos
            $list = Aseguradoras::create(request(['nombre', 'descripcion', 'tipo_seguro', 'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Aseguradoras
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!='' || request('id')!='' || request('tipo_seguro')!=''){ //estan los datos completos
            $aseguradora = Aseguradoras::find(request('id'));
            $aseguradora->nombre = request('nombre');
            $aseguradora->descripcion = request('descripcion');
            $aseguradora->marca = request('marca');
            $aseguradora->tipo_seguro = request('tipo_seguro');
            $aseguradora->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras Actualizada id:'.$aseguradora->id,
                'body' => $aseguradora
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Aseguradoras
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$aseguradora = Aseguradoras::find($id);
        $aseguradora->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras Eliminada id:'.$aseguradora->id,
                'body' => $aseguradora
            ]);
    }

    /**
	*  view index Aseguradoras
	*
	*  GET url 
	*/
    public function index(){
    	$list = Aseguradoras::all();
    	$marcas = marcas::all();
        $tipo_seguros = Tipo_seguros::all();
    	foreach ($list as $aseguradora) {
    		$aseguradora->marca       = marcas::find($aseguradora->marca);
            $aseguradora->tipo_seguro = Tipo_seguros::find($aseguradora->tipo_seguro);
    	}
    	return view('aseguradoras', array(
    		'aseguradoras' => $list,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros
    	));
    }

    /**
    *  respuesta una de las Aseguradoras
    *
    *  GET url
    *  @var id tipo_seguro 
    */
    public function tipo_seguro($tipo_seguro){
        $list = Aseguradoras::where('tipo_seguro',$tipo_seguro)->get();
        foreach ($list as $aseguradora) {
            $aseguradora->marca = marcas::find($aseguradora->marca);
            $aseguradora->tipo_seguro = Tipo_seguros::find($aseguradora->tipo_seguro);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Aseguradoras
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas = marcas::all();
        $tipo_seguros = Tipo_seguros::all();
    	$list = Aseguradoras::where('nombre', 'like', '%'.$filtro.'%')
						->orWhere('descripcion', 'like', '%'.$filtro.'%')
						->orWhere('marca', '=', $filtro)
						->orWhere('tipo_seguro', '=', $filtro)
						->get();
		foreach ($list as $aseguradora) {
    		$aseguradora->marca       = marcas::find($aseguradora->marca);
            $aseguradora->tipo_seguro = Tipo_seguros::find($aseguradora->tipo_seguro);
    	}
    	return view('aseguradoras', array(
    		'aseguradoras' => $list ,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros
    	));
    }

    /**
	*  view EXCEL Aseguradoras
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Aseguradoras::all();
		return Excel::create('Aseguradoras', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
