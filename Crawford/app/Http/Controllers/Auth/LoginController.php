<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{

    public function __construct(){
        $this->middleware('guest', ['only'=> 'showLoginForm']);
    }
    
   public function login(Request $request){
    //    $credenciales = $this->validate(request(), [
    //        'email'=> 'email|required|string',
    //        'password'=> 'required|string',
    //        'marca'=> 'int'
    //    ]);
    $user = Auth::user();

    $credenciales = $request->only('email', 'password', 'marca');

       if(Auth::attempt($credenciales) ){

        // if(Auth::user()->token_session == '' || Auth::user()->token_session == null){
            $token_session = md5(uniqid());

            User::where('email', $request->email)
                    // ->where('password', $request->password)
                    ->where('marca',$request->marca)
                    ->update(['token_session' => $token_session]);
            return redirect()->intended('/crawford/index');
        // }

       }

    //    return redirect()->route('login.crawford');
    return view('auth.login');

   }

   public function showLoginForm(){
    return view('auth.login');
   }

   public function logout(Request $request){
       Auth::logout();
       User::where('token_session', $request->token_session)                    
                    ->update(['token_session' => null]);
    //    return redirect()->route('/crawford');
    return view('auth.login');
   }

   public function eliminaToken(Request $request){
        User::where('email', $request->email)              
                ->where('password', $request->password)                   
                ->where('token_session', $request->token_session)
                ->update(['token_session' => null]);
   }


}
