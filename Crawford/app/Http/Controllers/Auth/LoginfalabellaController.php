<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;

class LoginfalabellaController extends Controller
{

    public function __construct(){
        $this->middleware('guest', ['only'=> 'showLoginForm']);
    }
    
   public function login(Request $request){
    //    $credenciales = $this->validate(request(), [
    //        'email'=> 'email|required|string',
    //        'password'=> 'required|string'
    //    ]);

    $credenciales = $credentials = $request->only('email', 'password', 'marca');

    if(Auth::attempt($credenciales)){
        if(Auth::user()->token_session == '' || Auth::user()->token_session == null){
            $token_session = md5(uniqid());

            User::where('email', $request->email)
                    // ->where('password', $request->password)
                    ->where('marca',$request->marca)
                    ->update(['token_session' => $token_session]);
                    return redirect()->intended('/falabella/index');
            
        }
   }

    //    return redirect()->route('login.falabella');
    return view('auth.loginfalabella');

   }

   public function showLoginForm(){
    return view('auth.loginfalabella');
   }

   public function logout(Request $request){
       Auth::logout();
       User::where('token_session', $request->token_session)                    
                    ->update(['token_session' => null]);
    //    return redirect()->route('login.falabella');
    return view('auth.loginfalabella');
   }

   public function eliminaToken(Request $request){
    User::where('email', $request->email)              
            ->where('password', $request->password)                   
            ->where('token_session', $request->token_session)
            ->update(['token_session' => null]);
}


}
