<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;

use Excel;

class CampanasController extends Controller
{
    //'nombre', 'descripcion', 'ifreclamos', 'ifcomprapoliza', 'aseguradora', 'tipo_seguro', 'marca'
    /**
	*  respuesta todos las Campanas
	*
	*  GET url
	*/
    public function all(){
    	$list = Campanas::all();
    	foreach ($list as $campana) {
    		$campana->marca       = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Campanas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Campanas::where('id',$id)->get();
    	foreach ($list as $campana) {
    		$campana->marca       = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Campanas
	*
	*  POST url
	*  @var nombre
	*  @var descripcion
	*  @var ifreclamos
	*  @var ifcomprapoliza
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('ifreclamos')!='' || request('ifcomprapoliza')!=''){ //estan los datos completos
            $list = Campanas::create(request(['nombre', 'descripcion', 'ifreclamos', 'ifcomprapoliza', 'aseguradora', 'tipo_seguro', 'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Campanas
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*  @var ifreclamos
	*  @var ifcomprapoliza
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('ifreclamos')!='' || request('id')!='' || request('ifcomprapoliza')!=''){ //estan los datos completos
            $campana = Campanas::find(request('id'));
            $campana->nombre = request('nombre');
            $campana->descripcion = request('descripcion');
            $campana->ifreclamos = request('ifreclamos');
            $campana->ifcomprapoliza = request('ifcomprapoliza');
            $campana->aseguradora = request('aseguradora');
            $campana->marca = request('marca');
            $campana->tipo_seguro = request('tipo_seguro');
            $campana->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'campana Actualizada id:'.$campana->id,
                'body' => $campana
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Campanas
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$campana = Campanas::find($id);
        $campana->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras Eliminada id:'.$campana->id,
                'body' => $campana
            ]);
    }

    /**
	*  view index Campanas
	*
	*  GET url 
	*/
    public function index(){
    	$list 			= Campanas::all();
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
    	foreach ($list as $campana) {
    		$campana->marca       = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
    	}
    	return view('campanas', array(
    		'campanas' => $list,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras
    	));
    }

    /**
    *  respuesta una de las Tipo_seguros
    *
    *  GET url
    *  @var id aseguradora 
    */
    public function aseguradora($aseguradora){
        $list = Campanas::where('aseguradora',$aseguradora)->get();
        foreach ($list as $campana) {
            $campana->marca = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
            
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Campanas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
    	$list = Campanas::where('nombre', 'like', '%'.$filtro.'%')
						->orWhere('descripcion', 'like', '%'.$filtro.'%')
						->orWhere('marca', '=', $filtro)
						->orWhere('tipo_seguro', '=', $filtro)
						->orWhere('ifreclamos', '=', $filtro)
						->orWhere('ifcomprapoliza', '=', $filtro)
						->orWhere('aseguradora', '=', $filtro)
						->get();
		foreach ($list as $campana) {
    		$campana->marca       = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
    	}
    	return view('campanas', array(
    		'campanas' => $list ,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras
    	));
    }

    /**
	*  view EXCEL Aseguradoras
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Campanas::all();
		return Excel::create('Campanas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
