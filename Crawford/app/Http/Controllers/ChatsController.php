<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Chats;
use App\Clientes;
use LRedis;
use Auth;

use Excel;

class ChatsController extends Controller
{

    //'message','state','sender','receptor'
    /**
	*  respuesta todos las Chats
	*
	*  GET url
	*/
    public function all(){
    	$list = Chats::all();
    	return response()->json([
            'Status' => 'successful',
            'Message' => 'Se encontraron coincidencias',
            'body' => $list
        ]);
    } 

    /**
	*  respuesta una de las Chats
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Chats::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta historial de conversacion con una persona de las Chats
    *
    *  GET url
    *  @var id identificador de la marca en la base de datos
    */
    public function historial($sender,$receptor){
        //$list = Chats::where('sender',$sender)->where('receptor',$receptor)->orderBy('created_at', 'DESC')->get();
        $list = DB::select("select * from chats  WHERE `sender` LIKE '".$sender."' AND `receptor` LIKE '".$receptor."' OR `sender` LIKE '".$receptor."' AND `receptor` LIKE '".$sender."'  ORDER BY `chats`.`id` ASC");
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    public function sendMessage(){
        $redis = LRedis::connection();
        $data = ['message' => Request::input('message'), 'user' => Request::input('user')];
        //$redis->publish('message', json_encode($data));
        return response()->json( $data );
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var message
	*  @var state
	*  @var sender
	*  @var receptor
	*/
    public function save(){
    	$list = Chats::create(request(['message','state','sender','receptor']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Chats guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Chats
	*
	*  POST url
	*  @var id
	*  @var message
	*  @var state
	*  @var sender
	*  @var receptor
	*/
    public function update(){
    	$Chats = Chats::find(request('id'));
        $Chats->message = request('message');
        $Chats->state = request('state');
        $Chats->sender = request('sender');
        $Chats->receptor = request('receptor');
        $Chats->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Chats Actualizada id:'.$Chats->id,
            'body' => $Chats
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Chats
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Chats = Chats::find($id);
        $Chats->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Chats Eliminada id:'.$Chats->id,
                'body' => $Chats
            ]);
    }

    /**
	*  view index Chats
	*
	*  GET url 
	*/
    public function index(){
    	$list = Chats::all();
    	return view('chats', array(
    		'chats' => $list 
    	));
    }

    /**
	*  view EXCEL Chats
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Chats::all();
		return Excel::create('Chats', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function chatcrawford(){
        // $chats = Chats::selectRaw('count(*) AS cnt, sender, state')->where('sender','!=','1')->groupBy('sender')->orderByRaw('max(created_at)DESC')->get();

        $chats = Chats::selectRaw('count(*) as cnt, sender, max(state) as estado, max(updated_at) as ultimo')->whereRaw('sender != 1')->groupBy('sender')->orderByRaw('max(updated_at) desc')->get();

        foreach ($chats as $chat) {
            $chat->sender = Clientes::find($chat->sender);
        }
        $sender = Clientes::find('1');

        if (Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER'])) {
            return view('crawford.chats', array(
                'chats' => $chats ,
                'desde' => $sender
            ));   
        }else{
            return view('crawford.error');
        }
    }

    public function backend($sender){
        // $chats = Chats::selectRaw('count(*) AS cnt, sender, state')->where('sender','!=','1')->groupBy('sender')->orderByRaw('max(created_at)DESC')->get();

        $chats = Chats::selectRaw('count(*) as cnt, sender, max(state) as estado, max(updated_at) as ultimo')->whereRaw('sender != 1')->groupBy('sender')->orderByRaw('max(updated_at) desc')->get();

        foreach ($chats as $chat) {
            $chat->sender = Clientes::find($chat->sender);
        }
        $sender = Clientes::find($sender);
        return view('crawford.chats', array(
            'chats' => $chats ,
            'desde' => $sender
        ));
    }

    public function cambio_estado($id_cliente){

        $update_estado = Chats::where('sender','=',$id_cliente)->update(['state' => 'LEIDO']);

        return $update_estado;

    }

    public function todos(){

        $todos = Chats::All();

        return $todos;

    }
}
