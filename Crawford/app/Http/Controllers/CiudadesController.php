<?php

namespace App\Http\Controllers;

use Excel;
use App\Ciudades;
use App\Departamento;
use Illuminate\Http\Request;
use App\Ciudades_departamentos;
use App\ZurichCiudadDepartamentos;

class CiudadesController extends Controller
{
    //'nombre','codeDian','codeUno','codeDos'
    /**
	*  respuesta todos las Ciudades
	*
	*  GET url
	*/
    public function all(){
    	$list = Departamento::select('*')->orderBy('nombre','asc')->get();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Ciudades
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Ciudades::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Ciudades
	*
	*  POST url
	*  @var nombre
	*  @var codeDian
	*  @var codeUno
	*  @var codeDos
	*/
    public function save(){
    	$list = Ciudades::create(request(['nombre','codeDian','codeUno','codeDos']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'ciudades guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Ciudades
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var codeDian
	*  @var codeUno
	*  @var codeDos
	*/
    public function update(){
    	$ciudades = Ciudades::find(request('id'));
        $ciudades->nombre = request('nombre');
		$ciudades->codeDian = request('codeDian');
		$ciudades->codeUno = request('codeUno');
		$ciudades->codeDos = request('codeDos');
        $ciudades->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'ciudades Actualizada id:'.$ciudades->id,
            'body' => $ciudades
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Ciudades
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$ciudades = Ciudades::find($id);
        $ciudades->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'ciudades Eliminada id:'.$ciudades->id,
                'body' => $ciudades
            ]);
    }

    /**
	*  view index Ciudades
	*
	*  GET url 
	*/
    public function index(){
    	$list = Ciudades::all();
    	return view('ciudades', array(
    		'ciudades' => $list 
    	));
    }

    /**
	*  view Filtros de  Ciudades
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Ciudades::where('nombre', 'like', '%'.$filtro.'%')
						->get();
    	return view('ciudades', array(
    		'ciudades' => $list 
    	));
    }

    /**
	*  view EXCEL Ciudades
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Ciudades::all();
		return Excel::create('Ciudades', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function show_ciudades($id_departamento, $marca)
    {
        if ($marca != 5) {
            $ciudades_dep = Ciudades_departamentos::where('idDepartamento','=',$id_departamento)->orderBy('nombre','ASC')->get();   
        }else{
            $ciudades_dep = ZurichCiudadDepartamentos::where('idDepartamento','=',$id_departamento)->orderBy('nombre','ASC')->get();   
        }
        return array(
            'success' => 'true',
            'message' => 'Se ha recibido la info',
            'body' => $ciudades_dep
        );

    }
}
