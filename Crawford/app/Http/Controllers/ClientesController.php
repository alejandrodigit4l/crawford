<?php

namespace App\Http\Controllers;

use File;
use Mail;
use Excel;
use Response;
use App\Imeis;
use Mpdf\Mpdf;
use OneSignal;
use App\marcas;
use App\Devices;
use App\Polizas;
use App\Clientes;
use App\ListaMarcas;
use App\Aseguradoras;
use App\Departamento;
use App\ListaTiendas;
use App\Mail\Welcome;
use App\Tipo_seguros;
use App\ListaCausales;
use App\ListaSponsors;
use App\ListaProductos;
use App\ZurichReclamos;
use App\ZurichDepartamentos;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;

class ClientesController extends Controller
{
    //'clientid','nombre','lastname','email','cedula','password','tokenAPI','celular','fechanacimiento','fechaCC','marca','tipo_seguro','aseguradora'
	/**
	*  respuesta todos las Clientes
	*
	*  GET url
	*/
    public function all(){
    	$list = Clientes::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Clientes
	*
	*  GET url
	*  @var id identificador de la Clientes en la base de datos
	*/
    public function get($id){
    	$list = Clientes::where('id',$id)->get();
    	foreach ($list as $cliente) {
    		$cliente->marca = marcas::find($cliente->marca);
    		$cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
    		$cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Clientes
	*
	*  POST url
	*  
	*  @var clientid
	*  @var nombre
	*  @var lastname
	*  @var email
	*  @var cedula
	*  @var password
	*  @var tokenAPI
	*  @var celular
	*  @var fechanacimiento
	*  @var fechaCC
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*/
    public function save(Request $request){
        $validate = Clientes::where('email','like',request('email'))->where('aseguradora','like',request('aseguradora'))->get();
        if(sizeof($validate)==0){
            $list = Clientes::create([
                'clientid' => $request->clientid,
                'nombre' => $request->nombre,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'cedula' => $request->cedula,
                'password' => encrypt($request->password),
                // 'tokenAPI' => $request->tokenAPI,
                'celular' => $request->celular,
                'fechanacimiento' => $request->fechanacimiento,
                'marca' => $request->marca,
                'tipo_seguro' => $request->tipo_seguro,
                'aseguradora' => $request->aseguradora,
                'sitio_creacion' => 'webapp'
            ]);
                
            
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Clientes guardada id:'.$list->id,
                'body' => $list
            ]);

            //push onesignal send
            $clientId = $list->id;
            $message = 'Bienvenido a Seguros Mundial '.$list->email;
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }

            //mail send
            $client = request('email');
            $fileName = '';
            if (request('marca') == 3) { /*-- Seguros Mundial --*/
                Mail::send('mails/mail_seguros', 
                    [
                            'titulo' => 'Bienvenido a Seguros Mundial '.$client,
                            'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/Notifications/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to($client, 'usuario')->subject('Bienvenid@ a Seguros Mundial!');
                    });
            }elseif(request('marca') == 1){
                Mail::send('mails/mail_crawford', 
                    [
                            'titulo' => 'Bienvenido a Seguros Mundial '.$client,
                            'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/Notifications/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to($client, 'usuario')->subject('Bienvenid@ a Seguros Mundial!');
                    });
            }elseif(request('marca') == 4) {
                Mail::send('mails/mail_samsung', 
                    [
                            'titulo' => 'Bienvenido a Seguros Mundial '.$client,
                            'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/Notifications/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to($client, 'usuario')->subject('Bienvenid@ a Seguros Mundial!');
                    });
            }else{
                Mail::send('mails/mail', 
                    [
                            'titulo' => 'Bienvenido a Seguros Mundial '.$client,
                            'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/Notifications/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to($client, 'usuario')->subject('Bienvenid@ a Seguros Mundial!');
                    });
            }

        }
        else{
            $response = response()->json([
                'Status' => 'false',
                'Message' => 'Correo ya se encuentra registrado',
                'body' => array()
            ]);
        }
        
        return $response;
    }

    public function save_admin(Request $request){
        $this->validate($request,[
            'nombres' => 'required',
            'apellidos' => 'required',
            'cedula' => 'required',
            'email_cliente' => 'required',
            'celular' => 'required',
            'fechanacimiento' => 'required',
            'password_cliente' => 'required',
            'password_cliente_validate' => 'required',
            'marca' => 'required'
        ]);
        if ($request->password_cliente != $request->password_cliente_validate) {
            return response()->json(['message' => 'Las contraseñas no coinciden'], 422);
        }
        
        $consulta_cliente = Clientes::where('marca', $request->marca)
                            ->where('cedula', $request->cedula)
                            ->get();

        if ($consulta_cliente->count() != 0) {
            return response()->json(['message' => 'Ya se encuentra registrado un usuario con estos valores.'], 422);
        }

        $list = Clientes::create([
            'clientid' => $request->clientid,
            'nombre' => $request->nombres,
            'lastname' => $request->apellidos,
            'email' => $request->email_cliente,
            'cedula' => $request->cedula,
            'password' => encrypt($request->password_cliente),
            // 'tokenAPI' => $request->tokenAPI,
            'celular' => $request->celular,
            'fechanacimiento' => $request->fechanacimiento,
            'marca' => $request->marca,
            'tipo_seguro' => $request->tipo_seguro,
            'aseguradora' => $request->aseguradora,
            'sitio_creacion' => $request->sitio_creacion
        ]);
            
        $response = response()->json(['id_cliente' => $list->id], 201);
    	
        return $response;
    }

    public function validar_correo_registro($correo){
        $buscar_correo = Clientes::where('email', $correo)->get();
        $cantidad_correos = $buscar_correo->count();
        return array(
            'correos' => $buscar_correo,
            'cantidad_correos' => $cantidad_correos
        );
    }

    /**
	*  respuesta Actualizacion de Clientes
	*
	*  POST url
	*  @var id
	*  @var clientid
	*  @var nombre
	*  @var lastname
	*  @var email
	*  @var cedula
	*  @var password
	*  @var tokenAPI
	*  @var celular
	*  @var fechanacimiento
	*  @var fechaCC
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*/
    public function update(Request $request){

        $consulta_token = Clientes::where('id',$request['id'])->where('marca',$request['marca'])->get();
        if ($consulta_token[0]->tokenAPI == $request['token_']) {

        	$cliente = Clientes::find(request('id'));
    		$cliente->nombre = request('nombre');
    		$cliente->lastname = request('lastname');
    		$cliente->email = request('email');
    		$cliente->cedula = request('cedula');
            if ($request->has('password')) {
                $cliente->password = encrypt(request('password'));   
            }
    		$cliente->celular = request('celular');
            $cliente->save();

            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);

            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'cliente Actualizada id:'.$cliente->id,
                'body' => $cliente
            ]);
            return $response;

        }else{
            return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION';
        }

    }

    public function update_admin(Request $request){

            $cliente = Clientes::find(request('id'));
            $cliente->nombre = request('nombre');
            $cliente->lastname = request('lastname');
            $cliente->email = request('email');
            $cliente->cedula = request('cedula');
            $cliente->password = md5(request('password'));
            $cliente->celular = request('celular');
            $cliente->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'cliente Actualizada id:'.$cliente->id,
                'body' => $cliente
            ]);
            return $response;
            // return $request['password'];

    }

    /**
	*  respuesta Eliminar Clientes
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$homologos = Clientes::find($id);
        $homologos->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'homologos Eliminada id:'.$homologos->id,
                'body' => $homologos
            ]);
    }

    /**
	*  view index Clientes
	*
	*  GET url 
	*/
    public function index(){
    	$list = Clientes::all();
    	$marcas = marcas::all();
    	foreach ($list as $cliente) {
    		$cliente->marca = marcas::find($cliente->marca);
    		$cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
    		$cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
    	}
    	return view('clientes', array(
    		'clientes' => $list ,
    		'marcas' => $marcas
    	));
    }

    /**
    *  respuesta una de las Productos
    *
    *  GET url
    *  @var id marca 
    */
    public function marca($marca){
        $list = Productos::where('marca',$marca)->get();
        foreach ($list as $cliente) {
            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Clientes
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas = marcas::all();
    	$list = Clientes::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $cliente) {
    		$cliente->marca = marcas::find($cliente->marca);
    		$cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
    		$cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
    	}
    	return view('clientes', array(
    		'clientes' => $list ,
    		'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Clientes
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Clientes::all();
		return Excel::create('clientes', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }




    /**
    *  view LOGIN Clientes
    *
    *  POST url 
    *  @var email
    *  @var password
    *  @var marca
    */
    public function login(Request $request){

        $marca = $request->marca;
        $aseguradora = $request->aseguradora;
        $email = $request->email;
        $pass = $request->password;

        $list = Clientes::where('marca', $marca)
                        ->where('aseguradora',$aseguradora)
                        ->where('email',$email)
                        ->where('sitio_creacion','webapp')
                        ->get();

        $validate_pass = $list->first();

        try {
            $decrypted = decrypt($validate_pass->password);

            if ($decrypted != $pass) {
                return response()->json([
                    'Status' => 'false',
                    'Message' => 'Existen datos que no coinciden con el usuario. Vuelve a intentar',
                    'body' => $list
                ]);
            }

        } catch (DecryptException $e) {
            return 'No es el formato correcto';
        }

        if(sizeof($list)==0){
            $response = response()->json([
                'Status' => 'false',
                'Message' => 'Existen datos que no coinciden con el usuario. Vuelve a intentar',
                'body' => $list
            ]);
        }
        else{

            foreach ($list as $cliente) {
                $cliente->marca = marcas::find($cliente->marca);
                $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
                $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
            }

            if ($list[0]->tokenAPI == '') {
                $token = csrf_token();

                Clientes::where('marca', $marca)
                            ->where('aseguradora',$aseguradora)
                            ->where('email',$email)
                            ->where('sitio_creacion','webapp')
                            ->update(['tokenAPI' => $token]);

                $response = response()->json([
                    'Status' => 'successful',
                    'Message' => 'Se encontraron coincidencias',
                    'body' => $list,
                    'token' => $token
                ]);

                if (request('device_token') && request('device_token') != '') {
                    $client = $list->first();
                    $devices = Devices::where('clientid', $client->id)->where('deviceid', request('device_token'))->get();
                    if (sizeof($devices) == 0) {
                        $device = new Devices;
                        $device->clientid = $client->id;
                        $device->registrationid = $client->id;
                        $device->platform = 'mobile';
                        $device->deviceid = request('device_token');
                        $device->cliente = '';
                        $device->save();
                    }
                }                
            }else{
                $response = response()->json([
                    'Status' => 'false',
                    'Message' => 'Ya existe un usuario sesionado! Vuelve a intentar mas tarde',
                    'body' => $list
                ]);
            }

        }
        return $response;
    }

    /**
    *  view OLVIDO Clientes
    *
    *  POST url 
    *  @var email
    *  @var password
    *  @var marca
    */
    public function olvido(){
        $list = Clientes::where('marca','like',request('marca'))
                        ->where('email','like',request('email'))
                        ->get();
        if(sizeof($list)==0){
            $response = response()->json([
                'Status' => 'false',
                'Message' => 'No se encuentra este correo registrado',
                'body' => $list
            ]);
        }
        else{
            //send push onesignal
            $clientId = $list[0]->id;
            $lista = $list[0];
            $message = 'confirmación clave verifique su correo electronico';
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }

            $fileName = '';
            if (request('marca') == 4) {
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'confirmación clave',
                        'mensaje'=> 'Hemos recibido la solicitud de envio de contraseña, recuerde que la contraseña es: '.decrypt($lista->password)
                ], 
                function ($m) use ($lista, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($lista->email, $lista->nombre )->subject('Confirmación clave');
                });    
            }elseif (request('marca') == 3) {
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'confirmación clave',
                        'mensaje'=> 'Hemos recibido la solicitud de envio de contraseña, recuerde que la contraseña es: '.decrypt($lista->password)
                ], 
                function ($m) use ($lista, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($lista->email, $lista->nombre )->subject('Confirmación clave');
                });   
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'confirmación clave',
                        'mensaje'=> 'Hemos recibido la solicitud de envio de contraseña, recuerde que la contraseña es: '.decrypt($lista->password)
                ], 
                function ($m) use ($lista, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($lista->email, $lista->nombre )->subject('Confirmación clave');
                });   
            }
            
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Se enviara un correo indicando cual es tu contraseña',
                'body' => $lista
            ]);
        }
        return $response;
    }

    /**
    *   GET sendmail
    */
    public function send(){
        $client = '';
        $fileName = '';
        Mail::send('mails/mail', 
            [
                    'titulo' => 'Bienvenido a Seguros Mundial',
                    'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
            ], 
            function ($m) use ($client, $fileName) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/Notifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to('alejandro@digit4l.co', 'alejandro')->subject('Notificación Seguros Mundial!');
            });
    }

    public function loginSamsung(Request $request){


        $reglas = [
            'email' => 'required|email',
            'imei' => 'required',
        ];

        $this->validate($request,$reglas);

        // * LOGUEO ACTUALIZACION DE DATOS O REGISTROS * //

        $client = Clientes::where('email', $request->email)
                    ->where('imei', $request->imei)->get();

            if( $client->count() > 0 ){
                $cliente =  $client->first();            
                $cliente->nombre = $request->nombre;
                $cliente->lastname = $request->lastname;
                $cliente->email = $request->email; 
                $cliente->cedula = $request->cedula;
                // $cliente->password = $request->password;
                $cliente->celular = $request->celular;
                $cliente->fechanacimiento = $request->fechanacimiento;
                $cliente->sexo = $request->sexo;
                $cliente->ciudad = $request->ciudad;
                $cliente->if_terminos = $request->if_terminos;
                $cliente->if_notificaciones = $request->if_notificaciones;
                $cliente->if_politicas = $request->if_politicas;
                $cliente->if_correo = $request->if_correo;
                $cliente->estado = $request->estado;
                $cliente->imei = $request->imei;
                $cliente->serial = $request->serial;
                $cliente->modelo = $request->modelo;
                $cliente->dispositivo = $request->dispositivo;
                $cliente->app = $request->app;
                $cliente->version_app = $request->version_app;
                $cliente->so = $request->so;
                $cliente->marca = 4;
                $cliente->tokenAPI = $request->token;
                $cliente->save();


                $polizas_existentes = Polizas::where('imei',$request->imei)->get();

                $busqueda_imei = Imeis::where('imeis',$request->imei)->OrWhere('imeis2',$request->imei)
                                      ->where('estado_enviado','CARGADO')
                                      ->first();

                if (!empty($busqueda_imei) && $polizas_existentes->count() == 0) {

                    $producto = Polizas::create([
                        'marca'=> 4,
                        'estado_poliza'=> 1,
                        'name_user'  => $request->nombre.' '.$request->lastname,
                        'fecha_compra' => date('Y-m-d'),
                        'price'    => '0',
                        'email'    => $request->email,
                        'name_product' => 'Seguro voluntario 12 meses de cobertura total beneficiario',
                        'serie'    => $request->serial,      
                        'imei'    => $request->imei,
                        'cobertura'  => '12',
                        'producto'  => '38',
                        'transaction_id' => 'beneficio',
                        'cliente'   => $client[0]->id,
                        'user_create' => 'app_samsung',
                    ]);

                    $busqueda_imei->estado_enviado = 'NOTIFICADO';
                    $busqueda_imei->save();
                    $this->getGenerar($producto->id);
                    
                }elseif (!empty($busqueda_imei) && $polizas_existentes->count() > 0) {
                    $busqueda_imei->estado_enviado = 'VOLUNTARIO ADQUIRIDO';
                    $busqueda_imei->save();
                }
                
                // return 'http://localhost:8100/#/samsung-club/'.$cliente->imei.'/'.$cliente->serial.'/'.$cliente->modelo;
                return 'https://admin.crawfordaffinitycolombia.com/s9/#/samsung-club/'.$cliente->imei.'/'.$cliente->serial.'/'.$cliente->modelo;
            }
            else{
                $cliente =  Clientes::create( $request->all() );
                $client = Clientes::where('email', $request->email)
                        ->where('imei', $request->imei)
                        ->update(["marca"=> 4, "tokenAPI"=> $request->token ]);

                $polizas_existentes = Polizas::where('imei',$request->imei)->get();

                $busqueda_imei = Imeis::where('imeis',$request->imei)->OrWhere('imeis2',$request->imei)
                                      ->where('estado_enviado','CARGADO')
                                      ->first();

                if (!empty($busqueda_imei) && $polizas_existentes->count() == 0) {

                    $producto = Polizas::create([
                        'marca'=> 4,
                        'estado_poliza'=> 1,
                        'name_user'  => $request->nombre.' '.$request->lastname,
                        'fecha_compra' => date('Y-m-d'),
                        'price'    => '0',
                        'email'    => $request->email,
                        'name_product' => 'Seguro voluntario 12 meses de cobertura total beneficiario',
                        'serie'    => $request->serial,      
                        'imei'    => $request->imei,
                        'cobertura'  => '12',
                        'producto'  => '38',
                        'transaction_id' => 'beneficio',
                        'cliente'   => $cliente[0]->id,
                        'user_create' => 'app_samsung',
                    ]);

                    $busqueda_imei->estado_enviado = 'NOTIFICADO';
                    $busqueda_imei->save();

                    $this->getGenerar($producto->id);
                    
                }elseif (!empty($busqueda_imei) && $polizas_existentes->count() > 0) {
                    $busqueda_imei->estado_enviado = 'VOLUNTARIO ADQUIRIDO';
                    $busqueda_imei->save();
                }
                
                // $client->tokenAPI = $request->token;
                // $client->marca = 4;
                // $client->save();
                // return 'http://localhost:8100/#/samsung-club/'.$cliente->imei.'/'.$cliente->serial.'/'.$cliente->modelo;
                return 'https://admin.crawfordaffinitycolombia.com/s9/#/samsung-club/'.$cliente->imei.'/'.$cliente->serial.'/'.$cliente->modelo;
            }
       
        
        

    }

    public function getGenerar($id)
    {
        error_reporting(0);

        // $data = Polizas::where('id', $id)
        // ->where('estado_poliza', 7)
        // ->with('productos','clientes')->first();

        $data = Polizas::where('id',$id)->first();

        $consecutivo = 3447 + $data->id;
        
        if( $data != null ){

            if( $data->productos->categoria != 'beneficio' ){
                
                $vista = array(
                    'iva' => ($data->productos->valor * 0.19),
                    'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
                    'precio' => $data->productos->valor,
                    'imei' => $data->imei,
                    'cubrimiento'=> $data->productos->cubrimiento,
                    'modelo'=> $data->productos->modelo,
                    'fecha'=> $data->fecha_vigencia,
                    'nombre'=> $data->clientes->nombre,
                    'apellido'=> $data->clientes->lastname,
                    'cedula'=> $data->clientes->cedula,
                    'telefono'=> $data->clientes->celular,
                    'ciudad'=> $data->clientes->ciudad,
                    'consecutivo' => $consecutivo,
                    'titulo' => $data->productos->titulo,
                    'descripcion' => $data->productos->descripcion,
                );
                $html = view('crawford.voluntariosSamsung',['data'=>$vista])->render();
                
                
            }else{
                $vista = array(
                    'iva' => ($data->productos->valor * 0.19),
                    'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
                    'precio' => $data->productos->valor,
                    'imei' => $data->imei,
                    'cubrimiento'=> $data->productos->cubrimiento,
                    'modelo'=> $data->productos->modelo,
                    'consecutivo' => $consecutivo,
                    'descripcion' => $data->productos->descripcion,
                );
                            
                $html = view('crawford.polizaSamsung',['data'=>$vista])->render();
            }


                $namefile = 'Poliza_'.$data->imei.'-'.date('y-m-d').'.pdf';
                $mpdf = new Mpdf();
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->WriteHTML($html);
                $path = public_path()."/polizasActivas/";
                $url = url('polizasActivas/'.$namefile);
                // $data->ruta_poliza = $path.$namefile;
                $data->url_poliza = $url;
                $data->state = 'notificado';
                $data->save();
                $fileName =$path.$namefile;
                $r = $mpdf->Output($path.$namefile, "F");

            $attachments = [
                public_path().'/condiciones/CONDICIONES PARTICULARES SAMSUNG MOBILECARE VOLUNTARIO- V GENERAL v fina....pdf',
                public_path().'/polizasActivas/'.$namefile
            ];

                Mail::send('mails/mail_samsung', 
                            [
                                'titulo' => 'Poliza adquirida equipo samsung',
                                'email' => $data->email,
                                'mensaje'=> 'En este mensaje '
                            ],
                                
                                function ($m) use ($data, $attachments)
                {   
                    foreach( $attachments as $adjunto ){
                        $m->attach($adjunto);
                    }
                    // $m->attach($fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($data->email, $data->name_user)->bcc('polizas@crawfordaffinitycolombia.com','Seguros Mundial')->subject('Notificación Seguros Mundial!');
                }); 
                return $data;

        }else{
            return "no hay clientes";
        }

    }

    public function clientesSamsung(Request $request){
        $client = Clientes::where('imei', $request->imei)
                    ->where('serial', $request->serial)
                    ->where('modelo', $request->modelo)->get();

        if( $client->count() > 0 ){
            return response()->json(
                ['data'=> $client->first()]
            );
        }else{
            return response()->json(
                ['data'=> 'no se encontraon datos realcionados con la petición']
            );
        }
        
    }

    public function updatePass(){
        $clientes = Clientes::All();
        foreach ($clientes as $cliente) {
            Clientes::where('id',$cliente->id)->update(["password" => md5($cliente->password)]);
        }
        return $clientes;
    }

    public function crsf(){

        $marca = request('marca');
        $aseguradora = request('aseguradora');
        $email = request('email');

        $res = Clientes::where('marca', $marca)
                        ->where('aseguradora',$aseguradora)
                        ->where('email',$email)
                        ->where('password',$pass)
                        ->update(['tokenAPI' => ""]);

        return $res->tokenAPI;

    }

    public function destruir_token(Request $request){
        $destruir_token = Clientes::where('email',$request->email_cliente)->where('marca',$request->marca_cliente)->update(['tokenAPI'=>'']);
        return $destruir_token;
    }

    public function updateTokenSamsung( Request $request ){

        $imei = $request->imei;
        $email = $request->email;
        $token = $request->token;

        $response = Clientes::where('imei', $imei)
                            ->where('email', $email)
                            ->update(['tokenAPI'=> $token]);

        $response = Clientes::where('imei', $imei)
        ->where('email', $email)->get();
        
        
        if( $response->count() > 0 ){
            $nuevotoken = $response->first()->tokenAPI;
            return array(
                'nuevoToken' => $nuevotoken
            );
        }else{
            return array(
                "success" => false,
                "message" => "El usuario no existe en la plataforma"
            );
        }

        

    }

    public function encriptar(){
        return encrypt('joel77757');
    }

    public function desencriptar(){
        try {
            $decrypted = decrypt('eyJpdiI6ImZuRmd2YUc4VFwvRVZobDNFcjBTeDBBPT0iLCJ2YWx1ZSI6InZ2UEJuQnhVWjNvdTkyNHFcL2FcL0pVYkZNTVRFSGkweVJvZTVyUk04ME1ROD0iLCJtYWMiOiJhNzlkODYyN2UzYzQ2NDJmZmRmMGIzMzE3NjAwMTg4YTYwNzVhMDVmYWQ3ZDA4NGMxYWRlNDM3NWJiYWEyYTk4In0=');
        } catch (DecryptException $e) {
            return 'no se puede desencriptar';
        }

        return $decrypted;
    }

    public function vista_creacion_cliente(){
        return view('crawford.cliente_crear');
    }

    public function vista_creacion_reclamo($id_cliente){
        $cliente = Clientes::find($id_cliente);
        $departamentos = Departamento::all();
        $departamentos_zurich = ZurichDepartamentos::all();
        $tiendas = ListaTiendas::all();
        $productos = ListaProductos::all();
        $marcas = ListaMarcas::all();
        $causales = ListaCausales::all();
        $sponsors = ListaSponsors::all();
        foreach ($sponsors as $sponsor) {
            $sponsor->nombre_trans = strtoupper(str_replace("_", " ", $sponsor->nombre_sponsor));
        }
        return view('crawford.cliente_reclamo', array(
            'cliente'=>$cliente,
            'departamentos' => $departamentos,
            'departamentos_zurich' => $departamentos_zurich,
            'tiendas' => $tiendas,
            'productos' => $productos,
            'marcas' => $marcas,
            'causales' => $causales,
            'id_cliente' => $id_cliente,
            'sponsors' => $sponsors
        ));
    }

    public function imagen($slug){
        $path = public_path() . '/img/' . $slug;
        if(!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function archivos_js($archivo){
        $path = storage_path() . '/app/archivos_js/' . $archivo;
        if(!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function subir_excel_clientes(Request $request){

        $this->validate($request,['carga_clientes_zurich' => 'required|file']);

        \Excel::load($request['carga_clientes_zurich'], function($reader) {

            $excel = $reader->get();

            foreach ($excel as $ex) {

                $list = Clientes::create([
                    'id' => $ex->id_cliente,
                    'nombre' => $ex->nombre_cliente,
                    'lastname' => $ex->apellido_cliente,
                    'email' => $ex->email_cliente,
                    'cedula' => $ex->cedula_cliente,
                    'password' => encrypt($ex->password_cliente),
                    'celular' => $ex->celular,
                    'fechanacimiento' => $ex->fecha_nacimiento_cliente,
                    'marca' => 5,
                    'tipo_seguro' => null,
                    'aseguradora' => 8,
                    'sitio_creacion' => 'administrador'
                ]);

            }
        
        });

        $clientes_hoy = Clientes::where('created_at','like','%'.date('Y-m-d').'%')->get();

        return $clientes_hoy;
    }

    public function subir_excel_reclamos(Request $request){

        $this->validate($request,['carga_reclamos_zurich' => 'required|file']);
        
        \Excel::load($request['carga_reclamos_zurich'], function($reader) {

            $excel = $reader->get();

            foreach ($excel as $ex) {

                // $list = ZurichReclamos::create([
                //     'id' => $ex->id,
                //     'id_cliente' => $ex->id_cliente,
                //     'ciudadrecidencia' => $ex->ciudadrecidencia,
                //     'telefonofijo' => $ex->telefonofijo,
                //     'celular' => $ex->celular,
                //     'direccion' => $ex->direccion,
                //     'telefonolaboral' => $ex->telefonolaboral,
                //     'tipo_producto' => $ex->tipo_producto,
                //     'marca_equipo' => $ex->marca_equipo,
                //     'modelo_equipo' => $ex->modelo_equipo,
                //     'imei' => $ex->imei,
                //     'numero_factura' => $ex->numero_factura,
                //     'precio_equipo' => $ex->precio_equipo,
                //     'fecha_compra' => $ex->fecha_compra,
                //     'tienda' => $ex->tienda,
                //     'fecha_asignacion_cst' => $ex->fecha_asignacion_cst,
                //     'nombre_cst' => $ex->nombre_cst,
                //     'fecha_ingreso_cst' => $ex->fecha_ingreso_cst,
                //     'fecha_diagnostico' => $ex->fecha_diagnostico,
                //     'tiempo_reparacion' => $ex->tiempo_reparacion,
                //     'causal' => $ex->causal,
                //     'valor_indemnizado' => $ex->valor_indemnizado,
                //     'fecha_notificacion' => $ex->fecha_notificacion,
                //     'tipo_siniestro' => $ex->tipo_siniestro,
                //     'fechasiniestro' => $ex->fechasiniestro,
                //     'horasiniestro' => $ex->horasiniestro,
                //     'descripcionsiniestro' => $ex->descripcionsiniestro,
                //     'estado_reclamo' => $ex->estado_reclamo,
                //     'ciudadsiniestro' => $ex->ciudadsiniestro,
                //     'plan_poliza' => $ex->plan_poliza,
                //     'sponsor' => $ex->sponsor,
                //     'certificado' => $ex->certificado,
                //     'radicado_zurich' => $ex->radicado_zurich,
                //     'fecha_envio_instructivo' => $ex->fecha_envio_instructivo,
                //     'fecha_aviso' => $ex->fecha_aviso,
                //     'fecha_recepcion_documentos' => $ex->fecha_recepcion_documentos,
                //     'fecha_recep_doc_completos' => $ex->fecha_recep_doc_completos,
                //     'hora_recepcion_documentos' => $ex->hora_recepcion_documentos,
                //     'fecha_entrega_ajuste' => $ex->fecha_entrega_ajuste,
                //     'fecha_ajuste' => $ex->fecha_ajuste,
                //     'observaciones' => $ex->observaciones,
                //     'sitio_creacion' => 'administrador'
                // ]);

                $reclamo = new ZurichReclamos();
                $reclamo->id = $ex->id;
                $reclamo->id_cliente = $ex->id_cliente;
                $reclamo->ciudadrecidencia = $ex->ciudadrecidencia;
                $reclamo->telefonofijo = $ex->telefonofijo;
                $reclamo->celular = $ex->celular;
                $reclamo->direccion = $ex->direccion;
                $reclamo->telefonolaboral = $ex->telefonolaboral;
                $reclamo->tipo_producto = $ex->tipo_producto;
                $reclamo->marca_equipo = $ex->marca_equipo;
                $reclamo->modelo_equipo = $ex->modelo_equipo;
                $reclamo->imei = $ex->imei;
                $reclamo->numero_factura = $ex->numero_factura;
                $reclamo->precio_equipo = $ex->precio_equipo;
                $reclamo->fecha_compra = $ex->fecha_compra;
                $reclamo->tienda = $ex->tienda;
                $reclamo->fecha_asignacion_cst = $ex->fecha_asignacion_cst;
                $reclamo->nombre_cst = $ex->nombre_cst;
                $reclamo->fecha_ingreso_cst = $ex->fecha_ingreso_cst;
                $reclamo->fecha_diagnostico = $ex->fecha_diagnostico;
                $reclamo->tiempo_reparacion = $ex->tiempo_reparacion;
                $reclamo->causal = $ex->causal;
                $reclamo->valor_indemnizado = $ex->valor_indemnizado;
                $reclamo->fecha_notificacion = $ex->fecha_notificacion;
                $reclamo->tipo_siniestro = $ex->tipo_siniestro;
                $reclamo->fechasiniestro = $ex->fechasiniestro;
                $reclamo->horasiniestro = $ex->horasiniestro;
                $reclamo->descripcionsiniestro = $ex->descripcionsiniestro;
                $reclamo->estado_reclamo = $ex->estado_reclamo;
                $reclamo->ciudadsiniestro = $ex->ciudadsiniestro;
                $reclamo->plan_poliza = $ex->plan_poliza;
                $reclamo->sponsor = $ex->sponsor;
                $reclamo->certificado = $ex->certificado;
                $reclamo->radicado_zurich = $ex->radicado_zurich;
                $reclamo->fecha_envio_instructivo = $ex->fecha_envio_instructivo;
                $reclamo->fecha_aviso = $ex->fecha_aviso;
                $reclamo->fecha_recepcion_documentos = $ex->fecha_recepcion_documentos;
                $reclamo->fecha_recep_doc_completos = $ex->fecha_recep_doc_completos;
                $reclamo->hora_recepcion_documentos = $ex->hora_recepcion_documentos;
                $reclamo->fecha_entrega_ajuste = $ex->fecha_entrega_ajuste;
                $reclamo->fecha_ajuste = $ex->fecha_ajuste;
                $reclamo->observaciones = $ex->observaciones;
                $reclamo->sitio_creacion = 'administrador';
                $reclamo->save();

            }
        
        });

        // $reclamos_hoy = ZurichReclamos::where('created_at','like','%'.date('Y-m-d').'%')->get();

        return 'se ha creado todo xd';
    }

}
