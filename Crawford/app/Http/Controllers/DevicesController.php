<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Devices;
use Excel;

class DevicesController extends Controller
{
    //'deviceid','registrationid','clientid','platform','cliente'
    /**
	*  respuesta todos las Devices
	*
	*  GET url
	*/
    public function all(){
    	$list = Devices::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Devices
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Devices::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var deviceid
	*  @var registrationid
	*  @var clientid
	*  @var platform
	*  @var cliente
	*/
    public function save(){
    	$list = Devices::create(request(['deviceid','registrationid','clientid','platform','cliente']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Devices guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Devices
	*
	*  POST url
	*  @var id
	*  @var deviceid
	*  @var registrationid
	*  @var clientid
	*  @var platform
	*  @var cliente
	*/
    public function update(){
    	$Devices = Devices::find(request('id'));
        $Devices->deviceid = request('deviceid');
        $Devices->registrationid = request('registrationid');
        $Devices->clientid = request('clientid');
        $Devices->platform = request('platform');
        $Devices->cliente = request('cliente');
        $Devices->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Devices Actualizada id:'.$Devices->id,
            'body' => $Devices
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Devices
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Devices = Devices::find($id);
        $Devices->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Devices Eliminada id:'.$Devices->id,
                'body' => $Devices
            ]);
    }

    /**
	*  view index Devices
	*
	*  GET url 
	*/
    public function index(){
    	$list = Devices::all();
    	return view('devices', array(
    		'devices' => $list 
    	));
    }

    /**
	*  view EXCEL Devices
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Devices::all();
		return Excel::create('Devices', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
