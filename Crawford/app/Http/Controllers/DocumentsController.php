<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;

use App\Documents;
use App\Listchekings;
use App\Reclamos;
use App\Clientes;
use App\notificaciones;
use App\Homologos;
use App\Tipo_polizas;
use App\Tiendas;
use App\Tipo_seguros;
use App\Aseguradoras;
use App\Observaciones;
use App\Ciudades_departamentos;
use App\Productos;
use App\marcas;
use App\Estados_polizas;

use Mail;
use Excel;

use Redirect;

class DocumentsController extends Controller
{
    //'estado','validacion','respuestaback','url','reclamo','listcheking'
    /**
	*  respuesta todos las Documents
	*
	*  GET url
	*/
    public function all(){
    	$list = Documents::all();
    	foreach ($list as $documents) {
    		$documents->reclamo = Reclamos::find($documents->reclamo);
    		$documents->listcheking = Listchekings::find($documents->listcheking);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Documents
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Documents::where('id',$id)->get();
    	foreach ($list as $documents) {
    		$documents->reclamo = Reclamos::find($documents->reclamo);
    		$documents->listcheking = Listchekings::find($documents->listcheking);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta una de las Documents
    *
    *  GET url
    *  @var id identificador de la marca en la base de datos
    *  @var lista identificador de la lista de chequeo
    */
    public function reclamo($reclamo){
        $list = Documents::where('reclamo',$reclamo)->where('listcheking',$_GET['listcheking'])->get();
        foreach ($list as $documents) {
            $documents->reclamo = Reclamos::find($documents->reclamo);
            $documents->listcheking = Listchekings::find($documents->listcheking);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var estado
	*  @var validacion
	*  @var respuestaback
	*  @var url
	*  @var reclamo
	*  @var listcheking
	*/
    public function save(){
    	$list = Documents::create(request(['estado','validacion','respuestaback','url','reclamo','listcheking']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Documents guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }
    /**
    *
    *
    *  Upload Submit archive (file input)
    *
    */

    public function uploadSubmit(Request $request){

        $consulta_token = Clientes::where('email',$request['email_cliente'])->where('marca',$request['marca_cliente'])->get();

        if ($consulta_token[0]->tokenAPI == $request['token_']) {

            $Reclamos = Reclamos::find(request('reclamo'));
            $Reclamos->estados_poliza = 25;
            $Reclamos->save();

            //dd($request);
            
            //dd($request->file('file')); //audio
            //$file = $request->file('file')[0]; imagen /= document

            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
                if (true) 
                {
                    switch ($_POST["FileType"])
                    {
                        case "ProfileImage":
                                    //$fileName = $file->getClientOriginalName();
                                    $fileName = "/avatar_C_".$_POST["cliente"].".".$ext;
                                    $destinationPath = public_path().'/profiles';
                                    $request->file('file')[0]->move($destinationPath, $fileName); 

                                    break;
                        case "camara":
                            //      //saber cuantos documentos tiene la lista de chequeo
                                    $list = DB::select("SELECT COUNT( DISTINCT listcheking )AS contador FROM `documents` WHERE `reclamo` = ".request('reclamo').";");

                                    $contador = intval($list[0]->contador) + 1;

                                    $fileName = "doc_C_".uniqid()."_".$_POST['cliente'].'_R_'.$_POST["reclamo"]."_".request('listcheking')."_".$contador.".".$ext;
                                    // $destinationPath = public_path().'/documents';
                                    // $request->file('file')->move($destinationPath, $fileName);
                                    Storage::putFileAs(
                                        '', $request->file('file'), $fileName
                                    );
                                    $Documents = new Documents;
                                    $Documents->estado = 'SUBIDO';
                                    $Documents->validacion = 'SIN_VALIDAR';
                                    $Documents->respuestaback = 'SIN_RESPUESTA';
                                    // $Documents->url = '/documents'.$fileName;
                                    $Documents->url = $fileName;
                                    $Documents->reclamo = request('reclamo');
                                    $Documents->listcheking = request('listcheking');
                                    $Documents->save();


                                    //saber el tipo_polizas de la lista de chequeo con el fin de recorrerlos
                                    $Documents->listcheking = Listchekings::find($Documents->listcheking);
                                    $completo = 0;
                                    $documentos = Documents::where('reclamo', request('reclamo'))->orderBy('listcheking')->get(); //todos los documentos de este reclamo
                                    $list = Listchekings::where('tipo_polizas',$Documents->listcheking->tipo_polizas)->orWhere('reclamo', request('reclamo'))->get(); //las listas de chequeo que pertenecen
                                    foreach ($list as $value) { // recorrer la lista de chequeo del reclamo
                                        foreach ($documentos as $doc) {
                                            if($value['id']==$doc['listcheking']){
                                                $completo = 1;
                                            }
                                            else{
                                                $completo = 0;
                                            }
                                        }
                                    }

                                    // if($completo != 0){
                                    //     //actualizar estado (YA COMPLETO TODOS LOS CHECKLIST)
                                    //     $doc_reclamo = Reclamos::find(request('reclamo'));
                                    //     $doc_reclamo->estados_poliza = 3;//EN ESTUDIO
                                    //     $doc_reclamo->save(); //guardar datos
                                    //     $client = Clientes::find($doc_reclamo->cliente);
                                    //     //envio de correo
                                    //     $fileName = '';
                                    //     Mail::send('mails/mail', 
                                    //         [
                                    //                 'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$doc_reclamo->reclamoid.': ',
                                    //                 'mensaje'=> 'Cambio de estado del reclamo'
                                    //         ], 
                                    //         function ($m) use ($client, $fileName,$doc_reclamo) 
                                    //         {
                                    //             if($fileName != "")
                                    //                 $m->attach(public_path().'/docnotifications/'.$fileName);
                                    //             $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                                    //             $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$doc_reclamo->reclamoid.' Seguros Mundial!');
                                    //         });
                                    // }

                                    break;
                        case "audio":
                                    $fileName = "/audio_C_".$_POST['cliente'].'_R_'.$_POST["reclamo"].".".$ext;
                                    $destinationPath = public_path().'/documents';
                                    /*shell_exec("ffmpeg -i ".$request->file('file')->getRealPath()." -c:a libmp3lame ".$destinationPath.$fileName);                            
                                    $request->file('file')->move($destinationPath, $fileName);    */
                                    $request->file('file')->move($destinationPath, $fileName);

                                    $Documents = new Documents;
                                    $Documents->estado = 'SUBIDO';
                                    $Documents->validacion = 'SIN_VALIDAR';
                                    $Documents->respuestaback = 'SIN_RESPUESTA';
                                    $Documents->url = '/documents'.$fileName;
                                    $Documents->reclamo = request('reclamo');
                                    $Documents->listcheking = request('listcheking');
                                    $Documents->save();

                                    break;                              
                    }
                }                  

                
                return $Documents;

        }else{
            return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION';
        }

    }

    /**
    *
    *
    *  Upload Submit archive (file input)
    *
    */

    public function uploadSubmitBackend(Request $request){

        //dd($request);
        
        //dd($request->file('file')); //audio
        //$file = $request->file('file')[0]; imagen /= document

        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
            if (true) 
            {
                switch ($_POST["FileType"])
                {
                    case "ProfileImage":
                                //$fileName = $file->getClientOriginalName();
                                $fileName = "/avatar_C_".$_POST["cliente"].".".$ext;
                                // $destinationPath = public_path().'/profiles';
                                // $request->file('file')[0]->move($destinationPath, $fileName);
                                Storage::putFileAs(
                                    '', $request->file('file'), $fileName
                                );

                                break;
                    case "camara":
                        //      //saber cuantos documentos tiene la lista de chequeo
                                $list = DB::select("SELECT COUNT( DISTINCT listcheking )AS contador FROM `documents` WHERE `reclamo` = ".request('reclamo').";");

                                $contador = intval($list[0]->contador) + 1;

                                $fileName = "/doc_C_".$_POST['cliente'].'_R_'.$_POST["reclamo"]."_".request('listcheking')."_".$contador.".".$ext;
                                // $destinationPath = public_path().'/documents';
                                // $request->file('file')->move($destinationPath, $fileName);
                                Storage::putFileAs(
                                    '', $request->file('file'), $fileName
                                );

                                $Documents = new Documents;
                                $Documents->estado = 'SUBIDO';
                                $Documents->validacion = 'SIN_VALIDAR';
                                $Documents->respuestaback = 'SIN_RESPUESTA';
                                $Documents->url = $fileName;
                                $Documents->reclamo = request('reclamo');
                                $Documents->listcheking = request('listcheking');
                                $Documents->save();


                                //saber el tipo_polizas de la lista de chequeo con el fin de recorrerlos
                                $Documents->listcheking = Listchekings::find($Documents->listcheking);
                                $completo = 0;
                                $documentos = Documents::where('reclamo', request('reclamo'))->orderBy('listcheking')->get(); //todos los documentos de este reclamo
                                $list = Listchekings::where('tipo_polizas',$Documents->listcheking->tipo_polizas)->orWhere('reclamo', request('reclamo'))->get(); //las listas de chequeo que pertenecen
                                foreach ($list as $value) { // recorrer la lista de chequeo del reclamo
                                    foreach ($documentos as $doc) {
                                        if($value['id']==$doc['listcheking']){
                                            $completo = 1;
                                        }
                                        else{
                                            $completo = 0;
                                        }
                                    }
                                }

                                // if($completo != 0){
                                //     //actualizar estado (YA COMPLETO TODOS LOS CHECKLIST)
                                //     $doc_reclamo = Reclamos::find(request('reclamo'));
                                //     $doc_reclamo->estados_poliza = 3;//EN ESTUDIO
                                //     $doc_reclamo->save(); //guardar datos
                                //     $client = Clientes::find($doc_reclamo->cliente);
                                //     //envio de correo
                                //     $fileName = '';
                                //     Mail::send('mails/mail', 
                                //         [
                                //                 'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$doc_reclamo->reclamoid.': ',
                                //                 'mensaje'=> 'Cambio de estado del reclamo'
                                //         ], 
                                //         function ($m) use ($client, $fileName,$doc_reclamo) 
                                //         {
                                //             if($fileName != "")
                                //                 $m->attach(public_path().'/docnotifications/'.$fileName);
                                //             $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                                //             $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$doc_reclamo->reclamoid.' Seguros Mundial!');
                                //         });
                                // }

                                break;
                    case "audio":
                                $fileName = "/audio_C_".$_POST['cliente'].'_R_'.$_POST["reclamo"].".".$ext;
                                $destinationPath = public_path().'/documents';
                                /*shell_exec("ffmpeg -i ".$request->file('file')->getRealPath()." -c:a libmp3lame ".$destinationPath.$fileName);                            
                                $request->file('file')->move($destinationPath, $fileName);    */
                                $request->file('file')->move($destinationPath, $fileName);

                                $Documents = new Documents;
                                $Documents->estado = 'SUBIDO';
                                $Documents->validacion = 'SIN_VALIDAR';
                                $Documents->respuestaback = 'SIN_RESPUESTA';
                                $Documents->url = '/documents'.$fileName;
                                $Documents->reclamo = request('reclamo');
                                $Documents->listcheking = request('listcheking');
                                $Documents->save();

                                break;                              
                }
            }                  

            
            $id = $request['reclamo'];

            $list = Reclamos::where('id','=',$id)->get();
            $documents = Documents::where('reclamo',$id)->get();
            $notificaciones = notificaciones::where('reclamo',$id)->get();
            $homologos = Homologos::all();

            $observaciones = Observaciones::where('reclamo','like',$id)->orderBy('id', 'desc')->get();
        
            $producto = Productos::find($list[0]->producto) ;
            $tipo_polizas = $producto->tipo_poliza;

            $listchekings = Listchekings::where('tipo_polizas','like',$tipo_polizas)->get();
            $arrayTipo_polizas = Tipo_polizas::where('id','like',$tipo_polizas)->get();
            $arrayTiendas = Tiendas::where('id','like',$arrayTipo_polizas[0]->tienda)->get();

            foreach ($notificaciones as $notificacion) {
                $notificacion->marca = Marcas::find($notificacion->marca);
                $notificacion->reclamo = Reclamos::find($notificacion->reclamo);
                $notificacion->cliente = Clientes::find($notificacion->cliente);
            }

            foreach ($list as $reclamo) {
                $reclamo->ciudadrecidencia = Ciudades_departamentos::find($reclamo->ciudadrecidencia);
                $reclamo->ciudadsiniestro = Ciudades_departamentos::find($reclamo->ciudadsiniestro);
                $reclamo->producto = Productos::find($reclamo->producto);
                $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
                $reclamo->cliente = Clientes::find($reclamo->cliente);
            }

            return Redirect::to('/crawford/idreclamo/'.$id);

            return view('crawford.idreclamos', array(
                'reclamos' => $list,
                'documents' => $documents,
                'notificaciones' => $notificaciones,
                'homologos' => $homologos,
                'listchekings' => $listchekings,
                'arrayTipo_polizas' => $arrayTipo_polizas,
                'arrayTiendas' => $arrayTiendas,
                'observaciones' => $observaciones
            ));

    }

    /**
    *
    *
    *  Upload Submit archive (file input)
    *
    */
    public function uploadSubmitAPP(Request $request){

        $list = DB::select("SELECT count(*) as contador FROM `documents` WHERE `reclamo` = ".request('reclamo')." AND `listcheking` = ".request('listcheking')." ;");
        $contador = intval($list[0]->contador) + 1;

        $fileType = $request->get('FileType');
        if ($fileType == 'camara') {
            $png_url = "user-documents-".time()."_".".jpg";
            // $path = public_path() . "/documents/" . $png_url;
            $img = $request->get('image_content');
            Storage::putFileAs(
                '', $request->file('file'), $png_url
            );
            // $img = substr($img, strpos($img, ",")+1);
            // $data = base64_decode($img);
            // $success = file_put_contents($path, $data);
            if ($success) {
                $Documents = new Documents;
                $Documents->estado = 'SUBIDO';
                $Documents->validacion = 'SIN_VALIDAR';
                $Documents->respuestaback = 'SIN_RESPUESTA';
                $Documents->url = $png_url;
                $Documents->reclamo = request('reclamo');
                $Documents->listcheking = request('listcheking') ? request('listcheking') : 0;
                $Documents->save();

            }
        }
        elseif ($fileType == 'audio') {
            $png_url = "user-documents-".time()."_".".mp3";
            Storage::putFileAs(
                '', $request->file('file'), $png_url
            );
            // $path = public_path() . "/documents/" . $png_url;
            // $img = $request->get('image_content');
            // $img = substr($img, strpos($img, ",")+1);
            // $data = base64_decode($img);
            // $success = file_put_contents($path, $data);
            if ($success) {
                $Documents = new Documents;
                $Documents->estado = 'SUBIDO';
                $Documents->validacion = 'SIN_VALIDAR';
                $Documents->respuestaback = 'SIN_RESPUESTA';
                $Documents->url = $png_url;
                $Documents->reclamo = request('reclamo');
                $Documents->listcheking = request('listcheking') ? request('listcheking') : 0;
                $Documents->save();

            }
        }

        //saber el tipo_polizas de la lista de chequeo con el fin de recorrerlos
        $Documents->listcheking = Listchekings::find($Documents->listcheking);
        $completo = 0;
        $documentos = Documents::where('reclamo', request('reclamo'))->orderBy('listcheking')->get(); //todos los documentos de este reclamo
        $list = Listchekings::where('tipo_polizas',$Documents->listcheking->tipo_polizas)->orWhere('reclamo', request('reclamo'))->get(); //las listas de chequeo que pertenecen
        foreach ($list as $value) { // recorrer la lista de chequeo del reclamo
            foreach ($documentos as $doc) {
                if($value['id']==$doc['listcheking']){
                    $completo = 1;
                }
                else{
                    $completo = 0;
                }
            }
        }

        if($completo != 0){
            //actualizar estado (YA COMPLETO TODOS LOS CHEKLIST)
            $doc_reclamo = Reclamos::find(request('reclamo'));
            $doc_reclamo->estados_poliza = 3; //EN ESTUDIO
            $doc_reclamo->save(); //guardar datos
            $client = Clientes::find($doc_reclamo->cliente);
            //envio de correo
            $fileName = '';
            Mail::send('mails/mail', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$doc_reclamo->reclamoid.': ',
                        'mensaje'=> 'Cambio de estado del reclamo'
                ], 
                function ($m) use ($client, $fileName,$doc_reclamo) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$doc_reclamo->reclamoid.' Seguros Mundial!');
                });
        }
            
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Documents guardada id:',
                'body' => $Documents
            ]);
            return $response;

    }

    /*
    * FUNCION INDICADA PARA LLEVAR TODA LA INFO DE LOS DOCUMENTOS AGRUPADA POR LA LISTA DE CHEQUEO
    */
    public function verificarcompletos(){

        $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Mensaje correcto:',
                'body' => Documents::select(DB::raw('count(*) as contador, listcheking'))
                                    ->where('reclamo', request('reclamo'))
                                    ->groupBy('listcheking')
                                    ->orderBy('listcheking', 'ASC')
                                    ->get()
            ]);
            return $response;
    }

    /*
    * FUNCION INDICADA PARA MOSTRAR TODAS LAS IMAGENES DE UN DETERMINADO CHECLIST
    * Recibe parametros del id del reclamo y el id de la lista de chequeo (GET)
    */
    public function urlDocuments($reclamo,$listcheking){
        $response = Documents::where('reclamo','like', $reclamo)
                                    ->where('listcheking','like', $listcheking)
                                    ->get();
            return view('falabella.urlDocuments', array(
            'response' => $response 
        ));
    }


    public function subirdocumento($reclamo,$listcheking){
        return view('crawford.subirdocumento',array(
                'reclamo'=>$reclamo,
                'listcheking'=>$listcheking
            ));
    }

    /**
	*  respuesta Actualizacion de Documents
	*
	*  POST url
	*  @var id
	*  @var estado
	*  @var validacion
	*  @var respuestaback
	*  @var url
	*  @var reclamo
	*  @var listcheking
	*/
    public function update(){
    	$Documents = Documents::find(request('id'));
        $Documents->estado = request('estado');
		$Documents->validacion = request('validacion');
		$Documents->respuestaback = request('respuestaback');
		$Documents->url = request('url');
		$Documents->reclamo = request('reclamo');
		$Documents->listcheking = request('listcheking');
        $Documents->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Documents Actualizada id:'.$Documents->id,
            'body' => $Documents
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Documents
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Documents = Documents::find($id);
        $Documents->delete();

        $doc_reclamo = Reclamos::find($Documents->reclamo);
        $doc_reclamo->estados_poliza = 1; //DOCUMENTACION PENDIENTE
        $doc_reclamo->save(); //guardar datos

        return response()->json([
                'Status' => 'successful',
                'Message' => 'Documents Eliminada id:'.$Documents->id,
                'body' => $Documents
            ]);
    }

    /**
	*  view index Documents
	*
	*  GET url 
	*/
    public function index(){
    	$list = Documents::all();
    	foreach ($list as $documents) {
    		$documents->reclamo = Reclamos::find($documents->reclamo);
    		$documents->listcheking = Listchekings::find($documents->listcheking);
    	}
    	$listchekings = Listchekings::all();
    	$reclamos = Reclamos::all();
    	return view('documents', array(
    		'documents' => $list ,
    		'listchekings' => $listchekings,
    		'reclamos' => $reclamos
    	));
    }

    /**
	*  view Filtros de  Documents
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Documents::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $documents) {
    		$documents->reclamo = Reclamos::find($documents->reclamo);
    		$documents->listcheking = Listchekings::find($documents->listcheking);
    	}
		$listchekings = Listchekings::all();
    	$reclamos = Reclamos::all();
    	return view('documents', array(
    		'documents' => $list ,
    		'listchekings' => $listchekings,
    		'reclamos' => $reclamos
    	));
    }

    /**
	*  view EXCEL Documents
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Documents::all();
		return Excel::create('Documents', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function traer_numero($reclamo,$checklist){
$list=DB::select("SELECT count(*) as contador FROM `documents` WHERE `reclamo` = ".$reclamo." AND `listcheking` = ".$checklist." ;");
      /* $numeros = Documents::where([
    ['reclamo','=',$reclamo],
    ['listcheking','=',$checklist]])->get();*/

    $contador = intval($list[0]->contador);
        return array(
            'success' => 'true',
            'message' => 'Se ha recibido la info',
            'body' => $contador
        );

    }

    public function cargarImagenSamsung( Request $request ){


        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $list = DB::select("SELECT COUNT( DISTINCT listcheking )AS contador FROM `documents` WHERE `reclamo` = ".request('reclamo').";");

        $contador = intval($list[0]->contador) + 1;

        $fileName = "doc_C_".$_POST['cliente'].'_R_'.$_POST["reclamo"]."_".request('listcheking')."_".$contador.".".$ext;
        // $destinationPath = public_path().'/documents-x-a/';
        // $request->file('file')->move($destinationPath, $fileName);

        Storage::putFileAs(
            '', $request->file('file'), $fileName
        );

        $Documents = new Documents;
        $Documents->estado = 'SUBIDO';
        $Documents->validacion = 'SIN_VALIDAR';
        $Documents->respuestaback = 'SIN_RESPUESTA';
        $Documents->url = $fileName;
        $Documents->reclamo = request('reclamo');
        $Documents->listcheking = request('listcheking');
        $Documents->save();

        $doc_reclamo = Reclamos::find(request('reclamo'));
        $doc_reclamo->estados_poliza = 25;//EN ESTUDIO
        $doc_reclamo->save(); //guardar datos
        $client = Clientes::find($doc_reclamo->cliente);
        //envio de correo
        $fileName = '';
        Mail::send('mails/mail_samsung', 
            [
                    'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$doc_reclamo->reclamoid.': ',
                    'mensaje'=> 'Cambio de estado del reclamo'
            ], 
            function ($m) use ($client, $fileName,$doc_reclamo) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/docnotifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$doc_reclamo->reclamoid.' Seguros Mundial!');
            });

        return $Documents;
    }
}
