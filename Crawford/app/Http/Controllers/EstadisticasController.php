<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Reclamos;
use App\Clientes;
use App\Estados_polizas;
use App\Productos;
use App\Ciudades;
use App\marcas;
use App\Aseguradoras;

class EstadisticasController extends Controller
{
    public function reclamacionesxfecha( $aseg ){

    	if($aseg == "all"){
    		$list = DB::select('SELECT *, COUNT(*) as contador FROM `reclamos` INNER JOIN clientes ON reclamos.cliente = clientes.id WHERE clientes.aseguradora > 0 GROUP BY reclamos.fechasiniestro');
	        /*$list = DB::table('reclamos')
	        	->select('fechasiniestro',DB::raw('COUNT(fechasiniestro) as contador'))
	        	->groupBy('fechasiniestro')
	        	->get();*/
	        foreach ($list as $value) {
	        	$value->mes = explode('-', explode('T', $value->fechasiniestro)[0]);
	        }
    	}


    	else{

    		$list = DB::select('SELECT *, COUNT(*) as contador FROM `reclamos` INNER JOIN clientes ON reclamos.cliente = clientes.id WHERE clientes.aseguradora = '.$aseg.' GROUP BY reclamos.fechasiniestro');
	        /*$list = DB::table('reclamos')
	        	->select('fechasiniestro',DB::raw('COUNT(fechasiniestro) as contador'))
	        	->groupBy('fechasiniestro')
	        	->get();*/
	        foreach ($list as $value) {
	        	$value->mes = explode('-', explode('T', $value->fechasiniestro)[0]);
	        }
    	}
    	
    	return $list;
    }

    public function clientesaseguradoras(){
    	$list = DB::select('SELECT *, COUNT(*) as contador FROM `clientes` GROUP BY `aseguradora`');
    	foreach ($list as $value) {
    		$value->aseguradora = Aseguradoras::find($value->aseguradora);
    	}
    	return $list;
    }

    public function reclamosaseguradora(){
    	$list = DB::select('SELECT *,COUNT(*) as contador FROM reclamos INNER JOIN clientes ON clientes.id = reclamos.cliente GROUP BY clientes.aseguradora');
    	foreach ($list as $value) {
    		$value->aseguradora = Aseguradoras::find($value->aseguradora);
    	}
    	return $list;
    }
}
