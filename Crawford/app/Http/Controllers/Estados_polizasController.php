<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Estados_polizas;
use App\marcas;
use App\Tipo_seguros;
use App\Homologos;

use Excel;

class Estados_polizasController extends Controller
{
    //'nombre','descripcion','tipo_seguro','marca','homologo'
    /**
	*  respuesta todos las Estados_polizas
	*
	*  GET url
	*/
    public function all(){
    	$list = Estados_polizas::all();
    	foreach ($list as $estados_poliza) {
    		$estados_poliza->marca       = marcas::find($estados_poliza->marca);
            $estados_poliza->tipo_seguro = Tipo_seguros::find($estados_poliza->tipo_seguro);
            $estados_poliza->homologo = Homologos::find($estados_poliza->homologo);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Estados_polizas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Estados_polizas::where('id',$id)->get();
    	foreach ($list as $estados_poliza) {
    		$estados_poliza->marca       = marcas::find($estados_poliza->marca);
            $estados_poliza->tipo_seguro = Tipo_seguros::find($estados_poliza->tipo_seguro);
            $estados_poliza->homologo = Homologos::find($estados_poliza->homologo);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Estados_polizas
	*
	*  POST url
	*  @var nombre
	*  @var descripcion
	*  @var tipo_seguro
	*  @var marca
	*  @var homologo
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!='' || request('tipo_seguro')!=''){ //estan los datos completos
            $list = Estados_polizas::create(request(['nombre','descripcion','tipo_seguro','marca','homologo']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'estados_poliza guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Estados_polizas
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*  @var tipo_seguro
	*  @var marca
	*  @var homologo
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!='' || request('id')!='' || request('tipo_seguro')!=''){ //estan los datos completos
            $estados_poliza = Estados_polizas::find(request('id'));
            $estados_poliza->nombre = request('nombre');
            $estados_poliza->descripcion = request('descripcion');
            $estados_poliza->marca = request('marca');
            $estados_poliza->tipo_seguro = request('tipo_seguro');
            $estados_poliza->homologo = request('homologo');
            $estados_poliza->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'estados_poliza Actualizada id:'.$estados_poliza->id,
                'body' => $estados_poliza
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Estados_polizas
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$estados_poliza = Estados_polizas::find($id);
        $estados_poliza->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'estados_poliza Eliminada id:'.$estados_poliza->id,
                'body' => $estados_poliza
            ]);
    }

    /**
	*  view index Estados_polizas
	*
	*  GET url 
	*/
    public function index(){
    	$list = Estados_polizas::all();
    	$marcas = marcas::all();
    	$homologos = Homologos::all();
        $tipo_seguros = Tipo_seguros::all();
    	foreach ($list as $estados_poliza) {
    		$estados_poliza->marca       = marcas::find($estados_poliza->marca);
            $estados_poliza->tipo_seguro = Tipo_seguros::find($estados_poliza->tipo_seguro);
            $estados_poliza->homologo = Homologos::find($estados_poliza->homologo);
    	}
    	return view('estados_polizas', array(
    		'estados_polizas' => $list,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'homologos' => $homologos
    	));
    }

    /**
    *  respuesta una de las Estados_polizas
    *
    *  GET url
    *  @var id tipo_seguro 
    */
    public function tipo_seguro($tipo_seguro){
        $list = Estados_polizas::where('tipo_seguro',$tipo_seguro)->get();
        foreach ($list as $estados_poliza) {
            $estados_poliza->marca = marcas::find($estados_poliza->marca);
            $estados_poliza->tipo_seguro = Tipo_seguros::find($estados_poliza->tipo_seguro);
            $estados_poliza->homologo = Homologos::find($estados_poliza->homologo);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Estados_polizas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas = marcas::all();
        $tipo_seguros = Tipo_seguros::all();
        $homologos = Homologos::all();
    	$list = Estados_polizas::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $estados_poliza) {
    		$estados_poliza->marca       = marcas::find($estados_poliza->marca);
            $estados_poliza->tipo_seguro = Tipo_seguros::find($estados_poliza->tipo_seguro);
            $estados_poliza->homologo = Homologos::find($estados_poliza->homologo);
    	}
    	return view('estados_polizas', array(
    		'estados_polizas' => $list ,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'homologos' => $homologos
    	));
    }

    /**
	*  view EXCEL Estados_polizas
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Estados_polizas::all();
		return Excel::create('Estados_polizas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
