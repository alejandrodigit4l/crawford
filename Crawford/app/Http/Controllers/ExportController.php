<?php

namespace App\Http\Controllers;

use Excel;
use Session;
use Redirect;
use App\marcas;
use App\Ciudades;
use App\Clientes;
use App\Reclamos;
use App\Documents;
use App\Productos;
use App\ListaMarcas;
use App\Aseguradoras;
use App\ListaTiendas;
use App\Tipo_seguros;
use App\ListaCausales;
use App\ListaProductos;
use App\ZurichReclamos;
use App\Estados_polizas;
use Chumper\Zipper\Zipper;
use App\ZurichDepartamentos;
use Illuminate\Http\Request;
use App\ZurichEstadosReclamos;
use App\Ciudades_departamentos;
use App\ZurichDocumentosReclamos;
use App\ZurichCiudadDepartamentos;

class ExportController extends Controller
{
	/**
	*
	* Exportar en excel todos los clientes
	*
	**/
    public function clientes_All(){

        // $job = (new ExportClientesJob())
        //                     ->delay(Carbon::now()->addSeconds(5));
        // dispatch($job);

        ini_set('max_execution_time', 180);
        ini_set('memory_limit', '-1');
    	$array = array();
    	$list = Clientes::all();
    	foreach ($list as $cliente) {
    		$cliente->marca = marcas::find($cliente->marca)->nombre;
    	}

    	array_push($array, $list);

    	$today = date('Y-m-d');
		return Excel::create('clientes'.$today, function($excel) use ($array) {
			foreach ($array as $value) {
				$excel->sheet('Clientes', function($sheet) use ($value)
		        {
					$sheet->fromArray($value);
		        });
			}			
		})->download('xls');
    }

    /**
	*
	* Exportar en excel todos los reclamos
	*
	**/
    public function reclamos_All(Request $request){

        ini_set('max_execution_time', 500);
        ini_set('memory_limit', '-1');

    	$array = array();

        if ($request->filtro_marca == 5) {
            $list = ZurichReclamos::marcacliente($request->filtro_marca)
                    ->estadoreclamo($request->filtro_estado_descarga)
                    ->nombrecliente($request->nombre_asegurado)
                    ->apellidocliente($request->apellido_asegurado)
                    ->emailcliente($request->email_asegurado)
                    ->cedulacliente($request->cedula)
                    ->id($request->id_reclamo)
                    ->get();
            foreach ($list as $reclamo) {
                $reclamo->nombre_cliente = Clientes::find($reclamo->id_cliente)->nombre;
                $reclamo->apellido_cliente = Clientes::find($reclamo->id_cliente)->lastname;
                $reclamo->email_cliente = Clientes::find($reclamo->id_cliente)->email;
                $reclamo->cedula_cliente = Clientes::find($reclamo->id_cliente)->cedula;
                $reclamo->fecha_nacimiento_cliente = Clientes::find($reclamo->id_cliente)->fechanacimiento;
                if (!empty($reclamo->ciudadrecidencia)) {
                    $ciudad_residencia = ZurichCiudadDepartamentos::find($reclamo->ciudadrecidencia);
                    $reclamo->ciudadrecidencia = $ciudad_residencia->nombre;
                    $reclamo->departamento_residencia = ZurichDepartamentos::find($ciudad_residencia->idDepartamento)->nombre;   
                }
                if (!empty($reclamo->tipo_producto)) {
                    $reclamo->tipo_producto = ListaProductos::find($reclamo->tipo_producto)->nombre_producto;
                }
                if (!empty($reclamo->marca_equipo)) {
                    $reclamo->marca_equipo = ListaMarcas::find($reclamo->marca_equipo)->nombre_marca;   
                }
                if (!empty($reclamo->tienda)) {
                    $reclamo->tienda = ListaTiendas::find($reclamo->tienda)->nombre_tienda;
                }
                if (!empty($reclamo->causal)) {
                    $reclamo->causal = ListaCausales::find($reclamo->causal)->nombre_causal;
                }
                if (!empty($reclamo->estado_reclamo)) {
                    $reclamo->estado_reclamo = ZurichEstadosReclamos::find($reclamo->estado_reclamo)->nombre_estado;    
                }
                if (!empty($reclamo->ciudadsiniestro)) {
                    $ciudad_siniestro = ZurichCiudadDepartamentos::find($reclamo->ciudadsiniestro);
                    $reclamo->ciudadsiniestro = $ciudad_siniestro->nombre;
                    $reclamo->departamento_siniestro = ZurichDepartamentos::find($ciudad_siniestro->idDepartamento)->nombre;   
                }
            }
        }else{
            $list = Reclamos::nombreasegurado($request->nombre_asegurado)
                    ->marcacliente($request->filtro_marca)
                    ->apellidoasegurado($request->apellido_asegurado)
                    ->email($request->email_asegurado)
                    ->cedula($request->cedula)
                    ->id($request->id_reclamo)
                    ->get();

            foreach ($list as $reclamo) {
                $reclamo->ciudadrecidencia = Ciudades_departamentos::find($reclamo->ciudadrecidencia)->nombre;
                $reclamo->ciudadsiniestro = Ciudades_departamentos::find($reclamo->ciudadsiniestro)->nombre;
                if (empty($reclamo->datos_cliente)) {
                    $reclamo->nombre_cliente = 'sin nombre';    
                }else{
                    $reclamo->nombre_cliente = $reclamo->datos_cliente->nombre." ".$reclamo->datos_cliente->lastname;    
                }
                $reclamo->estados_poliza = $reclamo->datos_estado_poliza->nombre;
            }
        }

    	array_push($array, $list);

    	$today = date('Y-m-d');
		return Excel::create('reclamos'.$today, function($excel) use ($array) {
			foreach ($array as $value) {
				$excel->sheet('Reclamos', function($sheet) use ($value)
		        {
                    $sheet->setColumnFormat(array(
                        'A:AZ' => '@'
                    ));
					$sheet->fromArray($value);
		        });
			}			
		})->download('xlsx');
    }

    /**
	*
	* Exportar en excel todos los reclamos
	*
	**/
    public function reclamos_one($reclamo){
    	$array = array();
    	$list = Reclamos::where('id','=',$reclamo)->get();
    	foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades_departamentos::find($reclamo->ciudadrecidencia)->nombre;
    		$reclamo->ciudadsiniestro = Ciudades_departamentos::find($reclamo->ciudadsiniestro)->nombre;
    		$reclamo->producto = $reclamo->plan_poliza;
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza)->nombre;
    		$reclamo->cliente = Clientes::find($reclamo->cliente)->email;
    	}

    	array_push($array, $list);
    	$today = date('Y-m-d');
		return Excel::create('reclamo'.$today, function($excel) use ($array) {
			$cont=0;
			foreach ($array as $value) {
				$sheet='hoja';
				if($cont==0){$sheet='Reclamos';}
				elseif($cont==1){$sheet='Ciudades';}
				elseif($cont==2){$sheet='Productos';}
				elseif($cont==3){$sheet='Estados_polizas';}
				elseif($cont==4){$sheet='Marcas';}
				$excel->sheet($sheet, function($sheet) use ($value)
		        {
					$sheet->fromArray($value);
		        });
		        $cont++;
			}			
		})->download('xls');
    }

    public function reclamo_zurich($reclamo){
        ini_set('max_execution_time', 180);
        ini_set('memory_limit', '-1');
        $array = array();
        $list = ZurichReclamos::where('id',$reclamo)->get();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = ZurichCiudadDepartamentos::find($reclamo->ciudadrecidencia)->nombre;
            $reclamo->ciudadsiniestro = ZurichCiudadDepartamentos::find($reclamo->ciudadsiniestro)->nombre;
            $reclamo->tienda = $reclamo->tienda_reclamo->nombre_tienda;
            $reclamo->estado_reclamo = $reclamo->estado_del_reclamo->nombre_estado;
        }
        array_push($array, $list);
        return Excel::create('reclamo_'.date('Y-m-d'), function($excel) use ($array) {
            foreach ($array as $value) {
                $excel->sheet('reclamo', function($sheet) use ($value)
                {
                    $sheet->fromArray($value);
                });
            }           
        })->download('xls');
    }

    /**
	*
	* Exportar en zip el reclamos
	*
	**/
    public function zip($reclamo, $marca){

        if ($marca != 5) {
            $reclamos = Reclamos::find($reclamo);
            $NewId = "Reclamo_".$reclamos->reclamoid. \Carbon\Carbon::now('America/Bogota')->toDateTimeString();
            $zipper = new Zipper;
            $documents = Documents::where('reclamo',$reclamo)->get();
            if ($documents == '[]') {
                Session::flash('error','No existen archivos adjuntos a este caso');
                return Redirect::to('/crawford/idreclamo/'.$reclamo);
            }else{

                //$zipper->make('/Applications/XAMPP/xamppfiles/htdocs/push_Crawford/crawford/Crawford/public/tmp/'.$NewId.'_1.zip');   
                $zipper->make(public_path().'/storage/crawford/tmp/'.$NewId.'_1.zip');
                foreach ($documents as $value) {
                    if (public_path().'/storage/crawford/'.$value->url) {
                        $zipper->add(public_path().'/storage/crawford/'.$value->url,$value->url);
                    }   
                }
                $zipper->close();
                //dd($zipper);

                return response()->download(public_path('/storage/crawford/tmp/'.$NewId.'_1.zip'));
                unlink(public_path('/storage/crawford/tmp/'.$NewId.'_1.zip'));
                //return response()->download('http://localhost:8080/push_Crawford/crawford/Crawford/public/tmp/'.$NewId.'_1.zip');

            }   
        }else{
            $reclamo_zurich = ZurichReclamos::find($reclamo);
            $nombre_archivo = "Reclamo_".$reclamo_zurich->id. \Carbon\Carbon::now('America/Bogota')->toDateTimeString();
            $zipper = new Zipper;
            $documentos = ZurichDocumentosReclamos::where('id_reclamo', $reclamo)->get();
            if ($documentos->count() == 0) {
                Session::flash('error','No existen archivos adjuntos a este caso');
                return Redirect::to('/crawford/idreclamo_zurich/'.$reclamo);
            }else{
                $zipper->make(public_path().'/storage/crawford/checklists_zurich/tmp/'.$nombre_archivo.'.zip');
                foreach ($documentos as $documento) {
                    if (public_path().'/storage/crawford/checklists_zurich/'.$documento->nombre_documento) {
                        $zipper->add(public_path().'/storage/crawford/checklists_zurich/'.$documento->nombre_documento, $documento->nombre_documento);
                    }   
                }
                $zipper->close();
                return response()->download(public_path('/storage/crawford/checklists_zurich/tmp/'.$nombre_archivo.'.zip'));
                unlink(public_path('/storage/crawford/checklists_zurich/tmp/'.$nombre_archivo.'.zip'));
            }
        }
		
    }

}
