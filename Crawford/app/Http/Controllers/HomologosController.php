<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Homologos;
use Excel;


class HomologosController extends Controller
{
    //'nombre', 'descripcion',
    /**
	*  respuesta todos las Homologos
	*
	*  GET url
	*/
    public function all(){
    	$list = Homologos::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Homologos
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Homologos::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var nombre
	*  @var descripcion
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!=''){ //estan los datos completos
            $list = Homologos::create(request(['nombre', 'descripcion', 'logo']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Homologos guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Homologos
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('id')!=''){ //estan los datos completos
            $homologos = Homologos::find(request('id'));
            $homologos->nombre = request('nombre');
            $homologos->descripcion = request('descripcion');
            $homologos->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'homologos Actualizada id:'.$homologos->id,
                'body' => $homologos
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Homologos
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$homologos = Homologos::find($id);
        $homologos->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'homologos Eliminada id:'.$homologos->id,
                'body' => $homologos
            ]);
    }

    /**
	*  view index Homologos
	*
	*  GET url 
	*/
    public function index(){
    	$list = Homologos::all();
    	return view('homologos', array(
    		'homologos' => $list 
    	));
    }

    /**
	*  view Filtros de  Homologos
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Homologos::where('nombre', 'like', '%'.$filtro.'%')
						->get();
    	return view('homologos', array(
    		'homologos' => $list 
    	));
    }

    /**
	*  view EXCEL Homologos
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Homologos::all();
		return Excel::create('homologos', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

}
