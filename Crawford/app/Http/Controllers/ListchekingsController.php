<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Listchekings;
use App\Tipo_polizas;
use App\Tipo_asegurados;
use App\Tiendas;
use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;
use App\Devices;
use App\Documents;
use App\Reclamos;
use App\Clientes;
use Illuminate\Support\Facades\DB;
use Mail;
use Excel;
use OneSignal;

class ListchekingsController extends Controller
{
    //'nombre', 'img', 'titulo', 'descripcion', 'tipo', 'subdescripcion', 'ayuda', 'tipo_polizas', 'asegurado', 'marca', 'tipo_seguro', 'aseguradora', 'campana', 'tienda','reclamo' 
    /**
	*  respuesta todos las Listchekings
	*
	*  GET url
	*/
    public function all(){
    	$list = Listchekings::all();
    	foreach ($list as $listchekings) {
    		$listchekings->marca       = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana 	 = Campanas::find($listchekings->campana);
            $listchekings->tienda 	 = Tiendas::find($listchekings->tienda);
            $listchekings->asegurado  = Tipo_asegurados::find($listchekings->asegurado);
            $listchekings->tipo_poliza  = Tipo_polizas::find($listchekings->tipo_poliza);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Listchekings
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Listchekings::where('id',$id)->get();
    	foreach ($list as $listchekings) {
    		$listchekings->marca       = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana 	 = Campanas::find($listchekings->campana);
            $listchekings->tienda 	 = Tiendas::find($listchekings->tienda);
            $listchekings->asegurado  = Tipo_asegurados::find($listchekings->asegurado);
            $listchekings->tipo_poliza  = Tipo_polizas::find($listchekings->tipo_poliza);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Listchekings
	*  
	*  POST url
	*  @var nombre
	*  @var img
	*  @var titulo
	*  @var descripcion
	*  @var tipo
	*  @var subdescripcion
	*  @var ayuda
	*  @var tipo_polizas
	*  @var asegurado
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*  @var campana
	*  @var tienda
    *  @var reclamo
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('campana')!='' || request('tienda')!=''){ //estan los datos completos
            $list = Listchekings::create(request(['nombre', 'img', 'titulo', 'descripcion', 'tipo', 'subdescripcion', 'ayuda', 'tipo_polizas', 'asegurado', 'marca', 'tipo_seguro', 'aseguradora', 'campana', 'tienda','reclamo']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'listchekings guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }

        //verificar si es de un reclamo en especifico
        if(request('reclamo')!='0'){
            //buscar para actualizar el estado a ampliacion de hechos
            $reclamos = Reclamos::find(request('reclamo'));
            $Reclamos2 = Reclamos::find(request('reclamo'));
            $Reclamos2->estados_poliza      = 2;
            $Reclamos2->save();

            $client = Clientes::find($reclamos->cliente);

            //send push onesignal
            $clientId = $client->id;
            $message = 'Seguros Mundial a cambiado el estado del reclamo #'.$reclamos->reclamoid;
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }

            //envio de correo
            $fileName = '';
            if ($client->marca == 4) {
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Solicitud de documento',
                        'mensaje'=> 'Estimado usuario para el reclamo #'.request('reclamo').' solicitamos que adjunte el adicional el documento '.request('nombre')
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre)->subject('Solicitud de documento extra');
                });
            }elseif ($client->marca == 3) {
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Solicitud de documento',
                        'mensaje'=> 'Estimado usuario para el reclamo #'.request('reclamo').' solicitamos que adjunte el adicional el documento '.request('nombre')
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre)->subject('Solicitud de documento extra');
                });
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Solicitud de documento',
                        'mensaje'=> 'Estimado usuario para el reclamo #'.request('reclamo').' solicitamos que adjunte el adicional el documento '.request('nombre')
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre)->subject('Solicitud de documento extra');
                });
            }
            
        }

        return $response;
    }

    /**
	*  respuesta Actualizacion de Listchekings
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var img
	*  @var titulo
	*  @var descripcion
	*  @var tipo
	*  @var subdescripcion
	*  @var ayuda
	*  @var tipo_polizas
	*  @var asegurado
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*  @var campana
	*  @var tienda
    *  @var reclamo
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('campana')!='' || request('id')!='' || request('tienda')!=''){ //estan los datos completos
            $listchekings = Listchekings::find(request('id'));
            $listchekings->nombre = request('nombre');
            $listchekings->img = request('img');
            $listchekings->titulo = request('titulo');
            $listchekings->descripcion = request('descripcion');
            $listchekings->tipo = request('tipo');
            $listchekings->subdescripcion = request('subdescripcion');
            $listchekings->ayuda = request('ayuda');
            $listchekings->aseguradora = request('aseguradora');
            $listchekings->marca = request('marca');
            $listchekings->tipo_seguro = request('tipo_seguro');
            $listchekings->tienda = request('tienda');
            $listchekings->campana = request('campana');
            $listchekings->asegurado = request('asegurado');
            $listchekings->tipo_polizas = request('tipo_polizas');
            $listchekings->reclamo = request('reclamo');
            $listchekings->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'listchekings Actualizada id:'.$listchekings->id,
                'body' => $listchekings
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Listchekings
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$listchekings = Listchekings::find($id);
        $listchekings->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'listchekings Eliminada id:'.$listchekings->id,
                'body' => $listchekings
            ]);
    }

    /**
	*  view index Listchekings
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Listchekings::all();
    	$marcas 		= marcas::all();
    	foreach ($list as $listchekings) {
    		$listchekings->marca       = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana 	 = Campanas::find($listchekings->campana);
            $listchekings->tienda 	 = Tiendas::find($listchekings->tienda);
            $listchekings->asegurado  = Tipo_asegurados::find($listchekings->asegurado);
            $listchekings->tipo_poliza  = Tipo_polizas::find($listchekings->tipo_poliza);
    	}
    	return view('listchekings', array(
    		'listchekings' => $list,
            'marcas' => $marcas
    	));
    }

    /**
    *  respuesta una de las Listchekings
    *
    *  GET url
    *  @var id tipo_poliza 
    */
    public function tipo_poliza($tipo_poliza){
        $list = Listchekings::where('tipo_polizas',$tipo_poliza)->get();
        foreach ($list as $listchekings) {
            $listchekings->marca = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana 	 = Campanas::find($listchekings->campana); 
            $listchekings->tienda 	 = Tiendas::find($listchekings->tienda);   
            $listchekings->tipo_poliza  = Tipo_polizas::find($listchekings->tipo_poliza);         
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta una de las Listchekings
    *
    *  GET url
    *  @var id tipo_poliza 
    */
    public function tipo_polizareclamo($tipo_poliza,$reclamo){
        $list = Listchekings::where('tipo_polizas',$tipo_poliza)->where('reclamo',$reclamo)->get();
        if( sizeof($list) == 0 ){
            $list = Listchekings::where('tipo_polizas',$tipo_poliza)->where('reclamo','0')->get();
        }
        foreach ($list as $listchekings) {
            $listchekings->marca = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana   = Campanas::find($listchekings->campana); 
            $listchekings->tienda    = Tiendas::find($listchekings->tienda);   
            $listchekings->tipo_poliza  = Tipo_polizas::find($listchekings->tipo_poliza);         
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Listchekings
	*
	*  GET url 
	*/
    public function filtros($filtro){
        $marcas         = marcas::all();
    	$list = Listchekings::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $listchekings) {
    		$listchekings->marca       = marcas::find($listchekings->marca);
            $listchekings->tipo_seguro = Tipo_seguros::find($listchekings->tipo_seguro);
            $listchekings->aseguradora = Aseguradoras::find($listchekings->aseguradora);
            $listchekings->campana    = Campanas::find($listchekings->campana);
            $listchekings->tienda     = Tiendas::find($listchekings->tienda);
            $listchekings->asegurado  = Tipo_asegurados::find($listchekings->asegurado);
            $listchekings->tipo_poliza  = Tipo_asegurados::find($listchekings->tipo_poliza);            
    	}
    	return view('listchekings', array(
    		'listchekings' => $list,
            'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Listchekings
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Listchekings::all();
		return Excel::create('Listchekings', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function traer_lista($reclamo){

        $traer2 = DB::table('documents')->selectRaw('listcheking, count(listcheking) as cantidad')->where('reclamo', $reclamo)->groupBy('listcheking')->get();
        //->where('reclamo','=',$reclamo)->groupBy('listcheking')->count();
        

        //$traer = Documents::select('listcheking')->where('reclamo',$reclamo)->count()->groupBy('listcheking');

        return array(
            'success' => 'true',
            'message' => 'Se ha consultado con exito',
            'body' => $traer2
        );
    }

    public function getCheklistReclamo( $id_reclamo, $marca ){

        $listAmpliacion = Listchekings::where( 'reclamo', $id_reclamo )
                                        ->orderBy('created_at', 'desc')
                                        ->get();

        $data = $listAmpliacion->first();

        return array(
            'success' => 'true',
            'body' => $data
        );

    }

}
