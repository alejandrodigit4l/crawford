<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class LoginExampleController extends Controller
{
    public function login(){

    	$credentials = $this->validate(request(),[
    		'email' => 'email|required|string',
    		'password' => 'required|string'
    	]);

    	return 'nada';

    }
}
