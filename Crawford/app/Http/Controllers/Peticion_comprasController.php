<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peticion_compras;
use App\Clientes;
use App\marcas;
use App\Producto;

class Peticion_comprasController extends Controller
{
    //'marca','usuario','src','estado','mensaje','serial','clientes_id','productos_id',
    /**
	*  respuesta todos las Peticion_compras
	*
	*  GET url
	*/
    public function all(){
    	$list = Peticion_compras::all();
    	foreach ($list as $payu) {
    		$payu->marca = Marcas::find($payu->marca);
    		$payu->productos_id = Producto::find($payu->productos_id);
    		$payu->clientes_id = Clientes::find($payu->clientes_id);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Peticion_compras
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Peticion_compras::where('id',$id)->get();
    	foreach ($list as $payu) {
    		$payu->marca = Marcas::find($payu->marca);
    		$payu->productos_id = Producto::find($payu->productos_id);
    		$payu->clientes_id = Clientes::find($payu->clientes_id);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Peticion_compras
	*
	*  POST url
	*  @var marca
	*  @var usuario
	*  @var src
	*  @var estado
	*  @var mensaje
	*  @var serial
	*  @var clientes_id
	*  @var productos_id
	*/
    public function save(){
    	$list = Peticion_compras::create(request(['marca','usuario','src','estado','mensaje','serial','clientes_id','productos_id']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Peticion_compras guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Peticion_compras
	*
	*  POST url
	*  @var id
	*  @var marca
	*  @var usuario
	*  @var src
	*  @var estado
	*  @var mensaje
	*  @var serial
	*  @var clientes_id
	*  @var productos_id
	*/
    public function update(){
    	$Peticion_compras = Peticion_compras::find(request('id'));
		$Peticion_compras->marca = request('marca');
		$Peticion_compras->usuario = request('usuario');
		$Peticion_compras->src = request('src');
		$Peticion_compras->estado = request('estado');
		$Peticion_compras->mensaje = request('mensaje');
		$Peticion_compras->serial = request('serial');
		$Peticion_compras->clientes_id = request('clientes_id');
		$Peticion_compras->productos_id = request('productos_id');
        $Peticion_compras->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Peticion_compras Actualizada id:'.$Peticion_compras->id,
            'body' => $Peticion_compras
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Peticion_compras
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Peticion_compras = Peticion_compras::find($id);
        $Peticion_compras->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Peticion_compras Eliminada id:'.$Peticion_compras->id,
                'body' => $Peticion_compras
            ]);
    }

    /**
	*  view index Peticion_compras
	*
	*  GET url 
	*/
    public function index(){
    	$marcas = Marcas::all();
    	$list = Peticion_compras::all();
    	foreach ($list as $payu) {
    		$payu->marca = Marcas::find($payu->marca);
    		$payu->productos_id = Producto::find($payu->productos_id);
    		$payu->clientes_id = Clientes::find($payu->clientes_id);
    	}
    	return view('peticion_compras', array(
    		'peticion_compras' => $list,
    		'marcas' => $marcas 
    	));
    }

    /**
	*  view Filtros de  Peticion_compras
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas = Marcas::all();
    	$list = Peticion_compras::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $payu) {
    		$payu->marca = Marcas::find($payu->marca);
    		$payu->productos_id = Producto::find($payu->productos_id);
    		$payu->clientes_id = Clientes::find($payu->clientes_id);
    	}
    	return view('peticion_compras', array(
    		'peticion_compras' => $list 
    	));
    }

    /**
	*  view EXCEL Peticion_compras
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Peticion_compras::all();
		return Excel::create('Peticion_compras', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
