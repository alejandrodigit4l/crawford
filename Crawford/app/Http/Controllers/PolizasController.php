<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use PayU;
use App\Imeis;
use Mpdf\Mpdf;
use App\Polizas;
use Environment;
use PayUReports;
use PayUPayments;
use Carbon\Carbon;
use PayUCountries;
use PayUParameters;
use SupportedLanguages;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class PolizasController extends Controller
{
    //'date_start','date_end','days_on_period','date_certificate_start','date_certificate_end','name_user','city','name_taker','city_taker','id_taker','price','takes','price_and_takes','email','name_product','url','serie','consecutivo','cobertura','deducible','producto','cliente',
    /**
	*  respuesta todos las Polizas
	*
	*  GET url
	*/
    public function all(){
    	$list = Polizas::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Polizas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Polizas::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Polizas
	*
	*  POST url
	*  @var date_start
	*  @var date_end
	*  @var days_on_period
	*  @var date_certificate_start
	*  @var date_certificate_end
	*  @var name_user
	*  @var city
	*  @var name_taker
	*  @var city_taker
	*  @var id_taker
	*  @var price
	*  @var takes
	*  @var price_and_takes
	*  @var email
	*  @var name_product
	*  @var url
	*  @var serie
	*  @var consecutivo
	*  @var cobertura
	*  @var deducible
	*  @var producto
	*  @var cliente
	*/
    public function save(){
		$list = Polizas::create(request(['date_start','date_end','days_on_period','date_certificate_start','date_certificate_end','name_user','city','name_taker','city_taker','id_taker','price','takes','price_and_takes','email','name_product','url','serie','consecutivo','cobertura','deducible','producto','cliente']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Polizas guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Polizas
	*
	*  POST url
	*  @var id
	*  @var date_start
	*  @var date_end
	*  @var days_on_period
	*  @var date_certificate_start
	*  @var date_certificate_end
	*  @var name_user
	*  @var city
	*  @var name_taker
	*  @var city_taker
	*  @var id_taker
	*  @var price
	*  @var takes
	*  @var price_and_takes
	*  @var email
	*  @var name_product
	*  @var url
	*  @var serie
	*  @var consecutivo
	*  @var cobertura
	*  @var deducible
	*  @var producto
	*  @var cliente
	*/
    public function update(){
    	$Polizas = Polizas::find(request('id'));
		$Polizas->date_start = request('date_start');
		$Polizas->date_end = request('date_end');
		$Polizas->days_on_period = request('days_on_period');
		$Polizas->date_certificate_start = request('date_certificate_start');
		$Polizas->date_certificate_end = request('date_certificate_end');
		$Polizas->name_user = request('name_user');
		$Polizas->city = request('city');
		$Polizas->name_taker = request('name_taker');
		$Polizas->city_taker = request('city_taker');
		$Polizas->id_taker = request('id_taker');
		$Polizas->price = request('price');
		$Polizas->takes = request('takes');
		$Polizas->price_and_takes = request('price_and_takes');
		$Polizas->email = request('email');
		$Polizas->name_product = request('name_product');
		$Polizas->url = request('url');
		$Polizas->serie = request('serie');
		$Polizas->consecutivo = request('consecutivo');
		$Polizas->cobertura = request('cobertura');
		$Polizas->deducible = request('deducible');
		$Polizas->producto = request('producto');
		$Polizas->cliente = request('cliente');
        $Polizas->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Polizas Actualizada id:'.$Polizas->id,
            'body' => $Polizas
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Polizas
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Polizas = Polizas::find($id);
        $Polizas->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Polizas Eliminada id:'.$Polizas->id,
                'body' => $Polizas
            ]);
    }

    /**
	*  view index Polizas
	*
	*  GET url 
	*/
    public function index(){
    	$list = Polizas::all();
    	return view('polizas', array(
    		'polizas' => $list 
    	));
    }

    /**
	*  view Filtros de  Polizas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Polizas::where('nombre', 'like', '%'.$filtro.'%')
						->get();
    	return view('polizas', array(
    		'polizas' => $list 
    	));
    }

    /**
	*  view EXCEL Polizas
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Polizas::all();
		return Excel::create('Polizas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function subirDocumentosPoliza( Request $request ){

		$consulta = Polizas::where( 'imei', $request->imei )
							->whereIn( 'estado_poliza', [1,4,5,6] )->get(); 

		if( $consulta->count() >0 ){

			$fileNameEquipo = '';
			if( !!$request->fotoCel ){
				$path_foto = $request->fotoCel;
				$extfoto = $request->fotoCel->extension();
				$fileNameEquipo = uniqid().".".$extfoto;
		
				Storage::putFileAs(
					'', $request->file('fotoCel'), $fileNameEquipo
				);
				
			}
			
			$path_factura = $request->fotoFactura;
			$extfactura = $request->fotoFactura->extension();
			$fileNameFactura = uniqid().".".$extfactura;
			Storage::putFileAs(
				'', $request->file('fotoFactura'), $fileNameFactura
			);
			$poliza = $request;
			if ( !!$request->fotoCel ) {
				$poliza->url_equipo = $fileNameEquipo;
			}
			$poliza->url_factura = $fileNameFactura;

			$consulta = Polizas::where( 'imei', $request->imei )->whereIn( 'estado_poliza', [1,4,5,6] )->update(['estado_poliza' => 4,'url_factura' => $fileNameFactura,'url_equipo' => $fileNameEquipo]); 

			return 'ya hay peticiones de compra pendientes por respuesta';

		}else{
			
					if( !!$request->fotoCel ){
						$path_foto = $request->fotoCel;
						$extfoto = $request->fotoCel->extension();
						$fileNameEquipo = uniqid().".".$extfoto;
						// $destinationPathEquipo = public_path().'/documentos_poliza';
						// $request->file('fotoCel')->move($destinationPathEquipo, $fileNameEquipo);
						Storage::putFileAs(
							'', $request->file('fotoCel'), $fileNameEquipo
						);
						
					}

					
					$path_factura = $request->fotoFactura;
					$extfactura = $request->fotoFactura->extension();
					$fileNameFactura = uniqid().".".$extfactura;
					Storage::putFileAs(
						'', $request->file('fotoFactura'), $fileNameFactura
					);
					// $destinationPathFactura = public_path().'/documentos_poliza';
					// $request->file('fotoFactura')->move($destinationPathFactura, $fileNameFactura);
					$poliza = $request;
					if ( !!$request->fotoCel ) {
						$poliza->url_equipo = $fileNameEquipo;
					}
					$poliza->url_factura = $fileNameFactura;
					
				

					$producto = Polizas::create([

							'marca'=> 4,
							'url_equipo'=> $poliza->url_equipo,
							'url_factura'=> $poliza->url_factura,
							'estado_poliza'=> 4,
							'name_user'  => $request->name_user,
							'fecha_compra' => $request->fecha_compra,
							'price'    => $request->price,
							'email'    => $request->email,
							'name_product' => $request->name_product,
							'serie'    => $request->serie,		
							'imei'    => $request->imei,
							'cobertura'  => $poliza->cobertura,
							'producto'  => $request->productos_id,
							'cliente'   => $request->cliente_id,
							'user_create' => 'app_samsung',
						
						]);



						return array(
						'response'=> 'success',
						'data'	=> $producto,
						'res' => $consulta
						);
		}

	}

	public function pagos( Request $request ){

		$deviceSessionId = md5(session_id().microtime());
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://maf.pagosonline.net/ws/fp/tags.js?id=".$deviceSessionId."80200",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_TIMEOUT => 30000,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				// Set Here Your Requesred Headers
				'Content-Type: application/json',
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);


		// $ip = explode(',', $_SERVER["HTTP_X_FORWARDED_FOR"]);
		// $ipcliente = $ip[0];

		PayU::$apiKey = "149389173be";
  		PayU::$apiLogin = "2G93S6avzdzTj66";
		PayU::$merchantId = "109157";
		$accountId = "644720";
		// PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
  //   	PayU::$apiLogin = "pRRXKOl8ikMmt9u";
		// PayU::$merchantId = "508029";
		// $accountId = "512321";
    	PayU::$language = SupportedLanguages::ES;
		PayU::$isTest = false;
		$reference = "Product_".rand(100, 100000);
    	Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
    	// Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
    	// Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi"); 
		// Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.3/");
		
		//$reference = "payment_test_00000002";
		$value = $request->precio;
		$TAX_VALUE = ($value * 19)/100;
		$fecha_expiracion = str_replace( '-', '/', $request->fecha_expira );
		$idPoliza = $request->id;
	
		$parameters = array(
			PayUParameters::ACCOUNT_ID => $accountId,
			PayUParameters::REFERENCE_CODE => $reference,
			PayUParameters::DESCRIPTION => 'Compra polizas Samsung',
			PayUParameters::VALUE => intval($value)+intval($TAX_VALUE),
			PayUParameters::CURRENCY => "COP",
			PayUParameters::TAX_VALUE => intval($TAX_VALUE),
			PayUParameters::TAX_RETURN_BASE => intval($value),
			PayUParameters::BUYER_NAME => $request->nombre_compra,
			PayUParameters::BUYER_EMAIL => $request->email_compra,
			PayUParameters::BUYER_CONTACT_PHONE => $request->celular,
			PayUParameters::BUYER_DNI => $request->cedula_compra,
			PayUParameters::BUYER_STREET => $request->direccion_compra,
			PayUParameters::BUYER_STREET_2 => $request->direccion_compra2,
			PayUParameters::BUYER_CITY => $request->ciudad_compra,
			PayUParameters::BUYER_STATE => $request->depart_compra,
			PayUParameters::BUYER_COUNTRY => "CO",
			PayUParameters::BUYER_POSTAL_CODE => '0000000',
			PayUParameters::BUYER_PHONE => $request->telefono_compra,
			PayUParameters::PAYER_NAME => $request->nombre_paga,
			PayUParameters::PAYER_EMAIL => $request->email_paga,
			PayUParameters::PAYER_CONTACT_PHONE => $request->celular_paga,
			PayUParameters::PAYER_DNI => $request->cedula_paga,
			PayUParameters::PAYER_STREET => $request->direccion_paga,
			PayUParameters::PAYER_STREET_2 => $request->direccion_paga2,
			PayUParameters::PAYER_CITY => $request->ciudad_paga,
			PayUParameters::PAYER_STATE => $request->depart_paga,
			PayUParameters::PAYER_COUNTRY => "CO",
			PayUParameters::PAYER_POSTAL_CODE => '0000000',
			PayUParameters::PAYER_PHONE => $request->telefono_paga,
			PayUParameters::CREDIT_CARD_NUMBER => $request->n_tarjeta, 
			PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $fecha_expiracion,
			PayUParameters::CREDIT_CARD_SECURITY_CODE=> $request->cvv,
			PayUParameters::PAYMENT_METHOD => $request->franquisia,
			PayUParameters::INSTALLMENTS_NUMBER => $request->cuotas,
			PayUParameters::COUNTRY => PayUCountries::CO,
			PayUParameters::DEVICE_SESSION_ID => $deviceSessionId,
			// PayUParameters::IP_ADDRESS => $ipcliente,
			PayUParameters::IP_ADDRESS => $_SERVER['REMOTE_ADDR'],
			PayUParameters::PAYER_COOKIE => @$_COOKIE['private_content_version'],
			PayUParameters::USER_AGENT => $_SERVER['HTTP_USER_AGENT'],
		);
		$response = PayUPayments::doAuthorizationAndCapture($parameters, SupportedLanguages::ES);

		$data = Polizas::where('id', $request->id)->get()->first();

		if( $response->transactionResponse->state == 'APPROVED' ){

			$data->response_code = $response->transactionResponse->state;
			$data->orderId = $response->transactionResponse->orderId;
			$data->transaction_id = $response->transactionResponse->transactionId;
			$data->estado_poliza = 1;
			$data->fecha_vigencia = date('Y-m-d');
			$data->save();
			$this->getGenerar( $idPoliza );

			$respuesta = "Transacción exitosa.";

		}else if( $response->transactionResponse->state == 'DECLINED' ){
			
			$data->response_code = $response->transactionResponse->state;
			$data->orderId = $response->transactionResponse->orderId;
			$data->transaction_id = $response->transactionResponse->transactionId;
			$data->estado_poliza = 2;
			$data->save();
			$respuesta = "Su transacción fue rechazada.";

		}else if($response->transactionResponse->state == 'PENDING'){
			
			$data->response_code = $response->transactionResponse->state;
			$data->orderId = $response->transactionResponse->orderId;
			$data->transaction_id = $response->transactionResponse->transactionId;
			$data->estado_poliza = 7;
			$data->save();

			$respuesta = "Su transacción se encuentra en estado pendiente.";
		}else{
			$respuesta = "Ocurrió un error en la transaccion";
		}

		return array( "respuesta" => $respuesta,
					  "respuesta payu"=> $response,
					  "ip"=> $_SERVER['REMOTE_ADDR']			
		);

	}

	public function getGenerar( $id ){

		error_reporting(0);

		$data = Polizas::where('id', $id)
		->where('estado_poliza', 1)
		->with('productos','clientes')->get()->first();
		$consecutivo = 3447 + $data->id;
		
		if( $data != null ){

			if( $data->productos->categoria != 'beneficio' ){
				
				$vista = array(
					'iva' => ($data->productos->valor * 0.19),
					'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
					'precio' => $data->productos->valor,
					'imei' => $data->imei,
					'cubrimiento'=> $data->productos->cubrimiento,
					'modelo'=> $data->productos->modelo,
					'fecha'=> $data->fecha_vigencia,
					'nombre'=> $data->clientes->nombre,
					'apellido'=> $data->clientes->lastname,
					'cedula'=> $data->clientes->cedula,
					'telefono'=> $data->clientes->celular,
					'ciudad'=> $data->clientes->ciudad,
					'consecutivo' => $consecutivo,
					'titulo' => $data->productos->titulo,
					'descripcion' => $data->productos->descripcion,
				);
				$html = view('crawford.voluntariosSamsung',['data'=>$vista])->render();
				
				
			}else{
				$vista = array(
					'iva' => ($data->productos->valor * 0.19),
					'precioTotal' => $data->productos->valor + ($data->productos->valor * 0.19),
					'precio' => $data->productos->valor,
					'imei' => $data->imei,
					'cubrimiento'=> $data->productos->cubrimiento,
					'modelo'=> $data->productos->modelo,
					'consecutivo' => $consecutivo
				);
							
				$html = view('crawford.polizaSamsung',['data'=>$vista])->render();
			}


				$namefile = 'Poliza_'.$data->imei.'-'.date('y-m-d').'.pdf';
				$mpdf = new Mpdf();
				$mpdf->SetDisplayMode('fullpage');
				$mpdf->WriteHTML($html);
				$path = public_path()."/polizasActivas/";
				$url = url('polizasActivas/'.$namefile);
				// $data->ruta_poliza = $path.$namefile;
				$data->url_poliza = $url;
				$data->state = 'notificado';
				$data->save();
				$fileName =$path.$namefile;
				$r = $mpdf->Output($path.$namefile, "F");

			$attachments = [
				public_path().'/condiciones/CONDICIONES PARTICULARES SAMSUNG MOBILECARE MANDATORIO- V GENERAL v final.pdf',
				public_path().'/condiciones/CONDICIONES_GENERALES.pdf',
				public_path().'/polizasActivas/'.$namefile
			];

				Mail::send('mails/mail_samsung', 
							[
								'titulo' => 'Poliza adquirida equipo samsung',
								'email' => $data->email,
								'mensaje'=> 'En este mensaje '
							],
								
								function ($m) use ($data, $attachments)
				{	
					foreach( $attachments as $adjunto ){
						$m->attach($adjunto);
					}
					// $m->attach($fileName);
					$m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
					$m->to($data->email, $data->clientes->nombre.' '.$data->clientes->lastname)->bcc('polizas@crawfordaffinitycolombia.com','Notificación Crawford')->subject('Notificación Seguros Mundial!');
				});	
				return $data;

		}else{
			return "no hay clientes";
		}
		

	}

	public function verificarpago($id){
		PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
    	PayU::$apiLogin = "pRRXKOl8ikMmt9u";
    	PayU::$merchantId = "508029";
    	PayU::$language = SupportedLanguages::ES;
		PayU::$isTest = true;
		$reference = "Product_".rand(100, 100000);
    	Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
    	Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi"); 
		Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.3/");

		$parameters = array(PayUParameters::TRANSACTION_ID => $id,PayUParameters::ACCOUNT_ID => '512321',);

		$response = PayUReports::getTransactionResponse($parameters);
		// dd($response);
		return json_encode($response);
	}

    public function polizasPendientes(){

		$polizas = Polizas::with('productos')->get();
		if (Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','CONSULTA_CLIENTES','GESTION','SOPORTE'])) {
			return view( 'crawford.polizas', array(
                'polizas' => $polizas
        	));	
		}else{
			return view('crawford.error');
		}

	}

	public function datos_poliza($id){
		$datos_poliza = Polizas::with('clientes')->find($id);
		if (!empty($datos_poliza->url_equipo)) {
			$datos_poliza->url_equipo = Storage::url($datos_poliza->url_equipo);	
		}else{
			$datos_poliza->url_equipo = null;
		}
		if (!empty($datos_poliza->url_factura)) {
			$datos_poliza->url_factura = Storage::url($datos_poliza->url_factura);
		}else{
			$datos_poliza->url_factura = null;
		}
		return $datos_poliza;
	}

	public function actualizar_estado_poliza(Request $request){
		$id_poliza = $request['id_poliza'];
		$estado_poliza = $request['estado_actualizado'];
		$cambio_estado = Polizas::find($id_poliza);
		$cambio_estado->estado_poliza = $estado_poliza;
		if ($estado_poliza == '2') {
			$estado_poliza = 'RECHAZADO';
		}elseif ($estado_poliza == '5'){
			$estado_poliza = 'HABILITADO';
		}elseif ($estado_poliza == '6') {
			$estado_poliza = 'REQUIRE NUEVA PETICION';
			Storage::delete($cambio_estado->url_equipo);
			Storage::delete($cambio_estado->url_factura);
			$cambio_estado->url_equipo = '';
			$cambio_estado->url_factura = '';
		}
		$cambio_estado->save();

		Mail::send('mails/mail_samsung', 
        [
                'titulo' => 'Seguros Mundial a cambiado el estado de su solicitud: ',
                'mensaje'=> 'Señor(a) '.$request['nombre_usuario'].', se ha cambiado el estado de su solicitud a '.$estado_poliza.'.'
        ], 
        function ($m) use ($request) 
        {
            $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
            $m->to($request['email_usuario'], $request['nombre_usuario'])->subject('Cambio de estado de solicitud Seguros Mundial!');
        });

		return array($cambio_estado,$estado_poliza);
	}

	public function notificacion_poliza(Request $request){
		Mail::send('mails/mail_samsung',
        [
                'titulo' => 'Seguros Mundial a enviado un mensaje de notificación: ',
                'mensaje'=> 'Señor(a), '.$request['nombre_usuario'].':'."\n".$request['mensaje_usuario']
        ], 
        function ($m) use ($request) 
        {
            $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
            $m->to($request['email_usuario'], $request['nombre_usuario'])->subject('Notificación Seguros Mundial!');
        });
	}

	public function download_poliza($file){
		$pathtoFile = public_path().'/polizasActivas/'.$file;
        return response()->download($pathtoFile);
	}

	public function filtro_estado($estado_poliza){
		$polizas_por_estado = Polizas::with('productos')->where('estado_poliza',$estado_poliza)->get();
		// return $polizas_por_estado;
		return view('crawford.polizas_estado', array('polizas_por_estado' => $polizas_por_estado, 'estado' => $estado_poliza));
	}

	public function vistaPoliza(){

		if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','SOPORTE'])){
			return view('crawford.envioPolizas');
		}else{
			return view('crawford.error');
		}

	}

	public function subirimeis(Request $request){

		\Excel::load($request['imeiscsv'], function($reader) {

	        $excel = $reader->get();

	        foreach ($excel as $ex) {

	        	$repetido = Imeis::where('imeis',$ex->imeis)->where('imeis2',$ex->imeis2)->get();
	        	$imeis = new Imeis;
	        	if ($repetido == '[]') {
	        		
		            $imeis->imeis = $ex->imeis;
		            $imeis->imeis2 = $ex->imeis2;
		            $imeis->estado_enviado = 'CARGADO';
		            $imeis->campana = $ex->campana;
		            $imeis->save();	
	        	}

	        }
	    
	    });

	    $imeis_hoy = Imeis::where('created_at','like','%'.date('Y-m-d').'%')->get();

	    return $imeis_hoy;

	}


	public function getIP(){
	
	 $ip = explode(',', $_SERVER["HTTP_X_FORWARDED_FOR"]);
	 
     return  array(
		 "ip"=>$ip[0]
	 );
	}

	public function prueba_meses(){

		$usuario_poliza = Polizas::first();

		$cobertura = $usuario_poliza->cobertura;

		$division_poliza = explode("-", $usuario_poliza->fecha_vigencia);
		$division_hoy = explode("-", date('y-m-d'));


		$fecha_inical = Carbon::create((int)$division_poliza[0], (int)$division_poliza[1], (int)$division_poliza[2]);
		$fecha_final = Carbon::now();

		$diferencia_dias = $fecha_final->diffInDays($fecha_inical);

		$numero_dias = 0;
		if ($cobertura == 6) {
			$numero_dias = 182;
		}else{
			$numero_dias = 365;
		}

		if ($diferencia_dias>$numero_dias) {
			$usuario_poliza->estado_poliza = 3;
			$usuario_poliza->save();
		}

		return $diferencia_dias; // 3

	}

}
