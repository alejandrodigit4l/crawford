<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Productos;
use App\Tipo_polizas;
use App\Tipo_asegurados;
use App\Tiendas;
use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;
use App\Polizas;

use Excel;

class ProductosController extends Controller
{
    //'titulo','descripcion','cubrimiento','valor','urlicono','referencia','tipo_poliza','marca','tipo_seguro','aseguradora','campana','tienda','asegurado' 
    /**
	*  respuesta todos las Productos
	*
	*  GET url
	*/
    public function all(){
    	$list = Productos::all();
    	foreach ($list as $productos) {
    		$productos->marca       = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana 	 = Campanas::find($productos->campana);
            $productos->tienda 	 = Tiendas::find($productos->tienda);
            $productos->asegurado  = Tipo_asegurados::find($productos->asegurado);
            $productos->tipo_poliza  = Tipo_polizas::find($productos->tipo_poliza);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Productos
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Productos::where('id',$id)->get();
    	foreach ($list as $productos) {
    		$productos->marca       = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana 	 = Campanas::find($productos->campana);
            $productos->tienda 	 	= Tiendas::find($productos->tienda);
            $productos->asegurado  = Tipo_asegurados::find($productos->asegurado);
            $productos->tipo_poliza  = Tipo_polizas::find($productos->tipo_poliza);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Productos
	*  
	*  POST url
	*  @var titulo
	*  @var descripcion
	*  @var cubrimiento
	*  @var valor
	*  @var urlicono
	*  @var referencia
	*  @var tipo_poliza
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*  @var campana
	*  @var tienda
	*  @var asegurado
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('campana')!='' || request('tienda')!=''){ //estan los datos completos
            $list = Productos::create(request(['titulo','descripcion','cubrimiento','valor','urlicono','referencia','tipo_poliza','marca','tipo_seguro','aseguradora','campana','tienda','asegurado' ]));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'productos guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Productos
	*
	*  POST url
	*  @var id
	*  @var titulo
	*  @var descripcion
	*  @var cubrimiento
	*  @var valor
	*  @var urlicono
	*  @var referencia
	*  @var tipo_poliza
	*  @var marca
	*  @var tipo_seguro
	*  @var aseguradora
	*  @var campana
	*  @var tienda
	*  @var asegurado
	*/
    public function update(){
    	if(request('cubrimiento')!='' || request('descripcion')!='' || request('campana')!='' || request('id')!='' || request('tienda')!=''){ //estan los datos completos
            $productos = Productos::find(request('id'));
            $productos->titulo		= request('titulo');
			$productos->descripcion	= request('descripcion');
			$productos->cubrimiento	= request('cubrimiento');
			$productos->valor		= request('valor');
			$productos->urlicono		= request('urlicono');
			$productos->referencia	= request('referencia');
			$productos->tipo_poliza	= request('tipo_poliza');
			$productos->marca		= request('marca');
			$productos->tipo_seguro	= request('tipo_seguro');
			$productos->aseguradora	= request('aseguradora');
			$productos->campana		= request('campana');
			$productos->tienda		= request('tienda');
			$productos->asegurado	= request('asegurado');
            $productos->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'productos Actualizada id:'.$productos->id,
                'body' => $productos
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Productos
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$productos = Productos::find($id);
        $productos->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'productos Eliminada id:'.$productos->id,
                'body' => $productos
            ]);
    }

    /**
	*  view index Productos
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Productos::all();
    	$marcas 		= marcas::all();
    	foreach ($list as $productos) {
    		$productos->marca       = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana 	 = Campanas::find($productos->campana);
            $productos->tienda 	 = Tiendas::find($productos->tienda);
            $productos->asegurado  = Tipo_asegurados::find($productos->asegurado);
            $productos->tipo_poliza  = Tipo_polizas::find($productos->tipo_poliza);
    	}
    	return view('productos', array(
    		'productos' => $list,
            'marcas' => $marcas
    	));
    }

    /**
    *  respuesta una de las Productos
    *
    *  GET url
    *  @var id tipo_poliza 
    */
    public function tipo_poliza($tipo_poliza){
        $list = Productos::where('tipo_poliza',$tipo_poliza)->get();
        foreach ($list as $productos) {
            $productos->marca = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana 	 = Campanas::find($productos->campana); 
            $productos->tienda 	 = Tiendas::find($productos->tienda);   
            $productos->tipo_poliza  = Tipo_polizas::find($productos->tipo_poliza);         
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta una de las Productos
    *
    *  GET url
    *  @var id marca 
    */
    public function marca($marca){
        $list = Productos::where('marca',$marca)->get();
        foreach ($list as $productos) {
            $productos->marca = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana      = Campanas::find($productos->campana); 
            $productos->tienda   = Tiendas::find($productos->tienda);   
            $productos->tipo_poliza  = Tipo_polizas::find($productos->tipo_poliza);         
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Productos
	*
	*  GET url 
	*/
    public function filtros($filtro){
        $marcas         = marcas::all();
    	$list = Productos::where('titulo', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $productos) {
    		$productos->marca       = marcas::find($productos->marca);
            $productos->tipo_seguro = Tipo_seguros::find($productos->tipo_seguro);
            $productos->aseguradora = Aseguradoras::find($productos->aseguradora);
            $productos->campana    = Campanas::find($productos->campana);
            $productos->tienda     = Tiendas::find($productos->tienda);
            $productos->asegurado  = Tipo_asegurados::find($productos->asegurado);
            $productos->tipo_poliza  = Tipo_asegurados::find($productos->tipo_poliza);            
    	}
    	return view('productos', array(
    		'productos' => $list,
            'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Productos
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Productos::all();
		return Excel::create('Productos', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function obtenerPolizasSamsung( $modelo, $imei, $email ){

        $polizas = Polizas::where( 'imei', $imei )
                           ->where( 'email', $email )
                           ->with('productos')
                           ->get();


       
        $listarExtension = false;
        $listarVoluntarios = true;
       
        foreach ($polizas as $value) {
            $categoria = $value->productos->categoria;
            if( $categoria != 'beneficio' && $categoria != 'extension' ){
                // dd($categoria);
                $listarVoluntarios = false;

                $listarExtension = false;
                
            }

            if( $categoria == 'beneficio' ){
                $listarExtension = true;
                $listarVoluntarios = false;
               
            }

            if( $categoria == 'extension' ){
                $listarExtension = false;
                $listarVoluntarios = false;
                
            }

        }

        
        switch ($modelo) {
            case 'SM-G9600':
            case 'SM-G9650':
                $modelo = 's9';
                break;

            case 'SM-G950F':
            case 'SM-G955F':
                $modelo = 's8';
                break;

            case 'SM-N950F':
                 $modelo = 'note8';
                 break;

            case 'SM-N9600':
                 $modelo = 'note9';
                 break;
            
            default:
                # code...
                break;
        }

        $productosQuery = Productos::where('modelo', $modelo);

        $productos = [];
         
        if( $listarExtension == true){
            $productos = $productosQuery->where('categoria', 'extension')->get();
        }
        if( $listarVoluntarios == true ){
            $productos = $productosQuery->where('categoria', '!=', 'extension')->where('categoria', '!=', 'beneficio')->get();
        }

        return array(
            'respuesta' => 'success',
            'productos' => $productos,
            'polizas'   => $polizas
        );

    }

}
