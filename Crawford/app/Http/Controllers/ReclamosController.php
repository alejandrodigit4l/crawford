<?php

namespace App\Http\Controllers;

use Mail;
use Excel;
use OneSignal;
use App\Devices;
use App\Ciudades;
use App\Clientes;
use App\Reclamos;
use App\Homologos;
use App\Productos;
use App\Observaciones;
use App\ZurichReclamos;
use App\Estados_polizas;
use Illuminate\Http\Request;
use App\ZurichDocumentosReclamos;
use Exception;

class ReclamosController extends Controller
{
    //'reclamoid','cedula','nombreasegurado','apellidoasegurado','fechanacimientoasse','ciudadrecidencia','telefonofijo','celular','direccion','email','telefonolaboral','jsonrespuestaform','fechasiniestro','horasiniestro','descripcionsiniestro','textobackend','observaciones','motivobaja','cliente','estados_poliza','producto','ciudadsiniestro'
    /**
	*  respuesta todos las Reclamos
	*
	*  GET url
	*/
    public function all(){
    	$list = Reclamos::all();
    	foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
    		$reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
    		$reclamo->producto = Productos::find($reclamo->producto);
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
    		$reclamo->cliente = Clientes::find($reclamo->cliente);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Reclamos
	*
	*  GET url
	*  @var id identificador de la reclamos en la base de datos
	*/
    public function get($id){
    	$list = Reclamos::where('id',$id)->get();
    	foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
    		$reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
    		//$reclamo->producto = Productos::find($reclamo->producto);
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
    		$reclamo->cliente = Clientes::find($reclamo->cliente);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de reclamos
	*
	*  POST url
	*  @var reclamoid
	*  @var cedula
	*  @var nombreasegurado
	*  @var apellidoasegurado
	*  @var fechanacimientoasse
	*  @var ciudadrecidencia
	*  @var telefonofijo
	*  @var celular
	*  @var direccion
	*  @var email
	*  @var telefonolaboral
	*  @var jsonrespuestaform
	*  @var fechasiniestro
	*  @var horasiniestro
	*  @var descripcionsiniestro
	*  @var textobackend
	*  @var observaciones
	*  @var motivobaja
	*  @var cliente
	*  @var estados_poliza
	*  @var producto
	*  @var ciudadsiniestro
	*/
    public function save(Request $request){
        $consulta_token = Clientes::where('email',$request['email_cliente'])->where('marca',$request['marca_cliente'])->get();

        if ($consulta_token[0]->tokenAPI == $request['token_']) {

            $last_reclamo = Reclamos::where('id','>','0')->orderBy('reclamoid','DESC')->first();
            //brindar un nuevo numero de reclamo
            if($last_reclamo == null){
                $numero_reclamo_nuevo = 1;
            }else{
                $numero_reclamo_nuevo = intval($last_reclamo->reclamoid)+1;
            }
            
        	$list = Reclamos::create(request(['reclamoid','cedula','nombreasegurado','apellidoasegurado','fechanacimientoasse','ciudadrecidencia','telefonofijo','celular','direccion','email','telefonolaboral','producto_reclamo','marca','imei','fechasiniestro','horasiniestro','descripcionsiniestro','textobackend','observaciones','motivobaja','cliente','estados_poliza','producto','tipo_siniestro','ciudadsiniestro','plan_poliza','sitio_creacion']));

            //reclamo id update new reclamo
            $list->reclamoid = $numero_reclamo_nuevo;
            $list->estados_poliza = 1;
            $list->save();

            //send push onesignal
            $clientId = $list->cliente;
            $message = 'Seguros Mundial a creado el reclamo #'.$list->reclamoid;
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }

            //mail send
            $client = Clientes::find($list->cliente);
            $fileName = '';

            if ($client->marca == 4) { /*-- Mensaje Samsung --*/
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }elseif($client->marca == 3){ /*-- Mensaje Seguros Mundial --*/
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }elseif($client->marca == 1){
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }

            //enviar por correo electronico mensaje de reclamo
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Reclamos guardada id:'.$list->id,
                'body' => $list
            ]);
            return $response;

        }else{
        return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION, SE HA LLAMADO A LA CIA PARA LOCALIZARLO';
        }
    }

    public function save_admin(Request $request){

            $this->validate($request, [
                'id_cliente' => 'required',
                'celular' => 'required',
                'dep_residencia' => 'required',
                'ciudad_residencia' => 'required',
                'direccion' => 'required',
                'plan_poliza' => 'required',
                // 'fecha_compra' => 'required',
                'tipo_producto' => 'required',
                'marca_equipo' => 'required',
                'fecha_siniestro' => 'required',
                'hora_siniestro' => 'required',
                'dep_siniestro' => 'required',
                'ciudad_siniestro' => 'required'
            ]);

            $last_reclamo = Reclamos::where('id','>','0')->orderBy('reclamoid','DESC')->first();
            //brindar un nuevo numero de reclamo
            if($last_reclamo == null){
                $numero_reclamo_nuevo = 1;
            }else{
                $numero_reclamo_nuevo = intval($last_reclamo->reclamoid)+1;
            }

            $cliente = Clientes::find($request->id_cliente);

            $producto = 0;

            if ($cliente->marca == 4) {
                $producto = 9;
                if ($request->tipo_siniestro == 'hurto') {
                    $producto = 19;
                }
            }else{
                $producto = 5;
                if ($request->tipo_siniestro == 'hurto') {
                    $producto = 1;
                }
            }

            $list = Reclamos::create([
                'reclamoid' => $numero_reclamo_nuevo,
                'cedula' => $cliente->cedula,
                'nombreasegurado' => $cliente->nombre,
                'apellidoasegurado' => $cliente->lastname,
                'fechanacimientoasse' => $cliente->fechanacimiento,
                'ciudadrecidencia' => $request->ciudad_residencia,
                'telefonofijo' => $request->tel_fijo,
                'celular' => $request->celular,
                'direccion' => $request->direccion,
                'email' => $cliente->email,
                'telefonolaboral' => $request->tel_laboral,
                'producto_reclamo' => $request->tipo_producto,
                'marca' => $request->marca_equipo,
                'imei' => $request->serial_equipo,
                'fechasiniestro' => $request->fecha_siniestro,
                'horasiniestro' => $request->hora_siniestro,
                'descripcionsiniestro' => $request->text_siniestro,
                'textobackend' => 'SIN TEXTO',
                'observaciones' => 'SIN OBSERVACIONES',
                'motivobaja' => 'NO BAJA',
                'cliente' => $request->id_cliente,
                'estados_poliza' => 1,
                'producto' => $producto,
                'tipo_siniestro' => $request->tipo_siniestro,
                'ciudadsiniestro' => $request->ciudad_siniestro,
                'plan_poliza' => $request->plan_poliza,
                'sitio_creacion' => 'administrador',
                'numero_factura' => $request->numero_factura,
                'modelo_equipo' => $request->modelo_equipo,
                'precio_equipo' => $request->precio_equipo,
                'tienda' => $request->tienda,
                'fecha_asignacion_cst' => $request->fecha_asignacion_cst,
                'nombre_cst' => $request->nombre_cs,
                'fecha_ingreso_cst' => $request->fecha_ingreso_cst,
                'fecha_diagnostico' => $request->fecha_diagnostico,
                'tiempo_reparacion' => $request->tiempo_reparacion,
                'causal' => $request->causal,
                'valor_indemnizado' => $request->valor_indemnizado,
                'fecha_notificacion' => $request->fecha_notificacion,
                'certificado' => $request->certificado,
                'radicado_zurich' => $request->radicado_zurich
            ]);

            // //reclamo id update new reclamo
            // $list->reclamoid = $numero_reclamo_nuevo;
            // $list->estados_poliza = 1;
            // $list->save();

            //mail send
            $client = Clientes::find($list->cliente);
            $fileName = '';

            if ($client->marca == 4) { /*-- Mensaje Samsung --*/
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }elseif($client->marca == 3){ /*-- Mensaje Seguros Mundial --*/
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }elseif($client->marca == 1){
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Seguros Mundial a creado el reclamo #'.$list->reclamoid.': ',
                        'mensaje'=> 'Hemos recibido tu solicitud de reclamo y hemos creado el reclamo #'.$list->reclamoid.', con este nos mantendremos en contacto'
                ], 
                function ($m) use ($client, $fileName,$list) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Creación reclamo #'.$list->reclamoid.' Seguros Mundial!');
                });
            }

            //enviar por correo electronico mensaje de reclamo
            $response = response()->json(['id_reclamo'=>$list->id]);
            return $response;

    }

    public function save_zurich(Request $request){

            $reglas = [
                'id_cliente' => 'required',
                'celular' => 'required',
                'dep_residencia' => 'required',
                'ciudad_residencia' => 'required',
                'direccion' => 'required',
                'sponsor' => 'required',
                'plan_poliza' => 'required',
                'tipo_producto' => 'required',
                'marca_equipo' => 'required',
                'certificado' => 'required',
                'fecha_aviso' => 'required',
                'fecha_siniestro' => 'required',
                'hora_siniestro' => 'required',
                'dep_siniestro' => 'required',
                'ciudad_siniestro' => 'required'
            ];

            $mensajes = [
                'fecha_compra.before_or_equal' => 'La fecha de compra no puede ser mayor a la fecha de siniestro.',
                'fecha_ingreso_cst.after_or_equal' => 'La fecha de ingreso a cst no puede ser menor a la fecha de asignacion cst.',
                'fecha_diagnostico.after_or_equal' => 'La fecha de diagnóstico no puede ser menor a la fecha de ingreso a cst.',
                'fecha_siniestro.before_or_equal' => 'La fecha de siniestro no puede ser mayor a la fecha de aviso.'
            ];

            if ($request->has('fecha_compra') && $request->has('fecha_siniestro')) {
                $reglas['fecha_compra'] = 'required|date|date_format:Y-m-d|before_or_equal:fecha_siniestro';
            }
            if ($request->has('fecha_ingreso_cst') && $request->has('fecha_asignacion_cst')) {
                $reglas['fecha_ingreso_cst'] = 'required|date|date_format:Y-m-d|after_or_equal:fecha_asignacion_cst';
            }
            if ($request->has('fecha_diagnostico') && $request->has('fecha_ingreso_cst')) {
                $reglas['fecha_diagnostico'] = 'required|date|date_format:Y-m-d|after_or_equal:fecha_ingreso_cst';   
            }
            if ($request->has('fecha_siniestro') && $request->has('fecha_aviso')) {
                $reglas['fecha_siniestro'] = 'required|date|date_format:Y-m-d|before_or_equal:fecha_aviso';   
            }

            $this->validate($request, $reglas, $mensajes);

            // if ($request->has('fecha_compra') && $request->has('fecha_siniestro')) {
            //     if ($request->fecha_compra > $request->fecha_siniestro) {
            //         return response()->json(['error'=>'La fecha de compra no puede ser mayor a la fecha de siniestro.'],422);
            //     }
            // }

            // if ($request->has('fecha_ingreso_cst') && $request->has('fecha_asignacion_cst')) {
            //     if ($request->fecha_ingreso_cst < $request->fecha_asignacion_cst) {
            //         return response()->json(['error'=>'la fecha de ingreso debe ser mayor a la fecha de asignacion a CST.'],422);
            //     }
            // }

            // if ($request->has('fecha_diagnostico') && $request->has('fecha_ingreso_cst')) {
            //     if ($request->fecha_diagnostico < $request->fecha_ingreso_cst) {
            //         return response()->json(['error'=>'la fecha de diagnostico debe ser mayor a la fecha de ingreso a CST.'],422);
            //     }
            // }

            // if ($request->has('fecha_siniestro') && $request->has('fecha_aviso')) {
            //     if ($request->fecha_siniestro > $request->fecha_aviso) {
            //         return response()->json(['error'=>'la fecha de siniestro debe ser menor a la fecha de aviso.'],422);   
            //     }
            // }

            $nuevo_reclamo = ZurichReclamos::create([
                'id_cliente' => $request->id_cliente,
                'ciudadrecidencia' => $request->ciudad_residencia,
                'telefonofijo' => $request->tel_fijo,
                'celular' => $request->celular,
                'direccion' => $request->direccion,
                'telefonolaboral' => $request->tel_laboral,
                'tipo_producto' => $request->tipo_producto,
                'marca_equipo' => $request->marca_equipo,
                'modelo_equipo' => $request->modelo_equipo,
                'imei' => $request->serial_equipo,
                'fechasiniestro' => $request->fecha_siniestro,
                'horasiniestro' => $request->hora_siniestro,
                'descripcionsiniestro' => $request->text_siniestro,
                'observaciones' => 'SIN OBSERVACIONES',
                'estado_reclamo' => 1,
                'tipo_siniestro' => $request->tipo_siniestro,
                'ciudadsiniestro' => $request->ciudad_siniestro,
                'sponsor' => $request->sponsor,
                'plan_poliza' => $request->plan_poliza,
                'sitio_creacion' => 'administrador',
                'numero_factura' => $request->numero_factura,
                'precio_equipo' => $request->precio_equipo,
                'fecha_compra' => $request->fecha_compra,
                'tienda' => $request->tienda,
                'fecha_asignacion_cst' => $request->fecha_asignacion_cst,
                'nombre_cst' => $request->nombre_cst,
                'fecha_ingreso_cst' => $request->fecha_ingreso_cst,
                'fecha_diagnostico' => $request->fecha_diagnostico,
                'tiempo_reparacion' => $request->tiempo_reparacion,
                'causal' => $request->causal,
                'valor_indemnizado' => $request->valor_indemnizado,
                'fecha_notificacion' => $request->fecha_notificacion,
                'certificado' => $request->certificado,
                'fecha_aviso' => $request->fecha_aviso,
                'radicado_zurich' => $request->radicado_zurich,
                'observaciones' => $request->observaciones,
                'usuario_creador' => $request->user()->id
            ]);

            // if ($nuevo_reclamo->sponsor == "exito" || $nuevo_reclamo->sponsor == "ibg" || $nuevo_reclamo->sponsor == "soelco") {
                
            //     $sponsor = $nuevo_reclamo->sponsor;
            //     $cliente = Clientes::find($nuevo_reclamo->id_cliente);
            //     $vista_instructivo = '';
            //     if($nuevo_reclamo->tipo_siniestro == "hurto") {
            //         $vista_instructivo = 'mails/instructivos_reclamo/hurto';
            //     }elseif ($nuevo_reclamo->tipo_siniestro == "daño" || $nuevo_reclamo->tipo_siniestro == "garantia_extendida") {
            //         $vista_instructivo = 'mails/instructivos_reclamo/daño';
            //     }

            //     try{
            //         Mail::send($vista_instructivo, ['sponsor' => $sponsor], function ($m) use ($sponsor, $cliente, $nuevo_reclamo) 
            //         {
            //             $m->attach(storage_path().'/app/public/crawford/formato_reclamacion_zurich/formato_reclamacion.pdf');
            //             if ($sponsor == "exito") {
            //                 $m->from('joel@digit4l.co', 'Garantía extendida Éxito');
            //             }elseif ($sponsor == "ibg") {
            //                 $m->from('joel@digit4l.co', 'Garantía extendida IBG');
            //             }elseif ($sponsor == "soelco") {
            //                 $m->from('joel@digit4l.co', 'Garantía extendida Soelco');
            //             }
            //             $m->to('joelalejandro77757@gmil.com', $cliente->nombre )->subject('Envío instructivo reclamo #'.$nuevo_reclamo->id);
            //         });
            //     }
            //     catch(Exception $e){
            //         return response()->json(['error'=>$e], 500);
            //     }

            // }

            // if ($request->tipo_siniestro == 'hurto') {
            //     $registrar_checkbox = ZurichDocumentosReclamos::create([
            //         'id_reclamo' => $list->id,
            //         'id_tipo_checklist' => 7,
            //         'valor' => 'false'
            //     ]);
            // }

            return response()->json(['id_reclamo'=>$nuevo_reclamo->id]);

    }

    /**
	*  respuesta Actualizacion de Reclamos
	*
	*  POST url
	*  @var id
	*  @var reclamoid
	*  @var cedula
	*  @var nombreasegurado
	*  @var apellidoasegurado
	*  @var fechanacimientoasse
	*  @var ciudadrecidencia
	*  @var telefonofijo
	*  @var celular
	*  @var direccion
	*  @var email
	*  @var telefonolaboral
	*  @var jsonrespuestaform
	*  @var fechasiniestro
	*  @var horasiniestro
	*  @var descripcionsiniestro
	*  @var textobackend
	*  @var observaciones
	*  @var motivobaja
	*  @var cliente
	*  @var estados_poliza
	*  @var producto
	*  @var ciudadsiniestro
	*/
    public function update(){

        $estado = Estados_polizas::where('homologo','like',request('estados_poliza'))->get()[0];

        
    	$Reclamos = Reclamos::find(request('id'));
        $Reclamos2 = Reclamos::find(request('id'));
        $Reclamos->reclamoid = request('reclamoid');
		$Reclamos->cedula = request('cedula');
		$Reclamos->nombreasegurado = request('nombreasegurado');
		$Reclamos->apellidoasegurado = request('apellidoasegurado');
		$Reclamos->fechanacimientoasse = request('fechanacimientoasse');
		$Reclamos->ciudadrecidencia = request('ciudadrecidencia');
		$Reclamos->telefonofijo = request('telefonofijo');
		$Reclamos->celular = request('celular');
		$Reclamos->direccion = request('direccion');
		$Reclamos->email = request('email');
		$Reclamos->telefonolaboral = request('telefonolaboral');
        $Reclamos->producto_reclamo = request('producto_reclamo');
        $Reclamos->marca = request('marca');
        $Reclamos->imei = request('imei');
		$Reclamos->fechasiniestro = request('fechasiniestro');
		$Reclamos->horasiniestro = request('horasiniestro');
		$Reclamos->descripcionsiniestro = request('descripcionsiniestro');
		$Reclamos->textobackend = request('textobackend');
		$Reclamos->observaciones = request('observaciones');
		$Reclamos->motivobaja = request('motivobaja');
		$Reclamos->cliente = request('cliente');
		$Reclamos->estados_poliza = $estado->id;
		$Reclamos->producto = request('producto');
		$Reclamos->ciudadsiniestro = request('ciudadsiniestro');
        $Reclamos->save();

        //reemplazar por el array completo de estados
        $Reclamos->estados_poliza = Estados_polizas::find($Reclamos->estados_poliza);
        $Reclamos2->estados_poliza = Estados_polizas::find($Reclamos2->estados_poliza);

        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Reclamos Actualizada id:'.$Reclamos->id,
            'body' => $Reclamos
        ]);

        $texto = '';

        if($Reclamos2->observaciones != request('observaciones')){
            //save to observaciones historial
            $obj = Observaciones::create();
            $obj->observacion = request('observaciones');
            $obj->reclamo = request('id');
            $obj->save();
        }

        if($Reclamos2->estados_poliza->id != $Reclamos->estados_poliza->id){
            $texto .= '
            Estado anterior: '.$Reclamos2->estados_poliza->descripcion.'
            Estado posterior: '.$Reclamos->estados_poliza->descripcion;
        }

        if($texto!=''){
            //send push onesignal
            $clientId = $Reclamos2->cliente;
            $message = 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos2->reclamoid;
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }
        
            //mail send
            $client = Clientes::find($Reclamos->cliente);
            $fileName = '';
            if ($client->marca == 4) {
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Se presenta en la siguiente tabla los datos de la reclamación antes y despues de los cambios: '.$texto
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });    
            }elseif ($client->marca == 3) {
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Se presenta en la siguiente tabla los datos de la reclamación antes y despues de los cambios: '.$texto
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });
            }elseif ($client->marca == 1) {
                Mail::send('mails/mail_crawford', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Se presenta en la siguiente tabla los datos de la reclamación antes y despues de los cambios: '.$texto
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Se presenta en la siguiente tabla los datos de la reclamación antes y despues de los cambios: '.$texto
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });
            }
            
            
        }

        return $response;
    }

    /**
	*  respuesta Eliminar Reclamos
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Reclamos = Reclamos::find($id);
        $Reclamos->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Reclamos Eliminada id:'.$Reclamos->id,
                'body' => $Reclamos
            ]);
    }

    /**
	*  view index Reclamos
	*
	*  GET url 
	*/
    public function index(){
    	$list = Reclamos::all();
    	$ciudades = Ciudades::all();
    	$productos = Productos::all();
    	$estados_polizas = Estados_polizas::all();
    	$clientes = Clientes::all();
    	foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
    		$reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
    		$reclamo->producto = Productos::find($reclamo->producto);
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
    		$reclamo->cliente = Clientes::find($reclamo->cliente);
    	}
    	return view('reclamos', array(
    		'reclamos' => $list ,
    		'ciudades' => $ciudades,
    		'estados_polizas' => $estados_polizas,
    		'clientes' => $clientes,
            'productos' => $productos
    	));
    }

    /**
	*  view Filtros de  Reclamos
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Reclamos::all();
    	$ciudades = Ciudades::all();
    	$productos = Productos::all();
    	$estados_polizas = Estados_polizas::all();
    	$clientes = Clientes::all();
    	$list = Reclamos::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
    		$reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
    		$reclamo->producto = Productos::find($reclamo->producto);
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
    		$reclamo->cliente = Clientes::find($reclamo->cliente);
    	}
    	return view('reclamos', array(
    		'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'clientes' => $clientes,
            'productos' => $productos
    	));
    }

    /**
	*  view EXCEL Reclamos
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Reclamos::all();
		return Excel::create('Reclamos', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }


    /**
    *  respuesta una de las Reclamos
    *
    *  GET url
    *  @var id identificador de la reclamos en la base de datos
    */
    public function getcliente($cliente, Request $request){
        $consulta_token = Clientes::where('email',$request['email_cliente'])->where('marca',$request['marca_cliente'])->get();

        if ($consulta_token[0]->tokenAPI == $request['token_']) {

            $list = Reclamos::where('cliente',$cliente)->get();
            foreach ($list as $reclamo) {
                $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
                $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
                $reclamo->producto = Productos::find($reclamo->producto);
                $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
                $reclamo->cliente = Clientes::find($reclamo->cliente);
            }
            return response()->json([
                    'Status' => 'successful',
                    'Message' => 'Se encontraron coincidencias',
                    'body' => $list
            ]);
            
        }else{
            return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION';
        }
    }

    public function getcliente_app($cliente){
        $list = Reclamos::where('cliente',$cliente)->get();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            // $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    public function updateToEnEstudio(Request $request){

        $consulta_token = Clientes::where('email',$request['email_cliente'])->where('marca',$request['marca_cliente'])->get();

        if ($consulta_token[0]->tokenAPI == $request['token_']) {

            $Reclamos = Reclamos::find(request('reclamo'));
            $Reclamos->estados_poliza = 3;
            $Reclamos->save();


            //send push onesignal
            $clientId = $Reclamos->cliente;
            $message = 'Reclamo en Estudio #'.$Reclamos->reclamoid;
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }
        
            //mail send
            $client = Clientes::find($Reclamos->cliente);
            $fileName = '';

            if (request('marca_cliente') == 4) {
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Seguros Mundial ha recibido satisfactoriamente los adjuntos que necesita para dar inicio a la reclamación. Tu caso se encuentra en ESTUDIO'
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });   
            }elseif(request('marca_cliente') == 3){
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Seguros Mundial ha recibido satisfactoriamente los adjuntos que necesita para dar inicio a la reclamación. Tu caso se encuentra en ESTUDIO'
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });
            }else{
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Seguros Mundial a cambiado el estado del reclamo #'.$Reclamos->reclamoid.': ',
                        'mensaje'=> 'Seguros Mundial ha recibido satisfactoriamente los adjuntos que necesita para dar inicio a la reclamación. Tu caso se encuentra en ESTUDIO'
                ], 
                function ($m) use ($client, $fileName,$Reclamos) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/docnotifications/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Cambio reclamo #'.$Reclamos->reclamoid.' Seguros Mundial!');
                });
            }

            return response()->json([
                    'Status' => 'successful',
                    'Message' => 'Reclamos Eliminada id:'.$Reclamos->id,
                    'body' => $Reclamos
                ]);

        }else{
            return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION, SE HA LLAMADO A LA CIA PARA LOCALIZARLO';
        }

    }

    public function downloadFile($file){
      $pathtoFile = public_path().'/doc_reclamos/'.$file;
      return response()->download($pathtoFile);
    }

    public function listarReclamosSamsung( $imei ){
        
        $reclamo = Reclamos::where('imei', $imei)->get();
        $reclamo = $reclamo->map(function($item, $key){
            $estado_poliza = $item->estados_poliza;
            $item->descripcion_estado_poliza = Estados_polizas::find($estado_poliza)->nombre;
            
            return $item;
        });
        return response()->json([
            'data' => $reclamo
        ]);
    }

    public function verificar_factura($factura){
        $busqueda_reclamos = ZurichReclamos::with('datos_cliente','tipo_producto_reclamo','marca_producto_reclamo')
                            ->select('id_cliente','tipo_producto','marca_equipo','modelo_equipo','certificado','tipo_siniestro')
                            ->where('numero_factura',$factura)
                            ->get();
        $cantidad_reclamos = $busqueda_reclamos->count();
        return response()->json(['reclamos_por_factura' => $busqueda_reclamos, 'cantidad_reclamos' => $cantidad_reclamos],200);
    }

    public function prueba_request_compratido(Request $request){
        return $this->uso_request($request);
    }
    public function uso_request($request){
        return 'mensaje';
    }

}
