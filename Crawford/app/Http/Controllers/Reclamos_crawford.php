<?php

namespace App\Http\Controllers;

use App\Aseguradoras;
use App\Ciudades;
use App\Ciudades_departamentos;
use App\Clientes;
use App\Documents;
use App\Estados_polizas;
use App\Homologos;
use App\ListaCausales;
use App\ListaMarcas;
use App\ListaProductos;
use App\ListaSponsors;
use App\ListaTiendas;
use App\Listchekings;
use App\Observaciones;
use App\Productos;
use App\Reclamos;
use App\SegurosMundialReclamos;
use App\Tiendas;
use App\Tipo_polizas;
use App\Tipo_seguros;
use App\ZurichCiudadDepartamentos;
use App\ZurichDepartamentos;
use App\ZurichDocumentosReclamos;
use App\ZurichEstadosReclamos;
use App\ZurichHistorialEstados;
use App\ZurichNotificaciones;
use App\ZurichReclamos;
use App\ZurichSiniestroTipoChecklist;
use App\ZurichTipoChecklist;
use App\marcas;
use App\notificaciones;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;

class Reclamos_crawford extends Controller
{


    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    //
    public function dashboard(){
    	return view('crawford.dashboard');
    }

    public function viewReclamos(){
        $list = Reclamos::with('datos_cliente','datos_estado_poliza')
                ->select('reclamos.*')
                ->join('clientes','reclamos.cliente','=','clientes.id')
                ->where('clientes.marca','=',1)
                ->paginate(20);
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return view('crawford.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas,
            'filtro_marca' => '',
            'id_reclamo' => '',
            'cedula' => '',
            'nombre_asegurado' => '',
            'apellido_asegurado' => '',
            'email_asegurado' => ''
        ));
    }

    public function ciudad($ciudad){
        $list = Reclamos::where('ciudadrecidencia','like',$ciudad)
                        ->where('ciudadsiniestro','like',$ciudad)
                        ->get();
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return view('crawford.ciudades', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function cliente($cliente){
        $list = Reclamos::where('cliente','like',$cliente)
                        ->get();
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades_departamentos::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades_departamentos::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return view('crawford.cliente', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function estado($estado){
        $list = Reclamos::where('estados_poliza','like',$estado)
                        ->get();
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::select('homologo')->where('id',$reclamo->estados_poliza)->get();
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return view('crawford.estado', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function filtro_marca($marca){
        //traer todos los lcientes de la marca que han reclamado
        $reclamos = Reclamos::select('reclamos.*')->join('clientes','reclamos.cliente','=','clientes.id')->where('clientes.marca','=',$marca)->paginate(20);
        if ($marca == 5) {
            $reclamos = ZurichReclamos::select('zurich_reclamos.*')->join('clientes','zurich_reclamos.id_cliente','=','clientes.id')->where('clientes.marca','=',$marca)->paginate(20);
        }
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($reclamos as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
            if ($marca == 5) {
                $reclamo->estados_poliza = ZurichEstadosReclamos::find($reclamo->estado_reclamo);
                $reclamo->cliente = Clientes::find($reclamo->id_cliente);
            }
            //$reclamo->jsonrespuestaform = str_replace('"', "'", $reclamo->jsonrespuestaform);
        }
        return view('crawford.reclamos', array(
            'reclamos' => $reclamos ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ))->with('filtro_marca','Marca: '.marcas::find($marca)->nombre);
    }

    public function idreclamo($id){
        $list = Reclamos::find($id);
        $documents = Documents::where('reclamo',$id)->get();
        $notificaciones = notificaciones::where('reclamo',$id)->get();
        $homologos = Homologos::where('id_marca',$list->datos_cliente->marca)->get();

        $observaciones = Observaciones::where('reclamo','like',$id)->orderBy('id', 'desc')->get();
    
        $producto = Productos::find($list->producto) ;
        $tipo_polizas = $producto->tipo_poliza;

        $listchekings = Listchekings::where('tipo_polizas','like',$tipo_polizas)->get();
        $arrayTipo_polizas = Tipo_polizas::where('id','like',$tipo_polizas)->get();

        $list->ciudadrecidencia = Ciudades_departamentos::find($list->ciudadrecidencia);
        $list->ciudadsiniestro = Ciudades_departamentos::find($list->ciudadsiniestro);
        $list->producto = Productos::find($list->producto);
        $list->estados_poliza = Estados_polizas::find($list->estados_poliza);
        $list->cliente = Clientes::find($list->cliente);

        foreach ($documents as $doc) {
            $doc->url = Storage::url($doc->url);
        }

        return view('crawford.idreclamos', array(
            'reclamos' => $list,
            'documents' => $documents,
            'notificaciones' => $notificaciones,
            'homologos' => $homologos,
            'listchekings' => $listchekings,
            'arrayTipo_polizas' => $arrayTipo_polizas,
            'observaciones' => $observaciones
        ));
    }

    public function idreclamo_zurich($id){

        $reclamo = ZurichReclamos::find($id);
        $notificaciones = ZurichNotificaciones::where('id_reclamo',$id)->get();
        $estados_reclamo = ZurichEstadosReclamos::all();
        $observaciones = Observaciones::where('reclamo',$id)->orderBy('id', 'desc')->get();
        $tiendas = ListaTiendas::all();
        $historial_estados = $reclamo->historial_estados;
        foreach ($historial_estados as $historial) {
            $historial->informacion_estado;
        }

        $pivot_tipo_checklist = ZurichSiniestroTipoChecklist::
              where('id_marca',$reclamo->datos_cliente->marca)
            ->where('id_aseguradora',$reclamo->datos_cliente->aseguradora)
            ->where('tipo_siniestro',$reclamo->tipo_siniestro)
            ->get()->pluck('id_tipo_checklist');

        $checklists = ZurichTipoChecklist::whereIn('id',$pivot_tipo_checklist)->get();
        foreach ($checklists as $check) {
            $check->documentos_checklist = $check->documentos()->where('id_reclamo', $id)->get();
            $check->cantidad_adjuntos = $check->documentos()->where('id_reclamo', $id)->count();
        }

        $documentos_adicionales = $reclamo->documentos_reclamo()->where('id_tipo_checklist', 8)->get();

        $causales = ListaCausales::all();
        $sponsors = ListaSponsors::all();
        foreach ($sponsors as $sponsor) {
            $sponsor->nombre_trans = strtoupper(str_replace("_", " ", $sponsor->nombre_sponsor));
        }
        $tipos_producto = ListaProductos::all();
        $marcas_producto = ListaMarcas::all();
        $ciudades_departamento = ZurichCiudadDepartamentos::where('idDepartamento',$reclamo->ciudad_siniestro->idDepartamento)->get();
        $ciudades_departamento_residencia = ZurichCiudadDepartamentos::where('idDepartamento', $reclamo->ciudad_residencia->idDepartamento)->get();
        $departamentos = ZurichDepartamentos::all();

        return view('crawford.idreclamos_zurich', array(
            'reclamos' => $reclamo,
            'notificaciones' => $notificaciones,
            'estados_reclamo' => $estados_reclamo,
            'observaciones' => $observaciones,
            'tiendas' => $tiendas,
            'historial_estados' => $historial_estados,
            'checklists' => $checklists,
            'documentos_adicionales' => $documentos_adicionales,
            'causales' => $causales,
            'tipos_producto' => $tipos_producto,
            'marcas_producto' => $marcas_producto,
            'ciudades_departamento' => $ciudades_departamento,
            'ciudades_departamento_residencia' => $ciudades_departamento_residencia,
            'departamentos' => $departamentos,
            'sponsors' => $sponsors
        ));
    }

    public function actualizar_informacion_reclamo_zurich(Request $request){
        $reclamo_zurich = ZurichReclamos::findOrFail($request->id_reclamo);

        if ($request->has('fecha_aviso')) {
            if ($request->fecha_aviso < $reclamo_zurich->fechasiniestro) {
                return response()->json(['error' => 'la fecha de aviso debe ser mayor a la fecha de siniestro.'], 422);
            }
        }

        if ($request->has('fecha_recepcion_documentos') && $request->has('fecha_aviso')) {
            if ($request->fecha_aviso > $request->fecha_recepcion_documentos) {
                return response()->json(['error' => 'la fecha de aviso debe ser menor a la fecha de recepcion de documentos.'], 422);
            }
        }

        if ($request->has('fecha_recep_doc_completos') && $request->has('fecha_aviso')) {
            if ($request->fecha_aviso > $request->fecha_recep_doc_completos) {
                return response()->json(['error' => 'la fecha de aviso debe ser menor a la fecha de recepcion de documentos completos.'], 422);
            }
        }

        if ($request->has('fecha_asignacion_cst') && $request->has('fecha_aviso')) {
            if ($request->fecha_aviso > $request->fecha_asignacion_cst) {
                return response()->json(['error' => 'la fecha de aviso debe ser menor a la fecha de asignación a CST.'], 422);
            }
        }

        if ($request->has('fecha_asignacion_cst') && $request->has('fecha_ingreso_cst')) {
            if ($request->fecha_asignacion_cst > $request->fecha_ingreso_cst) {
                return response()->json(['error' => 'la fecha de asignacion debe ser menor a la fecha de ingreso a CST.'], 422);
            }
        }

        if ($request->has('fecha_diagnostico') && $request->has('fecha_ingreso_cst')) {
            if ($request->fecha_diagnostico < $request->fecha_ingreso_cst) {
                return response()->json(['error'=>'la fecha de diagnostico debe ser mayor a la fecha de ingreso a CST.'],422);
            }
        }

        if ($request->has('fecha_entrega_ajuste') && $request->has('fecha_aviso')) {
            if ($request->fecha_entrega_ajuste < $request->fecha_aviso) {
                return response()->json(['error'=>'la fecha de entrega ajuste debe ser mayor a la fecha de aviso.'],422);
            }
        }

        if ($request->has('fecha_ajuste') && $request->has('fecha_recepcion_documentos')) {
            if ($request->fecha_ajuste < $request->fecha_recepcion_documentos) {
                return response()->json(['error'=>'la fecha de ajuste debe ser mayor a la fecha de recepcion de documentos.'],422);
            }
        }

        if ($request->has('fecha_siniestro') && $request->has('fecha_aviso')) {
            if ($request->fecha_siniestro > $request->fecha_aviso) {
                return response()->json(['error'=>'la fecha de siniestro debe ser menor a la fecha de aviso.'],422);   
            }
        }

        $cliente = Clientes::find($request->id_cliente);
        $cliente->nombre = $request->nombre_cliente;
        $cliente->lastname = $request->apellido_cliente;
        $cliente->cedula = $request->cedula_cliente;
        $cliente->email = $request->email_cliente;
        $cliente->save();

        $reclamo_zurich->ciudadrecidencia = $request->ciudad_residencia;
        $reclamo_zurich->celular = $request->celular_cliente;
        $reclamo_zurich->telefonofijo = $request->telefono_fijo;
        $reclamo_zurich->telefonolaboral = $request->telefono_laboral;
        $reclamo_zurich->direccion = $request->direccion_cliente;
        $reclamo_zurich->imei = $request->imei_producto;
        $reclamo_zurich->tipo_producto = $request->tipo_producto;
        $reclamo_zurich->marca_equipo = $request->marca_producto;
        $reclamo_zurich->modelo_equipo = $request->modelo_equipo;
        $precio_equipo = str_replace(",", "", $request->precio_equipo);
        $precio_equipo = str_replace(".00", "", $precio_equipo);
        $precio_equipo = str_replace("$", "", $precio_equipo);
        $reclamo_zurich->precio_equipo = $precio_equipo;
        $reclamo_zurich->fecha_compra = $request->fecha_compra;
        $reclamo_zurich->sponsor = $request->sponsor;
        $reclamo_zurich->numero_factura = $request->numero_factura;
        $reclamo_zurich->tienda = $request->tienda;
        $reclamo_zurich->fecha_asignacion_cst = $request->fecha_asignacion_cst;
        $reclamo_zurich->nombre_cst = $request->nombre_cst;
        $reclamo_zurich->fecha_ingreso_cst = $request->fecha_ingreso_cst;
        $reclamo_zurich->fecha_diagnostico = $request->fecha_diagnostico;
        $reclamo_zurich->tiempo_reparacion = $request->tiempo_reparacion;
        $reclamo_zurich->causal = $request->causal;
        $valor_indemnizado = str_replace(",","",$request->valor_indemnizado);
        $valor_indemnizado = str_replace(".00","",$valor_indemnizado);
        $valor_indemnizado = str_replace("$","",$valor_indemnizado);
        $reclamo_zurich->valor_indemnizado = $valor_indemnizado;
        $reclamo_zurich->fecha_notificacion = $request->fecha_notificacion;
        $reclamo_zurich->certificado = $request->certificado;
        $reclamo_zurich->radicado_zurich = $request->radicado_zurich;

        $reclamo_zurich->caso_reapertura = $request->caso_reapertura;
        $reclamo_zurich->caso_investigado = $request->caso_investigado;
        $reclamo_zurich->salvamento = $request->salvamento;
        $valor_reparacion = str_replace(",", "", $request->valor_reparacion);
        $valor_reparacion = str_replace(".00", "", $valor_reparacion);
        $valor_reparacion = str_replace("$", "", $valor_reparacion);
        $reclamo_zurich->valor_reparacion = $valor_reparacion;

        $reclamo_zurich->fecha_envio_instructivo = $request->fecha_envio_instructivo;
        $reclamo_zurich->fecha_aviso = $request->fecha_aviso;
        $reclamo_zurich->fecha_recepcion_documentos = $request->fecha_recepcion_documentos;
        $reclamo_zurich->fecha_recep_doc_completos = $request->fecha_recep_doc_completos;
        $reclamo_zurich->hora_recepcion_documentos = $request->hora_recepcion_documentos;
        $reclamo_zurich->fecha_entrega_ajuste = $request->fecha_entrega_ajuste;
        $reclamo_zurich->fecha_ajuste = $request->fecha_ajuste;
        $reclamo_zurich->save();
    }

    public function actualizar_informacion_siniestro_zurich(Request $request){
        $reclamo_zurich = ZurichReclamos::findOrFail($request->id_reclamo);
        $reclamo_zurich->fechasiniestro = $request->fecha_siniestro;
        $reclamo_zurich->horasiniestro = $request->hora_siniestro;
        $reclamo_zurich->ciudadsiniestro = $request->ciudad_siniestro;
        $reclamo_zurich->tipo_siniestro = $request->tipo_siniestro;
        $reclamo_zurich->descripcionsiniestro = $request->descripcion_siniestro;
        $reclamo_zurich->save();
    }

    public function nueva_observacion_zurich(Request $request){
        $nueva_observación = Observaciones::create([
            'observacion' => $request->observacion,
            'reclamo' => $request->id_reclamo
        ]);
    }

    public function nuevo_estado_zurich(Request $request){
        $nuevo_historial_estado = ZurichHistorialEstados::create([
            'id_reclamo' => $request->id_reclamo,
            'id_estado' => $request->estado_nuevo,
            'id_user' => $request->user()->id
        ]);
        $reclamo = ZurichReclamos::findOrFail($request->id_reclamo);
        $reclamo->estado_reclamo = $request->estado_nuevo;
        $reclamo->save();
    }

    public function adjunto_checklist_zurich(Request $request){
        $nombre_adjunto = "adjunto_checklist_reclamo_".$request->id_reclamo."_tipo_".$request->id_checklist."_".uniqid().".".$request->adjunto->extension();
        $request->adjunto->storeAs('checklists_zurich',$nombre_adjunto);
        $registrar_adjunto = ZurichDocumentosReclamos::create([
            'id_reclamo' => $request->id_reclamo,
            'id_tipo_checklist' => $request->id_checklist,
            'nombre_documento' => $nombre_adjunto
        ]);
        return $registrar_adjunto;
    }

    public function adjunto_adicional_zurich(Request $request){
        $nombre_adjunto = "adjunto_adicional_reclamo_".$request->id_reclamo."_tipo_8_".uniqid().".".$request->adjunto_adicional->extension();
        $request->adjunto_adicional->storeAs('checklists_zurich',$nombre_adjunto);
        $registrar_adjunto = ZurichDocumentosReclamos::create([
            'id_reclamo' => $request->id_reclamo,
            'id_tipo_checklist' => 8,
            'nombre_documento' => $nombre_adjunto,
            'valor' => $request->descripcion_adicional
        ]);
        return $registrar_adjunto;
    }

    public function eliminar_documento_zurich($id_documento){
        $documento_zurich = ZurichDocumentosReclamos::findOrFail($id_documento);
        Storage::delete('checklists_zurich/'.$documento_zurich->nombre_documento);
        $documento_zurich->delete();
    }

    public function url_archivos_checklist_zurich($archivo){
        $path = storage_path() . '/app/public/crawford/checklists_zurich/' . $archivo;
        if(!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function checkbox_checklist_zurich(Request $request){
        $buscar_checkbox = ZurichDocumentosReclamos::where('id_reclamo', $request->id_reclamo)->where('id_tipo_checklist', $request->id_checklist)->first();
        $buscar_checkbox->valor = $request->valor;
        $buscar_checkbox->save();
    }

    public function clientes(){
        $clientes = Clientes::with('marca_cliente')->paginate(100);
        $marcas = marcas::all();
        if (Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','SOPORTE','CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH'])) {
            return view('crawford.clientes', array(
                'clientes' => $clientes,
                'marcas' => $marcas,
                'filtro_marca' => '',
                'id_cliente' => '',
                'cedula_cliente' => '',
                'celular_cliente' => '',
                'nombre_cliente' => '',
                'apellido_cliente' => '',
                'email_cliente' => ''
            ));
        }else{
            return view('crawford.error');
        }
        
    }

    public function cliente_search(Request $request){
        $clientes = Clientes::with('marca_cliente')
                    ->id($request->id_cliente)
                    ->nombre($request->nombre_cliente)
                    ->lastname($request->apellido_cliente)
                    ->email($request->email_cliente)
                    ->cedula($request->cedula_cliente)
                    ->celular($request->celular_cliente)
                    ->marca($request->filtro_marca)
                    ->paginate(15)
                    ->setpath('');
        
        $marcas = marcas::all();

        $clientes->appends(array(
            'filtro_marca' => $request->filtro_marca,
            'id_cliente' => $request->id_cliente,
            'cedula_cliente' => $request->cedula_cliente,
            'celular_cliente' => $request->celular_cliente,
            'nombre_cliente' => $request->nombre_cliente,
            'apellido_cliente' => $request->apellido_cliente,
            'email_cliente' => $request->email_cliente
        ));

        return view('crawford.clientes', array(
            'clientes' => $clientes,
            'marcas' => $marcas,
            'filtro_marca' => $request->filtro_marca,
            'id_cliente' => $request->id_cliente,
            'cedula_cliente' => $request->cedula_cliente,
            'celular_cliente' => $request->celular_cliente,
            'nombre_cliente' => $request->nombre_cliente,
            'apellido_cliente' => $request->apellido_cliente,
            'email_cliente' => $request->email_cliente
        ));
    }

    public function create_reclamo($cliente){
        $clientes = Clientes::where('id',$cliente)->get();
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        foreach ($clientes as $cliente) {
            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
        }
        return view('crawford.create_reclamo', array(
            'clientes' => $clientes,
            'ciudades' => $ciudades,
            'productos' => $productos,
            'estados_polizas' => $estados_polizas
        ));
    }


    public function search(Request $request){

        $estados_reclamo = '';
        if ($request->filtro_marca == 5) {
            $reclamos = ZurichReclamos::with('datos_cliente','estado_del_reclamo')
                ->marcacliente($request->filtro_marca)
                ->estadoreclamo($request->filtro_estado)
                ->nombrecliente($request->nombre_asegurado)
                ->apellidocliente($request->apellido_asegurado)
                ->emailcliente($request->email_asegurado)
                ->cedulacliente($request->cedula)
                ->id($request->id_reclamo)
                ->paginate(15)
                ->setpath('');
            $estados_reclamo = ZurichEstadosReclamos::all();
        }else if($request->filtro_marca == 3){
            $reclamos = SegurosMundialReclamos::with('datos_cliente','datos_estado_poliza')
                ->nombreasegurado($request->nombre_asegurado)
                ->apellidoasegurado($request->apellido_asegurado)
                ->email($request->email_asegurado)
                ->cedula($request->cedula)
                ->id($request->id_reclamo)
                ->paginate(15)
                ->setpath('');
        }
        else{
            $reclamos = Reclamos::with('datos_cliente','datos_estado_poliza')
                ->marcacliente($request->filtro_marca)
                ->nombreasegurado($request->nombre_asegurado)
                ->apellidoasegurado($request->apellido_asegurado)
                ->email($request->email_asegurado)
                ->cedula($request->cedula)
                ->id($request->id_reclamo)
                ->paginate(15)
                ->setpath('');
        }

        $reclamos->appends(array(
            'filtro_marca' => $request->filtro_marca,
            'id_reclamo' => $request->id_reclamo,
            'cedula' => $request->cedula,
            'nombre_asegurado' => $request->nombre_asegurado,
            'apellido_asegurado' => $request->apellido_asegurado,
            'email_asegurado' => $request->email_asegurado
        ));

        $marcas = marcas::all();
 
        return view('crawford.reclamos', array(
            'reclamos' => $reclamos,
            'marcas' => $marcas,
            'filtro_marca' => $request->filtro_marca,
            'filtro_estado' => $request->filtro_estado,
            'id_reclamo' => $request->id_reclamo,
            'cedula' => $request->cedula,
            'nombre_asegurado' => $request->nombre_asegurado,
            'apellido_asegurado' => $request->apellido_asegurado,
            'email_asegurado' => $request->email_asegurado,
            'estados_reclamo' => $estados_reclamo
        ));
    }


    public function ajustes(){
        if (Auth::user()->authorizeRoles(['SUPERADMINISTRADOR'])) {
            return view('crawford.ajustes');    
        }else{
            return view('crawford.error');
        }
        
    }

    public function filtro_campaña(Request $request){

        session_start();
        if (isset($_SESSION['id_marca'])) {
            unset($_SESSION['id_marca']);
            $_SESSION['id_marca']=$request['id'];
        }else{
            $_SESSION['id_marca']=$request['id'];
        }

        return $_SESSION['id_marca'];

    }

    public function view_database(){
        if (Auth::user()->authorizeRoles(['SUPERADMINISTRADOR'])) {
            return view('database');    
        }else{
            return view('crawford.error');
        }
    }

}
