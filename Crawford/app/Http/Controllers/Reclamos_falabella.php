<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Reclamos;
use App\Clientes;
use App\Estados_polizas;
use App\Productos;
use App\Ciudades;
use App\marcas;
use App\Documents;
use App\Listchekings;
use App\notificaciones;
use App\Homologos;
use App\Tipo_polizas;
use App\Tiendas;
use App\Tipo_seguros;
use App\Aseguradoras;
use App\Observaciones;
use App\Ciudades_departamentos;



class Reclamos_falabella extends Controller
{
    //
    public function dashboard(){
    	return view('falabella.dashboard');
    }

    public function viewReclamos(){

        $aseguradora = null;
        $aseguradoras = Aseguradoras::all(); //trayebdo las aseguradoras
        foreach ($aseguradoras as $value) {
            if($value->nombre == 'FALABELLA'){  //saber cual es el id de falabella
                $aseguradora = $value;
            }
        }
        $clientes = Clientes::where('aseguradora',$aseguradora->id)->get(); //traigo todos los clientes de la aseguradora falabella
        $array = array();
        foreach ($clientes as $value) {
            $list = Reclamos::where('cliente','like',$value->id)->get(); //busco cuales reclamos hahy por cliente
            if(sizeof($list) != 0){ //si la consulta NO esta vacia
                foreach ($list as $value) {
                    array_push($array, $value); //agregar al array  
                }                     
            }                
        }
    	$list = $array;
        //dd($list);
    	$ciudades = Ciudades::all();
    	$productos = Productos::all();
    	$estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
    	foreach ($list as $reclamo) {
    		$reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
    		$reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
    		$reclamo->producto = Productos::find($reclamo->producto);
    		$reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
    		$reclamo->cliente = Clientes::find($reclamo->cliente);
            //$reclamo->jsonrespuestaform = str_replace('"', "'", $reclamo->jsonrespuestaform);
    	}
    	return view('falabella.reclamos', array(
    		'reclamos' => $list ,
    		'ciudades' => $ciudades,
    		'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
    	));
    }

    public function ciudad($ciudad){
        $aseguradora = null;
        $aseguradoras = Aseguradoras::all(); //trayebdo las aseguradoras
        foreach ($aseguradoras as $value) {
            if($value->nombre == 'FALABELLA'){  //saber cual es el id de falabella
                $aseguradora = $value;
            }
        }
        $clientes = Clientes::where('aseguradora',$aseguradora->id)->get(); //traigo todos los clientes de la aseguradora falabella
        $array = array();
        foreach ($clientes as $value) {
            $list = Reclamos::where('cliente','like',$value->id)->get(); //busco cuales reclamos hahy por cliente
            if(sizeof($list) != 0){ //si la consulta NO esta vacia
                foreach ($list as $value) {
                    array_push($array, $value); //agregar al array  
                }                     
            }                
        }
        $list = $array;
        //dd($list);
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
            //$reclamo->jsonrespuestaform = str_replace('"', "'", $reclamo->jsonrespuestaform);
        }
        return view('falabella.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function cliente($cliente){
        $aseguradora = null;
        $aseguradoras = Aseguradoras::all(); //trayebdo las aseguradoras
        foreach ($aseguradoras as $value) {
            if($value->nombre == 'FALABELLA'){  //saber cual es el id de falabella
                $aseguradora = $value;
            }
        }
        $clientes = Clientes::where('aseguradora',$aseguradora->id)->get(); //traigo todos los clientes de la aseguradora falabella
        $array = array();
        foreach ($clientes as $value) {
            $list = Reclamos::where('cliente','like',$value->id)->get(); //busco cuales reclamos hahy por cliente
            if(sizeof($list) != 0){ //si la consulta NO esta vacia
                foreach ($list as $value) {
                    array_push($array, $value); //agregar al array  
                }                     
            }                
        }
        $list = $array;
        //dd($list);
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
            //$reclamo->jsonrespuestaform = str_replace('"', "'", $reclamo->jsonrespuestaform);
        }
        return view('falabella.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function estado($estado){
        $aseguradora = null;
        $aseguradoras = Aseguradoras::all(); //trayebdo las aseguradoras
        foreach ($aseguradoras as $value) {
            if($value->nombre == 'FALABELLA'){  //saber cual es el id de falabella
                $aseguradora = $value;
            }
        }
        $clientes = Clientes::where('aseguradora',$aseguradora->id)->get(); //traigo todos los clientes de la aseguradora falabella
        $array = array();
        foreach ($clientes as $value) {
            $list = Reclamos::where('cliente','like',$value->id)->get(); //busco cuales reclamos hahy por cliente
            if(sizeof($list) != 0){ //si la consulta NO esta vacia
                foreach ($list as $value) {
                    array_push($array, $value); //agregar al array  
                }                     
            }                
        }
        $list = $array;
        //dd($list);
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
            //$reclamo->jsonrespuestaform = str_replace('"', "'", $reclamo->jsonrespuestaform);
        }
        return view('falabella.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function filtro_marca($marca){
        //traer todos los lcientes de la marca que han reclamado
        $list = array();
        $clientes = Clientes::where('marca','like',$marca)->get();
        foreach ($clientes as $key => $cliente) {
            array_push($list, Reclamos::where('cliente','like',$cliente->id)->get());
        }

        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();        
        $marcas = marcas::all();

        dd($list);

        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }

        return view('falabella.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'clientes' => $clientes,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }

    public function idreclamo($id){
        $list = Reclamos::where('id','=',$id)->get();
        $documents = Documents::where('reclamo',$id)->get();
        $notificaciones = notificaciones::where('reclamo',$id)->get();
        $homologos = Homologos::all();

        $observaciones = Observaciones::where('reclamo','like',$id)->orderBy('id', 'desc')->get();
    
        $producto = Productos::find($list[0]->producto) ;
        $tipo_polizas = $producto->tipo_poliza;

        $listchekings = Listchekings::where('tipo_polizas','like',$tipo_polizas)->get();
        $arrayTipo_polizas = Tipo_polizas::where('id','like',$tipo_polizas)->get();
        $arrayTiendas = Tiendas::where('id','like',$arrayTipo_polizas[0]->tienda)->get();

        foreach ($notificaciones as $notificacion) {
            $notificacion->marca = Marcas::find($notificacion->marca);
            $notificacion->reclamo = Reclamos::find($notificacion->reclamo);
            $notificacion->cliente = Clientes::find($notificacion->cliente);
        }

        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades_departamentos::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades_departamentos::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }

        foreach ($documents as $doc) {
            $doc->url = Storage::url($doc->url);
        }

        return view('falabella.idreclamos', array(
            'reclamos' => $list,
            'documents' => $documents,
            'notificaciones' => $notificaciones,
            'homologos' => $homologos,
            'listchekings' => $listchekings,
            'arrayTipo_polizas' => $arrayTipo_polizas,
            'arrayTiendas' => $arrayTiendas,
            'observaciones' => $observaciones
        ));
    }

    public function clientes(){ //ok
        $aseguradora = null;
        $aseguradoras = Aseguradoras::all();
        foreach ($aseguradoras as $value) {
            if($value->nombre == 'FALABELLA'){
                $aseguradora = $value;
            }
        }
        $clientes = Clientes::where('aseguradora',$aseguradora->id)->get();
        foreach ($clientes as $cliente) {
            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
        }
        return view('falabella.clientes', array(
            'clientes' => $clientes
        ));
    }

    public function cliente_search($search){
        if($search=='ALL'){
            $clientes = Clientes::all();
        }
        else{
            $clientes = Clientes::where('nombre','like','%'.$search.'%')
                                ->orWhere('lastname','like','%'.$search.'%')
                                ->orWhere('email','like','%'.$search.'%')
                                ->orWhere('cedula','like','%'.$search.'%')
                                ->orWhere('celular','like','%'.$search.'%')
                                ->orWhere('marca','like','%'.$search.'%')
                                ->orWhere('aseguradora','like','%'.$search.'%')
                                ->get();
            }
        
        //dd($clientes);
        foreach ($clientes as $cliente) {
            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
        }
        return view('falabella.clientes', array(
            'clientes' => $clientes
        ));
    }

    public function create_reclamo($cliente){
        $clientes = Clientes::where('id',$cliente)->get();
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        foreach ($clientes as $cliente) {
            $cliente->marca = marcas::find($cliente->marca);
            $cliente->tipo_seguro = Tipo_seguros::find($cliente->tipo_seguro);
            $cliente->aseguradora = Aseguradoras::find($cliente->aseguradora);
        }
        return view('falabella.create_reclamo', array(
            'clientes' => $clientes,
            'ciudades' => $ciudades,
            'productos' => $productos,
            'estados_polizas' => $estados_polizas
        ));
    }







    public function search($search){
         if($search=='ALL'){
            $list = Reclamos::all();
        }
        else{
            $list = Reclamos::where('nombreasegurado','like','%'.$search.'%')
                            ->orWhere('apellidoasegurado','like','%'.$search.'%')
                            ->orWhere('telefonofijo','like','%'.$search.'%')
                            ->orWhere('celular','like','%'.$search.'%')
                            ->orWhere('direccion','like','%'.$search.'%')
                            ->orWhere('email','like','%'.$search.'%')
                            ->orWhere('telefonolaboral','like','%'.$search.'%')
                            ->orWhere('telefonofijo','like','%'.$search.'%')
                            ->orWhere('direccion','like','%'.$search.'%')
                            ->orWhere('descripcionsiniestro','like','%'.$search.'%')
                            ->orWhere('reclamoid','like','%'.$search.'%')->get();
                            // ->orWhere('jsonrespuestaform','like','%'.$search.'%')
        }
        $ciudades = Ciudades::all();
        $productos = Productos::all();
        $estados_polizas = Estados_polizas::all();
        $marcas = marcas::all();
        foreach ($list as $reclamo) {
            $reclamo->ciudadrecidencia = Ciudades::find($reclamo->ciudadrecidencia);
            $reclamo->ciudadsiniestro = Ciudades::find($reclamo->ciudadsiniestro);
            $reclamo->producto = Productos::find($reclamo->producto);
            $reclamo->estados_poliza = Estados_polizas::find($reclamo->estados_poliza);
            $reclamo->cliente = Clientes::find($reclamo->cliente);
        }
        return view('falabella.reclamos', array(
            'reclamos' => $list ,
            'ciudades' => $ciudades,
            'estados_polizas' => $estados_polizas,
            'productos' => $productos,
            'marcas' => $marcas
        ));
    }


    public function ajustes(){
        return view('falabella.ajustes');
    }

}
