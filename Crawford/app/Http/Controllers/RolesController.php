<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Roles;
use App\marcas;

class RolesController extends Controller
{
    //'nombre', 'menu', 'submenu', 'marca'
     /**
	*  respuesta todos las Roles
	*
	*  GET url
	*/
    public function all(){
    	$list = Roles::all();
    	foreach ($list as $roles) {
    		$roles->marca = marcas::find($roles->marca);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Roles
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Roles::where('id',$id)->get();
    	foreach ($list as $roles) {
    		$roles->marca = marcas::find($roles->marca);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Roles
	*  
	*  POST url
	*  @var nombre
	*  @var menu
	*  @var submenu
	*  @var marca
	*/
    public function save(){
    	$list = Roles::create(request(['nombre', 'menu', 'submenu', 'marca']));
        return response()->json([
            'Status' => 'successful',
            'Message' => 'roles guardada id:'.$list->id,
            'body' => $list
        ]);
	}

    /**
	*  respuesta Actualizacion de Roles
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var menu
	*  @var submenu
	*  @var marca
	*/
    public function update(){
    	$roles = Roles::find(request('id'));
		$roles->nombre		= request('nombre');
		$roles->menu		= request('menu');
		$roles->submenu		= request('submenu');
		$roles->marca		= request('marca');			
        $roles->save();
        return response()->json([
            'Status' => 'successful',
            'Message' => 'roles Actualizada id:'.$roles->id,
            'body' => $roles
        ]);
    }

    /**
	*  respuesta Eliminar Roles
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$roles = Roles::find($id);
        $roles->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'roles Eliminada id:'.$roles->id,
                'body' => $roles
            ]);
    }

    /**
	*  view index Roles
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Roles::all();
    	$marcas 		= marcas::all();
    	foreach ($list as $roles) {
    		$roles->marca = marcas::find($roles->marca);
    	}
    	return view('roles', array(
    		'roles' => $list,
            'marcas' => $marcas
    	));
    }


    /**
	*  view Filtros de  Roles
	*
	*  GET url 
	*/
    public function filtros($filtro){
        $marcas         = marcas::all();
    	$list = Roles::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $roles) {
			$roles->marca = marcas::find($roles->marca);
    	}
    	return view('roles', array(
    		'roles' => $list,
            'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Roles
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Roles::all();
		return Excel::create('Roles', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
