<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Solicitudes;
use App\Clientes;
use App\Devices;
use OneSignal;
use Mail;

class SolicitudesController extends Controller
{
    //'mensaje','telefono','franja','diasemana','estado','cliente'
    /**
	*  respuesta todos las Solicitudes
	*
	*  GET url
	*/
    public function all(){
    	$list = Solicitudes::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Solicitudes
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Solicitudes::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var mensaje
	*  @var telefono
	*  @var franja
	*  @var diasemana
	*  @var estado
	*  @var cliente
	*/
    public function save(Request $request){

        // $consulta_token = Clientes::where('email',$request['email_cliente'])->where('marca',$request['marca_cliente'])->get();

        $consulta_token = Clientes::find($request->cliente);

        if ($consulta_token->tokenAPI == $request['token_']) {

            //enviar correo a administracion

            $list = Solicitudes::create([
                'mensaje' => $request['mensaje'],
                'telefono' => $request['telefono'],
                'franja' => $request['franja'],
                'diasemana' => $request['diasemana'],
                'cliente' => $request['cliente']
            ]);
            
            $client = Clientes::find($list->cliente); //buscar al cliente informacion

            //envio de notificacion one signal
            try{
                $clientId = $list->cliente;
                $message = 'Se envio la solicitud a la plataforma administrativa. Nosotros avisaremos primordialmente por este medio, gracias.';
                $devices = Devices::where('clientid', $clientId)->get();
                foreach($devices as $device) {
                    OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
                }
            }
            catch(Exception $e){
                return response()->json([
                    'Status' => 'false',
                    'Message' => 'Error: '.$e,
                    'body' => ''
                ]);
            }
            
            //envio de mail
            $fileName = '';
            try{

                if ($client->marca == 4) { /*-- Mensaje Samsung --*/
                    Mail::send('mails/mail_samsung', 
                    [
                            'titulo' => 'Mensaje de '.$client->email.' desde la App',
                            'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                    });
                }elseif($client->marca == 3) { /*-- Mensaje Seguros mundial --*/
                    Mail::send('mails/mail_seguros', 
                    [
                            'titulo' => 'Mensaje de '.$client->email.' desde la App',
                            'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                    });
                }elseif($client->marca == 2) {
                    if ($client->aseguradora == 6) {
                        Mail::send('mails/mail_chubb',
                        [
                                'titulo' => 'Mensaje de '.$client->email.' desde la App',
                                'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                        ], 
                        function ($m) use ($client, $fileName) 
                        {
                            if($fileName != "")
                                $m->attach(public_path().'/'.$fileName);
                            $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                            $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                        });    
                    }else{
                        Mail::send('mails/mail', 
                        [
                                'titulo' => 'Mensaje de '.$client->email.' desde la App',
                                'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                        ], 
                        function ($m) use ($client, $fileName) 
                        {
                            if($fileName != "")
                                $m->attach(public_path().'/'.$fileName);
                            $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                            $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                        });
                    }
                }else{ /*-- Mensaje Falabella --*/
                    Mail::send('mails/mail', 
                    [
                            'titulo' => 'Mensaje de '.$client->email.' desde la App',
                            'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                    });
                }
                
                $response = response()->json([
                    'Status' => 'successful',
                    'Message' => 'Solicitudes guardada id:'.$list->id,
                    'body' => $list
                ]);

            }
            catch(Exception $e){
                return response()->json([
                    'Status' => 'false',
                    'Message' => 'Error: '.$e,
                    'body' => ''
                ]);
            }

            return $response;

        }else{
            return 'ERROR_:: TOKEN INVALIDO PARA EJECUTAR LA PETICION';
        }

    }

    public function save_app(Request $request){

        //enviar correo a administracion

        $list = Solicitudes::create([
            'mensaje' => $request['mensaje'],
            'telefono' => $request['telefono'],
            'franja' => $request['franja'],
            'diasemana' => $request['diasemana'],
            'cliente' => $request['cliente']
        ]);
        
        $client = Clientes::find($list->cliente); //buscar al cliente informacion

        //envio de notificacion one signal
        try{
            $clientId = $list->cliente;
            $message = 'Se envio la solicitud a la plataforma administrativa. Nosotros avisaremos primordialmente por este medio, gracias.';
            $devices = Devices::where('clientid', $clientId)->get();
            foreach($devices as $device) {
                OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
            }
        }
        catch(Exception $e){
            return response()->json([
                'Status' => 'false',
                'Message' => 'Error: '.$e,
                'body' => ''
            ]);
        }
        
        //envio de mail
        $fileName = '';
        try{

            if ($client->marca == 4) { /*-- Mensaje Samsung --*/
                Mail::send('mails/mail_samsung', 
                [
                        'titulo' => 'Mensaje de '.$client->email.' desde la App',
                        'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                });
            }elseif($client->marca == 3) { /*-- Mensaje Seguros mundial --*/
                Mail::send('mails/mail_seguros', 
                [
                        'titulo' => 'Mensaje de '.$client->email.' desde la App',
                        'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                });
            }elseif($client->marca == 2) {
                if ($client->aseguradora == 6) {
                    Mail::send('mails/mail_chubb',
                    [
                            'titulo' => 'Mensaje de '.$client->email.' desde la App',
                            'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                    });    
                }else{
                    Mail::send('mails/mail', 
                    [
                            'titulo' => 'Mensaje de '.$client->email.' desde la App',
                            'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                    ], 
                    function ($m) use ($client, $fileName) 
                    {
                        if($fileName != "")
                            $m->attach(public_path().'/'.$fileName);
                        $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                        $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                    });
                }
            }else{ /*-- Mensaje Falabella --*/
                Mail::send('mails/mail', 
                [
                        'titulo' => 'Mensaje de '.$client->email.' desde la App',
                        'mensaje'=> 'de: '.$client->email.' mensaje: '.request('mensaje').' --- '.' Telefono:'. request('telefono').' franja:'. request('franja').' diasemana:'. request('diasemana') 
                ], 
                function ($m) use ($client, $fileName) 
                {
                    if($fileName != "")
                        $m->attach(public_path().'/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to('asesoroutbound@crawfordaffinity.com', $client->nombre )->subject('Solicitud de mensaje Seguros Mundial!');
                });
            }
            
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Solicitudes guardada id:'.$list->id,
                'body' => $list
            ]);

        }
        catch(Exception $e){
            return response()->json([
                'Status' => 'false',
                'Message' => 'Error: '.$e,
                'body' => ''
            ]);
        }

        return $response;
    }

    /**
	*  respuesta Actualizacion de Solicitudes
	*
	*  POST url
	*  @var id
	*  @var mensaje
	*  @var telefono
	*  @var franja
	*  @var diasemana
	*  @var estado
	*  @var cliente
	*/
    public function update(){
    	$Solicitudes = Solicitudes::find(request('id'));
        $Solicitudes->mensaje = request('mensaje');
		$Solicitudes->telefono = request('telefono');
		$Solicitudes->franja = request('franja');
		$Solicitudes->diasemana = request('diasemana');
		$Solicitudes->estado = request('estado');
		$Solicitudes->cliente = request('cliente');
        $Solicitudes->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Solicitudes Actualizada id:'.$Solicitudes->id,
            'body' => $Solicitudes
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Solicitudes
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Solicitudes = Solicitudes::find($id);
        $Solicitudes->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Solicitudes Eliminada id:'.$Solicitudes->id,
                'body' => $Solicitudes
            ]);
    }

    /**
	*  view index Solicitudes
	*
	*  GET url 
	*/
    public function index(){
    	$list = Solicitudes::all();
    	return view('solicitudes', array(
    		'solicitudes' => $list 
    	));
    }

    /**
	*  view Filtros de  Solicitudes
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Solicitudes::where('nombre', 'like', '%'.$filtro.'%')
						->get();
    	return view('solicitudes', array(
    		'solicitudes' => $list 
    	));
    }

    /**
	*  view EXCEL Solicitudes
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Solicitudes::all();
		return Excel::create('Solicitudes', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
