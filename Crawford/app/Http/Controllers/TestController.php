<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;

use App\Documents;
use App\Listchekings;
use App\Reclamos;
use App\Clientes;

use Mail;
use Excel;

class TestController extends Controller
{
    public function getPhpInfo() {
	phpinfo();
    }
    public function uploadaudio(Request $request) {

        $list = DB::select("SELECT count(*) as contador FROM `documents` WHERE `reclamo` = ".$_GET['id_reclamo']." AND `listcheking` = ".$_GET['id_checklist']." ;");
        $contador = intval($list[0]->contador) + 1;

        $audio_url = "user-documents-".time()."_".basename( $_FILES['file']['name']);
        $path = public_path() . "/documents/" . $audio_url;
        $success = move_uploaded_file($_FILES['file']['tmp_name'], $path);
        if ($success) {

            //save in database
            $Documents = new Documents;
            $Documents->estado = 'SUBIDO';
            $Documents->validacion = 'SIN_VALIDAR';
            $Documents->respuestaback = 'SIN_RESPUESTA';
            $Documents->url = "/documents/" . $audio_url;
            $Documents->reclamo = $_GET['id_reclamo'];
            $Documents->listcheking = $_GET['id_checklist'] ? $_GET['id_checklist'] : 0;
            $Documents->save();

            return response()->json([
                'Status' => 'successful',
                'body' => $Documents
            ]);
        }

        return response()->json([
            'Status' => 'fail'
        ]);
    }
}
