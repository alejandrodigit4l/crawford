<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tiendas;
use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;

use Excel;

class TiendasController extends Controller
{
    //nombre', 'estado', 'icono', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
    /**
	*  respuesta todos las Tiendas
	*
	*  GET url
	*/
    public function all(){
    	$list = Tiendas::all();
    	foreach ($list as $tienda) {
    		$tienda->marca       = marcas::find($tienda->marca);
            $tienda->tipo_seguro = Tipo_seguros::find($tienda->tipo_seguro);
            $tienda->aseguradora = Aseguradoras::find($tienda->aseguradora);
            $tienda->campana 	 = Campanas::find($tienda->campana);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Tiendas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Tiendas::where('id',$id)->get();
    	foreach ($list as $tienda) {
    		$tienda->marca       = marcas::find($tienda->marca);
            $tienda->tipo_seguro = Tipo_seguros::find($tienda->tipo_seguro);
            $tienda->aseguradora = Aseguradoras::find($tienda->aseguradora);
            $tienda->campana 	 = Campanas::find($tienda->campana);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Tiendas
	*  
	*  POST url
	*  @var nombre
	*  @var estado
	*  @var icono
	*  @var campana
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('estado')!='' || request('icono')!='' || request('campana')!=''){ //estan los datos completos
            $list = Tiendas::create(request(['nombre', 'estado', 'icono', 'campana', 'aseguradora', 'tipo_seguro', 'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'aseguradoras guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Tiendas
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var estado
	*  @var icono
	*  @var campana
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('estado')!='' || request('icono')!='' || request('id')!='' || request('campana')!=''){ //estan los datos completos
            $tienda = Tiendas::find(request('id'));
            $tienda->nombre = request('nombre');
            $tienda->estado = request('estado');
            $tienda->icono = request('icono');
            $tienda->campana = request('campana');
            $tienda->aseguradora = request('aseguradora');
            $tienda->marca = request('marca');
            $tienda->tipo_seguro = request('tipo_seguro');
            $tienda->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'tienda Actualizada id:'.$tienda->id,
                'body' => $tienda
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Tiendas
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$tienda = Tiendas::find($id);
        $tienda->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'tienda Eliminada id:'.$tienda->id,
                'body' => $tienda
            ]);
    }

    /**
	*  view index Tiendas
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Tiendas::all();
    	$campanas		= Campanas::all();
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
    	foreach ($list as $tienda) {
    		$tienda->marca       = marcas::find($tienda->marca);
            $tienda->tipo_seguro = Tipo_seguros::find($tienda->tipo_seguro);
            $tienda->aseguradora = Aseguradoras::find($tienda->aseguradora);
            $tienda->campana 	 = Campanas::find($tienda->campana);
    	}
    	return view('tiendas', array(
    		'tiendas' => $list,
    		'campanas' => $campanas,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras
    	));
    }

    /**
    *  respuesta una de las Tiendas
    *
    *  GET url
    *  @var id campana 
    */
    public function campana($campana){
        $list = Tiendas::where('campana',$campana)->get();
        foreach ($list as $tienda) {
            $tienda->marca = marcas::find($tienda->marca);
            $tienda->tipo_seguro = Tipo_seguros::find($tienda->tipo_seguro);
            $tienda->aseguradora = Aseguradoras::find($tienda->aseguradora);
            $tienda->campana 	 = Campanas::find($tienda->campana);            
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Campanas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
        $campanas		= Campanas::all();
    	$list = Tiendas::where('nombre', 'like', '%'.$filtro.'%')
						->orWhere('descripcion', 'like', '%'.$filtro.'%')
						->orWhere('marca', '=', $filtro)
						->orWhere('tipo_seguro', '=', $filtro)
						->orWhere('ifreclamos', '=', $filtro)
						->orWhere('ifcomprapoliza', '=', $filtro)
						->orWhere('aseguradora', '=', $filtro)
						->get();
		foreach ($list as $campana) {
    		$campana->marca       = marcas::find($campana->marca);
            $campana->tipo_seguro = Tipo_seguros::find($campana->tipo_seguro);
            $campana->aseguradora = Aseguradoras::find($campana->aseguradora);
    	}
    	return view('tiendas', array(
    		'tiendas' => $list ,
    		'campanas' => $campanas ,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras
    	));
    }

    /**
	*  view EXCEL Aseguradoras
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Tiendas::all();
		return Excel::create('Tiendas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
