<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tipo_asegurados;
use App\Tiendas;
use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;

use Excel;

class Tipo_aseguradosController extends Controller
{
    //'nombre', 'descripcion', 'logo', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
    /**
	*  respuesta todos las Tipo_asegurados
	*
	*  GET url
	*/
    public function all(){
    	$list = Tipo_asegurados::all();
    	foreach ($list as $tipo_asegurado) {
    		$tipo_asegurado->marca       = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
            $tipo_asegurado->campana 	 = Campanas::find($tipo_asegurado->campana);
            $tipo_asegurado->tienda 	 = Tiendas::find($tipo_asegurado->tienda);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Tipo_asegurados
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Tipo_asegurados::where('id',$id)->get();
    	foreach ($list as $tipo_asegurado) {
    		$tipo_asegurado->marca       = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
            $tipo_asegurado->campana 	 = Campanas::find($tipo_asegurado->campana);
            $tipo_asegurado->tienda 	 = Tiendas::find($tipo_asegurado->tienda);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Tipo_asegurados
	*  
	*  POST url
	*  @var nombre
	*  @var descripcion
	*  @var logo
	*  @var tienda
	*  @var campana
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('logo')!='' || request('tienda')!=''){ //estan los datos completos
            $list = Tipo_asegurados::create(request(['nombre', 'descripcion', 'logo', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_asegurado guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Tipo_asegurados
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*  @var logo
	*  @var tienda
	*  @var campana
	*  @var aseguradora
	*  @var tipo_seguro
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('logo')!='' || request('id')!='' || request('tienda')!=''){ //estan los datos completos
            $tipo_asegurado = Tipo_asegurados::find(request('id'));
            $tipo_asegurado->nombre = request('nombre');
            $tipo_asegurado->descripcion = request('descripcion');
            $tipo_asegurado->logo = request('logo');
            $tipo_asegurado->campana = request('campana');
            $tipo_asegurado->aseguradora = request('aseguradora');
            $tipo_asegurado->marca = request('marca');
            $tipo_asegurado->tipo_seguro = request('tipo_seguro');
            $tipo_asegurado->tienda = request('tienda');
            $tipo_asegurado->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_asegurado Actualizada id:'.$tipo_asegurado->id,
                'body' => $tipo_asegurado
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Tipo_asegurados
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$tipo_asegurado = Tipo_asegurados::find($id);
        $tipo_asegurado->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_asegurado Eliminada id:'.$tipo_asegurado->id,
                'body' => $tipo_asegurado
            ]);
    }

    /**
	*  view index Tipo_asegurados
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Tipo_asegurados::all();
    	$campanas		= Campanas::all();
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
        $tiendas 		= Tiendas::all();
    	foreach ($list as $tipo_asegurado) {
    		$tipo_asegurado->marca       = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
            $tipo_asegurado->campana 	 = Campanas::find($tipo_asegurado->campana);
            $tipo_asegurado->tienda 	 = Tiendas::find($tipo_asegurado->tienda);
    	}
    	return view('tipo_asegurado', array(
    		'tipo_asegurados' => $list,
    		'campanas' => $campanas,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras,
            'tiendas' => $tiendas
    	));
    }

    /**
    *  respuesta una de las Tipo_asegurados
    *
    *  GET url
    *  @var id tienda 
    */
    public function tienda($tienda){
        $list = Tipo_asegurados::where('tienda',$tienda)->get();
        foreach ($list as $tipo_asegurado) {
            $tipo_asegurado->marca = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
            $tipo_asegurado->campana 	 = Campanas::find($tipo_asegurado->campana); 
            $tipo_asegurado->tienda 	 = Tiendas::find($tipo_asegurado->tienda);            
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Tipo_asegurados
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$campanas		= Campanas::all();
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
        $tiendas 		= Tiendas::all();
    	$list = Tipo_asegurados::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $tipo_asegurado) {
    		$tipo_asegurado->marca       = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
    	}
    	return view('tipo_asegurado', array(
    		'tipo_asegurados' => $list ,
    		'campanas' => $campanas ,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras,
            'tiendas' => $tiendas
    	));
    }

    /**
	*  view EXCEL Tipo_asegurados
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Tipo_asegurados::all();
		return Excel::create('Tipo_asegurados', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
