<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tipo_polizas;
use App\Tipo_asegurados;
use App\Tiendas;
use App\Campanas;
use App\Aseguradoras;
use App\marcas;
use App\Tipo_seguros;

use Excel;

class Tipo_polizasController extends Controller
{
    //'nombre', 'descripcion', 'formulario', 'asegurado', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
     /**
	*  respuesta todos las Tipo_polizas
	*
	*  GET url
	*/
    public function all(){
    	$list = Tipo_polizas::all();
    	foreach ($list as $tipo_poliza) {
    		$tipo_poliza->marca       = marcas::find($tipo_poliza->marca);
            $tipo_poliza->tipo_seguro = Tipo_seguros::find($tipo_poliza->tipo_seguro);
            $tipo_poliza->aseguradora = Aseguradoras::find($tipo_poliza->aseguradora);
            $tipo_poliza->campana 	 = Campanas::find($tipo_poliza->campana);
            $tipo_poliza->tienda 	 = Tiendas::find($tipo_poliza->tienda);
            $tipo_poliza->asegurado  = Tipo_asegurados::find($tipo_poliza->asegurado);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Tipo_polizas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Tipo_polizas::where('id',$id)->get();
    	foreach ($list as $tipo_poliza) {
    		$tipo_poliza->marca       = marcas::find($tipo_poliza->marca);
            $tipo_poliza->tipo_seguro = Tipo_seguros::find($tipo_poliza->tipo_seguro);
            $tipo_poliza->aseguradora = Aseguradoras::find($tipo_poliza->aseguradora);
            $tipo_poliza->campana 	 = Campanas::find($tipo_poliza->campana);
            $tipo_poliza->tienda 	 = Tiendas::find($tipo_poliza->tienda);
            $tipo_poliza->asegurado  = Tipo_asegurados::find($tipo_poliza->asegurado);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Tipo_polizas
	*  
	*  POST url
	*  @var nombre 
    *  @var descripcion
    *  @var formulario
    *  @var asegurado
    *  @var tienda
    *  @var campana
    *  @var aseguradora
    *  @var tipo_seguro
    *  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('formulario')!='' || request('tienda')!=''){ //estan los datos completos
            $list = Tipo_polizas::create(request(['nombre', 'descripcion', 'formulario', 'asegurado', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca' ]));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_poliza guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Tipo_polizas
	*
	*  POST url
	*  @var id
	*  @var nombre 
    *  @var descripcion
    *  @var formulario
    *  @var asegurado
    *  @var tienda
    *  @var campana
    *  @var aseguradora
    *  @var tipo_seguro
    *  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('formulario')!='' || request('id')!='' || request('tienda')!=''){ //estan los datos completos
            $tipo_poliza = Tipo_polizas::find(request('id'));
            $tipo_poliza->nombre = request('nombre');
            $tipo_poliza->descripcion = request('descripcion');
            $tipo_poliza->formulario = request('formulario');
            $tipo_poliza->campana = request('campana');
            $tipo_poliza->aseguradora = request('aseguradora');
            $tipo_poliza->marca = request('marca');
            $tipo_poliza->tipo_seguro = request('tipo_seguro');
            $tipo_poliza->tienda = request('tienda');
            $tipo_poliza->asegurado = request('asegurado');
            $tipo_poliza->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_poliza Actualizada id:'.$tipo_poliza->id,
                'body' => $tipo_poliza
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Tipo_polizas
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$tipo_poliza = Tipo_polizas::find($id);
        $tipo_poliza->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'tipo_poliza Eliminada id:'.$tipo_poliza->id,
                'body' => $tipo_poliza
            ]);
    }

    /**
	*  view index Tipo_polizas
	*
	*  GET url 
	*/
    public function index(){
    	$list			= Tipo_polizas::all();
    	$campanas		= Campanas::all();
    	$marcas 		= marcas::all();
        $tipo_seguros 	= Tipo_seguros::all();
        $aseguradoras	= Aseguradoras::all();
        $tiendas 		= Tiendas::all();
        $asegurados   = Tipo_asegurados::all();
    	foreach ($list as $tipo_poliza) {
    		$tipo_poliza->marca       = marcas::find($tipo_poliza->marca);
            $tipo_poliza->tipo_seguro = Tipo_seguros::find($tipo_poliza->tipo_seguro);
            $tipo_poliza->aseguradora = Aseguradoras::find($tipo_poliza->aseguradora);
            $tipo_poliza->campana 	 = Campanas::find($tipo_poliza->campana);
            $tipo_poliza->tienda 	 = Tiendas::find($tipo_poliza->tienda);
            $tipo_poliza->asegurado  = Tipo_asegurados::find($tipo_poliza->asegurado);
    	}
    	return view('tipo_polizas', array(
    		'tipo_polizas' => $list,
    		'campanas' => $campanas,
    		'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras,
            'tiendas' => $tiendas,
            'asegurados' => $asegurados
    	));
    }

    /**
    *  respuesta una de las Tipo_polizas
    *
    *  GET url
    *  @var id asegurado 
    */
    public function asegurado($asegurado){
        $list = Tipo_polizas::where('id',$asegurado)->get();
        foreach ($list as $tipo_asegurado) {
            $tipo_asegurado->marca = marcas::find($tipo_asegurado->marca);
            $tipo_asegurado->tipo_seguro = Tipo_seguros::find($tipo_asegurado->tipo_seguro);
            $tipo_asegurado->aseguradora = Aseguradoras::find($tipo_asegurado->aseguradora);
            $tipo_asegurado->campana 	 = Campanas::find($tipo_asegurado->campana); 
            $tipo_asegurado->tienda 	 = Tiendas::find($tipo_asegurado->tienda);            
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  view Filtros de  Tipo_polizas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$campanas      = Campanas::all();
        $marcas         = marcas::all();
        $tipo_seguros   = Tipo_seguros::all();
        $aseguradoras   = Aseguradoras::all();
        $tiendas        = Tiendas::all();
        $asegurados   = Tipo_asegurados::all();
    	$list = Tipo_polizas::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $tipo_poliza) {
    		$tipo_poliza->marca       = marcas::find($tipo_poliza->marca);
            $tipo_poliza->tipo_seguro = Tipo_seguros::find($tipo_poliza->tipo_seguro);
            $tipo_poliza->aseguradora = Aseguradoras::find($tipo_poliza->aseguradora);
            $tipo_poliza->campana    = Campanas::find($tipo_poliza->campana);
            $tipo_poliza->tienda     = Tiendas::find($tipo_poliza->tienda);
            $tipo_poliza->asegurado  = Tipo_asegurados::find($tipo_poliza->asegurado);
    	}
    	return view('tipo_polizas', array(
    		'tipo_polizas' => $list,
            'campanas' => $campanas,
            'marcas' => $marcas,
            'tipo_seguros' => $tipo_seguros,
            'aseguradoras' => $aseguradoras,
            'tiendas' => $tiendas,
            'asegurados' => $asegurados
    	));
    }

    /**
	*  view EXCEL Tipo_polizas
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Tipo_polizas::all();
		return Excel::create('Tipo_polizas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
