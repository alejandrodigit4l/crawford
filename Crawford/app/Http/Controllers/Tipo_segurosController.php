<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tipo_seguros;
use App\marcas;

use Excel;

class Tipo_segurosController extends Controller
{
    //,
    /**
	*  respuesta todos las Tipo_seguros
	*
	*  GET url
	*/
    public function all(){
    	$list = Tipo_seguros::all();
    	foreach ($list as $tipo_seguro) {
    		$tipo_seguro->marca = marcas::find($tipo_seguro->marca);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Tipo_seguros
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Tipo_seguros::where('id',$id)->get();
    	foreach ($list as $tipo_seguro) {
    		$tipo_seguro->marca = marcas::find($tipo_seguro->marca);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta una de las Tipo_seguros
    *
    *  GET url
    *  @var id marca 
    */
    public function marca($marca){
        $list = Tipo_seguros::where('marca',$marca)->get();
        foreach ($list as $tipo_seguro) {
            $tipo_seguro->marca = marcas::find($tipo_seguro->marca);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Tipo_seguros
	*
	*  POST url
	*  @var nombre
	*  @var descripcion
	*  @var marca
	*/
    public function save(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!=''){ //estan los datos completos
            $list = Tipo_seguros::create(request(['nombre', 'descripcion', 'marca']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Tipo Seguro guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de Tipo_seguros
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var descripcion
	*  @var marca
	*/
    public function update(){
    	if(request('nombre')!='' || request('descripcion')!='' || request('marca')!='' || request('id')!=''){ //estan los datos completos
            $tipo_seguro = Tipo_seguros::find(request('id'));
            $tipo_seguro->nombre = request('nombre');
            $tipo_seguro->descripcion = request('descripcion');
            $tipo_seguro->marca = request('marca');
            $tipo_seguro->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'Marca Actualizada id:'.$tipo_seguro->id,
                'body' => $tipo_seguro
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar Tipo_seguros
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$tipo_seguro = Tipo_seguros::find($id);
        $tipo_seguro->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Marca Eliminada id:'.$tipo_seguro->id,
                'body' => $tipo_seguro
            ]);
    }

    /**
	*  view index Tipo_seguros
	*
	*  GET url 
	*/
    public function index(){
    	$list = Tipo_seguros::all();
    	$marcas = marcas::all();
    	foreach ($list as $tipo_seguro) {
    		$tipo_seguro->marca = marcas::find($tipo_seguro->marca);
    	}
    	return view('tipo_seguros', array(
    		'tipo_seguros' => $list,
    		'marcas' => $marcas
    	));
    }

    /**
	*  view Filtros de  Tipo_seguros
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$marcas = marcas::all();
    	$list = Tipo_seguros::where('nombre', 'like', '%'.$filtro.'%')
						->orWhere('descripcion', 'like', '%'.$filtro.'%')
						->orWhere('marca', '=', $filtro)
						->get();
		foreach ($list as $tipo_seguro) {
    		$tipo_seguro->marca = marcas::find($tipo_seguro->marca);
    	}
    	return view('tipo_seguros', array(
    		'tipo_seguros' => $list ,
    		'marcas' => $marcas
    	));
    }

    /**
	*  view EXCEL Tipo_seguros
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Tipo_seguros::all();
		return Excel::create('tipo_seguros', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
