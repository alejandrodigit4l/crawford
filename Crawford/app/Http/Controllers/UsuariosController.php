<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuarios;
use App\User;
use App\Roles;
use Mail;
use Hash;

class UsuariosController extends Controller
{
    //'username','firstname','lastname','email','password','remember_token','rol'
    /**
	*  respuesta todos las Usuarios
	*
	*  GET url
	*/
    public function all(){
    	$list = Usuarios::all();
    	foreach ($list as $usuarios) {
    		$usuarios->rol = Roles::find($usuarios->rol);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Usuarios
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = User::where('id',$id)->get();
    	foreach ($list as $usuarios) {
    		$usuarios->rol = Roles::find($usuarios->rol);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de Usuarios
	*  
	*  POST url
	*  @var username
	*  @var firstname
	*  @var lastname
	*  @var email
	*  @var password
	*  @var remember_token
	*  @var rol
	*/
    public function save(Request $request){
    	// $list = Usuarios::create(request(['username','firstname','lastname','email','password','remember_token','rol']));
        $list = User::create([
            'name' => $request->nombre_usuario,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'rol' => $request->rol,
            'marca' => $request->marca,
            'active' => $request->estado,
            'password' => bcrypt($request->password),
        ]);
        return response()->json([
            'Status' => 'successful',
            'Message' => 'usuarios guardada id:'.$list->id,
            'body' => $list
        ]);
	}

    /**
	*  respuesta Actualizacion de Usuarios
	*
	*  POST url
	*  @var id
	*  @var username
	*  @var firstname
	*  @var lastname
	*  @var email
	*  @var password
	*  @var remember_token
	*  @var rol
	*/
    public function update(){
    	$usuarios = User::find(request('id'));
        $usuarios->name	= request('nombre_usuario');
        $usuarios->email = request('email');
		$usuarios->telefono = request('telefono');
		$usuarios->rol	= request('rol');
        $usuarios->marca  = request('marca');
        $usuarios->active = request('estado');
		$usuarios->password	= bcrypt(request('password'));
		// $usuarios->remember_token	= request('remember_token');
        $usuarios->save();
        return response()->json([
            'Status' => 'successful',
            'Message' => 'usuarios Actualizada id:'.$usuarios->id,
            'body' => $usuarios
        ]);
    }

    /**
	*  respuesta Eliminar Usuarios
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$usuarios = User::find($id);
        $usuarios->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'usuarios Eliminada id:'.$usuarios->id,
                'body' => $usuarios
            ]);
    }

    /**
	*  view index Usuarios
	*
	*  GET url 
	*/
    public function index(){
    	$usuarios = User::all();
    	$roles = Roles::all();
    	foreach ($usuarios as $usuario) {
    		$usuario->rol = Roles::find($usuario->rol);
    	}
    	return view('usuarios', array(
    		'usuarios' => $usuarios,
            'roles' => $roles
    	));
    }


    /**
	*  view Filtros de  Usuarios
	*
	*  GET url 
	*/
    public function filtros($filtro){
        $roles         = Roles::all();
    	$list = Usuarios::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $usuarios) {
			$usuarios->rol = Roles::find($usuarios->rol);
    	}
    	return view('usuarios', array(
    		'usuarios' => $list,
            'roles' => $roles
    	));
    }

    /**
	*  view EXCEL Usuarios
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Usuarios::all();
		return Excel::create('Usuarios', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    public function login(Request $request){
        $list = Usuarios::where('username', 'like', $request['username'])
                        ->where('marca', 'like', $request['marca'])
                        ->get();
        foreach ($list as $usuarios) {
            $usuarios->rol = Roles::find($usuarios->rol);
        }
        if(sizeof($list)>0){
            if (Hash::check($request['password'], $list[0]->password) && $request['username'] == $list[0]->username) {
                $response = response()->json([
                    'Status' => 'successful',
                    'Message' => 'Se encontraron coincidencias',
                    'body' => $list
                ]);
            }else{
                $response = response()->json([
                    'Status' => 'false',
                    'Message' => 'Existen datos que no coinciden con el usuario. Vuelve a intentar',
                    'body' => $list
                ]);
            }
        }
        else{
            $response = response()->json([
                'Status' => 'false',
                'Message' => 'Existen datos que no coinciden con el usuario. Vuelve a intentar',
                'body' => $list
            ]);
        }

        return $response;
    }

    public function olvido(){
        $usuario = Usuarios::where('username', $_POST['username'])->get();
        //mail send
        $fileName = '';
        Mail::send('mails/mail', 
            [
                    'titulo' => 'Recuperacion de contraseña',
                    'mensaje'=> 'El usuario '.$usuario[0]->username.' ha pedido recuperar la contraseña la cual es: '.$usuario[0]->password
            ], 
            function ($m) use ($usuario, $fileName) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/docnotifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to($usuario[0]->email, $usuario[0]->username )->subject('Recuperacion de contraseña');
            });

        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $usuario
            ]);
    }
}
