<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Utilchats;
use Excel;

class UtilchatsController extends Controller
{
    //'mensajecierre', 'estado', 'usuario'
    /**
	*  respuesta todos las Utilchats
	*
	*  GET url
	*/
    public function all(){
    	$list = Utilchats::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las Utilchats
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = Utilchats::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta una de las Utilchats
	*   Se necesita el ultimo resultado de este para saber el estado de chat, si esta habilitado o no
	*/
    public function ultimedata(){
    	$list = Utilchats::where('id','>','0')->orderBy('id', 'DESC')->get()[0];
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var mensajecierre
	*  @var estado
	*  @var usuario
	*/
    public function save(){
    	$list = Utilchats::create(request(['mensajecierre', 'estado', 'usuario']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'Utilchats guardada id:'.$list->id,
            'body' => $list
        ]);
        return $response;
    }

    /**
	*  respuesta Actualizacion de Utilchats
	*
	*  POST url
	*  @var mensajecierre
	*  @var estado
	*  @var usuario
	*/
    public function update(){
    	$Utilchats = Utilchats::find(request('id'));
        $Utilchats->mensajecierre = request('mensajecierre');
        $Utilchats->estado = request('estado');
        $Utilchats->usuario = request('usuario');
        $Utilchats->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'Utilchats Actualizada id:'.$Utilchats->id,
            'body' => $Utilchats
        ]);
        return $response;
    }

    /**
	*  respuesta Eliminar Utilchats
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$Utilchats = Utilchats::find($id);
        $Utilchats->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Utilchats Eliminada id:'.$Utilchats->id,
                'body' => $Utilchats
            ]);
    }

    /**
	*  view index Utilchats
	*
	*  GET url 
	*/
    public function index(){
    	$list = Utilchats::all();
    	return view('utilchats', array(
    		'utilchats' => $list 
    	));
    }

    /**
	*  view Filtros de  Utilchats
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = Utilchats::where('nombre', 'like', '%'.$filtro.'%')
						->get();
    	return view('utilchats', array(
    		'utilchats' => $list 
    	));
    }

    /**
	*  view EXCEL Utilchats
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = Utilchats::all();
		return Excel::create('utilchats', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }
}
