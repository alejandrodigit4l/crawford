<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\marcas;

use Excel;


class marcasController extends Controller
{
	/**
	*  respuesta todos las marcas
	*
	*  GET url
	*/
    public function all(){
    	$list = marcas::all();
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las marcas
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = marcas::where('id',$id)->get();
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var nombre
	*  @var estado
	*  @var logo
	*/
    public function save(){
    	if(request('nombre')!='' || request('estado')!='' || request('logo')!=''){ //estan los datos completos
            $list = marcas::create(request(['nombre', 'estado', 'logo']));
            $response = response()->json([
                'Status' => 'successful',
                'Message' => 'Marca guardada id:'.$list->id,
                'body' => $list
            ]);
        } 
        else{
        	$response = response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Actualizacion de marca
	*
	*  POST url
	*  @var id
	*  @var nombre
	*  @var estado
	*  @var logo
	*/
    public function update(){
    	if(request('nombre')!='' || request('estado')!='' || request('logo')!='' || request('id')!=''){ //estan los datos completos
            $marca = marcas::find(request('id'));
            $marca->nombre = request('nombre');
            $marca->estado = request('estado');
            $marca->logo = request('logo');
            $marca->save();
            $response =  response()->json([
                'Status' => 'successful',
                'Message' => 'Marca Actualizada id:'.$marca->id,
                'body' => $marca
            ]);
        } 
        else{
        	$response =  response()->json([
                'Status' => 'false',
                'Message' => 'Datos incorrectos',
                'body' => array()
            ]);
        }
        return $response;
    }

    /**
	*  respuesta Eliminar marca
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$marca = marcas::find($id);
        $marca->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Marca Eliminada id:'.$marca->id,
                'body' => $marca
            ]);
    }

    /**
	*  view index marcas
	*
	*  GET url 
	*/
    public function index(){
    	$list = marcas::all();
    	return view('marcas', array(
    		'marcas' => $list 
    	));
    }

    /**
	*  view Filtros de  marcas
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = marcas::where('nombre', 'like', '%'.$filtro.'%')
						->orWhere('estado', 'like', '%'.$filtro.'%')
						->get();
    	return view('marcas', array(
    		'marcas' => $list 
    	));
    }

    /**
	*  view EXCEL marcas
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = marcas::all();
		return Excel::create('marcas', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }


}
