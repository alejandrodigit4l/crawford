<?php

namespace App\Http\Controllers;

use Mail;
use Excel;
use OneSignal;
use App\marcas;
use App\Devices;
use App\Clientes;
use App\Reclamos;
use App\notificaciones;
use Illuminate\Http\Request;
use App\ZurichNotificaciones;
use Illuminate\Support\Facades\Storage;

class notificacionesController extends Controller
{
    //'notyid','fecha','texto','estado','cliente','reclamo','marca',
    /**
	*  respuesta todos las notificaciones
	*
	*  GET url
	*/
    public function all(){
    	$list = notificaciones::all();
    	foreach ($list as $notificacion) {
    		$notificacion->marca = Marcas::find($notificacion->marca);
    		$notificacion->reclamo = Reclamos::find($notificacion->reclamo);
    		$notificacion->cliente = Clientes::find($notificacion->cliente);
    	}
    	return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    } 

    /**
	*  respuesta una de las notificaciones
	*
	*  GET url
	*  @var id identificador de la marca en la base de datos
	*/
    public function get($id){
    	$list = notificaciones::where('id',$id)->get();
    	foreach ($list as $notificacion) {
    		$notificacion->marca = Marcas::find($notificacion->marca);
    		$notificacion->reclamo = Reclamos::find($notificacion->reclamo);
    		$notificacion->cliente = Clientes::find($notificacion->cliente);
    	}
		return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
    *  respuesta una de las notificaciones
    *
    *  GET url
    *  @var id identificador de la marca en la base de datos
    */
    public function reclamo($reclamo){
        $list = notificaciones::where('reclamo',$reclamo)->get();
        foreach ($list as $notificacion) {
            $notificacion->marca = Marcas::find($notificacion->marca);
            $notificacion->reclamo = Reclamos::find($notificacion->reclamo);
            $notificacion->cliente = Clientes::find($notificacion->cliente);
        }
        return response()->json([
                'Status' => 'successful',
                'Message' => 'Se encontraron coincidencias',
                'body' => $list
            ]);
    }

    /**
	*  respuesta Registro de marca
	*
	*  POST url
	*  @var notyid
	*  @var fecha
	*  @var texto
	*  @var estado
	*  @var cliente
	*  @var reclamo
	*  @var marca
	*/
    public function save(){
    	$list = notificaciones::create(request(['notyid','fecha','texto','estado','cliente','reclamo','marca']));
        $response = response()->json([
            'Status' => 'successful',
            'Message' => 'notificaciones guardada id:'.$list->id,
            'body' => $list
        ]);

        $client = Clientes::find($list->cliente);
        $fileName = '';
        Mail::send('mails/mail', 
            [
                    'titulo' => 'Notificación #'.$list->id.' Seguros Mundial!',
                    'mensaje'=> $list->texto
            ], 
            function ($m) use ($client, $fileName,$list) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/docnotifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to($client->email, $client->nombre )->subject('Notificación #'.$list->id.' Seguros Mundial!');
            });

        
        return $response;
    }

    /**
	*  respuesta Actualizacion de notificaciones
	*
	*  POST url
	*  @var id
	*  @var notyid
	*  @var fecha
	*  @var texto
	*  @var estado
	*  @var cliente
	*  @var reclamo
	*  @var marca
	*/
    public function update(){
    	$notificaciones = notificaciones::find(request('id'));
        $notificaciones->notyid = request('notyid');
		$notificaciones->fecha = request('fecha');
		$notificaciones->texto = request('texto');
		$notificaciones->estado = request('estado');
		$notificaciones->cliente = request('cliente');
		$notificaciones->reclamo = request('reclamo');
		$notificaciones->marca = request('marca');
        $notificaciones->save();
        $response =  response()->json([
            'Status' => 'successful',
            'Message' => 'notificaciones Actualizada id:'.$notificaciones->id,
            'body' => $notificaciones
        ]);

        $client = Clientes::find($list->cliente);
        $fileName = $notificaciones->notyid;
        Mail::send('mails/mail', 
            [
                    'titulo' => 'Notificación #'.$list->id.' Seguros Mundial!',
                    'mensaje'=> $list->texto
            ], 
            function ($m) use ($client, $fileName,$list) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/docnotifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to($client->email, $client->nombre )->subject('Notificación #'.$list->id.' Seguros Mundial!');
            });

        return $response;
    }

    /**
	*  respuesta Eliminar notificaciones
	*
	*  GET url
	*  @var id 
	*/
    public function delete($id){
    	$notificaciones = notificaciones::find($id);
        $notificaciones->delete();
        return response()->json([
                'Status' => 'successful',
                'Message' => 'notificaciones Eliminada id:'.$notificaciones->id,
                'body' => $notificaciones
            ]);
    }

    /**
	*  view index notificaciones
	*
	*  GET url 
	*/
    public function index(){
    	$marcas = Marcas::all();
    	$reclamos = Reclamos::all();
    	$list = notificaciones::all();
    	foreach ($list as $notificacion) {
    		$notificacion->marca = Marcas::find($notificacion->marca);
    		$notificacion->reclamo = Reclamos::find($notificacion->reclamo);
    		$notificacion->cliente = Clientes::find($notificacion->cliente);
    	}
    	return view('notificaciones', array(
    		'notificaciones' => $list ,
    		'marcas' => $marcas ,
			'reclamos' => $reclamos
    	));
    }

    /**
	*  view Filtros de  notificaciones
	*
	*  GET url 
	*/
    public function filtros($filtro){
    	$list = notificaciones::where('nombre', 'like', '%'.$filtro.'%')
						->get();
		foreach ($list as $notificacion) {
    		$notificacion->marca = Marcas::find($notificacion->marca);
    		$notificacion->reclamo = Reclamos::find($notificacion->reclamo);
    		$notificacion->cliente = Clientes::find($notificacion->cliente);
    	}
    	return view('notificaciones', array(
    		'notificaciones' => $list 
    	));
    }


    /**
	*  view EXCEL notificaciones
	*
	*  GET url 
	*/
    public function downloadExcel(){
    	$list = notificaciones::all();
		return Excel::create('notificaciones', function($excel) use ($list) {
			$excel->sheet('mySheet', function($sheet) use ($list)
	        {
				$sheet->fromArray($list);
	        });
		})->download('xls');
    }

    /**
    *   GET sendmail
    */
    public function send(){
        $client = '';
        $fileName = '';
        Mail::send('mails/mail', 
            [
                    'titulo' => 'Bienvenido a Seguros Mundial',
                    'mensaje'=> 'Queremos darte la bienvenida a Seguros Mundial'
            ], 
            function ($m) use ($client, $fileName) 
            {
                if($fileName != "")
                    $m->attach(public_path().'/Notifications/'.$fileName);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                $m->to('alejandro@digit4l.co', 'alejandro')->subject('Notificación Seguros Mundial!');
            });
    }

    /**
    *   GET send push notifitacion one signal
    *   @var user_id 
    *   @var message
    */
    public function sendPush() {
        $clientId = request("user_id");
        $message = request("message");
        $devices = Devices::where('clientid', $clientId)->get();
        foreach($devices as $device) {
            OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
        }
        $response = response()->json([
            'Status' => 'success'
        ]);
        return $response;
    }

    public function sendBackend(Request $request){

        //dd($_FILES);
        // $target_path = "docnotifications/"; 

        // $target_path = $target_path . basename( $_FILES['file']['name']);   

        if (empty($request->file)) {
            $fileName = "";    
        }else{
            $fileName = uniqid()."_notification.".$request->file->extension();
            $request->file->storeAs('notificaciones',$fileName); 
        }
        

        // Storage::putFileAs(
        //     '', $request->file('file'), $fileName
        // );

        // if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
        //     $response = response()->json([
        //         'Status' => 'successful',
        //         'Message' => 'Movido correctamente',
        //         'body' => ''
        //     ]);
        // } else {
        // echo $target_path;
        //     $response = response()->json([
        //         'Status' => 'false',
        //         'Message' => 'Algo ocurrio intenta de nuevo',
        //         'body' => ''
        //     ]);
        // }

        //busqueda del reclamo
        $reclamo = Reclamos::find($request->id);
        $reclamo->cliente = Clientes::find($reclamo->cliente);
        $notificaciones = new notificaciones();
        $notificaciones->cliente = $reclamo->cliente->id;//cliente que reclamo
        $notificaciones->reclamo = $reclamo->id; //reclamo id
        $notificaciones->marca = $reclamo->cliente->marca;
        $notificaciones->notyid = $fileName;
        $notificaciones->fecha = date('Y-m-d');
        $notificaciones->texto = $_POST['textonotificacion'];
        $notificaciones->estado = 'ENVIADO';        
        $notificaciones->save();

        $client = $reclamo->cliente;
        $fileName = $notificaciones->notyid;

        //envio de notificacion one signal
        $clientId = $client->id;
        $message = $notificaciones->texto;
        $devices = Devices::where('clientid', $clientId)->get();
        foreach($devices as $device) {
            OneSignal::sendNotificationToUser($message, $device->deviceid, null, null, null, null);
        }

        //envio de mail
        if ($client->marca == 4) {
            Mail::send('mails/mail_samsung', 
            [
                    'titulo' => 'Notificación reclamo #'.$reclamo->reclamoid.' Seguros Mundial!',
                    'mensaje'=> $notificaciones->texto
            ], 
            function ($m) use ($client, $fileName) 
            {
                if($fileName != ""){
                    $m->attach(public_path().'/storage/crawford/notificaciones/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }else{
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }

            });
        }elseif ($client->marca == 3) {
            Mail::send('mails/mail_seguros', 
            [
                    'titulo' => 'Notificación reclamo #'.$reclamo->reclamoid.' Seguros Mundial!',
                    'mensaje'=> $notificaciones->texto
            ], 
            function ($m) use ($client, $fileName) 
            {
                if($fileName != ""){
                    // $m->attach(public_path().'/'.$fileName);
                    $m->attach(public_path().'/storage/crawford/notificaciones/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }else{
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }
                    
            });
        }else{
            Mail::send('mails/mail', 
            [
                    'titulo' => 'Notificación reclamo #'.$reclamo->reclamoid.' Seguros Mundial!',
                    'mensaje'=> $notificaciones->texto
            ], 
            function ($m) use ($client, $fileName) 
            {
                if($fileName != ""){
                    // $m->attach(public_path().'/'.$fileName);
                    $m->attach(public_path().'/storage/crawford/notificaciones/'.$fileName);
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }else{
                    $m->from('notificaciones@crawfordaffinitycolombia.com', 'Seguros Mundial');
                    $m->to($client->email, $client->nombre )->subject('Notificación Seguros Mundial!');
                }
                    
            });
        }
        
        

        return redirect('crawford/idreclamo/'.$reclamo->id);
    }

    public function enviar_notificacion_zurich(Request $request){

        $nombre_adjunto = "";
        if ($request->hasFile('adjunto')) {
            $nombre_adjunto = "adjunto_notificacion_".$request->id_reclamo."_".date('Y-m-d').".".$request->adjunto->extension();
            $request->adjunto->storeAs('notificaciones_zurich',$nombre_adjunto);
        }

        $notificacion = ZurichNotificaciones::create([
            'id_reclamo' => $request->id_reclamo,
            'nombre_adjunto' => $nombre_adjunto,
            'mensaje' => $request->mensaje_notificacion
        ]);

        $reclamo = $notificacion->reclamo;
        $cliente = $notificacion->reclamo->datos_cliente;

        Mail::send('mails/mail_zurich', 
        [
            'titulo' => 'Notificación reclamo #'.$reclamo->id,
            'mensaje'=> $request->mensaje_notificacion
        ], 
        function ($m) use ($cliente, $nombre_adjunto) 
        {
            if($nombre_adjunto != ""){
                $m->attach(storage_path().'/app/public/crawford/notificaciones_zurich/'.$nombre_adjunto);
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Zurich Reclamos');
                $m->to($cliente->email, $cliente->nombre )->subject('Notificación Zurich');
            }else{
                $m->from('notificaciones@crawfordaffinitycolombia.com', 'Zurich Reclamos');
                $m->to($cliente->email, $cliente->nombre )->subject('Notificación Zurich');
            }         
        });

        return redirect('crawford/idreclamo_zurich/'.$request->id_reclamo)->with('message','Se ha enviado la notificación exitosamente.');

    }

}
