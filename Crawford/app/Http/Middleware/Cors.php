<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
                ->header('X-Frame-Options', 'ALLOW FROM https://admin.crawfordaffinitycolombia.com/')
                ->header('Acces-Control-Allow-Origin', 'https://admin.crawfordaffinitycolombia.com/')
                ->header('Acces-control-Allow-Methods','*');
    }
}
