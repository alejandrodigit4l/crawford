<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imeis extends Model
{
   protected $fillable = [
   	   'campaña',
       'imeis',
       'estado_enviado'
   ];
}
