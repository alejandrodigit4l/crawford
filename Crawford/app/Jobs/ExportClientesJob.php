<?php

namespace App\Jobs;

use Excel;
use App\marcas;
use App\Clientes;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportClientesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 180);
        ini_set('memory_limit', '-1');
        $array = array();
        $list = Clientes::all();
        foreach ($list as $cliente) {
            $cliente->marca = marcas::find($cliente->marca)->nombre;
        }

        array_push($array, $list);

        $today = date('Y-m-d');
        return Excel::create('clientes'.$today, function($excel) use ($array) {
            foreach ($array as $value) {
                $excel->sheet('Clientes', function($sheet) use ($value)
                {
                    $sheet->fromArray($value);
                });
            }           
        })->download('xls');
    }
}
