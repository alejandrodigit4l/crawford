<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaCausales extends Model
{
    protected $fillable = [
    	'nombre_causal'
    ];
}
