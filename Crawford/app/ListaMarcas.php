<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaMarcas extends Model
{
    protected $fillable = [
    	'nombre_marca'
    ];
}
