<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaProductos extends Model
{
    protected $fillable = [
    	'nombre_producto'
    ];
}
