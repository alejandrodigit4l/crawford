<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaSponsors extends Model
{
    protected $fillable = [
    	'nombre_sponsor'
    ];
}
