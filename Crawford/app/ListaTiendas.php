<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaTiendas extends Model
{
    protected $fillable = [
    	'nombre_tienda'
    ];
}
