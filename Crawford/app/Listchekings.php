<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listchekings extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
        'nombre', 'img', 'titulo', 'descripcion', 'tipo', 'subdescripcion', 'ayuda', 'tipo_polizas', 'asegurado', 'marca', 'tipo_seguro', 'aseguradora', 'campana', 'tienda','reclamo' 
    ];
}
