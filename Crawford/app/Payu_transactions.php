<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payu_transactions extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
		'nombre','email','cedula','marca','producto','titulo','valor','solicitud_payu','valor_payu','payer_name','payer_email','payer_dni','payerment_method','code_payu','order_id','transactionid','state','responsecode','fecha_creacion','fecha_vencimiento','serial','poliza','enviado','url','clientes_id',
	];
}
