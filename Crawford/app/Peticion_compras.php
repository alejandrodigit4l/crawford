<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peticion_compras extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
		'marca','usuario','src','estado','mensaje','serial','clientes_id','productos_id',
	];
}
