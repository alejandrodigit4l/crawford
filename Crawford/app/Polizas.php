<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polizas extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [    
        'marca',
        'url_equipo',
        'url_factura',
        'url_poliza',
        'fecha_compra',
        'estado_poliza',
        'name_user',
        'price',
        'email',
        'name_product',
        'serie',
        'imei',
        'cobertura',
        'producto',
        'cliente',
        'orderId',
        'transaction_id',
        'response_code',
        'user_create',
        'reference',
        'state',
        'value',
        'user_id'
        ];
        
        public function productos(){
             return $this->belongsTo(Productos::class, 'producto');   
        }

        public function clientes(){
            return $this->belongsTo(Clientes::class, 'cliente');
        }


}
