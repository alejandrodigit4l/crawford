<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
		'titulo','descripcion','cubrimiento','valor','urlicono','referencia','tipo_poliza','marca','tipo_seguro','aseguradora','campana','tienda','asegurado'    
	];
}
