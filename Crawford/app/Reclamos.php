<?php

namespace App;

use App\Clientes;
use Illuminate\Database\Eloquent\Model;

class Reclamos extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'reclamoid',
        'cedula',
        'nombreasegurado',
        'apellidoasegurado',
        'fechanacimientoasse',
        'ciudadrecidencia',
        'telefonofijo',
        'celular',
        'direccion',
        'email',
        'telefonolaboral',
        'producto_reclamo',
        'marca',
        'imei',
        'numero_factura',
        'modelo_equipo',
        'precio_equipo',
        'tienda',
        'fecha_asignacion_cst',
        'nombre_cst',
        'fecha_ingreso_cst',
        'fecha_diagnostico',
        'tiempo_reparacion',
        'causal',
        'valor_indemnizado',
        'fecha_notificacion',
        'fechasiniestro',
        'horasiniestro',
        'descripcionsiniestro',
        'textobackend',
        'observaciones',
        'motivobaja',
        'cliente',
        'estados_poliza',
        'producto',
        'ciudadsiniestro',
        'plan_poliza',
        'tipo_siniestro',
        'certificado',
        'radicado_zurich'
    ];

    public function datos_cliente(){
        return $this->belongsTo(Clientes::class, 'cliente' , 'id');
    }

    public function datos_estado_poliza(){
        return $this->belongsTo(Estados_polizas::class, 'estados_poliza', 'id');
    }

    public function ciudad_residencia(){
        return $this->belongsTo(Ciudades_departamentos::class, 'ciudadrecidencia' ,'id');
    }

    public function ciudad_siniestro(){
        return $this->belongsTo(Ciudades_departamentos::class, 'ciudadsiniestro', 'id');
    }

    // QUERY SCOPES

    public function scopeId($query, $id){
        if ($id) {
            return $query->where('id', 'LIKE', '%'.$id.'%');
        }
    }
    public function scopeCedula($query, $cedula){
        if ($cedula) {
            return $query->where('cedula', 'LIKE', '%'.$cedula.'%');
        }
    }
    public function scopeNombreasegurado($query, $nombre_asegurado){
        if ($nombre_asegurado) {
            return $query->where('nombreasegurado', 'LIKE', '%'.$nombre_asegurado.'%');
        }
    }
    public function scopeApellidoasegurado($query, $apellido_asegurado){
        if ($apellido_asegurado) {
            return $query->where('apellidoasegurado', 'LIKE', '%'.$apellido_asegurado.'%');
        }
    }
    public function scopeEmail($query, $email){
        if ($email) {
            return $query->where('email', 'LIKE', '%'.$email.'%');
        }
    }

    public function scopeMarcacliente($query, $marca_cliente){
        if ($marca_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($marca_cliente){
                $q->where('marca',$marca_cliente);
            });
        }
    }

}
