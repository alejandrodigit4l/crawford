<?php

namespace App;

use App\SegurosMundialUsers;
use Illuminate\Database\Eloquent\Model;

class SegurosMundialReclamos extends Model
{
    protected $connection = 's_mundial';
    protected $table = 'reclamos';

    // ***** Inicio relaciones ***** //

    public function datos_cliente(){
        return $this->belongsTo(SegurosMundialUsers::class, 'cliente' , 'id');
    }

    public function datos_estado_poliza(){
        return $this->belongsTo(Estados_polizas::class, 'estados_poliza', 'id');
    }

    // ***** Fin relaciones ***** //



    // ***** Inicio Query Scopes ***** //

    public function scopeId($query, $id){
        if ($id) {
            return $query->where('id', 'LIKE', '%'.$id.'%');
        }
    }
    public function scopeCedula($query, $cedula){
        if ($cedula) {
            return $query->where('cedula', 'LIKE', '%'.$cedula.'%');
        }
    }
    public function scopeNombreasegurado($query, $nombre_asegurado){
        if ($nombre_asegurado) {
            return $query->where('nombreasegurado', 'LIKE', '%'.$nombre_asegurado.'%');
        }
    }
    public function scopeApellidoasegurado($query, $apellido_asegurado){
        if ($apellido_asegurado) {
            return $query->where('apellidoasegurado', 'LIKE', '%'.$apellido_asegurado.'%');
        }
    }
    public function scopeEmail($query, $email){
        if ($email) {
            return $query->where('email', 'LIKE', '%'.$email.'%');
        }
    }

    // ***** Fin Query Scopes ***** //
    
}
