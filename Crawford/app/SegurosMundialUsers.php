<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegurosMundialUsers extends Model
{
    protected $connection = 's_mundial';
    protected $table = 'users';

    protected $fillable = [
        'name','apellidos', 'email','cedula','celular','fecha_nacimiento','password','estado'
    ];
}
