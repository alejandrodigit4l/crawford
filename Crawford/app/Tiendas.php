<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiendas extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nombre', 'estado', 'icono', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
    ];
}
