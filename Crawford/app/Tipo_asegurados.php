<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_asegurados extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nombre', 'descripcion', 'logo', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
    ];
}
