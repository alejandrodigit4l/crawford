<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_polizas extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nombre', 'descripcion', 'formulario', 'asegurado', 'tienda', 'campana', 'aseguradora', 'tipo_seguro', 'marca',
    ];
}
