<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_seguros extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nombre', 'descripcion', 'marca',
    ];
}
