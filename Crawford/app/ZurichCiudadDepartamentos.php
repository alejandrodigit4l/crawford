<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZurichCiudadDepartamentos extends Model
{
    protected $fillable = [
    	'nombre', 'estado', 'idDepartamento'
    ];

    public function departamento(){
    	return $this->belongsTo(ZurichDepartamentos::class, 'idDepartamento', 'id');
    }

}
