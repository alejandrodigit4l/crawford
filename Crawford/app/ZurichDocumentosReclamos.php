<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZurichDocumentosReclamos extends Model
{
    protected $fillable = [
    	'id_reclamo',
    	'id_tipo_checklist',
    	'nombre_documento',
    	'valor'
    ];
}
