<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZurichEstadosReclamos extends Model
{
    protected $fillable = [
    	'nombre_estado'
    ];
}
