<?php

namespace App;

use App\User;
use App\ZurichEstadosReclamos;
use Illuminate\Database\Eloquent\Model;

class ZurichHistorialEstados extends Model
{
	
    protected $fillable = [
    	'id_reclamo',
    	'id_estado',
    	'id_user'
    ];

    public function informacion_estado(){
    	return $this->belongsTo(ZurichEstadosReclamos::class, 'id_estado', 'id');
    }

    public function informacion_usuario(){
    	return $this->belongsTo(User::class, 'id_user', 'id');
    }

}
