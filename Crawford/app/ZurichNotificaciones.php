<?php

namespace App;

use App\ZurichReclamos;
use Illuminate\Database\Eloquent\Model;

class ZurichNotificaciones extends Model
{
    protected $fillable = [
    	'id_reclamo',
    	'nombre_adjunto',
    	'mensaje'
    ];

    public function reclamo(){
    	return $this->belongsTo(ZurichReclamos::class, 'id_reclamo', 'id');
    }

}
