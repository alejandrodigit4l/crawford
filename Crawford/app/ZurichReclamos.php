<?php

namespace App;

use App\User;
use App\Clientes;
use App\ListaTiendas;
use App\ListaProductos;
use App\ZurichEstadosReclamos;
use App\ZurichHistorialEstados;
use App\ciudades_departamentos;
use App\ZurichDocumentosReclamos;
use Illuminate\Database\Eloquent\Model;

class ZurichReclamos extends Model
{
    protected $fillable = [
        'id_cliente',
        'ciudadrecidencia',
        'telefonofijo',
        'celular',
        'direccion',
        'telefonolaboral',
        'tipo_producto',
        'marca_equipo',
        'modelo_equipo',
        'imei',
        'numero_factura',
        'precio_equipo',
        'fecha_compra',
        'tienda',
        'fecha_asignacion_cst',
        'nombre_cst',
        'fecha_ingreso_cst',
        'fecha_diagnostico',
        'tiempo_reparacion',
        'causal',
        'valor_indemnizado',
        'fecha_notificacion',
        'fechasiniestro',
        'horasiniestro',
        'descripcionsiniestro',
        'motivobaja',
        'cliente',
        'estado_reclamo',
        'ciudadsiniestro',
        'sponsor',
        'plan_poliza',
        'tipo_siniestro',
        'certificado',
        'radicado_zurich',
        'caso_reapertura',
        'caso_investigado',
        'salvamento',
        'valor_reparacion',
        'fecha_envio_instructivo',
        'fecha_aviso',
        'fecha_recepcion_documentos',
        'fecha_recep_doc_completos',
        'hora_recepcion_documentos',
        'fecha_entrega_ajuste',
        'fecha_ajuste',
        'observaciones',
        'sitio_creacion',
        'usuario_creador'
    ];

    // RELACIONES
    public function datos_cliente(){
        return $this->belongsTo(Clientes::class, 'id_cliente' , 'id');
    }

    public function tienda_reclamo(){
        return $this->belongsTo(ListaTiendas::class, 'tienda', 'id');
    }

    public function estado_del_reclamo(){
        return $this->belongsTo(ZurichEstadosReclamos::class, 'estado_reclamo', 'id');
    }

    public function ciudad_residencia(){
        return $this->belongsTo(ZurichCiudadDepartamentos::class, 'ciudadrecidencia', 'id');
    }

    public function tipo_producto_reclamo(){
        return $this->belongsTo(ListaProductos::class, 'tipo_producto', 'id');
    }

    public function marca_producto_reclamo(){
        return $this->belongsTo(ListaMarcas::class, 'marca_equipo', 'id');
    }

    public function ciudad_siniestro(){
        return $this->belongsTo(ZurichCiudadDepartamentos::class, 'ciudadsiniestro', 'id');
    }

    public function historial_estados(){
        return $this->hasMany(ZurichHistorialEstados::class, 'id_reclamo', 'id');
    }

    public function documentos_reclamo(){
        return $this->hasMany(ZurichDocumentosReclamos::class, 'id_reclamo', 'id');
    }

    public function info_usuario_creador(){
        return $this->belongsTo(User::class, 'usuario_creador', 'id');
    }



    // QUERY SCOPES
    public function scopeMarcacliente($query, $marca_cliente){
        if ($marca_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($marca_cliente){
                $q->where('marca',$marca_cliente);
            });
        }
    }

    public function scopeNombrecliente($query, $nombre_cliente){
        if ($nombre_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($nombre_cliente) {
                $q->where('nombre', 'LIKE', '%'.$nombre_cliente.'%');
            });
        }
    }

    public function scopeApellidocliente($query, $apellido_cliente){
        if ($apellido_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($apellido_cliente){
                $q->where('lastname', 'LIKE', '%'.$apellido_cliente.'%');
            });
        }
    }

    public function scopeEmailcliente($query, $email_cliente){
        if ($email_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($email_cliente){
                $q->where('email', 'LIKE', '%'.$email_cliente.'%');
            });
        }
    }

    public function scopeCedulacliente($query, $cedula_cliente){
        if ($cedula_cliente) {
            return $query->whereHas('datos_cliente', function($q) use ($cedula_cliente){
                $q->where('cedula', 'LIKE', '%'.$cedula_cliente.'%');
            });
        }
    }

    public function scopeEstadoReclamo($query, $estado_reclamo){
        if ($estado_reclamo) {
            return $query->where('estado_reclamo', $estado_reclamo);
        }
    }

    public function scopeId($query, $id){
        if ($id) {
            return $query->where('id', 'LIKE', '%'.$id.'%');
        }
    }




}
