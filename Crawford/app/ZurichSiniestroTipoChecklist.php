<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZurichSiniestroTipoChecklist extends Model
{
    protected $fillable = [
    	'id_tipo_checklist',
    	'id_marca',
    	'id_aseguradora',
    	'tipo_siniestro'
    ];
}
