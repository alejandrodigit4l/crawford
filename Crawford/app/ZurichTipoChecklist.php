<?php

namespace App;

use App\ZurichDocumentosReclamos;
use Illuminate\Database\Eloquent\Model;

class ZurichTipoChecklist extends Model
{
    protected $fillable = [
    	'nombre_checklist',
    	'descripcion',
    	'tipo'
    ];

    public function documentos(){
    	return $this->hasMany(ZurichDocumentosReclamos::class, 'id_tipo_checklist' , 'id');
    }

}
