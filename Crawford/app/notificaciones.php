<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notificaciones extends Model
{
    //
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $fillable = [	
		'notyid','fecha','texto','estado','cliente','reclamo','marca',
	];
}
