<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListchekingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listchekings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('img');
            $table->string('titulo');
            $table->string('descripcion');
            $table->string('tipo'); //camera, video, audio, archivo
            $table->string('subdescripcion');
            $table->text('ayuda');
            $table->integer('tipo_polizas');
            $table->integer('asegurado');
            $table->integer('marca');
            $table->integer('tipo_seguro');
            $table->integer('aseguradora');
            $table->integer('campana');
            $table->integer('tienda');
            $table->integer('reclamo'); //este es unicamente para agreagar una lista de chequeo a partir de un reclamo 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listchekings');
    }
}
