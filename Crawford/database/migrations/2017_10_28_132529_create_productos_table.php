<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('descripcion');
            $table->string('cubrimiento');
            $table->double('valor', 20, 4);
            $table->string('urlicono');
            $table->string('referencia');
            $table->integer('tipo_poliza');
            $table->integer('marca');
            $table->integer('tipo_seguro');
            $table->integer('aseguradora');
            $table->integer('campana');
            $table->integer('tienda');
            $table->integer('asegurado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
