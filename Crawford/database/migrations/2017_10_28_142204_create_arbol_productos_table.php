<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArbolProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbol_productos', function (Blueprint $table) {
            $table->increments('id');            
            $table->text('json');
            $table->string('nombre');
            $table->integer('administrador');
            $table->integer('productos_id');
            $table->integer('marca');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arbol_productos');
    }
}
