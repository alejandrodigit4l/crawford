<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReclamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reclamos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reclamoid');
            $table->string('cedula');
            $table->string('nombreasegurado');
            $table->string('apellidoasegurado');
            $table->string('fechanacimientoasse');
            $table->integer('ciudadrecidencia');
            $table->string('telefonofijo');
            $table->string('celular');
            $table->string('direccion');
            $table->string('email');
            $table->string('telefonolaboral');
            $table->text('jsonrespuestaform');
            $table->string('fechasiniestro');
            $table->string('horasiniestro');
            $table->text('descripcionsiniestro');
            $table->text('textobackend');
            $table->text('observaciones');
            $table->text('motivobaja');
            $table->integer('cliente');
            $table->integer('estados_poliza');
            $table->integer('producto');
            $table->integer('ciudadsiniestro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reclamos');
    }
}
