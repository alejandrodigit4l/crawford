<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePyuTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payu_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('email');
            $table->string('cedula');
            $table->string('marca');
            $table->integer('producto');
            $table->string('titulo');
            $table->string('valor');
            $table->string('solicitud_payu');
            $table->string('valor_payu');
            $table->string('payer_name');
            $table->string('payer_email');
            $table->string('payer_dni');
            $table->string('payerment_method');
            $table->string('code_payu');
            $table->string('order_id');
            $table->string('transactionid');
            $table->string('state');
            $table->string('responsecode');
            $table->string('fecha_creacion');
            $table->string('fecha_vencimiento');
            $table->string('serial');
            $table->string('poliza');
            $table->string('enviado');
            $table->string('url');
            $table->integer('clientes_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payu_transactions');
    }
}
