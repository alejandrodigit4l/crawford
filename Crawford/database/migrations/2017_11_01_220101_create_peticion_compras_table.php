<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeticionComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peticion_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marca');
            $table->string('usuario');
            $table->string('src');
            $table->string('estado');
            $table->string('mensaje');
            $table->string('serial');
            $table->integer('clientes_id');
            $table->integer('productos_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peticion_compras');
    }
}
