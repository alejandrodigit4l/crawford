<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolizasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polizas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date_start');
            $table->string('date_end');
            $table->string('days_on_period');
            $table->string('date_certificate_start');
            $table->string('date_certificate_end');
            $table->string('name_user');
            $table->string('city');
            $table->string('name_taker');
            $table->string('city_taker');
            $table->string('id_taker');
            $table->string('price');
            $table->string('takes');
            $table->string('price_and_takes');
            $table->string('email');
            $table->string('name_product');
            $table->string('url');
            $table->string('serie');
            $table->string('consecutivo');
            $table->string('cobertura');
            $table->string('deducible');
            $table->integer('producto');
            $table->integer('cliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polizas');
    }
}
