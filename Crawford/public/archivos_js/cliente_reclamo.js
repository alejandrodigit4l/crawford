$("select[name='dep_residencia']").change(function(){
    reclamo.traer_ciudades_residencia($('#dep_residencia').val(), $('#marca_cliente').val());
});
$("select[name='dep_siniestro']").change(function(){
    reclamo.traer_ciudades_siniestro($('#dep_siniestro').val(), $('#marca_cliente').val());
});
let reclamo = new Reclamo();
function Reclamo(){
    this.traer_ciudades_residencia = function(id_departamento, marca){
        $('.option_ciudad_residencia').remove();
        $.ajax({
            url: `/ciudades/show_ciudades/${id_departamento}/${marca}`,
            type: 'get',
            success: function(res){
                for(let i in res.body){
                    $('#ciudad_residencia').append(`<option class="option_ciudad_residencia" value="${res.body[i].id}">${res.body[i].nombre}</option>`);
                }
            },
            error: function(e){
                console.log(e);
            }
        });
    };
    this.traer_ciudades_siniestro = function(id_departamento, marca){
        $('.option_ciudad_siniestro').remove();
        $.ajax({
            url: `/ciudades/show_ciudades/${id_departamento}/${marca}`,
            type: 'get',
            success: function(res){
                for(let i in res.body){
                    $('#ciudad_siniestro').append(`<option class="option_ciudad_siniestro" value="${res.body[i].id}">${res.body[i].nombre}</option>`);
                }
            },
            error: function(e){
                console.log(e);
            }
        });
    };  
    this.enviar_formulario = function(){
        $('#enviar_formulario').attr("disabled", true);
        let url = '/reclamos/create_admin';
        if ($('#marca_cliente').val() == 5) {
            url = '/reclamos/create_admin_zurich';
        }
        $.ajax({
            data:{
                'id_cliente': $('#id_cliente').val(),
                'tel_fijo': $('#tel_fijo').val(),
                'celular': $('#celular').val(),
                'tel_laboral': $('#tel_laboral').val(),
                'dep_residencia': $('#dep_residencia').val(),
                'ciudad_residencia': $('#ciudad_residencia').val(),
                'direccion': $('#direccion').val(),
                'sponsor': $('#sponsor').val(),
                'plan_poliza': $('#plan_poliza').val(),
                'fecha_compra': $('#fecha_compra').val(),
                'tipo_producto': $('#tipo_producto').val(),
                'marca_equipo': $('#marca_equipo').val(),
                'serial_equipo': $('#serial_equipo').val(),
                'fecha_siniestro': $('#fecha_siniestro').val(),
                'hora_siniestro': $('#hora_siniestro').val(),
                'dep_siniestro': $('#dep_siniestro').val(),
                'ciudad_siniestro': $('#ciudad_siniestro').val(),
                'text_siniestro': $('#text_siniestro').val(),
                'tipo_siniestro': $('#tipo_siniestro').val(),
                'numero_factura': $('#numero_factura').val(),
                'modelo_equipo': $('#modelo_equipo').val(),
                'precio_equipo': $('#precio_equipo').val(),
                'tienda': $('#tienda').val(),
                'fecha_asignacion_cst': $('#fecha_asignacion_cst').val(),
                'nombre_cst': $('#nombre_cst').val(),
                'fecha_ingreso_cst': $('#fecha_ingreso_cst').val(),
                'fecha_diagnostico': $('#fecha_diagnostico').val(),
                'tiempo_reparacion': $('#tiempo_reparacion').val(),
                'causal': $('#causal').val(),
                'valor_indemnizado': $('#valor_indemnizado').val(),
                'fecha_notificacion': $('#fecha_notificacion').val(),
                'certificado' : $('#certificado').val(),
                'fecha_aviso': $('#fecha_aviso').val(),
                'radicado_zurich' : $('#radicado_zurich').val(),
                'observaciones': $('#observaciones').val()
            },
            url: url,
            type: 'post',
            success: function(res){
                swal({
                    title: 'Se ha creado el reclamo correctamente!',
                    type: "success",
                    showLoaderOnConfirm: true,
                },function(){
                    if ($('#marca_cliente').val() == '5') {
                        location.href = `/crawford/idreclamo_zurich/${res.id_reclamo}`;  
                    }else{
                        location.href = `/crawford/idreclamo/${res.id_reclamo}`;
                    }
                });
            },
            error: function(e){
                $('#enviar_formulario').removeAttr("disabled", true);
                console.log(e);
                if (e.status == 422) {
                    for(var i in e.responseJSON){
                        $.notify({
                            icon: "pe-7s-look",
                            message: e.responseJSON[i]
                        },{
                            type: 'danger',
                            timer: 200,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });
                    }
                }
            }
        });
    };
    this.validar_factura = function(){
        $.ajax({
            url: `/cliente/verificar_factura/${$('#numero_factura').val()}`,
            type: 'get',
            success: function(res){
                if (res.cantidad_reclamos > 0 ) {
                    let tbody = '<tbody>';
                    for( let i  in res.reclamos_por_factura ){
                        tbody += `<tr>
                                    <td>${res.reclamos_por_factura[i].tipo_producto_reclamo.nombre_producto}</td>
                                    <td>${res.reclamos_por_factura[i].marca_producto_reclamo.nombre_marca}</td>
                                    <td>${res.reclamos_por_factura[i].modelo_equipo}</td>
                                    <td>${res.reclamos_por_factura[i].certificado}</td>
                                    <td>${res.reclamos_por_factura[i].datos_cliente.nombre} ${res.reclamos_por_factura[i].datos_cliente.lastname}</td>
                                    <td>${res.reclamos_por_factura[i].tipo_siniestro}</td>
                                  </tr>`;
                    }
                    tbody += '</tbody>';
                    swal({
                      title: '<h5>Se encontraron productos con reclamo relacionados a la factura:</h5>',
                      text: `
                            <div class="container" id="cargar_imagen" style="width: 100%;overflow-y: scroll; overflow-x: scroll;">
                                <table class="table table-hover table-striped">
                                <thead>
                                  <th>Tipo producto</th>
                                  <th>Marca</th>
                                  <th>Modelo</th>
                                  <th>Certificado</th>
                                  <th>Encargado</th>
                                  <th>Evento</th>
                                <thead>
                                ${tbody}
                               </table>
                            </div>
                          `,
                      customClass: 'swal-wide',
                      showConfirmButton: true,
                      html: true
                    })
                }
            }
        });
    };
}