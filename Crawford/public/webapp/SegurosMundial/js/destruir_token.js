var token = new Token();
function Token(){
	this.destruir = function(){
		var token_ = JSON.parse(sessionStorage.getItem('token_'));
        var email_cliente = JSON.parse(sessionStorage.getItem('cliente_')).email;
        var marca_cliente = JSON.parse(sessionStorage.getItem('cliente_')).marca.id;
        $.ajax({
            data:{
                'marca_cliente': marca_cliente,
                'email_cliente': email_cliente,
                'token_': token_
            },
            url: config.url+'/clientes/destruir_token',
            type: 'post',
            success: function(response){
                sessionStorage.clear();
                location.href = config.url+'/webapp/SegurosMundial/index.html';
            }
        });
	};
}