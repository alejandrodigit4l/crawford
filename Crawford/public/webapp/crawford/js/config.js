/***************************************************************
*
*	CONFIGURACION DEL TIPO DE APLICACION QUE ES
*	Se debe configurar lo inicial, marcas, tipo_seguro, todas
*   estas variables con el fin de ponerlas automaticamente.
*
****************************************************************/

var config = {
	marcas: {
		id: 1,
		nombre: "CRAWFORD",
		tipo_seguros: [
			{
				id: 3,
				nombre: "CORRIENTE DEBIL",
				aseguradoras: [
					{
						id: 1,
						nombre: "SEGUROS MUNDIAL",
					},
				],
			},
		],
	},
	url: "https://admin.crawfordaffinitycolombia.com",
	//url: "http://localhost:8000",
};

