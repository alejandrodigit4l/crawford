/***************************************************************
*
*	CONFIGURACION DEL TIPO DE APLICACION QUE ES
*	Se debe configurar lo inicial, marcas, tipo_seguro, todas
*   estas variables con el fin de ponerlas automaticamente.
*
****************************************************************/

var config = {
	marcas: {
		id: 2,
		nombre: "FALABELLA",
		tipo_seguros: [
			{
				id: 2,
				nombre: "CORRIENTE DEBIL",
				aseguradoras: [
					{
						id: 2,
						nombre: "FALABELLA",
					},
				],
			},
		],
	},
	url: "https://admin.crawfordaffinitycolombia.com",
	// url: "http://localhost:8000",
	// url: "http://localhost:8080",

};

