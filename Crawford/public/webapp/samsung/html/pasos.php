<!DOCTYPE html>
<html style="height:676px;">

<script type="text/javascript">
    var menu = new Inicio(); 
    menu.iniciar();
    function Inicio(){
        this.iniciar = function(){
            if(sessionStorage.length == 0 || sessionStorage.getItem('cliente_') == null){ //no esta vacio
                location.href = '/webapp/samsung/index.html';
            }else if(JSON.parse(sessionStorage.getItem('cliente_')).marca.id != 4){
                location.href = '/webapp/samsung/index.html';
            }
            console.log(sessionStorage.length);
        };
    }
</script>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <title>Samsung-app</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="../assets/css/Footer-Basic.css">
    <link rel="stylesheet" href="../assets/css/Header-Blue.css">
    <link rel="stylesheet" href="../assets/css/Navigation-Clean1.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/config.js"></script>
    <script src="../js/destruir_token.js"></script>
    <script src="../js/md5.pack.js"></script>
</head>

<body>
    <div>
        <nav class="navbar navbar-inverse navbar-static-top navigation-clean" data-color="samsung" id="navegador">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand navbar-link" href="#" style="height:100px;margin:0px;padding:0px;"><img src="../assets/img/samsung_club_web.svg" style="height:50px;margin:0px;margin-top:13px;margin-right:0px;margin-bottom:0px;margin-left:10px;"></a>
                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li role="presentation">
                            <a href="panel.html" style="width:80px;"><img src="../assets/img/inicio-blanco.svg" style="width:30px;margin-right:10px;margin-left:14px;margin-bottom:2px;">
                            <h6 class="text-center" style="margin:0px;width:58px;color: white">Inicio</h6></a>
                        </li>
                        <li role="presentation">
                            <a href="reclamos.html" style="width:70px;"><img src="../assets/img/reclamos-blanco.svg" style="width:30px;margin-left:11px;margin-right:7px;margin-bottom:2px;">
                            <h6 class="text-center" style="margin:0px;width:58px;color: white;">Reclamar </h6></a>
                        </li>
                        <li role="presentation">
                            <a href="ayuda.html" style="width:69px;"><img src="../assets/img/ayuda-blanco.svg" style="width:30px;margin-right:0px;margin-left:14px;margin-bottom:2px;">
                            <h6 class="text-center" style="margin:0px;height:12px;width:58px;color: white;">Ayuda</h6></a>
                        </li>
                        <!-- <li role="presentation">
                            <a href="cerrar.html" style="width:69px;"><img src="../assets/img/cerrar-blanco.svg" style="width:30px;margin-right:0px;margin-left:14px;margin-bottom:2px;">
                            <h6 class="text-center" style="margin:0px;height:12px;width:58px;color: white;">Cerrar</h6></a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </nav>
    </div>

	<div>
        <!--<header style="margin-top:20px;"><img src="../assets/img/logo-crawford.svg" style="margin-right:0px;margin-left:0px;height:90px;width:100%;margin-bottom:0px;margin-top:0px;"></header>-->

        <!-- Button trigger modal -->
        <center><img src="../assets/img/seguros_mundial.png" style="width: 160px;"></center>
        <br><br>
			<div class="container">
				<h4>Has cargado correctamente tus documentos</h4><br><br>

			Tu número de caso con el que podrás hacer seguimiento es el No. <?php echo $_GET['reclamo'];?>. Por favor conserva este número ya que lo necesitarás en tu proceso de reclamación.<br><br>

			Los pasos a seguir son:<br><br>
			paso #1 Recibirás un correo indicandote el éxito del cargue<br>
			paso #2 Procederemos a revisar tu documentación<br>
			paso #3 Te recordamos que el tiempo aproximado de atención del siniestro una vez recibamos toda la documentación, y esta esté completa, correcta y legible es de quince (15) días hábiles”<br><br><br>
			<a href="reclamos.html" class="btn btn-primary" style="width: 100%;">Continuar</a>
            </div>
            


    </div>

    <div class="footer-basic" data-color="samsung">
        <footer style="margin-top:0px;margin-bottom:0px;">
            <br><br><p class="text-center copyright" style="margin-top:150px;">Crawford Affinity Colombia 2017 inspired by Digit4l © Todos los derechos reservados</p>
        </footer>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>


<script type="text/javascript">
var cerrar = new Cerrar();

cerrar.init();

function Cerrar(){
	this.init = function(){
        setTimeout('session_time()',3600000);
		$('#cerrarbuton').trigger( "click" );
	};

	this.panel = function(){
		location.href = config.url+'/webapp/samsung/html/panel.html';
	};

	this.index = function(){
		sessionStorage.setItem("cliente_", '');
		location.href = config.url+'/webapp/samsung/index.html';
	};
}
function session_time(){
    token.destruir();
    sessionStorage.clear();
}
</script>