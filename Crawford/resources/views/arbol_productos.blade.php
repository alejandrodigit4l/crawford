@extends('crawford.layout')

@section('content')

	<h4>Arbol_productos </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="productos/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Arbol_productos</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            @foreach( $marcas as $marca)
                            <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                            @endforeach
                        </select>
                        <select name="productos_id" id="productos_id" class="form-control" >
                            <option value="">Tipo productos_id</option>
                        </select>
                        <select name="administrador" id="administrador" class="form-control" >
                            <option value="">Administrador que autoriza</option>
                            @foreach( $usuarios as $usuario)
                            <option value="{{ $usuario['id'] }}">{{ $usuario['username'] }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="json" class="form-control" id="json" placeholder="json" value="">
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="arbol_productos.save();">Guardar</div>
                        <div class="btn btn-success" onclick="arbol_productos.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                	<th>id</th>
                                    <th>json</th>
                                    <th>nombre</th>
                                    <th>administrador</th>
                                    <th>productos_id</th>
                                    <th>marca</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $arbol_productos as $producto )
                                <tr>
                                	<td>{{ $producto['id'] }}</td>
                                    <td>{{ $producto['json'] }}</td>
                                    <td>{{ $producto['nombre'] }}</td>
                                    <td>{{ $producto['administrador']['username'] }}</td>
                                    <td>{{ $producto['productos_id']['titulo'] }}</td>
                                    <td>{{ $producto['marca']['nombre'] }}</td>
                                    <td>{{ $producto['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="arbol_productos.update({{ $producto['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="arbol_productos.delete({{ $producto['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var arbol_productos = new Arbol_productos();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        arbol_productos.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    arbol_productos.productos_id($('#marca').val(),'#productos_id','productos_id');
});

function Arbol_productos(){

    this.save = function(){

        var json = this.validate('json');
        var nombre = this.validate('nombre');
        var administrador = this.validate('administrador');
        var productos_id = this.validate('productos_id');
        var marca = this.validate('marca');

        var parametrer = {
            'json' : json,
            'nombre' : nombre,
            'administrador' : administrador,
            'productos_id' : productos_id,
            'marca' : marca
        };

        $.ajax({
            url: 'arbol_productos/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'arbol_productos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var json = this.validate('json');
        var nombre = this.validate('nombre');
        var administrador = this.validate('administrador');
        var productos_id = this.validate('productos_id');
        var marca = this.validate('marca');

        var parametrer = {
            'id'    : id,
            'json' : json,
            'nombre' : nombre,
            'administrador' : administrador,
            'productos_id' : productos_id,
            'marca' : marca
        };

        $.ajax({
            url: 'arbol_productos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'arbol_productos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'arbol_productos/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'arbol_productos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'arbol_productos/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#json').val(response.body[0].json);
                    $('#nombre').val(response.body[0].nombre);
                    $('#administrador').val(response.body[0].administrador);
                    $('#productos_id').val(response.body[0].productos_id);
                    $('#marca').val(response.body[0].marca);         
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'arbol_productos/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.productos_id = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'productos/search/marca/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('PRODUCTOS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.titulo+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
@endsection