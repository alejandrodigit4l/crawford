@extends('crawford.layout')

@section('content')

	<h4>Chats </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Chats</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="message" class="form-control" id="message" style="color: black;" placeholder="message" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="state" class="form-control" id="state" style="color: black;" placeholder="state" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="sender" class="form-control" id="sender" style="color: black;" placeholder="sender" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="receptor" class="form-control" id="receptor" style="color: black;" placeholder="receptor" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="nombre" class="form-control" id="nombre" style="color: black;" placeholder="Nombre Estado" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripcion" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        <div class="btn btn-success" onclick="chats.save();">Guardar</div>
                        <div class="btn btn-success" onclick="chats.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>message</th>
                                    <th>state</th>
                                    <th>sender</th>
                                    <th>receptor</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $chats as $homologo )
                                <tr>
                                    <td>{{ $homologo['id'] }}</td>
                                    <td>{{ $homologo['message'] }}</td>
                                    <td>{{ $homologo['state'] }}</td>
                                    <td>{{ $homologo['sender'] }}</td>
                                    <td>{{ $homologo['receptor'] }}</td>
                                    <td><a href="javascript:;" onclick="chats.update({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="chats.delete({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var chats = new Chats();


function Chats(){

    this.save = function(){
        
        var message = this.validate('message');
        var state = this.validate('state');
        var sender = this.validate('sender');
        var receptor = this.validate('receptor');

        var parametrer = {
            'message': message,
            'state': state,
            'sender': sender,
            'receptor': receptor
        };

        $.ajax({
            url: 'chats/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'chats';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var message = this.validate('message');
        var state = this.validate('state');
        var sender = this.validate('sender');
        var receptor = this.validate('receptor');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'message': message,
            'state': state,
            'sender': sender,
            'receptor': receptor
        };

        $.ajax({
            url: 'chats/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'chats';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'chats/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'chats';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'chats/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#message').val(response.body[0].message);
                    $('#state').val(response.body[0].state);
                    $('#sender').val(response.body[0].sender);
                    $('#receptor').val(response.body[0].receptor);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'chats/'+DATA;
    }
}


</script>
	
@endsection