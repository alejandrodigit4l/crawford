@extends('crawford.layout')

@section('content')

	<h4>Ciudades </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Ciudades</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="nombre" class="form-control" id="nombre" style="color: black;" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="codeUno" class="form-control" id="codeUno" style="color: black;" placeholder="codeUno" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="codeDos" class="form-control" id="codeDos" style="color: black;" placeholder="codeDos" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="codeDian" id="codeDian" class="form-control" placeholder="codeDian" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        <div class="btn btn-success" onclick="ciudades.save();">Guardar</div>
                        <div class="btn btn-success" onclick="ciudades.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
									<th>codeDian</th>
									<th>codeUno</th>
									<th>codeDos</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $ciudades as $data )
                                <tr>
                                    <td>{{ $data['id'] }}</td>
                                    <td>{{ $data['nombre'] }}</td>
									<td>{{ $data['codeDian'] }}</td>
									<td>{{ $data['codeUno'] }}</td>
									<td>{{ $data['codeDos'] }}</td>
                                    <td><a href="javascript:;" onclick="ciudades.update({{ $data['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="ciudades.delete({{ $data['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var ciudades = new Ciudades();


function Ciudades(){

    this.save = function(){
    	var nombre = this.validate('nombre');
		var codeDian = this.validate('codeDian');
		var codeUno = this.validate('codeUno');
		var codeDos = this.validate('codeDos');

        var parametrer = {
            'nombre' : nombre,
            'codeDian' : codeDian,
            'codeUno' : codeUno,
            'codeDos' : codeDos
        };

        $.ajax({
            url: 'ciudades/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'ciudades';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
		var codeDian = this.validate('codeDian');
		var codeUno = this.validate('codeUno');
		var codeDos = this.validate('codeDos');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'codeDian' : codeDian,
            'codeUno' : codeUno,
            'codeDos' : codeDos
        };

        $.ajax({
            url: 'ciudades/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'ciudades';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'ciudades/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'ciudades';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'ciudades/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                	$('#nombre').val(response.body[0].nombre);
					$('#codeDian').val(response.body[0].codeDian);
					$('#codeUno').val(response.body[0].codeUno);
					$('#codeDos').val(response.body[0].codeDos);
                    $('#id').val(response.body[0].id);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'ciudades/'+DATA;
    }
}


</script>
	
@endsection