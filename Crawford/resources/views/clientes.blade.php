@extends('crawford.layout')

@section('content')

	<h4>Clientes </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 450px;">
            		<p>Formulario Usuarios</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-4">
                            <label>Seleccione la marca del cliente</label><br>
                            <select name="marca" id="marca" class="form-control" >
                                <option value="">Estado de la marca</option>
                                @foreach( $marcas as $marca)
                                <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label>Seleccione el tipo de seguro de este cliente</label><br>
                            <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                                <option value="">Tipo seguro</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label>Seleccione la aseguradora de este cliente</label><br>
                            <select name="aseguradora" id="aseguradora" class="form-control" >
                                <option value="">Aseguradora</option>
                            </select>
                        </div>
                        <input type="hidden" name="clientid" class="form-control" id="clientid" placeholder="clientid" value="dsadsadsa" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <div class="col-sm-4">
                            <label>Escribe los nombres del cliente</label><br>
                            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombres" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe los Apellidos del usuario</label><br>
                            <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Apellidos" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe el correo del usuario</label><br>
                            <input type="email_cliente" name="email_cliente" class="form-control" id="email_cliente" placeholder="correo del usuario" value="" >
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe la fecha de nacimiento del usuario</label><br>
                            <input type="date" name="fechanacimiento" class="form-control" id="fechanacimiento" placeholder="fechanacimiento" value="" >
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe la cédula del usuario</label><br>
                            <input type="text" name="cedula" class="form-control" id="cedula" placeholder="Cédula del usuario" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe la fehca de expedición del usuario</label><br>
                            <input type="date" name="fechaCC" class="form-control" id="fechaCC" placeholder="fechaCC" value="" >
                        </div>
                        <div class="col-sm-4">
                            <label>Escribe la contraseña del usuario</label><br>
                            <input type="password" name="password_cliente" class="form-control" id="password_cliente" placeholder="Contraseña del usuario">
                        </div>
                        <input type="hidden" name="tokenAPI" class="form-control" id="tokenAPI" placeholder="tokenAPI" value="dsadsa" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <div class="col-sm-4">
                            <label>Escribe el celular del usuario</label><br>
                            <input type="text" name="celular" class="form-control" id="celular" placeholder="celular" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-4">
                            <br><br><br><br>
                        </div>
                        <br>
                        {{-- <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="clientes.save();">Guardar</div>
                        </div> --}}
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="clientes.sendUpdate();">Actualizar</div>
                        </div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    {{-- <th>ID</th> --}}
									<th>nombre</th>
									<th>lastname</th>
									<th>email</th>
									<th>cedula</th>
									{{-- <th>password</th> --}}
									<th>celular</th>
									<th>fechanacimiento</th>
									{{-- <th>fechaCC</th> --}}
									<th>marca</th>
									{{-- <th>tipo_seguro</th> --}}
									<th>aseguradora</th>                              
                                    {{-- <th>fecha creado</th> --}}
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $clientes as $usuario )
                                <tr>
                                    {{-- <td>{{ $usuario['id'] }}</td> --}}
									<td>{{ $usuario['nombre'] }}</td>
									<td>{{ $usuario['lastname'] }}</td>
									<td>{{ $usuario['email'] }}</td>
									<td>{{ $usuario['cedula'] }}</td>
									{{-- <td>{{ $usuario['password'] }}</td> --}}
									<td>{{ $usuario['celular'] }}</td>
									<td>{{ $usuario['fechanacimiento'] }}</td>
									{{-- <td>{{ $usuario['fechaCC'] }}</td> --}}
									<td>{{ $usuario['marca']['nombre'] }}</td>
									{{-- <td>{{ $usuario['tipo_seguro']['nombre'] }}</td> --}}
									<td>{{ $usuario['aseguradora']['nombre'] }}</td>
                                    {{-- <td>{{ $usuario['created_at'] }}</td> --}}
                                    <td><a href="javascript:;" onclick="clientes.update({{ $usuario['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="clientes.delete({{ $usuario['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var clientes = new Clientes();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        clientes.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    clientes.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    clientes.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});


$('#searchestado').on('change', function() {
    clientes.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

$('#actualizar').hide();
$('#guardar').show();

function Clientes(){

    this.save = function(){
    	var clientid = this.validate('clientid');
		var nombre = this.validate('nombre');
		var lastname = this.validate('lastname');
		var email = this.validate('email_cliente');
		var cedula = this.validate('cedula');
		var password = this.validate('password_cliente');
		var tokenAPI = this.validate('tokenAPI');
		var celular = this.validate('celular');
		var fechanacimiento = this.validate('fechanacimiento');
		var fechaCC = this.validate('fechaCC');
		var marca = this.validate('marca');
		var tipo_seguro = this.validate('tipo_seguro');
		var aseguradora = this.validate('aseguradora');

        var parametrer = {
        	'clientid' : clientid,
			'nombre' : nombre,
			'lastname' : lastname,
			'email' : email,
			'cedula' : cedula,
			'password' : password,
			'tokenAPI' : tokenAPI,
			'celular' : celular,
			'fechanacimiento' : fechanacimiento,
			'fechaCC' : fechaCC,
			'marca' : marca,
			'tipo_seguro' : tipo_seguro,
			'aseguradora' : aseguradora
        };

        $.ajax({
            url: 'clientes/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        // var clientid = this.validate('clientid');
		var nombre = this.validate('nombre');
		var lastname = this.validate('lastname');
		var email = this.validate('email_cliente');
		var cedula = this.validate('cedula');
		var password = this.validate('password_cliente');
		// var tokenAPI = this.validate('tokenAPI');
		var celular = this.validate('celular');
		// var fechanacimiento = this.validate('fechanacimiento');
		// var fechaCC = this.validate('fechaCC');
		// var marca = this.validate('marca');
		// var tipo_seguro = this.validate('tipo_seguro');
		// var aseguradora = this.validate('aseguradora');  

        var parametrer = {
            'id'    : id,
            // 'clientid' : clientid,
			'nombre' : nombre,
			'lastname' : lastname,
			'email' : email,
			'cedula' : cedula,
			'password' : password,
			// 'tokenAPI' : tokenAPI,
			'celular' : celular
			// 'fechanacimiento' : fechanacimiento,
			// 'fechaCC' : fechaCC,
			// 'marca' : marca,
			// 'tipo_seguro' : tipo_seguro,
			// 'aseguradora' : aseguradora
        };

        $.ajax({
            url: '/clientes/update_admin', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                if(response.Status == 'successful'){
                    console.log(response);
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'clientes/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'clientes/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                	$('#clientid').val(response.body[0].clientid);
					$('#nombre').val(response.body[0].nombre);
					$('#lastname').val(response.body[0].lastname);
					$('#email_cliente').val(response.body[0].email);
					$('#cedula').val(response.body[0].cedula);
					// $('#password').val(response.body[0].password);
					$('#tokenAPI').val(response.body[0].tokenAPI);
					$('#celular').val(response.body[0].celular);
					$('#fechanacimiento').val(response.body[0].fechanacimiento);
					$('#fechaCC').val(response.body[0].fechaCC);
					$('#marca').val(response.body[0].marca.id);
                    clientes.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
					$('#tipo_seguro').val(response.body[0].tipo_seguro.id);
                    clientes.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
					$('#aseguradora').val(response.body[0].aseguradora.id);
                    clientes.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'clientes/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
@endsection