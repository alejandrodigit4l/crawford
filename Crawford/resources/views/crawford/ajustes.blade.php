@extends('crawford.layout')

@section('content')

	<h4>Ajustes </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Escoje la base de datos a modificar</p>            		
            	</div>
            </div>
            <div class="col-md-2" id="tipo_polizas">
                <a href="/tipo_polizas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>1. tipo_polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="listchekings">
                <a href="/listchekings" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>2. listchekings</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="productos">
                <a href="/productos" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>3. productos</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="roles">
                <a href="/roles" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>4.roles</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="usuarios">
                <a href="/usuarios" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>5. usuarios</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="homologos">
                <a href="/homologos" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>6.homologos </p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="estados_polizas">
                <a href="/estados_polizas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>7.estados polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="clientes2">
                <a href="/clientes" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>8. clientes</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="ciudades">
                <a href="/ciudades" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>9. ciudades</p>                 
                </div></a>
            </div>
                     
           

        </div>
    </div>
</div>

<style type="text/css">
	.data{
		color: gray;
	}
	.data:hover{
		color: black;
	}
</style>

<script type="text/javascript">

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#ajustes').addClass('active');
});

var menu = new Menu();
menu.init();

function Menu(){
    this.init = function(){
        /*-- ocultar elementos --*/
        $('#tipo_polizas').hide();
        $('#listchekings').hide();
        $('#productos').hide();
        $('#roles').hide();
        $('#usuarios').hide();
        $('#homologos').hide();
        $('#estados_polizas').hide();
        $('#clientes2').hide();
        $('#ciudades').hide();
        /*-- traer datos de sesion --*/
        var mainMenu = sessionStorage.getItem('rol').split(",");
        console.log('MENU');
        console.log(mainMenu);

        for (var i = mainMenu.length - 1; i >= 0; i--) {
            if(mainMenu[i]=='tipo_polizas'){
                $('#tipo_polizas').show();
            }
            if(mainMenu[i]=='listchekings'){
                $('#listchekings').show();
            }
            if(mainMenu[i]=='productos'){
                $('#productos').show();
            }
            if(mainMenu[i]=='roles'){
                $('#roles').show();
            }
            if(mainMenu[i]=='usuarios'){
                $('#usuarios').show();
            }
            if(mainMenu[i]=='homologos'){
                $('#homologos').show();
            }
            if(mainMenu[i]=='estados_polizas'){
                $('#estados_polizas').show();
            }
            if(mainMenu[i]=='clientes2'){
                $('#clientes2').show();
            }
            if(mainMenu[i]=='ciudades'){
                $('#ciudades').show();
            }
        }

    };
}

</script>
	
@endsection