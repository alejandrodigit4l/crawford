@extends('crawford.layout')

@section('content')


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-3">
            	<div class="card" data-color="crawford" style="overflow-y: scroll;height: 550px;">
                    <h4>Contactos</h4>
                    <div id="chatestado">Estado Chat: </div>
                    @foreach( $chats as $usuario )
                        @if( $usuario['estado'] == 'LEIDO' )

                        <pre>{{ $usuario['estado']}}</pre>
                        <a href="/chats/backend/{{ $usuario['sender']['id'] }}"><label class="btn-success form-control" onclick="chats.actualizar({{ $usuario['sender']['id'] }})">{{ $usuario['sender']['nombre'] }} {{ $usuario['sender']['lastname'] }} - {{ $usuario['sender']['cedula'] }}</label></a>
                        <strong style="background-color: red; position: relative;left: 90%;top: -38px;"> {{ $usuario['cnt'] }}</strong>

                        @elseif( $usuario['estado'] == 'ENVIADO' )

                        <pre>{{ $usuario['estado']}}</pre>
                        <a href="/chats/backend/{{ $usuario['sender']['id'] }}"><label class="btn-success form-control" onclick="chats.actualizar({{ $usuario['sender']['id'] }})">{{ $usuario['sender']['nombre'] }} {{ $usuario['sender']['lastname'] }} - {{ $usuario['sender']['cedula'] }}</label></a>
                        <strong style="background-color: green; position: relative;left: 90%;top: -38px;"> {{ $usuario['cnt'] }}</strong>

                        @endif
                    @endforeach
            	</div>
            </div>


            <div class="col-md-5">
                <div class="card" id="respuestascroll" data-color="crawford" style="overflow-y: scroll;height: 450px;">
                    <h4>Conversación</h4>
                    <div class="card" id="response" style="padding: 10px;">
                        ...
                    </div>
                </div>
                <input type="hidden" name="receptortext" id="receptortext" value="{{ $desde['id'] }}">
                <input id="message" class="form-control" style="height: 90px;width: 100%; position: relative;top: -20px;" placeholder="Escribe el mensaje">
            </div>

            <div class="col-md-4" id="open">
                <div class="card" style="padding: 20px;">
                    <h4>Abrir Chat CTRL</h4>
                    <button class="btn btn-success" style="width: 100%;" onclick="chats.abrirChat();"> ABRIR CHAT EN LA APP</button>
                </div>
            </div>

            <div class="col-md-4" id="close">
                <div class="card" style="padding: 20px;">
                    <h4>Cerrar Chat CTRL</h4>
                    <label>Mensaje cierre backend:</label>
                    <input type="text" name="mensajecierre" id="mensajecierre" class="form-control" placeholder="Escriba mensaje para cerrar el chat">
                    <span style="color: red;">Este mensaje unicamente se podra visualizar desde la aplicación.</span><br><br>
                    <button class="btn btn-danger" style="width: 100%;" onclick="chats.cerrarChat();"> CERRAR CHAT EN LA APP</button>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#chat').addClass('active');

});


var chats = new Chats();

chats.init();
chats.estadoChat();

$('#message').keypress(function(e) {
    if(e.which == 13) {
        chats.send();
    }
});

$('#respuestascroll').click(function(){
    console.log('Reload stop.....');
    chats.stop();
});

$('#message').click(function(){
    console.log('Reload start.....');
    chats.start();
});


function Chats(){

    var fila = 0;
    var intervalchat;

    this.init = function(){       
        chats.inicialchat();
        intervalchat = setInterval( "chats.historial()" , 3000);
        setInterval("chats.estadoChat()", 9000);
    }

    this.stop = function(){ //parar el intervalo del chat
         clearInterval(intervalchat);
    }

    this.start = function(){ //start el intervalo del chat
         intervalchat = setInterval( "chats.historial()" , 3000);
    }

    this.actualizar = function(id_sender){
        $.ajax({
            url: '/chats/cambio_estado/'+id_sender,
            type: 'GET',
            success: function(response){
                console.log(response);
            }
        });
    };

    this.send = function(){
        
        var message = this.validate('message');
        var state = 'ENVIADO';
        var sender = '1';//id cliente
        var receptor =  this.validate('receptortext'); //administrador

        var parametrer = {
            'message': message,
            'state': state,
            'sender': sender,
            'receptor': receptor
        };

        $.ajax({
            url: '/chats/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    //location.href = 'chats';
                    $('#message').val('');
                    chats.historial();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    /* HISTOIRAL DE CONVERSACIONES */
    this.historial = function(){
        $.ajax({
            url: '/chats/historial/'+$('#receptortext').val()+'/1', //This is the marca
            type: "GET",
            success: function(response){
                console.log('historial append:');
                console.log(response);
                //recorrer los mensajes y ponerlos en el container
                console.log('FILA:  '+fila);
                for (var i = response.body.length-1; i >= fila; i--) {
                    console.log('--------:');
                    console.log(response.body.length);
                    if(response.body[i].sender=='1'){//sender ya que es administrador
                        $('#response').append('<div class="card" style="background: #f6f6f6;padding: 10px;text-align: left;color:#4a4a4a">'+response.body[i].message+'<br><small style="font-size:9px">'+response.body[i].created_at+'</small></div>');
                    }
                    else{
                        $('#response').append('<div class="card" style="background: #d1e1eb;padding: 10px;text-align: right;color: #2b2b2b;">'+response.body[i].message+'<br><small style="font-size:9px">'+response.body[i].created_at+'</small></div>');
                    }                    
                }
                //fila funciona como referente de desde donde empieza a servir el chat
                fila = response.body.length;
            }
        }); 
        var altura =  fila * 100;
        $("#respuestascroll").animate({scrollTop:altura+"px"});
    };

    /* HISTOIRAL DE CONVERSACIONES */
    this.inicialchat = function(){
        $('#response').html('');
        $.ajax({
            url: '/chats/historial/'+$('#receptortext').val()+'/1', //This is the marca
            type: "GET",
            success: function(response){
                console.log('historial:');
                console.log(response);
                //fila funciona como referente de desde donde empieza a servir el chat
                fila = response.body.length;
                //recorrer los mensajes y ponerlos en el container
                for (var i = 0; i < response.body.length; i++) {
                    if(response.body[i].sender=='1'){//sender ya que es administrador
                        $('#response').append(`
                            <div class="card" style="background: #f6f6f6;padding: 10px;text-align: left;color:#4a4a4a">${response.body[i].message}<br><small style="font-size:9px">${response.body[i].created_at}</small></div>
                            `);
                    }
                    else{
                        $('#response').append(`
                            <div class="card" style="background: #d1e1eb;padding: 10px;text-align: right;color: #2b2b2b;">${response.body[i].message}<br><small style="font-size:9px">${response.body[i].created_at}</small></div>
                            `);
                    }                    
                }
            }
        }); 
        var altura =  fila * 100;
        $("#respuestascroll").animate({scrollTop:altura+"px"});
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.cerrarChat = function(){
        var mensajecierre = $('#mensajecierre').val();
        if(mensajecierre == ''){
            alert('Escribe el mensaje de cirre para el chat');
            exit();
        }
         var parametrer = {
            'mensajecierre' : mensajecierre,
            'estado'  : "DESHABILITADO",
            'usuario' : JSON.parse(sessionStorage.getItem('cliente')).id
        };
        $.ajax({
            url: '/utilchats/create', //This is the marca
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('create cerrar chat:');
                console.log(response);
                alert('El chat se apago');  
                $('#close').hide();
                $('#open').show();
            }
        }); 
    };

    this.abrirChat = function(){
         var parametrer = {
            'mensajecierre' : "Habilitado",
            'estado'  : "HABILITADO",
            'usuario' : JSON.parse(sessionStorage.getItem('cliente')).id
        };
        $.ajax({
            url: '/utilchats/create', //This is the marca
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('create Abrir chat:');
                console.log(response);
                alert('El chat se inicio');    
                $('#open').hide();
                $('#close').show();            
            }
        }); 
    };

    this.estadoChat = function(){
        $.ajax({
            url: '/utilchats/search', //This is the marca
            type: "GET",
            success: function(response){
                console.log('UtilChats:');
                console.log(response);
                $('#chatestado').html('Estado de Chat: '+response.body.estado);
                if(response.body.estado == 'DESHABILITADO'){
                    $('#close').hide();
                    $('#open').show();
                }
                if(response.body.estado == 'HABILITADO'){
                    $('#open').hide();
                    $('#close').show();
                }
            }
        }); 
    };
}
</script>

@endsection