@extends('crawford.layout')

@section('content')

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	    <div class="card" data-color="crawford">
	        <h4>Crear Cliente Web app</h4>

	        <div class="row">
	        	<div class="col-sm-6">
	        		<label>Nombres:</label>
	        		<div class="input-group">
						<input type="text" id="nombres" name="nombres" placeholder="nombres" class="form-control" required><span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
					</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<label>Apellidos:</label>
	        		<div class="input-group">
	        			<input type="text" id="apellidos" name="apellidos" placeholder="apellidosnombres" class="form-control" required><span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        </div>

	        <div class="row" style="margin-top: 17px;">
	        	<div class="col-sm-6">
	        		<label>Correo electrónico:</label>
	        		<div class="input-group">
	        			<input type="email" id="email_cliente" name="email_cliente" placeholder="correo electronico" class="form-control" onkeyup="cliente.validar_correo_registrado()" required><span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<label>Cédula:</label>
	        		<div class="input-group">
	        			<input type="number" id="cedula" name="cedula" placeholder="cédula" class="form-control" required><span class="input-group-addon"><i class="fa fa-id-card" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        </div>

	        <div class="row" style="margin-top: 17px;">
	        	<div class="col-sm-6">
	        		<label>Contraseña:</label>
	        		<div class="input-group">
	        			<input type="password" id="password_cliente" name="password_cliente" placeholder="contraseña" class="form-control" required><span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<label>Comprobar contraseña:</label>
	        		<div class="input-group">
	        			<input type="password" id="password_cliente_validate" name="password_cliente_validate" placeholder="comprobar contraseña" class="form-control" required><span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        </div>

	        <div class="row" style="margin-top: 17px;">
	        	<div class="col-sm-6">
	        		<label>Celular:</label>
	        		<div class="input-group">
	        			<input type="number" id="celular" name="celular" placeholder="celular" class="form-control" required><span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<label>Fecha de nacimiento:</label>
	        		<div class="input-group">
	        			<input type="text" id="txtTest" name="txtTest" placeholder="fecha de nacimiento" class="form-control" required><span class="input-group-addon" onclick="cliente.mostrar_datepicker()"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	        		</div>
	        	</div>
	        </div>

	        <div class="row" style="margin-top: 17px;"	>
	        	<div class="col-sm-6">
	        		<label>Marca:</label>
        			<select class="form-control" id="marca" name="marca" required>
        				<option value="2">FALABELLA</option>
        				<option value="3">SEGUROS MUNDIAL</option>
        				<option value="4">SAMSUNG</option>
        				<option value="5">ZURICH</option>
        			</select>
	        	</div>
	        </div>

	        <div class="row" style="margin-top: 30px;">
	        	<div class="col-sm-6">
	        	</div>
	        	<div class="col-sm-6">
	        		<button class="btn btn-success btn-block btn-fill" onclick="cliente.crear()">Registrar</button>
	        	</div>
	        </div>

	    </div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#admin_clientes').addClass('active');
});
</script>
<script type="text/javascript" src="/archivos_js/cliente_crear.js"></script>

@endsection