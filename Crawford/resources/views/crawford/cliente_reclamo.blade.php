@extends('crawford.layout')

@section('content')

<input type="hidden" id="id_cliente" value="{{$id_cliente}}">

<div class="row">

	<div class="col-sm-8 col-sm-offset-2">

	    <div class="card" data-color="crawford">
	        <h4>Formulario reclamo cliente <b>#{{$id_cliente}}</b></h4><br>

	        <div class="row">
	        	<div class="col-sm-12">
	        		 <div class="table-responsive">
						<table class="table table-hover">
					        <tbody>
					            <tr>
					            	<td><b>Cédula:</b></td>
					            	<td>{{$cliente->cedula}}</td>
					            </tr>
					            <tr>
					            	<td><b>Nombres:</b></td>
					            	<td>{{$cliente->nombre}}</td>
					            </tr>
					            <tr>
					            	<td><b>Apellidos:</b></td>
					            	<td>{{$cliente->lastname}}</td>
					            </tr>
					            <tr>
					            	<td><b>Fecha Nacimiento:</b></td>
					            	<td>{{$cliente->fechanacimiento}}</td>
					            </tr>
					            <tr>
					            	<td><b>Correo electrónico:</b></td>
					            	<td>{{$cliente->email}}</td>
					            </tr>
					            <tr>
					            	<td><b>Marca:</b></td>
					            	<td>
					            		<input type="hidden" id="marca_cliente" value="{{$cliente->marca}}">
					            		@if($cliente->marca == 2)
					            		FALABELLA
					            		@elseif($cliente->marca == 3)
					            		SEGUROS MUNDIAL
					            		@elseif($cliente->marca == 4)
					            		SAMSUNG
					            		@elseif($cliente->marca == 5)
					            		ZURICH
					            		@endif
					            	</td>
					            </tr>
					        </tbody>
						</table>
					</div>	
	        	</div>
	        </div>

	        <div class="panel panel-default filterable">
				<div class="panel-heading">
			        <h4 class="panel-title">Información del asegurado</h4>
			    </div>	
				<div class="panel-body">	
					<div class="row">
			        	<div class="col-sm-4">
			        		<label>Telefono Fijo:</label>
			        		<div class="input-group">
			        			<input type="number" id="tel_fijo" name="tel_fijo" class="form-control"><span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
			        		</div>
			        	</div>
			        	<div class="col-sm-4">
			        		<label>Celular:</label>
			        		<div class="input-group">
			        			<input type="number" id="celular" name="celular" class="form-control" value="{{$cliente->celular}}"><span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
			        		</div>
			        	</div>
			        	<div class="col-sm-4">
			        		<label>Teléfono laboral:</label>
			        		<div class="input-group">
			        			<input type="number" id="tel_laboral" name="tel_laboral" class="form-control"><span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
			        		</div>
			        	</div>
			        	<div class="col-sm-4">
			        		<label>Departamento de residencia:</label>
		        			<select id="dep_residencia" name="dep_residencia" class="form-control">
		        				<option value="">-- seleccione --</option>
		        				@if($cliente->marca != 5)
			        				@foreach($departamentos as $departamento)
			        					<option value="{{$departamento->idDepartamento}}">{{$departamento->nombre}}</option>
			        				@endforeach
			        			@else
			        				@foreach($departamentos_zurich as $departamento)
			        					<option value="{{$departamento->id}}">{{$departamento->nombre}}</option>
			        				@endforeach
			        			@endif
		        			</select>
			        	</div>
			        	<div class="col-sm-4">
			        		<label>Ciudad de residencia:</label>
		        			<select id="ciudad_residencia" name="ciudad_residencia" class="form-control">
		        				<option value="">-- seleccione --</option>
		        			</select>
			        	</div>
			        	<div class="col-sm-4">
			        		<label>Dirección:</label>
			        		<div class="input-group">
			        			<input type="text" id="direccion" name="direccion" class="form-control"><span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
			        		</div>
			        	</div>
			        </div>	
				</div>
			</div>

			<div class="panel panel-default filterable">
				<div class="panel-heading">
			        <h4 class="panel-title">Información de poliza</h4>
			    </div>	
				<div class="panel-body">	
					<div class="row">
						<div class="col-sm-4">
							<label>Cobertura afectada:</label>
		        			<select id="tipo_siniestro" name="tipo_siniestro" class="form-control" >
								<option value="">-- seleccione --</option>
								<option value="hurto">Hurto</option>
								<option value="daño">Daño</option>
								<option value="garantia_extendida">Garantía extendida</option>
							</select>
						</div>
						<div class="col-sm-4">
							<label>Sponsor:</label>
		        			<select id="sponsor" name="sponsor" class="form-control" >
		        				<option value="">-- seleccione --</option>
		        				@foreach($sponsors as $sponsor)
		        					<option value="{{$sponsor->nombre_sponsor}}">{{$sponsor->nombre_trans}}</option>
		        				@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label>Plan:</label>
		        			<select id="plan_poliza" name="plan_poliza" class="form-control" >
								<option value="">-- seleccione --</option>
								@if($cliente->marca == 5)
									<option value="garantia_extendida">Garantía extendida</option>
								@else
									<option value="total_guard">Total Guard</option>
									<option value="movil_esencial">Movil Esencial</option>
									<option value="estrena_siempre">Estrena Siempre</option>
								@endif
							</select>
						</div>
						<div class="col-sm-4">
							<label>Fecha compra producto:</label>
		        			<input type="date" id="fecha_compra" name="fecha_compra" class="form-control" >
						</div>
						<div class="col-sm-4">
							<label>Tipo de producto:</label>
		        			<select id="tipo_producto" name="tipo_producto" class="form-control">
		        				<option value="">-- seleccione --</option>
		        				@foreach($productos as $producto)
		        					<option value="{{$producto->id}}">{{$producto->nombre_producto}}</option>
		        				@endforeach
		        			</select>
						</div>
						<div class="col-sm-4">
							<label>Marca del equipo:</label>
		        			<select id="marca_equipo" name="marca_equipo" class="form-control">
		        				<option value="">-- seleccione --</option>
		        				@foreach($marcas as $marca)
		        					<option value="{{$marca->id}}">{{$marca->nombre_marca}}</option>
		        				@endforeach
		        			</select>
						</div>
						<div class="col-sm-4">
							<label>Modelo del equipo:</label>
		        			<input type="text" id="modelo_equipo" name="modelo_equipo" class="form-control" >
						</div>
						<div class="col-sm-4">
							<label>Imei / Serial:</label>
		        			<input type="number" id="serial_equipo" name="serial_equipo" class="form-control" >
						</div>
						<div class="col-sm-4">
							<label>Precio:</label>
							<input type="number" id="precio_equipo" name="precio_equipo" class="form-control">
						</div>
						<div class="col-sm-4">
							<label>Número de factura:</label>
							<input type="text" id="numero_factura" name="numero_factura" class="form-control" onkeyup="reclamo.validar_factura()">
						</div>
						<div class="col-sm-4">
							<label>Tienda:</label>
							<select class="form-control" id="tienda" name="tienda">
								<option value="">-- seleccione --</option>
								@foreach($tiendas as $tienda)
									<option value="{{$tienda->id}}">{{$tienda->nombre_tienda}}</option>
								@endforeach
							</select>
						</div>
						@if(!Auth::user()->authorizeRoles(['CC_ZURICH']))
							<div class="col-sm-4">
								<label>Fecha de asignacion a CST:</label>
								<input type="date" id="fecha_asignacion_cst" name="fecha_asignacion_cst" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Nombre CST:</label>
								<input type="text" id="nombre_cst" name="nombre_cst" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Fecha de ingreso a CST:</label>
								<input type="date" id="fecha_ingreso_cst" name="fecha_ingreso_cst" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Fecha recepcion diagnostico:</label>
								<input type="date" id="fecha_diagnostico" name="fecha_diagnostico" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Tiempo de reparación (dias):</label>
								<input type="number" id="tiempo_reparacion" name="tiempo_reparacion" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Causal:</label>
								<select class="form-control" id="causal" name="causal">
									<option value="">-- seleccione --</option>
									@foreach($causales as $causal)
										<option value="{{$causal->id}}">{{$causal->nombre_causal}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<label>Valor Indemnizado:</label>
								<input type="number" id="valor_indemnizado" name="valor_indemnizado" class="form-control">
							</div>
							<div class="col-sm-4">
								<label>Fecha de notificación:</label>
								<input type="date" id="fecha_notificacion" name="fecha_notificacion" class="form-control">
							</div>
						@endif
						<div class="col-sm-4">
							<label>Certificado:</label>
							<input type="number" id="certificado" name="certificado" class="form-control">
						</div>
						<div class="col-sm-4">
							<label>Fecha de aviso:</label>
							<input type="date" id="fecha_aviso" name="fecha_aviso" class="form-control" >
						</div>
						@if(!Auth::user()->authorizeRoles(['CC_ZURICH']))
						<div class="col-sm-4">
							<label>Radicado Zurich:</label>
							<input type="number" id="radicado_zurich" name="radicado_zurich" class="form-control">
						</div>
						@endif
						<div class="col-sm-12">
							<label>Observaciones:</label>
							<textarea id="observaciones" name="observaciones" class="form-control"></textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default filterable">
				<div class="panel-heading">
			        <h4 class="panel-title">Información del siniestro</h4>
			    </div>	
				<div class="panel-body">	
					<div class="row">
						<div class="col-sm-6">
							<label>Fecha siniestro:</label>
							<input type="date" id="fecha_siniestro" name="fecha_siniestro" class="form-control">
						</div>
						<div class="col-sm-6">
							<label>Hora siniestro:</label>
							<input type="time" id="hora_siniestro" name="hora_siniestro" class="form-control">
						</div>
						<div class="col-sm-6">
			        		<label>Departamento de siniestro:</label>
		        			<select id="dep_siniestro" name="dep_siniestro" class="form-control">
		        				<option value="">-- seleccione --</option>
		        				@if($cliente->marca != 5)
			        				@foreach($departamentos as $departamento)
			        					<option value="{{$departamento->idDepartamento}}">{{$departamento->nombre}}</option>
			        				@endforeach
			        			@else
			        				@foreach($departamentos_zurich as $departamento)
			        					<option value="{{$departamento->id}}">{{$departamento->nombre}}</option>
			        				@endforeach
			        			@endif
		        			</select>
			        	</div>
			        	<div class="col-sm-6">
			        		<label>Ciudad de siniestro:</label>
		        			<select id="ciudad_siniestro" name="ciudad_siniestro" class="form-control">
		        				<option value="">-- seleccione --</option>
		        			</select>
			        	</div>
			        	<div class="col-sm-12">
			        		<label>Relato de los hechos acontecidos:</label>
		        			<textarea id="text_siniestro" name="text_siniestro" class="form-control"></textarea>
			        	</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-offset-4 col-sm-4">
					<button class="btn btn-primary btn-block btn-fill" onclick="reclamo.enviar_formulario()" id="enviar_formulario">Crear Reclamo</button>
				</div>
			</div>

	    </div>
	</div>
</div>

<script type="text/javascript" src="/archivos_js/cliente_reclamo.js"></script>

@endsection