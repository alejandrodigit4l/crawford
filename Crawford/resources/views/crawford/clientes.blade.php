@extends('crawford.layout')

@section('content')

<div class="main-content">

    <div class="container-fluid">

        <h4>Clientes</h4>

        <div class="row">

            <div class="col-md-10">
            	
                <div class="card" data-color="crawford">
                    <form action="/crawford/clientes/search" method="GET">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Id:</label>
                                <input type="number" name="id_cliente" class="form-control" value="{{ $id_cliente }}">  
                            </div>
                            <div class="col-md-3">
                                <label>Nombre:</label>
                                <input type="text" name="nombre_cliente" class="form-control" value="{{ $nombre_cliente }}">
                            </div>
                            <div class="col-md-3">
                                <label>Apellido:</label>
                                <input type="text" name="apellido_cliente" class="form-control" value="{{ $apellido_cliente }}">
                            </div>
                            <div class="col-md-4">
                                <label>Email:</label>
                                <input type="text" name="email_cliente" class="form-control" value="{{ $email_cliente }}">
                            </div>
                            <div class="col-md-3">
                                <label>Cédula:</label>
                                <input type="text" name="cedula_cliente" class="form-control" value="{{ $cedula_cliente }}">
                            </div>
                            <div class="col-md-3">
                                <label>Celular:</label>
                                <input type="text" name="celular_cliente" class="form-control" value="{{ $celular_cliente }}">
                            </div>
                            <div class="col-md-3">
                                <label>Filtro Marca:</label>
                                <select class="form-control" name="filtro_marca">
                                    <option value="">-- seleccione --</option>
                                    @foreach($marcas as $marca)
                                        @if($filtro_marca == $marca->id)
                                            <option value="{{ $marca->id }}" selected>{{ $marca->nombre }}</option>
                                        @else
                                            <option value="{{ $marca->id }}">{{ $marca->nombre }}</option>
                                        @endif
                                    @endforeach
                                </select>          
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-success btn-fill btn-block margin-top-24">Filtrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            	
            </div>

            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION']))
            <div class="col-md-2">
                <div class="card" data-color="crawford">
                    <center>
                    <a href="/crawford/export/clientes" class="btn btn-success" ><img width="28" src="http://www.iconarchive.com/download/i86104/graphicloads/filetype/excel-xls.ico"><br>Descargar Formato XLS</a></center>
                </div>
            </div>
            @endif

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width heigth-600-overflow">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>lastname</th>
                                    <th>email</th>
                                    <th>cedula</th>
                                    <th>celular</th>
                                    <th>fechanacimiento</th>
                                    <th>marca</th>                         
                                    <th>fecha_creado</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $clientes as $usuario )
                                <tr>
                                    <td>{{ $usuario->id }}</td>
                                    <td>{{ $usuario->nombre }}</td>
                                    <td>{{ $usuario->lastname }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>{{ $usuario->cedula }}</td>
                                    <td>{{ $usuario->celular }}</td>
                                    <td>{{ $usuario->fechanacimiento }}</td>
                                    <td>{{ $usuario->marca_cliente->nombre }}</td>
                                    <td>{{ $usuario->created_at }}</td>
                                    @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']))
                                    <td><a href="/cliente/crear_reclamo/{{ $usuario['id'] }}" class="btn btn-success"> Crear Reclamo</a></td>
                                    @endif
                                    @if(Auth::user()->authorizeRoles(['GESTION','AJUSTADOR','SOPORTE']))
                                    <td><a href="/cliente/crear_reclamo/{{ $usuario['id'] }}" class="btn btn-success" disabled> Crear Reclamo</a></td>
                                    @endif
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>

                    {{ $clientes->render() }}

                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#clientes').addClass('active');
});
</script>

@endsection