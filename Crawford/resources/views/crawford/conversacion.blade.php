<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
        	<div class="card" data-color="crawford" style="overflow-y: scroll;height: 550px;">
                <h4>Contactos</h4>
                <div id="chatestado">Estado Chat: </div>
                @foreach( $chats as $usuario )
                    {{-- <a href="/chats/backend/{{ $usuario['sender']['id'] }}"><label class="btn-success form-control ">{{ $usuario['sender']['nombre'] }} {{ $usuario['sender']['lastname'] }} - {{ $usuario['sender']['cedula'] }}</label></a>
                    <strong style="position: relative;left: 90%;top: -38px;"> {{ $usuario['cnt'] }}</strong> --}}
                    <button class="btn btn-default">{{ $usuario['sender']['nombre'] }} {{ $usuario['sender']['lastname'] }} - {{ $usuario['sender']['cedula'] }}<strong> {{ $usuario['cnt'] }}</strong></button>
                @endforeach
        	</div>
        </div>


        <div class="col-md-5">
            <div class="card" id="respuestascroll" data-color="crawford" style="overflow-y: scroll;height: 450px;">
                <h4>Conversación</h4>
                <div class="card" id="response" style="padding: 10px;">
                    ...
                </div>
            </div>
            <input type="hidden" name="receptortext" id="receptortext" value="{{-- {{ $desde['id'] }} --}}">
            <input id="message" class="form-control" style="height: 90px;width: 100%; position: relative;top: -20px;" placeholder="Escribe el mensaje">
        </div>
    </div>
</div>