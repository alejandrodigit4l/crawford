@extends('crawford.layout')

@section('content')

	<h4>Nuevo Reclamo  {{ $clientes[0]['email'] }}</h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="reclamoid" id="reclamoid" class="form-control" placeholder="reclamoid" value="11" >
						<input type="text" name="cedula" id="cedula" class="form-control" placeholder="cedula" value="{{ $clientes[0]['cedula'] }}" >
						<input type="text" name="nombreasegurado" id="nombreasegurado" class="form-control" placeholder="nombreasegurado" value="{{ $clientes[0]['nombre'] }}" >
						<input type="text" name="apellidoasegurado" id="apellidoasegurado" class="form-control" placeholder="apellidoasegurado" value="{{ $clientes[0]['lastname'] }}">
						<input type="date" name="fechanacimientoasse" id="fechanacimientoasse" class="form-control" placeholder="fechanacimientoasse" >
						<select name="ciudadrecidencia" id="ciudadrecidencia" class="form-control">
							<option value="">Seleccione ciudad de recidencia</option>
							@foreach( $ciudades as $ciudad )
								<option value="{{ $ciudad['id'] }}">{{ $ciudad['nombre'] }}</option>
							@endforeach
						</select>
						<input type="text" name="telefonofijo" id="telefonofijo" class="form-control" placeholder="telefonofijo" value="{{ $clientes[0]['celular'] }}">
						<input type="text" name="celular" id="celular" class="form-control" placeholder="celular" value="{{ $clientes[0]['celular'] }}">
						<input type="text" name="direccion" id="direccion" class="form-control" placeholder="direccion" value="{{ $clientes[0]['direccion'] }}">
						<input type="email" name="email" id="email" class="form-control" placeholder="email" value="{{ $clientes[0]['email'] }}">
						<input type="text" name="telefonolaboral" id="telefonolaboral" class="form-control" placeholder="telefonolaboral" value="{{ $clientes[0]['telefonolaboral'] }}">
						<input type="text" name="jsonrespuestaform" id="jsonrespuestaform" class="form-control" placeholder="jsonrespuestaform" >
						<input type="date" name="fechasiniestro" id="fechasiniestro" class="form-control" placeholder="fechasiniestro" >
						<input type="time" name="horasiniestro" id="horasiniestro" class="form-control" placeholder="horasiniestro" >
						<input type="text" name="descripcionsiniestro" id="descripcionsiniestro" class="form-control" placeholder="descripcionsiniestro" >
						<input type="text" name="textobackend" id="textobackend" class="form-control" placeholder="Codigo interno" >
						<input type="hidden" name="observaciones" id="observaciones" class="form-control" placeholder="observaciones" value="SIN OBSERVACIONES">
						<input type="hidden" name="motivobaja" id="motivobaja" class="form-control" placeholder="motivobaja" value="SIN BAJA">
						<input type="hidden" name="cliente" id="cliente" class="form-control"  placeholder="motivobaja"  value="{{ $clientes[0]['id'] }}">
						<select name="estados_poliza" id="estados_poliza" class="form-control" >
							<option value="">Seleccione Estado Poliza</option>
							@foreach( $estados_polizas as $estados_poliza )
								<option value="{{ $estados_poliza['id'] }}">{{ $estados_poliza['nombre'] }}</option>
							@endforeach
						</select>
						<select name="producto" id="producto" class="form-control" >
							<option value="">Seleccione producto</option>
							@foreach( $productos as $producto )
								<option value="{{ $producto['id'] }}">{{ $producto['titulo'] }}</option>
							@endforeach
						</select>
						<select name="ciudadsiniestro" id="ciudadsiniestro" class="form-control" >
							<option value="">Seleccione Ciudad Siniestro</option>
							@foreach( $ciudades as $ciudad )
								<option value="{{ $ciudad['id'] }}">{{ $ciudad['nombre'] }}</option>
							@endforeach
						</select>
						<br>
                        <div class="btn btn-success" onclick="reclamos.save();">Guardar</div>
                    </form>     
            	</div>
            </div>

            
        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#clientes').addClass('active');
});

var reclamos = new Reclamos();

function Reclamos(){

    this.save = function(){
    	var reclamoid = this.validate('reclamoid');
		var cedula = this.validate('cedula');
		var nombreasegurado = this.validate('nombreasegurado');
		var apellidoasegurado = this.validate('apellidoasegurado');
		var fechanacimientoasse = this.validate('fechanacimientoasse');
		var ciudadrecidencia = this.validate('ciudadrecidencia');
		var telefonofijo = this.validate('telefonofijo');
		var celular = this.validate('celular');
		var direccion = this.validate('direccion');
		var email = this.validate('email');
		var telefonolaboral = this.validate('telefonolaboral');
		var jsonrespuestaform = this.validate('jsonrespuestaform');
		var fechasiniestro = this.validate('fechasiniestro');
		var horasiniestro = this.validate('horasiniestro');
		var descripcionsiniestro = this.validate('descripcionsiniestro');
		var textobackend = this.validate('textobackend');
		var observaciones = this.validate('observaciones');
		var motivobaja = this.validate('motivobaja');
		var cliente = this.validate('cliente');
		var estados_poliza = this.validate('estados_poliza');
		var producto = this.validate('producto');
		var ciudadsiniestro = this.validate('ciudadsiniestro');

        var parametrer = {
        	'reclamoid' : reclamoid,
			'cedula' : cedula,
			'nombreasegurado' : nombreasegurado,
			'apellidoasegurado' : apellidoasegurado,
			'fechanacimientoasse' : fechanacimientoasse,
			'ciudadrecidencia' : ciudadrecidencia,
			'telefonofijo' : telefonofijo,
			'celular' : celular,
			'direccion' : direccion,
			'email' : email,
			'telefonolaboral' : telefonolaboral,
			'jsonrespuestaform' : jsonrespuestaform,
			'fechasiniestro' : fechasiniestro,
			'horasiniestro' : horasiniestro,
			'descripcionsiniestro' : descripcionsiniestro,
			'textobackend' : textobackend,
			'observaciones' : observaciones,
			'motivobaja' : motivobaja,
			'cliente' : cliente,
			'estados_poliza' : estados_poliza,
			'producto' : producto,
			'ciudadsiniestro' : ciudadsiniestro
        };

        $.ajax({
            url: '/reclamos/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/crawford/reclamos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'reclamos/'+DATA;
    }
}


</script>

@endsection