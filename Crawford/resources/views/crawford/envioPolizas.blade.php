@extends('crawford.layout')

@section('content')

    <h4>Envío Polizas</h4>

    
<div class="main-content">
    <div class="container-fluid">
        <div class="card" data-color="crawford">
            <div class="row">
                <div class="col-sm-4">
                    <label>SUBIR IMEIS</label>
                    <input name="imeiscsv" type="file" id="imeiscsv" class="form-control">
                </div>
                {{-- <div class="col-sm-4">
                    <label>Fecha de envío</label>
                    <input name="fecha_envio" type="date" id="fecha_envio" class="form-control">
                </div> --}}
                <div class="col-sm-3">
                    <button  style="margin-top: 26px; width: 100%;" id="send-btn" type="button"  class="btn btn-primary" onclick="envio_poliza.cargar_imeis();">Subir</button>
                </div>
            </div>
        </div>
        <div class="card" data-color="crawford">
            <table class="table">
                <thead>
                    <tr>
                        <th>CAMPAÑA</th>
                        <th>IMEI</th>
                        <th>IMEI 2</th>
                        <th>ESTADO</th>
                    </tr>
                </thead>
                <tbody id="imeis_cargados"></tbody>
            </table>
        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">

    $(document).ready(function(){
        $('ul li').removeClass('active');
        $('#campañas').addClass('active');
    });

    var envio_poliza = new EnvioPoliza();

    function EnvioPoliza(){
        this.cargar_imeis = function(){

            var csv = document.getElementById('imeiscsv');
            var carga = csv.files[0];
            // var fecha_envio = document.getElementById('fecha_envio').value;
            var formdata = new FormData();
            formdata.append('imeiscsv',carga);
            // formdata.append('fecha_envio',fecha_envio);

            $.ajax({
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formdata,
                url: '/crawford/subirimeis',
                type: 'POST',
                success: function(response){
                    // console.log(response[0]);
                    for(var i in response){
                        // console.log(response[i]);
                        $('#imeis_cargados').append(`
                            <tr>
                                <td>${response[i].campana}</td>
                                <td>${response[i].imeis}</td>
                                <td>${response[i].imeis2}</td>
                                <td>${response[i].estado_enviado}</td>
                            </tr>
                        `);
                    }
                }
            });  
        };
    }

</script>

@endsection