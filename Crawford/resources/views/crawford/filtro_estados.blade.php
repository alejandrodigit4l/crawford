@if(isset($estado))

	@if($estado == 1)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Activos</strong></div>
	@elseif($estado == 2)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Rechazados</strong></div>
	@elseif($estado == 3)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Vencidos</strong></div>
	@elseif($estado == 4)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Pendientes</strong></div>
	@elseif($estado == 5)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Habilitados</strong></div>
	@elseif($estado == 6)
		<div class="alert alert-info" role="alert">Filtro aplicado por estado: <strong>Requiere una nueva petición</strong></div>
	@endif
	
@endif