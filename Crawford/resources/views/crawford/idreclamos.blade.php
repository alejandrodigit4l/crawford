@extends('crawford.layout')

@section('content')

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<h4>Reclamo #  {{ $reclamos['reclamoid'] }}</h4><br>
            		<p>Seguros Mundial </p>
            	</div>
            </div>

            <!-- DATOS CLIENTE -->

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <h4>Cliente</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nombre</th>
                                <th>cedula</th>
                                <th>fecha cc</th>
                                <th>fecha nacimiento</th>
                                <th>email</th>
                                <th>celular</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="/crawford/cliente/{{ $reclamos['cliente']['id'] }}">{{ $reclamos['cliente']['id'] }}</a></td>
                                <td>{{ $reclamos['cliente']['nombre'].' '.$reclamos['cliente']['lastname'] }}</td>
                                <td>{{ $reclamos['cliente']['cedula'] }}</td>
                                <td>{{ $reclamos['cliente']['fechaCC'] }}</td>
                                <td>{{ $reclamos['cliente']['fechanacimiento'] }}</td>
                                <td>{{ $reclamos['cliente']['email'] }}</td>
                                <td>{{ $reclamos['cliente']['celular'] }}</td>
                                <td>{{ $reclamos['cliente']['created_at'] }}</td>
                            </tr>                                                
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- DATOS DEL RECLAMO -->

            <div class="col-md-9">
            	<div class="card" data-color="crawford">

                    @if(Session::has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('error')}}
</div>
@endif

            		<h4>Datos del Reclamo</h4>
            		<div class="row">

            			<div class="col-md-3">
            				<label for="nombreasegurado">Nombre Asegurado</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos->cliente->nombre }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad Residencia</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['ciudadrecidencia']['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Email</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos->cliente->email }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Celular</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['celular'] }}" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Cedula</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos->cliente->cedula }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha nacimiento</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['cliente']['fechanacimiento'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono fijo</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['telefonofijo'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono laboral</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['telefonolaboral'] }}" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Direccion Laboral</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['direccion'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Tipo Producto</label>
            				<input type="text" class="form-control" name="" value="{{ $arrayTipo_polizas[0]['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Producto</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['producto']['titulo'] }}" disabled>
            			</div>
            		</div>
            	</div>
            </div>

            <!-- EXPORTACION DE CASO -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-3" id="div_exportar">
                <div class="card" data-color="crawford">
                    <h4>Exportar Reclamo</h4>
                    <a href="/crawford/export/zip/{{ $reclamos['id'] }}/{{ $reclamos['cliente']['marca'] }}" class="btn btn-primary" style="width: 100%">Exportar zip </a> <br><br>
                    <a href="/crawford/export/excel/{{ $reclamos['id'] }}" class="btn btn-success" style="width: 100%">Exportar Excel </a>
                </div>
            </div>
            @endif
            <!-- DATOS DEL SINIESTRO -->

            <div class="col-md-9">
            	<div class="card" data-color="crawford">
            		<h4>Datos del Siniestro</h4>
            		<div class="row">
            			<div class="col-md-3">
            				<label for="nombreasegurado">Estado</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['estados_poliza']['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['fechasiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Hora</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['horasiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['ciudadsiniestro']['nombre'] }}" disabled>
            			</div>

            			<div class="col-md-12">
            				<label for="nombreasegurado">Descripcion</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos['descripcionsiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-12">
            				<label for="nombreasegurado">Observaciones</label>
            				<input type="text" class="form-control" name="newobservaciones" id="newobservaciones" value="" >
            			</div>
                        <div class="col-md-12">
                            <h4>Historial Observaciones</h4>
                            @foreach( $observaciones as $observacion )
                            <p>* <label style="font-size: 15px;">{{ $observacion['observacion'] }} </label> <small style="font-size: 10px;">{{ $observacion['created_at'] }}</small></p>
                            @endforeach
                        </div>
            		</div>
            	</div>
            </div>

            <!-- CAMBIO DE ESTADO DEL RECLAMO -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-3" id="div_cambio_estado">
                <div class="card" data-color="crawford">
                    <h4>Cambiar Estado/código interno</h4>
                    <label>Estado</label>
                    <select class="form-control" id="newestado">
                    	<option value="">Seleccione estado</option>
                    	@foreach( $homologos as $homologo)
                    	<option value="{{  $homologo['id'] }}">{{ $homologo['nombre'] }}</option>
                    	@endforeach
                    </select>
                    <label>Codigo Interno</label>
                    <input type="text" class="form-control" name="codigointerno" id="codigointerno" value="{{ $reclamos['textobackend'] }}">
                </div>
            </div>
            @endif
            <!-- DOCUMENTOS SUBIDOS POR CLIENTE -->

            <div class="col-md-9">
                <div class="card" data-color="crawford" style="overflow-x: scroll;">
                    <h4>Documentos</h4>
                    <table class="table table " data-color="crawford">
                        <thead>
                            <tr>
                                <th><div style="width: 70px;text-align: center;">Titulo</div></th>
                                <th><div style="width: 120px;text-align: center;">Ayuda</div></th>
                                <th>Imagenes</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $listchekings as $data )
                            @if( $data['reclamo']==0 )
                            <tr>
                                <td><div style="width: 70px;">{{ $data['titulo'] }}</div></td>
                                <td><div style="width: 120px;">{{ $data['ayuda'] }}</div></td>
                                <td>
                                    
                                    <table class="table " data-color="crawford">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $documents as $document )
                                            <tr>
                                            @if( $document['listcheking'] == $data['id'] )
                                                
                                                @if( $data['tipo'] == 'camara' )
                                                <td>
                                                <div style="width: 100px;">
                                                    <button data-popup-open="popup-1" class="btn btn-primary" onclick="reclamo.cargar_imagen('{{ $document['url'] }}')"> Ver imagen </button>
                                                    {{-- <a href="{{ $document['url'] }}" download target="_blank"><img style="width: 100%;" src="{{ $document['url'] }}" ></a> --}}
                                                </div>
                                                </td>
                                                @endif

                                                @if( $data['tipo'] == 'audio' )
                                                <td><div style="width: 100px;">
                                                    <audio controls>
                                                      <source src="{{ $document['url'] }}" type="audio/ogg">
                                                      <source src="{{ $document['url'] }}" type="audio/mpeg">
                                                    Your browser does not support the audio element.
                                                    </audio>
                                                </div></td>
                                                @endif

                                                <td><div style="width: 180px;">{{ $document['created_at'] }}</div></td>
                                                @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
                                                <td><a onclick="eliminar(`/documentsmodel/delete/{{ $document['id'] }}`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td>
                                                @endif
                                                 
                                            @endif
                                            </tr> 
                                        @endforeach                                              
                                        </tbody>
                                    </table>

                                </td>
                                @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
                                <td><a href="/documentsmodel/subirdocumento/{{ $reclamos['id'] }}/{{ $data['id'] }}" class="btn btn-success ">Subir</a></td>
                                @endif
                            </tr> 
                            @endif

                            @if( $data['reclamo']== $reclamos['id'] )
                            <tr>
                                <td><div style="width: 70px;">{{ $data['titulo'] }}</div></td>
                                <td><div style="width: 120px;">{{ $data['ayuda'] }}</div></td>
                                <td>
                                    
                                    <table class="table " data-color="crawford">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $documents as $document )
                                            <tr>
                                            @if( $document['listcheking'] == $data['id'] )
                                                
                                                <td>
                                                    <div style="width: 100px;">
                                                        <button data-popup-open="popup-1" class="btn btn-primary" onclick="reclamo.cargar_imagen('{{ $document['url'] }}')"> Ver imagen </button>
                                                        {{-- <a href="{{ $document['url'] }}"  target="_blank"><img style="width: 100%;" src="{{ $document['url'] }}" ></a> --}}
                                                    </div>
                                                </td>
                                                <td><div style="width: 180px;">{{ $document['created_at'] }}</div></td>
                                                <td><a onclick="eliminar(`/documentsmodel/delete/{{ $document['id'] }}`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td>
                                                 
                                            @endif
                                            </tr> 
                                        @endforeach                                              
                                        </tbody>
                                    </table>

                                </td>
                                <td><a href="/documentsmodel/subirdocumento/{{ $reclamos['id'] }}/{{ $data['id'] }}" class="btn btn-success ">Subir</a></td>
                            </tr> 
                            @endif
                        @endforeach                                              
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- SOLICITUD DE DOCUMENTO -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-3" id="div_documento_extra">
                <div class="card" data-color="crawford">
                        <input type="hidden" name="marca" id="marca" class="form-control" value="{{ $reclamos['producto']['marca'] }}">
                        <input type="hidden" name="tipo_seguro" id="tipo_seguro" class="form-control" value="{{ $reclamos['producto']['tipo_seguro'] }}">
                        <input type="hidden" name="aseguradora" id="aseguradora" class="form-control" value="{{ $reclamos['producto']['aseguradora'] }}">
                        <input type="hidden" name="campana" id="campana" class="form-control" value="{{ $reclamos['producto']['campana'] }}">
                        <input type="hidden" name="tienda" id="tienda" class="form-control" value="{{ $reclamos['producto']['campana'] }}">
                        <input type="hidden" name="asegurado" id="asegurado" class="form-control" value="{{ $reclamos['producto']['asegurado'] }}">
                        <input type="hidden" name="tipo_polizas" id="tipo_polizas" class="form-control" value="{{ $reclamos['producto']['tipo_poliza'] }}">
                        <input type="hidden" name="img" id="img" class="form-control" placeholder="img" value="https://image.flaticon.com/icons/svg/148/148769.svg">
                        <label>Título</label>
                        <input type="text" name="titulo" id="titulo" class="form-control"  onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Descripción</label>
                        <input type="text" name="descripcion" class="form-control" id="descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Sub-descripción</label>
                        <input type="text" name="subdescripcion" class="form-control" id="subdescripcion"  value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Ayuda</label>
                        <input type="text" name="ayuda" class="form-control" id="ayuda" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Tipo de formato</label>
                        <select name="tipo" id="tipo" class="form-control" placeholder="tipo">
                            <option value="">Seleccione el tipo de formato</option>
                            <option value="camara">Camara</option>
                            <option value="audio">Audio</option>
                        </select>
                        <input type="hidden" name="reclamo" id="reclamo" placeholder="reclamo SI existe de lo contrario es un 0" value="{{ $reclamos['id'] }}"><br>
                    <button class="btn btn-success form-control" onclick="reclamo.listchekings();">Solicitar</button><br><br>
                    <a href="/img/ejemploapp.jpg" class="btn btn-warning " style="width: 100%" target="_blank">Ejemplo de vista APP</a>
                </div>
            </div>
            @endif

            <!-- TABLA VISTA DE NOTIFICACION -->

            <div class="col-md-7" id="div_vista_notificacion">
                <div class="card" data-color="crawford">
                    <h4>Notificaciones</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>fecha</th>
                                <th>mensaje</th>
                                {{-- <th>adjunto</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $notificaciones as $notificacion )
                            <tr>
                                <td><p style="font-size: 12px;">{{ $notificacion['fecha'] }} </p></td>
                                <td><textarea style="width: 270px; height: 100px; max-width: 350px;" disabled>{{ $notificacion['texto'] }}</textarea></td>
                                {{-- <td><a href="https://admin.crawfordaffinitycolombia.com/{{ $notificacion['notyid'] }}">{{ $notificacion['notyid'] }}</a></td> --}}
                            </tr>  
                        @endforeach                                              
                        </tbody>
                    </table>
                </div>
            </div>


            <!-- ENVIO DE NOTIFICACION -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-5" id="div_crear_notificacion">
                <div class="card" data-color="crawford">
                    <form action="/notificaciones/sendBackend" method="POST" enctype="multipart/form-data">
                        <h4>Enviar notificación</h4>
                        <input type="hidden" name="id" id="id" value="{{ $reclamos['id'] }}">
                        <label>Texto</label>
                        <textarea class="form-control" name="textonotificacion"></textarea>
                        <label>Adjuntar</label>
                        <input type="file" name="file" id="file" class="inputfile" />
                        <label class="btn btn-succes form-control" for="file">Escoger un archivo</label><br>
                        <input type="submit" value="Enviar" class="btn btn-success form-control" name="submit">
                    </form>
                </div>
            </div>
            @endif

        </div>

        <div >
            <input type="hidden" name="id"                  id="id"                 value="{{ $reclamos['id'] }}">
            <input type="hidden" name="reclamoid"           id="reclamoid"          value="{{ $reclamos['reclamoid'] }}">
            <input type="hidden" name="cedula"              id="cedula"             value="{{ $reclamos['cedula'] }}">
            <input type="hidden" name="nombreasegurado"     id="nombreasegurado"    value="{{ $reclamos['nombreasegurado'] }}">
            <input type="hidden" name="apellidoasegurado"   id="apellidoasegurado"  value="{{ $reclamos['apellidoasegurado'] }}">
            <input type="hidden" name="fechanacimientoasse" id="fechanacimientoasse" value="{{ $reclamos['fechanacimientoasse'] }}">
            <input type="hidden" name="ciudadrecidencia"    id="ciudadrecidencia"   value="{{ $reclamos['ciudadrecidencia']['id'] }}">
            <input type="hidden" name="telefonofijo"        id="telefonofijo"       value="{{ $reclamos['telefonofijo'] }}">
            <input type="hidden" name="celular"             id="celular"            value="{{ $reclamos['celular'] }}">
            <input type="hidden" name="direccion"           id="direccion"          value="{{ $reclamos['direccion'] }}">
            <input type="hidden" name="email"               id="email"              value="{{ $reclamos['email'] }}">
            <input type="hidden" name="telefonolaboral"     id="telefonolaboral"    value="{{ $reclamos['telefonolaboral'] }}">
            <input type="hidden" name="jsonrespuestaform"   id="jsonrespuestaform"  value="{{ $reclamos['jsonrespuestaform'] }}">
            <input type="hidden" name="producto_reclamo"   id="producto_reclamo"  value="{{ $reclamos['producto_reclamo'] }}">
            <input type="hidden" name="marca"   id="marca"  value="{{ $reclamos['marca'] }}">
            <input type="hidden" name="imei"   id="imei"  value="{{ $reclamos['imei'] }}">
            <input type="hidden" name="jsonrespuestaform"   id="jsonrespuestaform"  value="{{ $reclamos['jsonrespuestaform'] }}">
            <input type="hidden" name="jsonrespuestaform"   id="jsonrespuestaform"  value="{{ $reclamos['jsonrespuestaform'] }}">
            <input type="hidden" name="fechasiniestro"      id="fechasiniestro"     value="{{ $reclamos['fechasiniestro'] }}">
            <input type="hidden" name="horasiniestro"       id="horasiniestro"      value="{{ $reclamos['horasiniestro'] }}">
            <input type="hidden" name="descripcionsiniestro" id="descripcionsiniestro" value="{{ $reclamos['descripcionsiniestro'] }}">
            <input type="hidden" name="textobackend"        id="textobackend"       value="{{ $reclamos['textobackend'] }}">
            <input type="hidden" name="observaciones"       id="observaciones"      value="{{ $reclamos['observaciones'] }}">
            <input type="hidden" name="motivobaja"          id="motivobaja"         value="{{ $reclamos['motivobaja'] }}">
            <input type="hidden" name="cliente"             id="cliente"            value="{{ $reclamos['cliente']['id'] }}">
            <input type="hidden" name="estados_poliza"      id="estados_poliza"     value="{{ $reclamos['estados_poliza']['homologo'] }}">
            <input type="hidden" name="producto"            id="producto"           value="{{ $reclamos['producto']['id'] }}">
            <input type="hidden" name="ciudadsiniestro"     id="ciudadsiniestro"    value="{{ $reclamos['ciudadsiniestro']['id'] }}">
        </div>

    </div>
</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <div class="panel panel-default">
            <div class="panel-body">
              
                {{-- <img src="{{ $document['url'] }}"> --}}
                <div class="container" id="cargar_imagen" style="height: 550px;width: 100%;overflow-y: scroll; overflow-x: scroll;"></div>

            </div>
        </div>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<style type="text/css">
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    display: inline-block;
    color: #207ce5;
}

.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #207ce5;
    color: white;
}
.inputfile + label {
    cursor: pointer; /* "hand" cursor */
}
.inputfile:focus + label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>

<script type="text/javascript">

var reclamo = new Reclamo();

// reclamo.init();


function eliminar(urls){
    swal({
        title: '<b>Desea eliminar la imagen?<b><br>',
        text: `Esta acción es irreversible.`,
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: true,
    }, function () {
        $.ajax({
            url: urls, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    swal({
                      title: 'Se ha eliminado!',
                      type: "success",
                      showLoaderOnConfirm: true,
                    },function(){
                      location.reload();
                    });
                }  
                else{
                    swal('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }); 
}


$('#newobservaciones').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#observaciones').val($('#newobservaciones').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});

$('#codigointerno').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#textobackend').val($('#codigointerno').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});


$('#newestado').on('change', function() {
    var r = confirm("Desea actualizar la información?");
    if (r == true) {
        //actualizar informacion
        $('#estados_poliza').val($('#newestado').val());
        reclamo.updload();
    } else {
        console.log('Sera para un proximo cambio');
    }
});


function Reclamo(){

    this.init = function(){
        /*-- traer datos de sesion --*/
        var mainMenu = localStorage.getItem('rol').split(",");
        console.log('MENU');
        console.log(mainMenu);

        for (var i = mainMenu.length - 1; i >= 0; i--) {

            /*-- ocultar elementos --*/
            if(mainMenu[i]=='_sin_accesos'){
                $('#div_exportar').hide();
                $('#div_cambio_estado').hide();
                $('#div_documento_extra').hide();
                $('#div_vista_notificacion').hide();
                $('#div_crear_notificacion').hide();
            }
        }

    };

    this.cargar_imagen = function(url_imagen){
        $('.imagen_cargada').remove();
        $('#cargar_imagen').append(`
            <img class="imagen_cargada img-responsive" style="width: 90%;" src="${url_imagen}">
            `);
    };

    this.updload = function(){
        var reclamoid = this.validate('reclamoid');
        var cedula = this.validate('cedula');
        var nombreasegurado = this.validate('nombreasegurado');
        var apellidoasegurado = this.validate('apellidoasegurado');
        var fechanacimientoasse = this.validate('fechanacimientoasse');
        var ciudadrecidencia = this.validate('ciudadrecidencia');
        var telefonofijo = this.validate('telefonofijo');
        var celular = this.validate('celular');
        var direccion = this.validate('direccion');
        var email = this.validate('email');
        var telefonolaboral = this.validate('telefonolaboral');
        // var jsonrespuestaform = this.validate('jsonrespuestaform');
        var producto_reclamo = this.validate('producto_reclamo');
        var marca = this.validate('marca');
        var imei = this.validate('imei');
        var fechasiniestro = this.validate('fechasiniestro');
        var fechasiniestro = this.validate('fechasiniestro');
        var horasiniestro = this.validate('horasiniestro');
        var descripcionsiniestro = this.validate('descripcionsiniestro');
        var textobackend = this.validate('textobackend');
        var observaciones = this.validate('observaciones');
        var motivobaja = this.validate('motivobaja');
        var cliente = this.validate('cliente');
        var estados_poliza = this.validate('estados_poliza');
        var producto = this.validate('producto');
        var ciudadsiniestro = this.validate('ciudadsiniestro');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'reclamoid' : reclamoid,
            'cedula' : cedula,
            'nombreasegurado' : nombreasegurado,
            'apellidoasegurado' : apellidoasegurado,
            'fechanacimientoasse' : fechanacimientoasse,
            'ciudadrecidencia' : ciudadrecidencia,
            'telefonofijo' : telefonofijo,
            'celular' : celular,
            'direccion' : direccion,
            'email' : email,
            'telefonolaboral' : telefonolaboral,
            'producto_reclamo' : producto_reclamo,
            'marca' : marca,
            'imei' : imei,
            'fechasiniestro' : fechasiniestro,
            'horasiniestro' : horasiniestro,
            'descripcionsiniestro' : descripcionsiniestro,
            'textobackend' : textobackend,
            'observaciones' : observaciones,
            'motivobaja' : motivobaja,
            'cliente' : cliente,
            'estados_poliza' : estados_poliza,
            'producto' : producto,
            'ciudadsiniestro' : ciudadsiniestro
        };

        console.log(parametrer);

        $.ajax({
            url: '/reclamos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.listchekings = function(){
        var nombre = this.validate('nombre');
        var img = this.validate('img');
        var tipo = this.validate('tipo');
        var titulo = this.validate('titulo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var subdescripcion = this.validate('subdescripcion');
        var ayuda = this.validate('ayuda');
        var tipo_polizas = this.validate('tipo_polizas');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 
        var reclamo = this.validate('reclamo');

        var parametrer = {
            'nombre' : nombre,
            'img' : img,
            'tipo': tipo,
            'titulo': titulo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'subdescripcion' : subdescripcion,
            'ayuda' : ayuda,
            'tipo_polizas' : tipo_polizas,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado,
            'reclamo' : reclamo
        };

        $.ajax({
            url: '/listchekings/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }
}
</script>

@endsection