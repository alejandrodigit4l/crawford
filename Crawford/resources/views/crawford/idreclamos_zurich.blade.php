@extends('crawford.layout')

@section('content')

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{Session::get('error')}}
                    </div>
                    @endif
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{Session::get('message')}}
                    </div>
                    @endif
                    <h4>Reclamo #  {{ $reclamos->id }}</h4><br>
                </div>
            </div>

            <!-- DATOS CLIENTE -->
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <h4>Cliente</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nombre</th>
                                <th>cedula</th>
                                <th>fecha nacimiento</th>
                                <th>email</th>
                                <th>celular</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <input type="hidden" id="id_cliente" value="{{$reclamos->datos_cliente->id}}">
                                <td><a href="/crawford/cliente/{{ $reclamos->datos_cliente->id }}" style="color: black;">{{ $reclamos->datos_cliente->id }}</a></td>
                                <td>{{ $reclamos->datos_cliente->nombre.' '.$reclamos->datos_cliente->lastname }}</td>
                                <td>{{ $reclamos->datos_cliente->cedula }}</td>
                                <td>{{ $reclamos->datos_cliente->fechanacimiento }}</td>
                                <td>{{ $reclamos->datos_cliente->email }}</td>
                                <td>{{ $reclamos->datos_cliente->celular }}</td>
                                <td>{{ $reclamos->datos_cliente->created_at }}</td>
                            </tr>                                                
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- DATOS CLIENTE -->



            <!-- DATOS DEL RECLAMO -->
            <div class="col-md-9">
            	<div class="card" data-color="crawford">

            		<h4>Datos del Reclamo</h4>
            		<div class="row">

            			<div class="col-md-3">
            				<label>Nombre Asegurado</label>
            				<input type="text" class="form-control" id="nombre_cliente" value="{{ $reclamos->datos_cliente->nombre }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
            			</div>
                        <div class="col-md-3">
                            <label>Apellido Asegurado</label>
                            <input type="text" class="form-control" id="apellido_cliente" value="{{ $reclamos->datos_cliente->lastname }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Cedula</label>
                            <input type="text" class="form-control" id="cedula_cliente" value="{{ $reclamos->datos_cliente->cedula }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Email</label>
                            <input type="text" class="form-control" id="email_cliente" value="{{ $reclamos->datos_cliente->email }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Departamento Residencia</label>
                            <select class="form-control" id="dep_residencia" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}} onchange="reclamo.traer_ciudades_residencia()">
                                @foreach($departamentos as $departamento)
                                    <option value="{{$departamento->id}}" {{$reclamos->ciudad_residencia->idDepartamento == $departamento->id ? 'selected':''}}>{{$departamento->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
            			<div class="col-md-3">
            				<label>Ciudad Residencia</label>
                            <select class="form-control" id="ciudad_residencia" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}}>
                                <option value>-- seleccione --</option>
                                @foreach($ciudades_departamento_residencia as $ciudad)
                                    <option class="option_ciudad_residencia" value="{{$ciudad->id}}" {{$reclamos->ciudadrecidencia == $ciudad->id ? 'selected':'' }}>{{$ciudad->nombre}}</option>
                                @endforeach
                            </select>
            			</div>
            			<div class="col-md-3">
            				<label>Celular</label>
            				<input type="text" class="form-control" id="celular_cliente" value="{{ $reclamos->celular }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
            			</div>
            			<div class="col-md-3">
            				<label>Telefono fijo</label>
            				<input type="text" class="form-control" id="telefono_fijo" value="{{ $reclamos->telefonofijo }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
            			</div>
            			<div class="col-md-3">
            				<label>Telefono laboral</label>
            				<input type="text" class="form-control" id="telefono_laboral" value="{{ $reclamos->telefonolaboral }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
            			</div>

            			<div class="col-md-3">
            				<label>Direccion Laboral</label>
            				<input type="text" class="form-control" id="direccion_cliente" value="{{ $reclamos->direccion }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
            			</div>
            			<div class="col-md-3">
            				<label>Tipo Producto</label>
                            <select class="form-control" id="tipo_producto" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                                <option value>-- seleccione --</option>
                                @foreach($tipos_producto as $tipo)
                                    <option value="{{$tipo->id}}" {{$reclamos->tipo_producto == $tipo->id ?'selected':''}}>{{$tipo->nombre_producto}}</option>
                                @endforeach
                            </select>
            			</div>
                        <div class="col-md-3">
                            <label>Marca producto</label>
                            <select class="form-control" id="marca_producto" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                                <option value>-- seleccione --</option>
                                @foreach($marcas_producto as $marca)
                                    <option value="{{$marca->id}}" {{$reclamos->marca_equipo == $marca->id ?'selected':''}}>{{$marca->nombre_marca}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Modelo Producto</label>
                            <input type="text" class="form-control" id="modelo_equipo" value="{{ $reclamos->modelo_equipo }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Imei / Serial</label>
                            <input type="text" class="form-control" id="imei_producto" value="{{ $reclamos->imei }}" {{ Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Precio</label>
                            <input type="text" class="form-control" id="precio_equipo" value="${{ number_format($reclamos->precio_equipo, 2) }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha compra</label>
                            <input type="date" class="form-control" id="fecha_compra" value="{{ $reclamos->fecha_compra }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Sponsor</label>
                            <select id="sponsor" name="sponsor" class="form-control" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled' }}>
                                <option value="">-- seleccione --</option>
                                @foreach($sponsors as $sponsor)
                                    @if($sponsor->nombre_sponsor == $reclamos->sponsor)
                                        <option value="{{$sponsor->nombre_sponsor}}" selected>{{$sponsor->nombre_trans}}</option>
                                    @else
                                        <option value="{{$sponsor->nombre_sponsor}}">{{$sponsor->nombre_trans}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Plan poliza</label>
                            <input type="text" class="form-control" value="{{ $reclamos->plan_poliza }}" disabled>
                        </div>
                        <div class="col-md-3">
                            <label>Número de factura</label>
                            <input type="text" class="form-control" id="numero_factura" value="{{ $reclamos->numero_factura }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Tienda</label>
                            <select id="tienda" class="form-control" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                                <option>-- seleccione --</option>
                                @foreach($tiendas as $tienda)
                                    <option value="{{ $tienda->id }}">{{ $tienda->nombre_tienda }}</option>
                                    @if($tienda->id == $reclamos->tienda)
                                    <option value="{{ $tienda->id }}" selected>{{ $tienda->nombre_tienda }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha asignacion CST</label>
                            <input type="date" class="form-control" id="fecha_asignacion_cst" value="{{ $reclamos->fecha_asignacion_cst }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Nombre CST</label>
                            <input type="text" class="form-control" id="nombre_cst" value="{{ $reclamos->nombre_cst }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha ingreso CST</label>
                            <input type="date" class="form-control" id="fecha_ingreso_cst" value="{{ $reclamos->fecha_ingreso_cst }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha diagnóstico</label>
                            <input type="date" class="form-control" id="fecha_diagnostico" value="{{ $reclamos->fecha_diagnostico }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Tiempo reparacion</label>
                            <input type="number" class="form-control" id="tiempo_reparacion" value="{{ $reclamos->tiempo_reparacion }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Causal</label>
                            <select class="form-control" id="causal">
                                <option value="">-- seleccione --</option>
                                @foreach($causales as $causal)
                                    @if($reclamos->causal == $causal->id)
                                        <option value="{{ $causal->id }}" selected>{{ $causal->nombre_causal }}</option>
                                    @else
                                        <option value="{{ $causal->id }}">{{ $causal->nombre_causal }}</option>
                                    @endif
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-md-3">
                            <label>Valor indemnizado</label>
                            <input type="text" class="form-control" id="valor_indemnizado" value="${{ number_format($reclamos->valor_indemnizado, 2) }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha notificación</label>
                            <input type="date" class="form-control" id="fecha_notificacion" value="{{ $reclamos->fecha_notificacion }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Número certificado</label>
                            <input type="number" class="form-control" id="certificado" value="{{ $reclamos->certificado }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Radicado Zurich</label>
                            <input type="number" class="form-control" id="radicado_zurich" value="{{ $reclamos->radicado_zurich }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha envío Instructivo</label>
                            <input type="date" class="form-control" id="fecha_envio_instructivo" value="{{ $reclamos->fecha_envio_instructivo }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha Aviso</label>
                            <input type="date" class="form-control" id="fecha_aviso" value="{{ $reclamos->fecha_aviso }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha recepción de documentos</label>
                            <input type="date" class="form-control" id="fecha_recepcion_documentos" value="{{ $reclamos->fecha_recepcion_documentos }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha rec. documentos completos</label>
                            <input type="date" class="form-control" id="fecha_recep_doc_completos" value="{{ $reclamos->fecha_recep_doc_completos }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Hora recepción documentos</label>
                            <input type="time" class="form-control" id="hora_recepcion_documentos" value="{{ $reclamos->hora_recepcion_documentos }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha entrega ajuste</label>
                            <input type="date" class="form-control" id="fecha_entrega_ajuste" value="{{ $reclamos->fecha_entrega_ajuste }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Fecha de ajuste</label>
                            <input type="date" class="form-control" id="fecha_ajuste" value="{{ $reclamos->fecha_ajuste }}" {{ !Auth::user()->authorizeRoles(['CC_ZURICH','A_PROOV_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-3">
                            <label>Caso de reapertura</label>
                            <select class="form-control" id="caso_reapertura" {{ Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','GESTION_ZURICH']) ? '' : 'disabled' }}>
                                <option value="NO" {{ $reclamos->caso_reapertura == "NO" ? 'selected' : '' }}>NO</option>
                                <option value="SI" {{ $reclamos->caso_reapertura == "SI" ? 'selected' : '' }}>SI</option>                                
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Caso investigado</label>
                            <select class="form-control" id="caso_investigado" {{ Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                                <option value="NO" {{ $reclamos->caso_investigado == "NO" ? 'selected' : '' }}>NO</option>
                                <option value="SI" {{ $reclamos->caso_investigado == "SI" ? 'selected' : '' }}>SI</option> 
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Salvamento</label>
                            <select class="form-control" id="salvamento" {{ Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','A_PROOV_ZURICH']) ? '' : 'disabled' }}>
                                <option value="NO" {{ $reclamos->salvamento == "NO" ? 'selected' : '' }}>NO</option>
                                <option value="SI" {{ $reclamos->salvamento == "SI" ? 'selected' : '' }}>SI</option>                                                            
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Valor de reparación</label>
                        <input type="text" class="form-control" id="valor_reparacion" value="${{ number_format($reclamos->valor_reparacion, 2) }}" {{ Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
                        </div>
                        <div class="col-md-9">
                            <label>Observacion Inicial</label>
                            <textarea class="form-control" disabled>{{ $reclamos->observaciones }}</textarea>
                        </div>
                        <div class="col-md-offset-3 col-md-6" style="margin-top: 25px;">
                            <button class="btn btn-success btn-block btn-fill" onclick="reclamo.actualizar_informacion_reclamo({{ $reclamos->id }})" {{ !Auth::user()->authorizeRoles(['CC_ZURICH']) ? '' : 'disabled' }}>Actualizar Información</button>
                        </div>
            		</div>
            	</div>
            </div>
            <!-- DATOS DEL RECLAMO -->



            <!-- EXPORTACION DE CASO -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-3" id="div_exportar">
                <div class="card" data-color="crawford">
                    <h4>Exportar Reclamo</h4>
                    <a href="/crawford/export/zip/{{ $reclamos->id }}/{{ $reclamos->datos_cliente->marca }}" class="btn btn-primary btn-fill" style="width: 100%">Exportar zip </a> <br><br>
                    <a href="/crawford/export_excel_zurich/{{ $reclamos->id }}" class="btn btn-success btn-fill" style="width: 100%">Exportar Excel </a>
                </div>
            </div>
            @endif
            <!-- EXPORTACION DE CASO -->



            <!-- DATOS DEL SINIESTRO -->
            <div class="col-md-8">
            	<div class="card" data-color="crawford">
            		<h4>Datos del Siniestro</h4>
            		<div class="row">
            			<div class="col-md-12">
            				<label><h5>Estado del reclamo: &nbsp;&nbsp; <b>{{ $reclamos->estado_del_reclamo->nombre_estado }}</b></h5></label>
            			</div>
            			<div class="col-md-3">
            				<label>Fecha</label>
            				<input type="date" class="form-control" id="fecha_siniestro" value="{{ $reclamos->fechasiniestro }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}}>
            			</div>
            			<div class="col-md-3">
            				<label>Hora</label>
            				<input type="time" class="form-control" id="hora_siniestro" value="{{ $reclamos->horasiniestro }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}}>
            			</div>
                        <div class="col-md-3">
                            <label>Departamento</label>
                            <select class="form-control" id="dep_siniestro" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}} onchange="reclamo.traer_ciudades_siniestro()">
                                @foreach($departamentos as $departamento)
                                    <option value="{{$departamento->id}}" {{$reclamos->ciudad_siniestro->idDepartamento == $departamento->id ? 'selected':''}}>{{$departamento->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
            			<div class="col-md-3">
            				<label>Ciudad</label>
                            <select class="form-control" id="ciudad_siniestro" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled'}}>
                                <option value>-- Seleccione --</option>
                                @foreach($ciudades_departamento as $ciudad)
                                    <option class="option_ciudad_siniestro" value="{{$ciudad->id}}" {{$reclamos->ciudadsiniestro == $ciudad->id ? 'selected':'' }}>{{$ciudad->nombre}}</option>
                                @endforeach
                            </select>
            			</div>
                        <div class="col-md-3">  
                            <label>Tipo siniestro</label>
                            <input type="text" class="form-control" id="tipo_siniestro" value="{{ $reclamos->tipo_siniestro }}" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '':'disabled'}}>
                        </div>
            			<div class="col-md-6">  
            				<label>Descripcion</label>
                            <textarea class="form-control" id="descripcion_siniestro" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR','SUPERADMINISTRADOR']) ? '' : 'disabled'}}>{{ $reclamos->descripcionsiniestro }}</textarea>
            			</div>
                        <div class="col-md-3">  
                            <button class="btn btn-success btn-fill btn-block" style="margin-top: 25px;" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR']) ? '' : 'disabled'}} onclick="reclamo.actualizar_informacion_siniestro({{$reclamos->id}})">Actualizar</button>
                        </div>
            			<div class="col-md-9">
            				<label>Observaciones</label>
            				<input type="text" class="form-control" id="observacion" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>
            			</div>
                        <div class="col-md-3">
                            <button style="margin-top: 25px;" class="btn btn-success btn-fill btn-block" onclick="reclamo.añadir_observacion({{ $reclamos->id }})" {{ Auth::user()->authorizeRoles(['ADMINISTRADOR','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']) ? '' : 'disabled' }}>Añadir observacion</button>
                        </div>
                        <div class="col-md-12">
                            <h4>Historial Observaciones</h4>
                            @foreach( $observaciones as $observacion )
                            <p>* <label style="font-size: 15px;">{{ $observacion['observacion'] }} </label> <small style="font-size: 10px;">{{ $observacion['created_at'] }}</small></p>
                            @endforeach
                        </div>
            		</div>
            	</div>
            </div>
            <!-- DATOS DEL SINIESTRO -->

            

            <!-- CAMBIO DE ESTADO DEL RECLAMO -->
            <div class="col-md-4">
                <div class="card" data-color="crawford">
                    @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR','CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']))
                    <h4>Cambiar Estado</h4>
                    <label>Estado</label>
                    <select class="form-control" id="estado_reclamo" onchange="reclamo.cambiar_estado_reclamo({{ $reclamos->id }})">
                    	<option value="0">-- seleccione estado --</option>
                    	@foreach( $estados_reclamo as $estado)
                    	<option value="{{  $estado->id }}">{{ $estado->nombre_estado }}</option>
                    	@endforeach
                    </select><br>
                    @endif
                    <h4>Historial Estados</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <th>Estado</th>
                            <th>Usuario</th>
                            <th>Fecha</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>CREADO</td>
                                <td>
                                    @if(empty($reclamos->usuario_creador))
                                    SISTEMA
                                    @else
                                    {{strtoupper($reclamos->info_usuario_creador->name)}}
                                    @endif
                                </td>
                                <td>{{strtoupper($reclamos->created_at)}}</td>
                            </tr>
                            @foreach($historial_estados as $historial)
                                <tr>
                                    <td>{{ $historial->informacion_estado->nombre_estado }}</td>
                                    <td>{{ $historial->informacion_usuario->name }}</td>
                                    <td>{{ $historial->created_at }}</td>
                                </tr>
                            @endforeach                                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- CAMBIO DE ESTADO DEL RECLAMO -->




            <!-- DOCUMENTOS CHEKLIST -->
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <h4>Documentos del reclamo</h4>
                    <table class="table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Descripción</th>
                                <th>Cantidad Documentos</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($checklists as $checklist)
                                <tr>
                                    <td>{{ $checklist->nombre_checklist }}</td>
                                    <td>{{ $checklist->descripcion }}</td>
                                    <td>
                                        @if($checklist->tipo == 'check')
                                            No lleva documentos
                                        @elseif($checklist->tipo == 'adjunto')
                                            {{ $checklist->cantidad_adjuntos }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($checklist->tipo == 'check')
                                            @if($checklist->documentos()->first()->valor == 'true')
                                                <input type="checkbox" class="switch_1" id="checkbox" onclick="reclamo.cambio_checkbox({{ $checklist->id }}, {{ $reclamos->id }})" checked>
                                            @else
                                                <input type="checkbox" class="switch_1" id="checkbox" onclick="reclamo.cambio_checkbox({{ $checklist->id }}, {{ $reclamos->id }})">
                                            @endif
                                        @elseif($checklist->tipo == 'adjunto')
                                            <button class="btn btn-primary btn-fill" onclick="reclamo.ver_documentos_checklist('{{ $checklist->nombre_checklist }}', {{ $checklist->documentos_checklist }})" {{ !Auth::user()->authorizeRoles(['CC_ZURICH']) ? '' : 'disabled' }}>Ver documentos</button>
                                            <button class="btn btn-info btn-fill" onclick="reclamo.subir_adjunto_checklist('{{ $checklist->nombre_checklist }}', {{ $checklist->id }}, {{ $reclamos->id }})" {{ !Auth::user()->authorizeRoles(['CC_ZURICH']) ? '' : 'disabled' }}>Subir Adjunto</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- DOCUMENTOS CHEKLIST -->




            <!-- ADICIONALES CHECKLIST -->
            @if(!Auth::user()->authorizeRoles(['CC_ZURICH']))
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <h4>Documentos adicionales reclamo</h4>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Documento</label>
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-default btn-fill">
                                        Buscar&hellip; <input type="file" id="adjunto_adicional" style="display: none;" multiple>
                                    </span>
                                </label>
                                <input type="text" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label>Descripción</label>
                            <input type="text" id="descripcion_adicional" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <button style="margin-top: 25px;" class="btn btn-success btn-fill btn-block" onclick="reclamo.subir_adjunto_adicional({{ $reclamos->id }})">Añadir</button>
                        </div>
                    </div><br>

                    @if($documentos_adicionales->count() != 0)
                        <h4>Listado Adicionales</h4>
                        <table class="table table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Descripcion</th>
                                    <th>Fecha creación</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($documentos_adicionales as $documento)
                                    <tr>
                                        <td>
                                            <a href="/crawford/url_archivos_checklist_zurich/{{$documento->nombre_documento}}" target="_blank" style="color: black;">{{$documento->nombre_documento}}</a>
                                        </td>
                                        <td>{{$documento->valor}}</td>
                                        <td>{{$documento->created_at}}</td>
                                        <td>
                                            <button class="btn btn-danger btn-fill" onclick="reclamo.eliminar_documentos({{$documento->id}})">Eliminar</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>
            </div>
            @endif
            <!-- ADICIONALES CHECKLIST -->

            


            <!-- TABLA VISTA DE NOTIFICACION -->
            <div class="col-md-7" id="div_vista_notificacion">
                <div class="card" data-color="crawford">
                    <h4>Notificaciones</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>fecha</th>
                                <th>mensaje</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $notificaciones as $notificacion )
                            <tr>
                                <td>{{ $notificacion->created_at }}</td>
                                <td>{{ $notificacion->mensaje }}</td>
                            </tr>  
                        @endforeach                                              
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- TABLA VISTA DE NOTIFICACION -->




            <!-- ENVIO DE NOTIFICACION -->
            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR']))
            <div class="col-md-5">
                <div class="card" data-color="crawford">
                    <form action="/crawford/enviar_notificacion_zurich" method="POST" enctype="multipart/form-data">
                        <h4>Enviar notificación</h4>
                        <input type="hidden" name="id_reclamo" value="{{ $reclamos->id }}">
                        <label>Mensaje</label>
                        <textarea class="form-control" name="mensaje_notificacion"></textarea><br>
                        <label>Adjuntar</label>
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-default btn-fill">
                                    Buscar&hellip; <input type="file" name="adjunto" style="display: none;" multiple>
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div><br>  
                        <input type="submit" value="Enviar" class="btn btn-success btn-fill form-control" name="submit">
                    </form>
                </div>
            </div>
            @endif
            <!-- ENVIO DE NOTIFICACION -->


        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#reclamos').addClass('active');
});
</script>
<script type="text/javascript" src="/archivos_js/id_reclamo_zurich.js"></script>

@endsection