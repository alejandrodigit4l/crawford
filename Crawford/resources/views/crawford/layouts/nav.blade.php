
<div class="sidebar" data-color="crawford" data-image="../digit4l_sass/imagen-menu-crawford.jpg" >
    <div class="sidebar-wrapper">
	    <div class="logo">
	        <a href="/crawford/index" class="simple-text">
	            <img style="width: 100%;left: -10px;position: relative;" src="https://www.crawfordaffinitycolombia.com/images/logo.png">
	        </a>
		</div>
		

	    <!--  **************************  -->
	    <!--  NAV  -->
	    <ul class="nav" >
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','CONSULTA_CLIENTES','GESTION','AJUSTADOR','SOPORTE','CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']))
		    	<li class="active" id="estadistica">
		            <a href="/crawford/index" class="dropdown-toggle" data-color="crawford" > <p>Estadistícas</p></a>
		        </li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','CONSULTA_CLIENTES','GESTION','AJUSTADOR','SOPORTE','CC_ZURICH','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']))
		    	<li id="reclamos">
					<a href="/crawford/reclamos" class="dropdown-toggle" data-color="crawford" ><p>Reclamos </p></a>
				</li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','SOPORTE','CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']))
		    	<li id="clientes">
					<a class="dropdown-toggle"  href="/crawford/clientes" data-color="crawford"><p>Clientes </p></a>
				</li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER']))
		    	<li id="chat">
					<a class="dropdown-toggle"  href="/crawford/chat" data-color="crawford"><p>Chat </p></a>
				</li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR']))
		    	<li id="ajustes">
					<a class="dropdown-toggle"  href="/crawford/ajustes" data-color="crawford"><p>Ajustes </p></a>
				</li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR']))
		    	<li id="database">
					<a class="dropdown-toggle"  href="/database" data-color="crawford"><p>DataBase </p></a>
				</li>
	    	@endif
	    	@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','CONSULTA_CLIENTES','GESTION','AJUSTADOR','SOPORTE']))
				<li id="polizas">
					<a class="dropdown-toggle"  href="/crawford/polizas" data-color="crawford"><p>Polizas </p></a>
				</li>
			@endif
			@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR']))
				<li id="polizas">
					<a class="dropdown-toggle"  href="/crawford/polizasbeneficio" data-color="crawford"><p>Subir Imeis </p></a>
				</li>
			@endif
			@if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','CC_ZURICH','GESTION_ZURICH','A_AJUSTE_ZURICH']))
				<li id="admin_clientes">
					<a class="dropdown-toggle"  href="/cliente/crear_cliente" data-color="crawford"><p>Admin Clientes </p></a>
				</li>
			@endif
		</ul>
	    <!--  NAV  -->
	    <!--  **************************  -->


	    <div id="cerrar" class="active" style="position: absolute;left:5%;bottom: 5%;width: 90%;height:50px;background: #207ce5;border-radius: 5px 5px 5px 5px;-moz-border-radius: 5px 5px 5px 5px;-webkit-border-radius: 5px 5px 5px 5px;border: 0px solid #000000;">
			<a class="dropdown-toggle" href="javascript:;" onclick="event.preventDefault();document.getElementById('logout-form').submit();" data-color="crawford"><p style="text-align: center;margin-top: 10px;color:white;">Cerrar Sesion </p></a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
				<input type="hidden" id="token_session" name="token_session" value="{{Auth::user()->token_session}}">
				<input type="hidden" id="email" name="email" value="{{Auth::user()->email}}">
				<input type="hidden" id="password" name="password" value="{{Auth::user()->password}}">
			</form>
		</div>
	</div>
</div>

<style type="text/css">
div a:hover{
	text-decoration: none;
}
</style>

<script type="text/javascript">
$( document ).ready(function() {
	console.log( "ready!" ); 
	setTimeout(eliminaToken, 3600000);
	function eliminaToken() {
		var token = document.getElementById("token_session").value;
		var email = document.getElementById("email").value;
		var password = document.getElementById("password").value;
		data = {
			'token_session': token,
			'password': password,
			'email': email
		}
		$.ajax({
			url: '/eliminaTokenSession',
			type: 'POST',
			data: data,
			success: function(r){
				location.href = 'https://admin.crawfordaffinitycolombia.com/crawford';
			}
		});
	}
});
</script>
