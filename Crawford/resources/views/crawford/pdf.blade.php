<!doctype html>
  <html>
  <head>
  <style>
  *{ box-sizing:border-box; font-size:10px; font-family: helvetica;}
  h1,h2,h3,h4,h5,h6{ margin:0; padding:0;}
  table tr{ float:left; width:100%;}

  .lowercase {text-transform: lowercase;}
  .uppercase {text-transform: uppercase;}
  .capitalize {text-transform: capitalize;}
  .main-header h3{font-size:10; margin:0;}
  .main-header h4{font-size:8; font-weight:bold;}
  .main-header h6{font-size:7; font-weight:bold;}

  </style>
  <meta charset="utf-8">
  <title>PDF</title>
  </head>

  <body>
  <div class="wrapper" style="border:2px solid #000; border-radius:10px; float:left; width:100%; padding:0 10px;">
    <table cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding-top:0px; float:left; width:15%;">
                <a href="http://www.segurosmundial.com.co" target="_blank"><img src="https://admin.crawfordaffinitycolombia.com/img/logo.png" width="120px" alt="logo"></a> 
              </td>
              <td style="text-align:center;  float:left; width:85%; padding:5px 0px 10px 40px;">
                <h3 style="padding:2px 0 0px; font-size:12px; line-height:14px;">COMPAÑÍA MUNDIAL DE SEGUROS S.A.</h3>
                  <h5 style="padding:2px 0 0px; font-size:12px; line-height:14px;">SAMSUNG MOBILECARE</h5>
                  <h5 style="padding:2px 0 0px; font-size:11px; line-height:12px;">PÓLIZA DE CORRIENTE DEBIL PROTECCIÓN A EQUIPOS ELECTRICOS Y ELECTRONICOS</h5>
                  <h5 style="padding:2px 0 5px; font-size:10px; line-height:12px;">VERSIÓN CLAUSULADO NUMERO 13-03-2017-1317-P-11-PSUS9R0000000002</h5>
                  <span style="padding-right:40px; font-weight:normal; font-size:8px; float:right; width:100%; text-align:right;">HOJA No. <b style="font-weight:normal; padding-left:20px; font-size:8px;">1</b></span>  
              </td>
          </tr>

      <tr style="margin-bottom:10px; float:left; width:100%;">
            <td style="float:left; width:10%; text-align:center;">
                <span style="font-size:10px; font-weight:bold; padding-bottom:5px; line-height:14px;">NIT 860.037.013-6</span><br/>
                  <a href="http://www.segurosmundial.com.co" target="_blank" style="font-size:9px; line-height:14px;">www.segurosmundial.com.co</a>
              </td>
              
              <td style="float:left; width:90%; font-size:8px;">
                <table style=" width:100%; border:1px solid #000; border-bottom:0; border-radius:6px 6px 0 0;" cellpadding="4" >
                      <tr>
                          <td width="25%" style="padding-left:10px; font-size:8px;">No.POLIZA <strong>300000010</strong></td>
                          <td width="25%" style="padding-left:10px; font-size:8px;">No.ANEXO</td>
                          <td width="25%" style="padding-left:10px; font-size:8px;">No.CERTIFICADO <strong>00</strong></td>
                          <td width="25%" style="padding-left:10px; font-size:8px;">No.RIESGO</td>
                      </tr>
                      <tr>
                        <td width="30%"  style="font-size:8px; text-align:center;">TIPO DE DOCUMENTO</td>
                          <td colspan="3">
                            <span style=" font-weight:bold; font-size:8px;">CERTIFICADO INDIVIDUAL DE SEGURO</span>
                          </td>
                      </tr>
          </table>              
          
                  <table style="width:100%; border-left:1px solid #000; border-right:1px solid #000;" cellspacing="0" cellpadding="4">                    
                      <tr style="background-color:#00b0f0; color:#fff; ">
                          <th width="20%" style="border:1px solid #000; font-size:8px;">VIGENCIA DE LA POLIZA</th>
                          <!-- <th width="20%" style="border:1px solid #000; font-size:8px; padding:0;">VIGENCIA HASTA</th> -->
                          <!-- <th width="10%" style="border:1px solid #000; font-size:8px;">DIAS</th> -->
                          <th width="25%" style="border:1px solid #000; font-size:8px;">VIGENCIA DEL CERTIFICADO DESDE</th>
                          <!-- <th width="25%" style="border:1px solid #000; font-size:8px;">VIGENCIA DEL CERTIFICADO HASTA</th>  -->
                      </tr>
                      
                      <tr style="background-color:#dce6f1; color:#000; border:1px solid #000; font-size:8px;">
                        <td width="20%" style="font-size:10px; border:1px solid #000;">&nbsp;</td>
                          <!-- <td width="20%" style="font-size:8px; border:1px solid #000;">24:00 Horas del &nbsp;</td> -->
                          <!-- <td width="10%" style="font-size:8px; border:1px solid #000;">&nbsp;</td> -->
                          <td width="25%" style="font-size:10px; border:1px solid #000;">&nbsp;</td>
                          <!-- <td width="25%" style="font-size:8px; border:1px solid #000;">24:00 Horas del &nbsp;</td> -->
                      </tr>
                      </table>
                  
                  <table style="width:100%; border:1px solid #000; border-radius:0 0 6px 6px;"> 
                      <tr>
                        <td style="font-size:8px; padding:5px 0 0;" colspan="4">FECHA DE EXPEDICION</td>  
                      </tr>
                      
                      <tr>
                        <td style="font-size:16px;" width="20%">SUC.EXPEDIDORA</td>
                          <td style="font-size:16px;" width="20%">BOGOTÁ</td>
                          <td style="font-size:16px;" width="30%">DIRECCION: CALLE 33 # 6 B - 24</td>
                          <td style="font-size:16px;" width="30%">TELEFONO: 2855600</td>
                      </tr>
                  </table>
              </td>
          </tr>
          
          <tr>
            <td style="padding-top:10px;"></td>
          </tr>
      </table>
      
    <div style="border:1px solid #000; border-radius:6px; margin-bottom:10px;">
      <table style="font-size:5; border-radius:10px; text-align:center; margin-bottom:0px;" cellpadding="2" cellspacing="0" width="100%" align="center">
      <tr>
          <td width="12%" bgcolor="#DCE6F0" style="border-right:1px solid #000; border-radius:10px; font-size:8px;">TOMADOR</td>
        <td width="30%"  style=" font-size:8px; font-weight:bolder; text-align:left; padding-left:10px;"></td>
        <td width="8%" bgcolor="#DCE6F0" style=" border:1px solid #000000; font-size:8px;">CIUDAD</td>
        <td width="22%" style="font-weight:bolder; font-size:8px;"></td>
        <td width="12%" bgcolor="#DCE6F0" style="border-right:1px solid #000; border-left:1px solid #000; font-size:8px;">NIT / CC</td>
        <td width="16%" style="font-size:8px;"></td>
      </tr>
      <tr>
        <td width="12%" bgcolor="#DCE6F0" style="border-right:1px solid #000; font-size:8px;">DIRECCION</td>
        <td width="30%" style="text-align:left; font-size:7px; padding-left:10px;"></td>
        <td width="8%" ></td>
        <td width="22%" ></td>
        <td width="12%" bgcolor="#DCE6F0" style=" font-size:8px; border-right:1px solid #000; border-left:1px solid #000;">TELEFONO</td>
        <td width="16%" style="font-size:8px;"></td>
      </tr>
       <tr>
          <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-top:1px solid #000; border-right:1px solid #000; ">ASEGURADO</td>
        <td width="100%" rowspan="2" style="border-top:1px solid #000; font-size:9px;">Es la persona natural o jurídica que figure en la factura de venta del equipo asegurado siempre y cuando sea reportado por el tomador</td>
        <td width="8%" style="border-top:1px solid #000;"></td>
        <td width="22%" rowspan="2" style="border-top:1px solid #000; font-size:8px;"></td>
        <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-top:1px solid #000; border-right:1px solid #000; border-left:1px solid #000;">NIT / CC   </td>
        <td width="16%" rowspan="2" style="border-top:1px solid #000; font-size:8px;"></td>
      </tr>
     <!-- <tr>
          <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-right:1px solid #000;">EMAIL</td>
        <td width="8%" bgcolor="#DCE6F0" style="font-size:8px; border:1px solid #000000;">CIUDAD</td>
        <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-right:1px solid #000; border-left:1px solid #000;">TELEFONO</td>
      </tr> -->
      <!-- <tr>
          <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-top:1px solid #000; border-right:1px solid #000;">BENEFICIARIO</td>
        <td width="30%" rowspan="2" style="border-top:1px solid #000; font-size:8px;"></td>
        <td width="8%"  style="border-top:1px solid #000;"></td>
        <td width="22%" rowspan="2" style="border-top:1px solid #000; font-size:8px;"></td>
        <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-top:1px solid #000; border-right:1px solid #000; border-left:1px solid #000;">NIT / CC</td>
        <td width="16%" rowspan="2" style="border-top:1px solid #000; font-size:8px;"></td>
      </tr>
      <tr style=" border-radius:10px;">
          <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-radius:10px; border-right:1px solid #000;">EMAIL</td>
        <td width="8%" bgcolor="#DCE6F0" style="font-size:8px; border:1px solid #000000;">CIUDAD</td>
        <td width="12%" bgcolor="#DCE6F0" style="font-size:8px; border-right:1px solid #000; border-left:1px solid #000;">TELEFONO</td>
      </tr> -->
    </table>
      </div>
      
      <div style="border:1px solid #000; border-radius:6px; margin-bottom:10px;">
          <table style="font-size:5;  text-align:center; " cellpadding="5" cellspacing="0" width="100%" align="center">
            <tr>
                <td width="12%" bgcolor="#DCE6F0" style="border-right:1px solid #000; font-size:8px;">MARCA</td>
                <td width="27%" style="font-weight:bolder; text-align:left; padding-left:20px;font-size:8px;">SAMSUNG</td>
                <td width="10%" bgcolor="#DCE6F0" style=" border-left:1px solid #000; font-size:8px; border-right:1px solid #000000;">MODELO</td>
                <td width="24%" style="font-size:8px;"></td>
                <td width="12%" bgcolor="#DCE6F0" style="border-left:1px solid #000; border-right:1px solid #000; font-size:8px;">IMEI</td>
                <td width="16%" style="font-size:8px;"></td>
            </tr>
            <!-- <tr>
                <td width="12%;" bgcolor="#DCE6F0" style="border-right:1px solid #000; font-size:8px;">DIRECCION</td>
                <td width="27%"></td>
                <td width="10%" bgcolor="#DCE6F0" style="border-left:1px solid #000000; font-size:8px; border-right:1px solid #000000;">CIUDAD</td>
                <td width="24%" style="font-weight:bolder;"></td>
                <td width="12%" bgcolor="#DCE6F0" style="border-right:1px solid #000; border-left:1px solid #000; font-size:8px;">No FACTURA</td>
                <td width="16%" style="font-weight:bolder;"></td>
            </tr> -->
          </table>
    </div>
    
    <table style="font-size:5; border:1px solid black; text-align:center; margin-bottom:10px;" cellpadding="2" cellspacing="0" width="100%" align="center">
      <tr>
          <td width="100%" colspan="3" bgcolor="#00AFEF" style="border:1px solid #000; color:#fff; font-weight:bolder; font-size:7;">CONDICIONES DE COBERTURA</td>
      </tr>
      <tr>
          <td width="44%" bgcolor="#DCE6F0" style="font-size:8px; font-weight:bolder; border-right:1px solid #000; border-left:1px solid #000;">COBERTURA</td>
        <td width="28%" bgcolor="#DCE6F0" style="font-size:8px; font-weight:bolder;">LIMITE ASEGURADO </td>
        <td width="28%" bgcolor="#DCE6F0" style="font-size:8px; font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;">DEDUCIBLES</td>
      </tr>
      <tr>
        <td width="44%" style=" text-align:left; font-size:8px; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000;"></td>
        <td width="28%" style="font-size:8px; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000;"></td>
        <td width="28%" style="font-size:8px; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000;">20% del valor de la reparación incluyendo respuestos, mano de obra e impuestos.</td>
      </tr>
      <tr>
        <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      </tr>
      <tr>
        <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      </tr>
      <tr>
        <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      <td style="font-weight:bolder; border-left:1px solid #000000; border-right:1px solid #000000;"></td>
      </tr>
    </table>
    
    <table style="font-size:5; text-align:center; margin-bottom:10px;"  width="100%" align="center">
      <tr>
            <td style=" width:40%; float:right;"></td>
              <td style=" width:40%; float:right; text-align:center;">
                  <table cellpadding="8" cellspacing="0" width="100%">
                   <tr>
                      <td width="35%" bgcolor="#00AFEF" style="font-size:8px; border:1px solid #000; color:#fff;">PRIMA</td>
                      <td width="30%" bgcolor="#00AFEF" style="font-size:8px; border:1px solid #000; color:#fff;">IVA </td>
                      <td width="35%" bgcolor="#00AFEF" style="font-size:8px; border:1px solid #000; color:#fff;">TOTAL PRIMA + IVA</td>
                  </tr>
                  <tr>
                      <td width="35%" style="border:1px solid #000; font-size:8px;">$ </td>
                      <td width="30%" style="border:1px solid #000; font-size:8px;">$ </td>
                      <td width="35%" style="border:1px solid #000; font-size:8px;">$ </td>
                  </tr> 
                  </table>
              </td>
          </tr>
    </table>
     
      <table cellpadding="5" cellspacing="0" style="border:1px solid #000; margin-bottom:10px;">
      <tr>
        <td bgcolor="#00AFEF" style="border:1px solid #000; color:#fff; font-weight:bolder; font-size:8px;" align="center">OBSERVACIONES</td>
      </tr>
      <tr>
        <td style="color:#000;font-size:6px;">ESTIMADO CLIENTE TENGA EN CUENTA LAS SIGUIETES RECOMENDACIONES:</td>
      </tr>
      <tr>
        <td style="color:#000;font-size:6px; line-height:10px;padding:5px;padding-right:10px;">SI REQUIERE DAR AVISO DE SINIESTRO PUEDE HACERLO A TRAVÉS DE LA APLICACIÓN SAMSUNG CONCIERGE O EN <a style="color:#00AFEF;font-size:6px;" href="http://www.samsung.com.co">www.samsung.com.co</a> O EN LA LÍNEA 018000511102 O EN BOGOTÁ AL 7486072 DE LUNES A SÁBADO DE 8 A.M A 6 P.M </td>
      </tr>
      <tr>
        <td style="color:#000;font-size:6px;">SI SU EQUIPO TIENE CAMBIO POR GARANTÍA DE FABRICANTE, DEBE REPORTAR ESTA NOVEDAD PARA CONSERVAR LA COBERTURA DE ESTE SEGURO.</td>
      </tr>
     </table>
     
      <div style=" position:relative; clear:both;">
        <span style=" position:absolute; left:-11px; top:-10px; "><img src="img/left_vigilado.png" width="11" alt="logo"></span>
      </div> 
      <table cellpadding="5" cellspacing="0" style="border:1px solid #000; margin-bottom:30px;">
        <tr>
        <td bgcolor="#00AFEF" style="border:1px solid #000; color:#fff; font-weight:bolder; font-size:8px; line-height:14px;" align="center">CONDICIONES GENERALES DE LA POLIZA</td>
      </tr>
      <tr>
        <td style="color:#000;font-size:6px; line-height:10px; word-wrap:break-word; word-break:break-all; hyphen:auto;">ADJUNTO ENCONTRARÁ LAS CONDICIONES PARTICULARES DE ESTE SEGURO, SI REQUIERE CONOCER LAS CONDICIONES GENERALES, O TAMBIÉN PUEDE DESCARGARLAS EN LA PAGINA <a style="color:#00AFEF;font-size:6px;" href="www.samsung.com.co/offer">www.samsung.com.co/offer</a> , SI REQUIERE CONOCER LAS CONDICIONES GENERALES, ESTÁS SE </td>
      </tr>
      <tr>
        <td style="color:#000;font-size:6px; line-height:10px; word-wrap:break-word; word-break:break-all; hyphen:auto;padding:5px;padding-right:10px;">ENCUENTRAN MENCIONADAS EN EL TEXTO DEL CLAUSULADO NUMERO 13-03-2017-1317-P-11-PSUS9R0000000002, LAS CUALES PUEDEN SER CONSULTADAS EN LA PÁGINA WEB DE SEGUROS MUNDIAL WWW.SEGUROSMUNDIAL.COM.CO   <a href="http://www.segurosmundial.com.co/media/clausulado-corriente-debil-revisado-vpji-2017.pdf">http://www.segurosmundial.com.co/media/clausulado-corriente-debil-revisado-vpji-2017.pdf</a></td>
      </tr>
     </table>
     
      <table align="center;" cellpadding="5" cellspacing="0" style="table-layout:fixed; width:100%; margin-bottom:10px;">
      <tr>
        <td style="border-bottom:2px solid #000; text-align:center;">
          <img src="img/firma_2.jpg" style="text-align:center;">       
        </td>
              
              <td style="text-align:right;" align="right">
          <span style="display:inline-block; float:right; border-bottom:2px solid #000; width:200px; margin-right:40px; margin-top:60px;"></span>
        </td>
      </tr>
      
      <tr>
        <td width="50%" style="font-size:5; font-weight:bold" align="center">
          COMPAÑÍA MUNDIAL DE SEGUROS S.A. <br/>
  DIRECCION GENERAL CALLE 33 N. 6B - 24 PISOS 2 Y 3 <br/>
  TELEFONO: 2855600 FAX 2851220 <br/>
  SOMOS GRANDES CONTRIBUYENTES - IVA REGIMEN COMUN - AUTORETENEDORES
        </td>
        <td width="50%" style="font-size:5; font-weight:bold" align="center">
          <span style="font-size:8px; text-align:center; vertical-align:top; margin-top:-20px; display:block; padding-left:40px;">TOMADOR</span>
        </td>
      </tr>   
    </table>
    
      <table style="font-size:8px; font-weight:bold; color:#454F63; table-layout:fixed; width:100%; margin-bottom:20px;">
      <tr>
        <td>
          <a href="http://www.segurosmundial.com.co" target="_blank"><img src="img/logo.png" width="140px" alt="footer-logo"></a>
              </td>
        <td>
          <table style="width:100%;">
            <tr>
                        <td colspan="2">Líneas de Atención al Cliente: </td>
                      </tr>
                      <tr>
              <td width="20%">
                <img style="width:20px" src="img/telephone.png" >
              </td>
              <td width="80%">
                Bogota: 748 6072<br/>
                Nacional:01 8000 511 102
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table>
            <tr>
              <td width="20%">
                <img style="width:25px" src="img/tele.png" >
              </td>
              <td width="70%">
                Portal Web <br/>
                <a href="http://www.segurosmundial.com.co" target="_blank">www.segurosmundial.com.co</a>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table>
            <tr>
              <td width="15%">
                <a href="#" target="_blank"><img src="img/facebook.png" ></a>
              </td>
              <td width="15%">
                <a href="#" target="_blank"><img src="img/twitter.png" ></a>
              </td>
              <td width="15%">
                <a href="#" target="_blank"><img src="img/youtube.png" ></a>
              </td>
              <td width="55%" style="white-space:nowrap;">
                Seguros Mundial
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  </body>
  </html>';