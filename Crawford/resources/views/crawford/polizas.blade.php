@extends('crawford.layout')

@section('content')

<style type="text/css">
    .swal-wide{
        width:850px !important;
        max-width: 850px !important;
        height: 800px !important;
        max-height: 800px !important;
    }
</style>

<h4>Polizas pendientes</h4>
    
<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8">
            	<div class="card" data-color="crawford">
                    <label>Buscar:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
            	</div>
            </div>

             <div class="col-md-2">
                <div class="card" data-color="crawford">
                    <label>Filtro Estados:</label>
                    <select class="form-control" name="filtroestado" id="filtroestado" style="color: black;">
                        <option>-- selecciona estado --</option>
                        <option value="1">Activos</option>
                        <option value="2">Rechazadas</option>
                        <option value="3">Vencidos</option>
                        <option value="4">Pendientes</option>
                        <option value="5">Habilitados</option>
                        <option value="6">Requiere nueva petición</option>                         
                    </select>
                </div>
            </div>
          
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " id="tabla" data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Producto</th>
                                    <th>Imei</th>
                                    <th>Modelo</th>
                                    <th>Estado</th>
                                    <th>Ver Poliza</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $polizas as $data )
                                <tr>
                                    <td>{{ $data['id'] }}</td>
                                    <td>{{ $data['name_user'] }}</td>
                                    <td>{{ $data['name_product'] }}</td>
                                    <td>{{ $data['imei'] }}</td>
                                    <td>{{ $data['productos']['modelo'] }}</td>
                                    @if($data['estado_poliza'] == 1)
                                        <td>ACTIVO</td>
                                    @elseif($data['estado_poliza'] == 2)
                                        <td>RECHAZADO</td>
                                    @elseif($data['estado_poliza'] == 3)
                                        <td>VENCIDOS</td>
                                    @elseif($data['estado_poliza'] == 4)
                                        <td>PENDIENTES</td>
                                    @elseif($data['estado_poliza'] == 5)
                                        <td>HABILITADO</td>
                                    @elseif($data['estado_poliza'] == 6)
                                        <td>REQUIERE NUEVA PETICION</td>
                                    @elseif($data['estado_poliza'] == 7)
                                        <td>PAGO PENDIENTE POR APROBACIÓN</td>
                                    @endif
                                    <td><button class="btn btn-primary" onclick="poliza.get_datos_poliza({{$data['id']}})" data-popup-open="popup-1">Ver</button></td>
                                </tr> 
                               
                            @endforeach                                                    
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Datos de poliza</h3></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Nombre de Usario</label>
                        <input class="form-control" type="text" id="nombre_usuario" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Email</label>
                        <input class="form-control" type="text" id="email_usuario" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Celular</label>
                        <input class="form-control" type="text" id="celular_usuario" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Nombre de Producto</label>
                        <input class="form-control" type="text" id="nombre_producto" disabled>
                    </div>
                    <div class="col-sm-4">
                        <label>Fecha de Compra</label>
                        <input class="form-control" type="date" id="fecha_producto" disabled>
                    </div>
                    <div class="col-sm-4">
                        <label>Fecha de Vigencia</label>
                        <input class="form-control" type="date" id="fecha_vigencia" disabled>
                    </div>
                    <div class="col-sm-4">
                        <label>Precio Producto</label>
                        <input class="form-control" type="text" id="precio_producto" disabled>
                    </div>
                    @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER']))
                    <div class="col-sm-4">
                        <label>ID transacción</label>
                        <input class="form-control" type="text" id="id_transaccion" disabled>
                    </div>
                    <div class="col-sm-4">
                        <label>Estado</label>
                        <select class="form-control" id="estado_producto">
                            <option></option>
                            <option value="2">Rechazadas</option>
                            <option value="5">Habilitados</option>
                            <option value="6">Requiere nueva petición</option>
                        </select>
                    </div>
                    <div class="col-sm-offset-8 col-sm-2">
                        <button class="btn btn-success btn-block" style="margin-top: 25px;" onclick="poliza.actualizar_estado()">Actualizar Estado</button>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary btn-block" style="margin-top: 25px;" onclick="poliza.modal_notificacion()">Notificar</button>
                    </div>
                    @endif
                    @if(Auth::user()->authorizeRoles(['CONSULTA_CLIENTES','GESTION','AJUSTADOR','SOPORTE']))
                    <div class="col-sm-4">
                        <label>ID transacción</label>
                        <input class="form-control" type="text" id="id_transaccion" disabled>
                    </div>
                    <div class="col-sm-4">
                        <label>Estado</label>
                        <select class="form-control" id="estado_producto" disabled>
                            <option></option>
                            <option value="2">Rechazadas</option>
                            <option value="5">Habilitados</option>
                            <option value="6">Requiere nueva petición</option>
                        </select>
                    </div>
                    <div class="col-sm-offset-8 col-sm-2">
                        <button class="btn btn-success" style="margin-top: 25px;" onclick="poliza.actualizar_estado()" disabled>Actualizar Estado</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Foto de Equipo y factura</h3></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th>Equipo</th>
                        <th>Factura</th>
                    </thead>
                    <tbody id="imagenes_poliza" align="content"></tbody>
                </table>
            </div>
        </div>
        @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','AJUSTADOR','SOPORTE']))
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Descargar poliza</h3></div>
            <div class="panel-body" id="pdf_poliza"></div>
        </div>
        @endif
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('ul li').removeClass('active');
        $('#polizas').addClass('active');
    });
    $("select[name='filtroestado']").change(function(){
        var estado_poliza = $('#filtroestado').val();
        location.href = "/crawford/filtro_estado/"+estado_poliza;
    });
    var poliza = new Polizas();
    var idactualizar;
    function Polizas(){
        this.init = function(){
            idactualizar = 0;
        };
        this.get_datos_poliza = function(id){
            idactualizar = id;
            $('.url_images').remove();
            $('#bajar_pdf').remove();
            $.ajax({
                url: '/crawford/datos_poliza/'+id,
                type: 'get',
                success: function(response){
                    console.log(response);
                    $('#nombre_usuario').val(response.name_user);
                    $('#email_usuario').val(response.email);
                    $('#celular_usuario').val(response.clientes.celular);
                    $('#nombre_producto').val(response.name_product);
                    $('#fecha_producto').val(response.fecha_compra);
                    $('#precio_producto').val(response.price);
                    $('#estado_producto').val(response.estado_poliza);
                    $('#id_transaccion').val(response.transaction_id);
                    $('#fecha_vigencia').val(response.fecha_vigencia);

                    if (response.url_equipo == null) {
                        $('#imagenes_poliza').append('<td class="url_images"><p>AUN NO EXISTE ALGUN ARCHIVO</p></td>');
                    }else{
                        $('#imagenes_poliza').append(`<td class="url_images"><button class="btn btn-primary" onclick="poliza.ver_imagen('${response.url_equipo}');">Ver Imagen</button></td>`);
                    }

                    if (response.url_factura == null) {
                        $('#imagenes_poliza').append('<td class="url_images"><p>AUN NO EXISTE ALGUN ARCHIVO</p></td>');
                    }else{
                        $('#imagenes_poliza').append(`<td class="url_images"><button class="btn btn-primary" onclick="poliza.ver_imagen('${response.url_factura}');">Ver Imagen</button></td>`);
                    }

                    var archivo_pdf = response.url_poliza.replace("https://admin.crawfordaffinitycolombia.com/polizasActivas/","")
                    $('#pdf_poliza').append('<a href="/crawford/download_poliza/'+archivo_pdf+'" id="bajar_pdf"><button class="btn btn-primary form-control" type="button">Descargar Poliza</button></a>');
                }    
            });
        }; 
        this.actualizar_estado = function(){
            var id = idactualizar;  

            var estado_nuevo = $('#estado_producto').val();

            if (!estado_nuevo) {
                swal("Atención!", "Debe seleccionar el estado nuevo para actualizar!", "error");
            }else{
                $.ajax({
                    data: {
                        'estado_actualizado': $('#estado_producto').val(),
                        'nombre_usuario': $('#nombre_usuario').val(),
                        'email_usuario': $('#email_usuario').val(),
                        'id_poliza': id
                    },
                    url: '/crawford/actualizar_estado_poliza/',
                    type: 'post',
                    success: function(response){
                        console.log(response);
                        location.reload();
                    }
                });
            }
        };
        this.ver_imagen = function(url_imagen){
            swal({
               title: 'FOTO DE EQUIPO',
               text: `
               <div class="container" id="cargar_imagen" style="height: 750px;width: 100%;overflow-y: scroll; overflow-x: scroll;">
               <img class="imagen_cargada img-responsive" style="width: 90%;" src="${url_imagen}">
               </div>
               `,
               html: true,
               showCancelButton: false,
               showConfirmButton: false,
               allowOutsideClick: true,
               closeOnClickOutside: true,
               customClass: 'swal-wide',
           });
        };
        this.modal_notificacion = function(){
            swal({
                title: '<b style="font-weight: normal;">Mensaje de Notificación</b><br>',
                text: `
                    <div class="row">
                        <div class="col-sm-12">
                        <fieldset>
                            <div class="col-sm-offset-1 col-sm-10">
                                <textarea id="mensaje_usuario" class="form-control" style="height: 200px;" placeholder="A continuacion escriba la notificacion. Esta será remitida al correo electronico del usuario."></textarea>
                            </div>
                        </fieldset> 
                        </div>
                    </div><br>`,
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                html: true,
            }, function () {
                $.ajax({
                    data: {
                        'nombre_usuario': $('#nombre_usuario').val(),
                        'email_usuario': $('#email_usuario').val(),
                        'mensaje_usuario': $('#mensaje_usuario').val(),
                    },
                    url: '/crawford/notificacion_poliza/',
                    type: 'post',
                    success: function(response){
                        console.log(response);
                        swal({
                            title: 'Se ha enviado la notificación con exito!',
                            showLoaderOnConfirm: true,
                            html: true,
                        });
                    }
                });
            });
        }

    }
</script>

@endsection