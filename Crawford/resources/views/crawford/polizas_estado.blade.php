@extends('crawford.layout')

@section('content')

<h4>Polizas pendientes</h4>

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8">
            	<div class="card" data-color="crawford">
                    <label>Buscar:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
            	</div>
            </div>

             <div class="col-md-2">
                <div class="card" data-color="crawford">
                    <label>Filtro Estados:</label>
                    <select class="form-control" name="filtroestado" id="filtroestado" style="color: black;">
                        <option>-- selecciona estado --</option>
                        <option value="1">Activos</option>
                        <option value="2">Rechazadas</option>
                        <option value="3">Vencidos</option>
                        <option value="4">Pendientes</option>
                        <option value="5">Habilitados</option>
                        <option value="6">Requiere nueva petición</option>                         
                    </select>
                </div>
            </div>

            <div class="col-md-4">
            	@include('crawford.filtro_estados')
            </div>
          
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " id="tabla" data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Producto</th>
                                    <th>Imei</th>
                                    <th>Modelo</th>
                                    <th>Estado</th>
                                    <th>Ver Poliza</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $polizas_por_estado as $data )
                                <tr>
                                    <td>{{ $data['id'] }}</td>
                                    <td>{{ $data['name_user'] }}</td>
                                    <td>{{ $data['name_product'] }}</td>
                                    <td>{{ $data['imei'] }}</td>
                                    <td>{{ $data['productos']['modelo'] }}</td>
                                    <td>{{ $data['estado_poliza'] }}</td>
                                    <td><button class="btn btn-primary" onclick="poliza.get_datos_poliza({{$data['id']}})" data-popup-open="popup-1">Ver</button></td>
                                </tr> 
                               
                            @endforeach                                                    
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Datos de poliza</h3></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Nombre de Usario</label>
                        <input class="form-control" type="text" id="nombre_usuario" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Email</label>
                        <input class="form-control" type="text" id="email_usuario" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Nombre de Producto</label>
                        <input class="form-control" type="text" id="nombre_producto" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Fecha de Compra</label>
                        <input class="form-control" type="date" id="fecha_producto" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Precio Producto</label>
                        <input class="form-control" type="text" id="precio_producto" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Estado</label>
                        <select class="form-control" id="estado_producto">
                            <option></option>
                            <option value="2">Rechazadas</option>
                            <option value="5">Habilitados</option>
                            <option value="6">Requiere nueva petición</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-success" style="margin-top: 25px;" onclick="poliza.actualizar_estado()">Actualizar Estado</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Foto de Equipo y factura</h3></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th>Equipo</th>
                        <th>Factura</th>
                    </thead>
                    <tbody id="imagenes_poliza" align="content"></tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Descargar poliza</h3></div>
            <div class="panel-body" id="pdf_poliza"></div>
        </div>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
	$(document).ready(function(){
	    $('ul li').removeClass('active');
	    $('#polizas').addClass('active');
	});
    $("select[name='filtroestado']").change(function(){
        // alert('Ha cambiado el estado a: ' + $('#filtroestado').val());
        var estado_poliza = $('#filtroestado').val();
        location.href = "/crawford/filtro_estado/"+estado_poliza;
        // poliza.filtro_estado();
    });
    var poliza = new Polizas();
    var idactualizar;
    function Polizas(){
        this.init = function(){
            idactualizar = 0;
        };
        this.get_datos_poliza = function(id){
            idactualizar = id;
            $('.url_images').remove();
            $('#bajar_pdf').remove();
            $.ajax({
                url: '/crawford/datos_poliza/'+id,
                type: 'get',
                success: function(response){
                    console.log(response);
                    $('#nombre_usuario').val(response.name_user);
                    $('#email_usuario').val(response.email);
                    $('#nombre_producto').val(response.name_product);
                    $('#fecha_producto').val(response.fecha_compra);
                    $('#precio_producto').val(response.price);
                    $('#estado_producto').val(response.estado_poliza);

                    if (response.url_equipo == null) {
                        $('#imagenes_poliza').append('<td class="url_images"><p>AUN NO EXISTE ALGUN ARCHIVO</p></td>');
                    }else{
                        $('#imagenes_poliza').append('<td class="url_images"><img src="'+response.url_equipo+'" style="width: 400px;"></td>');
                    }

                    if (response.url_factura == null) {
                        $('#imagenes_poliza').append('<td class="url_images"><p>AUN NO EXISTE ALGUN ARCHIVO</p></td>');
                    }else{
                        $('#imagenes_poliza').append('<td class="url_images"><img src="'+response.url_factura+'" style="width: 400px;"></td>');
                    }

                    var archivo_pdf = response.url_poliza.replace("https://admin.crawfordaffinitycolombia.com/doc_reclamos/","")
                    $('#pdf_poliza').append('<a href="/crawford/download_poliza/'+archivo_pdf+'" id="bajar_pdf"><button class="btn btn-primary form-control" type="button">Descargar Poliza</button></a>');
                }    
            });
        }; 
        this.actualizar_estado = function(){
            var id = idactualizar;
            $.ajax({
                data: {
                    'estado_actualizado': $('#estado_producto').val(),
                    'id_poliza': id
                },
                url: '/crawford/actualizar_estado_poliza/',
                type: 'post',
                success: function(response){
                    console.log(response);
                    location.reload();
                }
            });
        };
    }
</script>

@endsection