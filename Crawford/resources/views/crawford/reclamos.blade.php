@extends('crawford.layout')

@section('content')

<h4>Reclamaciones </h4>

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-10">
            	<div class="card" data-color="crawford">
                    <form action="/crawford/search" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Id:</label>
                                <input type="number" name="id_reclamo" class="form-control" value="{{ $id_reclamo }}">  
                            </div>
                            <div class="col-md-3">
                                <label>Cédula:</label>
                                <input type="text" name="cedula" class="form-control" value="{{ $cedula }}">
                            </div>
                            <div class="col-md-3">
                                <label>Nombre:</label>
                                <input type="text" name="nombre_asegurado" class="form-control" value="{{ $nombre_asegurado }}">
                            </div>
                            <div class="col-md-3">
                                <label>Apellido:</label>
                                <input type="text" name="apellido_asegurado" class="form-control" value="{{ $apellido_asegurado }}">
                            </div>
                            <div class="col-md-3">
                                <label>Email:</label>
                                <input type="text" name="email_asegurado" class="form-control" value="{{ $email_asegurado }}">
                            </div>
                            <div class="col-md-3">
                                <label>Filtro Marca:</label>
                                <select class="form-control" name="filtro_marca">
                                    @foreach( $marcas as $marca )
                                        @if($filtro_marca == $marca->id)
                                            <option value="{{ $marca->id }}" selected>{{ $marca->nombre }}</option>
                                        @else
                                            <option value="{{ $marca->id }}">{{ $marca->nombre }}</option>
                                        @endif
                                    @endforeach
                                </select>          
                            </div>
                            @if($filtro_marca == 5)
                                <div class="col-md-3">
                                    <label>Filtro estado:</label>
                                    <select class="form-control" name="filtro_estado">
                                        <option value="">NINGUNO</option>
                                        @foreach( $estados_reclamo as $estado )
                                            @if($filtro_estado == $estado->id)
                                                <option value="{{ $estado->id }}" selected>{{ $estado->nombre_estado }}</option>
                                            @else
                                                <option value="{{ $estado->id }}">{{ $estado->nombre_estado }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="col-md-3">
                                <button class="btn btn-success btn-fill btn-block margin-top-24">Filtrar</button>
                            </div>
                        </div>
                    </form>
            	</div>
            </div>

            @if(Auth::user()->authorizeRoles(['SUPERADMINISTRADOR','ADMINISTRADOR','LIDER','GESTION','AJUSTADOR','GESTION_ZURICH','A_PROOV_ZURICH','A_AJUSTE_ZURICH']))
            <div class="col-md-2">
                <div class="card" data-color="crawford">
                    <form id="descarga_excel" action="/crawford/export/reclamos" method="get" style="display: none;">
                        <input type="hidden" name="id_reclamo" value="{{ $id_reclamo }}">
                        <input type="hidden" name="cedula" value="{{ $cedula }}">
                        <input type="hidden" name="nombre_asegurado" value="{{ $nombre_asegurado }}">
                        <input type="hidden" name="apellido_asegurado" value="{{ $apellido_asegurado }}">
                        <input type="hidden" name="email_asegurado" value="{{ $email_asegurado }}">
                        <input type="hidden" name="filtro_marca" value="{{ $filtro_marca }}">
                        @if($filtro_marca == 5)
                            <input type="hidden" name="filtro_estado_descarga" value="{{ $filtro_estado }}">
                        @endif
                    </form>
                    <center>
                    <a href="javascript:;" onclick="event.preventDefault();document.getElementById('descarga_excel').submit();" class="btn btn-success" ><img width="28" src="http://www.iconarchive.com/download/i86104/graphicloads/filetype/excel-xls.ico"><br>Descargar</a></center>
                </div>
            </div>
            @endif

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width heigth-600-overflow">
                        <table class="table table-hover " id="tabla" data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>cedula</th>
                                    <th>nombre asegurado</th>
                                    <th>apellido asegurado</th>
                                    <th>email</th>
                                    <th>estado reclamo</th>
                                    <th>Actualizado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $reclamos as $data )
                                    <tr>
                                        @if($data->datos_cliente->marca == 5)
                                        <td><a href="/crawford/idreclamo_zurich/{{ $data->id }}" class="a-black">{{ $data->id }}</a></td>
                                        @else
                                        <td><a href="/crawford/idreclamo/{{ $data->id }}" class="a-black">{{ $data->id }}</a></td>
                                        @endif
                                        <td><a href="/crawford/cliente/{{ $data->datos_cliente->id }}" class="a-black">{{ $data->datos_cliente->cedula }}</a></td>
                                        @if($filtro_marca == 3)
                                            <td>{{ $data->datos_cliente->name }}</td>
                                            <td>{{ $data->datos_cliente->apellidos }}</td>
                                        @else
                                            <td>{{ $data->datos_cliente->nombre }}</td>
                                            <td>{{ $data->datos_cliente->lastname }}</td>
                                        @endif
                                        <td>{{ $data->datos_cliente->email }}</td>
                                        <td>
                                            @if($filtro_marca == 5)
                                                <a href="#" class="a-black">{{ $data->estado_del_reclamo->nombre_estado }}</a>
                                            @else
                                                <a href="#" class="a-black">{{ $data->datos_estado_poliza->nombre }}</a>
                                            @endif
                                        </td>
                                        <td>{{ $data['updated_at'] }}</td>
                                    </tr> 
                                @endforeach                                                    
                            </tbody>
                        </table>
                        
                    </div>

                    {{ $reclamos->render() }}
                    
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#reclamos').addClass('active');
});
</script>

@endsection