@extends('crawford.layout')

@section('content')

	<h4>Documents id[{{ $reclamo }}]</h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Documents</p>      
                    <form action="/documentsmodel/uploadSubmitBackend" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="estado" id="estado" class="form-control" value="Aceptado">
						<input type="hidden" name="validacion" id="validacion" class="form-control" placeholder="validacion" value="SIN VALIDACION">
						<input type="hidden" name="respuestaback" id="respuestaback" class="form-control" placeholder="respuestaback" value="SIN RESPUESTA">
						<input type="file" name="file" id="file" class="inputfile" />
                        <label class="btn btn-succes form-control" for="file">Escoger un archivo</label>
                        <input type="hidden" name="FileType" value="camara">
                        <input type="hidden" name="redirect" value="/crawford/idreclamo/{{ $reclamo }}">
						<input type="hidden" name="reclamo" id="reclamo" class="form-control" value="{{ $reclamo }}">
						<input type="hidden" name="listcheking" id="listcheking" class="form-control" value="{{ $listcheking }}">
                        <input type="hidden" name="cliente" id="cliente" class="form-control" value="">
                        <input type="submit" value="Subir" class="btn btn-success form-control" />
                    </form>      		
            	</div>
            </div>
           
           
        </div>
    </div>
</div>

<style type="text/css">
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    display: inline-block;
    color: #207ce5;
}

.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #207ce5;
    color: white;
}
.inputfile + label {
    cursor: pointer; /* "hand" cursor */
}
.inputfile:focus + label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>
	
@endsection

