    @extends('crawford.layout')

@section('content')

	<h4>Estados_polizas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 300px;">
            		<p>Formulario Estados_polizas</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-4">
                            <label>Selecciona la marca a la que pertenece este estado</label><br>
                            <select name="marca" id="marca" class="form-control" >
                                <option value="">Seleccione marca</option>
                                @foreach( $marcas as $marca)
                                <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label>Escriba el tipo de seguro del estado</label><br>
                            <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                                <option value="">Seleccione tipo seguro</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label>Selecciona el homologo (BACK)</label><br>
                            <select name="homologo" id="homologo" class="form-control" >
                                <option value="">Seleccione homologo</option>
                                @foreach( $homologos as $homologo)
                                <option value="{{ $homologo['id'] }}">{{ $homologo['nombre'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe el nombre del estado (FRONT)</label><br>
                            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre del estado (FRONT)" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe la descripcion del estado</label><br>
                            <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="estados_polizas.save();">Guardar</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="estados_polizas.sendUpdate();">Actualizar</div>
                        </div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>homologo</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $estados_polizas as $estados_poliza )
                                <tr>
                                    <td>{{ $estados_poliza['id'] }}</td>
                                    <td>{{ $estados_poliza['homologo']['nombre'] }}</td>
                                    <td>{{ $estados_poliza['nombre'] }}</td>
                                    <td>{{ $estados_poliza['descripcion'] }}</td>
                                    <td>{{ $estados_poliza['marca']['nombre'] }}</td>
                                    <td>{{ $estados_poliza['tipo_seguro']['nombre'] }}</td>
                                    <td>{{ $estados_poliza['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="estados_polizas.update({{ $estados_poliza['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="estados_polizas.delete({{ $estados_poliza['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var estados_polizas = new Estados_polizas();


$('#marca').on('change', function() {
    estados_polizas.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});


$('#actualizar').hide();
$('#guardar').show();

function Estados_polizas(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');     
        var homologo = this.validate('homologo'); 

        var parametrer = {
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'homologo' : homologo
        };

        $.ajax({
            url: 'estados_polizas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'estados_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');   
        var id     = this.validate('id');   
        var tipo_seguro = this.validate('tipo_seguro'); 
        var homologo = this.validate('homologo');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'homologo' : homologo
        };

        $.ajax({
            url: 'estados_polizas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'estados_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'estados_polizas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'estados_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'estados_polizas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#marca').val(response.body[0].marca.id);
                    estados_polizas.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
                    $('#tipo_seguro').val(response.body[0].tipo_seguro.id);
                    $('#homologo').val(response.body[0].homologo.id);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'estados_polizas/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    }
}


</script>
	
@endsection