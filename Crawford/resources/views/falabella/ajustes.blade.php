@extends('falabella.layout')

@section('content')

	<h4>Ajustes </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="falabella">
            		<p>Escoje la base de datos a modificar</p>            		
            	</div>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/tipo_polizas" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>1. tipo_polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/listchekings" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>2. listchekings</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="/productos" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>3. productos</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/roles" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>4.roles</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/usuarios" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>5. usuarios</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="/arbol_productos" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>6. arbol productos</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/homologos" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>7.homologos </p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/estados_polizas" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>8.estados polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/clientes" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>9. clientes</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="/ciudades" class="data"><div class="card" data-color="falabella">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>10. ciudades</p>                 
                </div></a>
            </div>
            
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apiconfirm note8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card " data-color="falabella" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apiconfirm s8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apirecives note8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apirecives s8s</p>            		
            	</div></a>
            </div>           
           <div class="col-md-2" id="reclamo">
            	<a href="#" class="data"><div class="card" data-color="falabella" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>ben. campanas</p>            		
            	</div></a>
            </div>
           
           

        </div>
    </div>
</div>

<style type="text/css">
	.data{
		color: gray;
	}
	.data:hover{
		color: black;
	}
</style>

<script type="text/javascript">

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#ajustes').addClass('active');
});

</script>
	
@endsection