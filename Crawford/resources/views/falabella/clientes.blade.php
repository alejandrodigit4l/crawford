@extends('falabella.layout')

@section('content')

	<h4>Todos los clientes</h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-10">
            	<div class="card" data-color="falabella">
                    <label>Buscar:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
            	</div>
            </div>

            <div class="col-md-2">
                <div class="card" data-color="falabella">
                    <center>
                    <a href="/falabella/export/clientes" class="btn btn-success" ><img width="28" src="http://www.iconarchive.com/download/i86104/graphicloads/filetype/excel-xls.ico"><br>Descargar Formato XLS</a></center>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card" data-color="falabella">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="falabella">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>clientid</th>
                                    <th>nombre</th>
                                    <th>lastname</th>
                                    <th>email</th>
                                    <th>cedula</th>
                                    <th>password</th>
                                    <th>tokenAPI</th>
                                    <th>celular</th>
                                    <th>fechanacimiento</th>
                                    <th>fechaCC</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>                              
                                    <th><div style="width: 140px;text-align: center;">fecha_creado</div></th>
                                    <!--th></th-->
                                    <!--th>Crear</th-->
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $clientes as $usuario )
                                <tr>
                                    <td>{{ $usuario['id'] }}</td>
                                    <td>{{ $usuario['clientid'] }}</td>
                                    <td>{{ $usuario['nombre'] }}</td>
                                    <td>{{ $usuario['lastname'] }}</td>
                                    <td>{{ $usuario['email'] }}</td>
                                    <td>{{ $usuario['cedula'] }}</td>
                                    <td>{{ $usuario['password'] }}</td>
                                    <td>{{ $usuario['tokenAPI'] }}</td>
                                    <td>{{ $usuario['celular'] }}</td>
                                    <td>{{ $usuario['fechanacimiento'] }}</td>
                                    <td>{{ $usuario['fechaCC'] }}</td>
                                    <td>{{ $usuario['marca']['nombre'] }}</td>
                                    <td>{{ $usuario['tipo_seguro']['nombre'] }}</td>
                                    <td>{{ $usuario['aseguradora']['nombre'] }}</td>
                                    <td>{{ $usuario['created_at'] }}</td>
                                    <!--td><a href="/falabella/create_reclamo/{{ $usuario['id'] }}"><button class="btn btn-success form-control">Crear reclamo</button></a></td-->
                                    <!--td><button class="btn btn-primary" onclick="sendTestPush({{ $usuario['id'] }})">Send Test Push</button></td-->
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#clientes').addClass('active');
    $('#search').focus();
});


var reclamos_crawford = new Reclamos_crawford;

$('#search').keypress(function(e) {
    if(e.which == 13) {
        console.log('buscando /.....');
        if($('#search').val()==''){
            location.href = '/falabella/clientes/search/ALL';
        }
        else{
            location.href = '/falabella/clientes/search/'+$('#search').val();
        }
        
    }
});

function sendTestPush(userId) {
    var message = prompt("Escribe el texto para enviar:", "Bienvenido...");
    if (message == null || message == "") {
        console.log('[ envio de mensaje push onesignal ] Sera en otra oportunidad ');
    } else {
        $.ajax({
            url: '/send_push?user_id=' + userId + '&message=' + message, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
            }
        });
    }
    
}


function Reclamos_crawford(){

    this.init = function(){
        //
    };

    this.marca = function(MARCA){
        //redireccionar a MARCA
        window.location.href = "/falabella/filtro_marca/"+MARCA;
    }

}

</script>

@endsection