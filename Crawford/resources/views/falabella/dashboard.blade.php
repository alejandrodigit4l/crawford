@extends('falabella.layout')

@section('content')

<h4>Estadísticas </h4>

	<div class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <p>Número de reclamaciones por fecha</p>
                    <canvas id="lineChart"></canvas>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card" data-color="crawford">
                    <p>Cantidad de clientes por aseguradora</p>
                    <canvas id="polarChart"></canvas>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card" data-color="crawford">
                    <p>Cantidad productos comprados</p>
                    <canvas id="barChart"></canvas>
                </div>
            </div>

            <!--div class="col-md-4">
                <div class="card" data-color="crawford">
                    <p>Cantidad de reclamaciones por productos</p>
                    <canvas id="radarChart"></canvas>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" data-color="crawford">
                    <p>Cantidad productos comprados</p>
                    <canvas id="pieChart"></canvas>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" data-color="crawford">
                    <p>Cantidad productos comprados</p>
                    <canvas id="doughnutChart"></canvas>
                </div>
            </div-->

        </div>
    </div>
</div>


<script type="text/javascript">


var estadistica = new Estadistica();


estadistica.lineChart('2');
estadistica.polar();
estadistica.bar();

function Estadistica(){


    this.lineChart = function( aseguradora ){
        urls = '/reclamacionesxfecha/'+aseguradora;
        $.ajax({
            url: urls, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                //linealchart
                labelss = [];
                datas = [];

                response.forEach(function(element) {
                    //console.log(element);
                    contador = 0;
                    labelss.push(element.fechasiniestro);
                    datas.push(element.contador);
                });

                var ctxL = document.getElementById("lineChart").getContext('2d');
                var myLineChart = new Chart(ctxL, {
                    type: 'line',
                    data: {
                        labels: labelss,
                        datasets: [
                            {
                                label: "# de reclamos",
                                fillColor: "rgba(220,220,220,0.2)",
                                strokeColor: "rgba(220,220,220,1)",
                                pointColor: "rgba(220,220,220,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(220,220,220,1)",
                                data: datas
                            }
                        ]
                    },
                    options: {
                        responsive: true
                    }    
                });
            }
        }); 
    }

    this.polar = function(){
        $.ajax({
            url: '/clientesaseguradoras',
            type: "GET",
            success: function(response){
                console.log(response);
                labelss = [];
                datas = [];
                response.forEach(function(element){
                    console.log(element);
                    if(element.aseguradora.nombre == 'FALABELLA'){
                        labelss.push(element.aseguradora.nombre);
                        datas.push(element.contador);
                    }                    
                });
                //polar
                var ctxPA = document.getElementById("polarChart").getContext('2d');
                var myPolarChart = new Chart(ctxPA, {
                    type: 'polarArea',
                    data: {
                        labels: labelss,
                        datasets: [
                            {
                                data: datas,
                                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                            }
                        ]
                    },
                    options: {
                        responsive: true
                    }    
                });    
            }
        });         
    }

    this.bar = function(){
        urls = '/reclamosaseguradora';
        $.ajax({
            url: urls,
            type: "GET",
            success: function(response){
                console.log('reclamosaseguradora');
                console.log(response);
                labelss = [];
                datas = [];
                response.forEach(function(element){
                    console.log(element);
                    if(element.aseguradora.nombre == 'FALABELLA'){
                        labelss.push(element.aseguradora.nombre);
                        datas.push(element.contador);
                    } 
                });
                var ctxB = document.getElementById("barChart").getContext('2d');
                var myBarChart = new Chart(ctxB, {
                    type: 'bar',
                    data: {
                        labels: labelss,
                        datasets: [{
                            label: '# de reclamos x aseguradora',
                            data: datas,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    optionss: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });  
            }
        });
    }



}

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#estadistica').addClass('active');
});


    //line
/*var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Crawford",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "Falabella",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label: "Allianz",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [90, 27, 86, 19, 40, 48, 28]
            },
            {
                label: "segurosmundial",
                fillColor: "rgba(94,171,77,0.2)",
                strokeColor: "rgba(94,171,77,1)",
                pointColor: "rgba(94,171,77,1)",
                pointStrokeColor: "#207ce5",
                pointHighlightFill: "#207ce5",
                pointHighlightStroke: "rgba(94,171,77,1)",
                data: [30, 48, 20, 1, 70, 27, 90]
            }
        ]
    },
    options: {
        responsive: true
    }    
});*/


//radar
var ctxR = document.getElementById("radarChart").getContext('2d');
var myRadarChart = new Chart(ctxR, {
    type: 'radar',
    data: {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [
            {
                label: "Celular S8",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 90, 81, 56, 55, 40]
            },
            {
                label: "Seguro de vida",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 96, 27, 100]
            }
        ]
    },
    options: {
        responsive: true
    }    
});
            

//bar
/*var ctxB = document.getElementById("barChart").getContext('2d');
var myBarChart = new Chart(ctxB, {
    type: 'bar',
    data: {
        labels: ["producto1", "producto2", "producto3", "producto4", "producto5", "producto6"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    optionss: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});*/
           

/*//polar
var ctxPA = document.getElementById("polarChart").getContext('2d');
var myPolarChart = new Chart(ctxPA, {
    type: 'polarArea',
    data: {
        labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
        datasets: [
            {
                data: [300, 50, 100, 40, 120],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true
    }    
});*/


//pie
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
    type: 'pie',
    data: {
        labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
        datasets: [
            {
                data: [300, 50, 100, 40, 120],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true
    }    
});

//doughnut
var ctxD = document.getElementById("doughnutChart").getContext('2d');
var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
        labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
        datasets: [
            {
                data: [300, 50, 100, 40, 120],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true
    }    
});




</script>
    
@endsection