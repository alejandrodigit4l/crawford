@extends('falabella.layout')

@section('content')

	<h4>Reclamaciones Estado</h4>

    <?php $cont=0; ?>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="falabella">
                    <label>Buscar:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
                </div>
            </div>

            <div class="col-md-12">
                <div class="card" data-color="falabella">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover "  id="tabla" data-color="falabella">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>reclamoid</th>
                                    <th>cedula</th>
                                    <th>nombreasegurado</th>
                                    <th>apellidoasegurado</th>
                                    <th>fechanacimientoasse</th>
                                    <th>ciudadrecidencia</th>
                                    <th>telefonofijo</th>
                                    <th>celular</th>
                                    <th>direccion</th>
                                    <th>email</th>
                                    <th>telefonolaboral</th>
                                    <th>jsonrespuestaform</th>
                                    <th>fechasiniestro</th>
                                    <th>horasiniestro</th>
                                    <th>descripcionsiniestro</th>
                                    <th>textobackend</th>
                                    <th>observaciones</th>
                                    <th>motivobaja</th>
                                    <th>cliente</th>
                                    <th>estados_poliza</th>
                                    <th>producto</th>
                                    <th>ciudadsiniestro</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $reclamos as $data )
                                <tr>
                                    <td>{{ $data['id'] }}</td>
                                    <td><a href="/falabella/idreclamo/{{ $data['id'] }}">{{ $data['reclamoid'] }}</a></td>
                                    <td>{{ $data['cedula'] }}</td>
                                    <td>{{ $data['nombreasegurado'] }}</td>
                                    <td>{{ $data['apellidoasegurado'] }}</td>
                                    <td>{{ $data['fechanacimientoasse'] }}</td>
                                    <td><a href="/falabella/ciudad/{{ $data['ciudadrecidencia']['id'] }}">{{ $data['ciudadrecidencia']['nombre'] }}</a></td>
                                    <td>{{ $data['telefonofijo'] }}</td>
                                    <td>{{ $data['celular'] }}</td>
                                    <td>{{ $data['direccion'] }}</td>
                                    <td>{{ $data['email'] }}</td>
                                    <td>{{ $data['telefonolaboral'] }}</td>
                                    <td id="reclamo{{ $cont }}"><input type="hidden" id="input{{ $cont }}" value="{{ $data['jsonrespuestaform'] }}"></td>
                                    <td>{{ $data['fechasiniestro'] }}</td>
                                    <td>{{ $data['horasiniestro'] }}</td>
                                    <td>{{ $data['descripcionsiniestro'] }}</td>
                                    <td>{{ $data['textobackend'] }}</td>
                                    <td>{{ $data['observaciones'] }}</td>
                                    <td>{{ $data['motivobaja'] }}</td>
                                    <td><a href="/falabella/cliente/{{ $data['cliente']['id'] }}">{{ $data['cliente']['cedula'] }}</a></td>
                                    <td>{{ $data['estados_poliza']['nombre'] }}</td>
                                    <td>{{ $data['producto']['titulo'] }}</td>
                                    <td><a href="/falabella/ciudad/{{ $data['ciudadsiniestro']['id'] }}">{{ $data['ciudadsiniestro']['nombre'] }}</a></td>
                                </tr> 
                                <?php $cont++; ?>
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
function prueba(r,id){
    $(id).html('');
    console.log(r);
    console.log('Cantidad'+r.length);
    var obj = JSON.parse(r);
    console.log(obj.pasos);
    for (var i = 0; i < obj.pasos.length; i++) {
        console.log('//'+obj.pasos[i].name);
        $(id).append(obj.pasos[i].name+':'+obj.pasos[i].value+'<br>');
    }
}

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#reclamos').addClass('active');
    $('#search').focus();
    //recorrer tabla para sacar las respuestas
    var nFilas = $("#tabla tr").length;
    for (var i = 0; i < nFilas; i++) {
        prueba( $('#input'+i).val() , '#reclamo'+i);
    }

});


$('#search').keypress(function(e) {
    if(e.which == 13) {
        console.log('buscando /.....');
        if($('#search').val()==''){
            location.href = '/falabella/search/ALL';
        }
        else{
            location.href = '/falabella/search/'+$('#search').val();
        }
        
    }
});

var reclamos_crawford = new Reclamos_crawford;

$('#filtromarca').on('change', function() {
    reclamos_crawford.marca($('#filtromarca').val());
});

function Reclamos_crawford(){

    this.init = function(){
        //
    };

    this.marca = function(MARCA){
        //redireccionar a MARCA
        window.location.href = "/falabella/filtro_marca/"+MARCA;
    }

}

</script>

@endsection