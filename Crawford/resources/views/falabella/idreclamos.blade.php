@extends('falabella.layout')

@section('content')

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="falabella">
            		<h4>Reclamo #  {{ $reclamos[0]['reclamoid'] }}</h4><br>
            		<p>Falabella </p>
            	</div>
            </div>

            <div class="col-md-12">
                <div class="card" data-color="falabella">
                    <h4>Cliente</h4>
                    <table class="table table-hover " data-color="falabella">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nombre</th>
                                <th>cedula</th>
                                <th>fecha cc</th>
                                <th>fecha nacimiento</th>
                                <th>email</th>
                                <th>celular</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="/falabella/cliente/{{ $reclamos[0]['cliente']['id'] }}">{{ $reclamos[0]['cliente']['id'] }}</a></td>
                                <td>{{ $reclamos[0]['cliente']['nombre'].' '.$reclamos[0]['cliente']['lastname'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['cedula'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['fechaCC'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['fechanacimiento'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['email'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['celular'] }}</td>
                                <td>{{ $reclamos[0]['cliente']['created_at'] }}</td>
                            </tr>                                                
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-9">
            	<div class="card" data-color="falabella">
            		<h4>Datos del Reclamo</h4>
            		<div class="row">

            			<div class="col-md-3">
            				<label for="nombreasegurado">Nombre Asegurado</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['nombreasegurado'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad Residencia</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['ciudadrecidencia']['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Email</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['email'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Celular</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['celular'] }}" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Cedula</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['cedula'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha nacimiento</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['cliente']['fechanacimiento'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono fijo</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['telefonofijo'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono laboral</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['telefonolaboral'] }}" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Direccion Laboral</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['direccion'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Tipo Producto</label>
            				<input type="text" class="form-control" name="" value="{{ $arrayTipo_polizas[0]['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Producto</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['producto']['titulo'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Tienda</label>
            				<input type="text" class="form-control" name="" value="{{ $arrayTiendas[0]['nombre'] }}" disabled>
            			</div>
            		</div>
            	</div>
            </div>

            <div class="col-md-3">
                <div class="card" data-color="falabella">
                    <h4>Exportar Reclamo</h4>
                    {{-- <a href="/falabella/export/zip/{{ $reclamos[0]['id'] }}" class="btn btn-primary" style="width: 100%">Exportar zip </a> <br><br> --}}
                    <a href="/falabella/export/excel/{{ $reclamos[0]['id'] }}" class="btn btn-success" style="width: 100%">Exportar Excel </a>
                </div>
            </div>

            <div class="col-md-9">
            	<div class="card" data-color="falabella">
            		<h4>Datos del Siniestro</h4>
            		<div class="row">
            			<div class="col-md-3">
            				<label for="nombreasegurado">Estado</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['estados_poliza']['nombre'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['fechasiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Hora</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['horasiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['ciudadsiniestro']['nombre'] }}" disabled>
            			</div>

            			<div class="col-md-12">
            				<label for="nombreasegurado">Descripcion</label>
            				<input type="text" class="form-control" name="" value="{{ $reclamos[0]['descripcionsiniestro'] }}" disabled>
            			</div>
            			<div class="col-md-12">
            				<label for="nombreasegurado">Observaciones</label>
            				<input type="text" class="form-control" name="newobservaciones" id="newobservaciones" value="{{ $reclamos[0]['observaciones'] }}" >
            			</div>
                        <div class="col-md-12">
                            <h4>Historial Observaciones</h4><hr>
                            @foreach( $observaciones as $observacion )
                            <p>* <label style="font-size: 15px;">{{ $observacion['observacion'] }} </label> <small style="font-size: 10px;">{{ $observacion['created_at'] }}</small></p>
                            @endforeach
                        </div>
            		</div>
            	</div>
            </div>

            <!--div class="col-md-3">
                <div class="card" data-color="falabella">
                    <h4>Cambiar Estado/código interno</h4>
                    <label>Estado</label>
                    <select class="form-control" id="newestado">
                    	<option value="">Seleccione estado</option>
                    	@foreach( $homologos as $homologo)
                    	<option value="{{  $homologo['id'] }}">{{ $homologo['nombre'] }}</option>
                    	@endforeach
                    </select>
                    <label>Codigo Interno</label>
                    <input type="text" class="form-control" name="codigointerno" id="codigointerno" value="{{ $reclamos[0]['textobackend'] }}">
                </div>
            </div-->

            <div class="col-md-9">
                <div class="card" data-color="falabella" style="overflow-x: scroll;">
                    <h4>Documentos</h4>
                    <table class="table table " data-color="falabella">
                        <thead>
                            <tr>
                                <th><div style="width: 70px;text-align: center;">Titulo</div></th>
                                <th><div style="width: 120px;text-align: center;">Ayuda</div></th>
                                <th>Imagenes</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $listchekings as $data )
                            @if( $data['reclamo']==0 )
                            <tr>
                                <td><div style="width: 70px;">{{ $data['titulo'] }}</div></td>
                                <td><div style="width: 120px;">{{ $data['ayuda'] }}</div></td>
                                <td>
                                    
                                    <table class="table " data-color="falabella">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <!--th></th-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $documents as $document )
                                            <tr>
                                            @if( $document['listcheking'] == $data['id'] )
                                                
                                                @if( $data['tipo'] == 'camara' )
                                                <td><div style="width: 100px;"><a href="{{ $document['url'] }}" download target="_blank"><img style="width: 100%;" src="{{ $document['url'] }}" ></a></div></td>
                                                @endif

                                                @if( $data['tipo'] == 'audio' )
                                                <td><div style="width: 100px;">
                                                    <audio controls>
                                                      <source src="{{ $document['url'] }}" type="audio/ogg">
                                                      <source src="{{ $document['url'] }}" type="audio/mpeg">
                                                    Your browser does not support the audio element.
                                                    </audio>
                                                </div></td>
                                                @endif

                                                <td><div style="width: 180px;">{{ $document['created_at'] }}</div></td>
                                                <!--td><a onclick="eliminar(`/documentsmodel/delete/{{ $document['id'] }}`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td-->
                                                 
                                            @endif
                                            </tr> 
                                        @endforeach                                              
                                        </tbody>
                                    </table>

                                </td>
                                <!--td><a href="/documentsmodel/subirdocumento/{{ $reclamos[0]['id'] }}/{{ $data['id'] }}" class="btn btn-success ">Subir</a></td-->
                            </tr> 
                            @endif

                            @if( $data['reclamo']== $reclamos[0]['id'] )
                            <tr>
                                <td><div style="width: 70px;">{{ $data['titulo'] }}</div></td>
                                <td><div style="width: 120px;">{{ $data['ayuda'] }}</div></td>
                                <td>
                                    
                                    <table class="table " data-color="falabella">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <!--th></th-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $documents as $document )
                                            <tr>
                                            @if( $document['listcheking'] == $data['id'] )
                                                
                                                <td><div style="width: 100px;"><a href="https://admin.crawfordaffinitycolombia.com{{ $document['url'] }}"  target="_blank"><img style="width: 100%;" src="https://admin.crawfordaffinitycolombia.com{{ $document['url'] }}" ></a></div></td>
                                                <td><div style="width: 180px;">{{ $document['created_at'] }}</div></td>
                                                <!--td><a onclick="eliminar(`/documentsmodel/delete/{{ $document['id'] }}`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td-->
                                                 
                                            @endif
                                            </tr> 
                                        @endforeach                                              
                                        </tbody>
                                    </table>

                                </td>
                                <!--td><a href="/documentsmodel/subirdocumento/{{ $reclamos[0]['id'] }}/{{ $data['id'] }}" class="btn btn-success ">Subir</a></td-->
                            </tr> 
                            @endif
                        @endforeach                                              
                        </tbody>
                    </table>
                </div>
            </div>

            <!--div class="col-md-3">
                <div class="card" data-color="falabella">
                        <input type="hidden" name="marca" id="marca" class="form-control" value="{{ $reclamos[0]['producto']['marca'] }}">
                        <input type="hidden" name="tipo_seguro" id="tipo_seguro" class="form-control" value="{{ $reclamos[0]['producto']['tipo_seguro'] }}">
                        <input type="hidden" name="aseguradora" id="aseguradora" class="form-control" value="{{ $reclamos[0]['producto']['aseguradora'] }}">
                        <input type="hidden" name="campana" id="campana" class="form-control" value="{{ $reclamos[0]['producto']['campana'] }}">
                        <input type="hidden" name="tienda" id="tienda" class="form-control" value="{{ $reclamos[0]['producto']['campana'] }}">
                        <input type="hidden" name="asegurado" id="asegurado" class="form-control" value="{{ $reclamos[0]['producto']['asegurado'] }}">
                        <input type="hidden" name="tipo_polizas" id="tipo_polizas" class="form-control" value="{{ $reclamos[0]['producto']['tipo_poliza'] }}">
                        <input type="hidden" name="img" id="img" class="form-control" placeholder="img" value="https://image.flaticon.com/icons/svg/148/148769.svg">
                        <label>Título</label>
                        <input type="text" name="titulo" id="titulo" class="form-control"  onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Descripción</label>
                        <input type="text" name="descripcion" class="form-control" id="descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Sub-descripción</label>
                        <input type="text" name="subdescripcion" class="form-control" id="subdescripcion"  value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Ayuda</label>
                        <input type="text" name="ayuda" class="form-control" id="ayuda" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Tipo de formato</label>
                        <select name="tipo" id="tipo" class="form-control" placeholder="tipo">
                            <option value="">Seleccione el tipo de formato</option>
                            <option value="camara">Camara</option>
                            <option value="audio">Audio</option>
                        </select>
                        <input type="hidden" name="reclamo" id="reclamo" placeholder="reclamo SI existe de lo contrario es un 0" value="{{ $reclamos[0]['id'] }}"><br>
                    <button class="btn btn-success form-control" onclick="reclamo.listchekings();">Solicitar</button><br><br>
                    <a href="/img/ejemploapp.jpg" class="btn btn-warning " style="width: 100%" target="_blank">Ejemplo de vista APP</a>
                </div>
            </div-->

            <div class="col-md-9">
                <div class="card" data-color="falabella">
                    <h4>Notificaciones</h4>
                    <table class="table table-hover " data-color="falabella">
                        <thead>
                            <tr>
                                <th>fecha</th>
                                <th>mensaje</th>
                                <th>adjunto</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $notificaciones as $notificacion )
                            <tr>
                                <td>{{ $notificacion['fecha'] }}</td>
                                <td>{{ $notificacion['texto'] }}</td>
                                <td><a href="https://admin.crawfordaffinitycolombia.com/{{ $notificacion['notyid'] }}">{{ $notificacion['notyid'] }}</a></td>
                            </tr>  
                        @endforeach                                              
                        </tbody>
                    </table>
                </div>
            </div>

            <!--div class="col-md-3">
                <div class="card" data-color="falabella">
                    <form action="/notificaciones/sendBackend" method="POST" enctype="multipart/form-data">
                        <h4>Enviar notificación</h4>
                        <input type="hidden" name="id" id="id" value="{{ $reclamos[0]['id'] }}">
                        <label>Texto</label>
                        <input type="text" class="form-control" name="textonotificacion">
                        <label>Adjuntar</label>
                        <input type="file" name="file" id="file" class="inputfile" />
                        <label class="btn btn-succes form-control" for="file">Sube un archivo</label><br>
                        <input type="submit" value="Enviar" class="btn btn-success form-control" name="submit">
                    </form>
                </div>
            </div-->

        </div>

        <div >
            <input type="hidden" name="id"                  id="id"                 value="{{ $reclamos[0]['id'] }}">
            <input type="hidden" name="reclamoid"           id="reclamoid"          value="{{ $reclamos[0]['reclamoid'] }}">
            <input type="hidden" name="cedula"              id="cedula"             value="{{ $reclamos[0]['cedula'] }}">
            <input type="hidden" name="nombreasegurado"     id="nombreasegurado"    value="{{ $reclamos[0]['nombreasegurado'] }}">
            <input type="hidden" name="apellidoasegurado"   id="apellidoasegurado"  value="{{ $reclamos[0]['apellidoasegurado'] }}">
            <input type="hidden" name="fechanacimientoasse" id="fechanacimientoasse" value="{{ $reclamos[0]['fechanacimientoasse'] }}">
            <input type="hidden" name="ciudadrecidencia"    id="ciudadrecidencia"   value="{{ $reclamos[0]['ciudadrecidencia']['id'] }}">
            <input type="hidden" name="telefonofijo"        id="telefonofijo"       value="{{ $reclamos[0]['telefonofijo'] }}">
            <input type="hidden" name="celular"             id="celular"            value="{{ $reclamos[0]['celular'] }}">
            <input type="hidden" name="direccion"           id="direccion"          value="{{ $reclamos[0]['direccion'] }}">
            <input type="hidden" name="email"               id="email"              value="{{ $reclamos[0]['email'] }}">
            <input type="hidden" name="telefonolaboral"     id="telefonolaboral"    value="{{ $reclamos[0]['telefonolaboral'] }}">
            <input type="hidden" name="jsonrespuestaform"   id="jsonrespuestaform"  value="{{ $reclamos[0]['jsonrespuestaform'] }}">
            <input type="hidden" name="fechasiniestro"      id="fechasiniestro"     value="{{ $reclamos[0]['fechasiniestro'] }}">
            <input type="hidden" name="horasiniestro"       id="horasiniestro"      value="{{ $reclamos[0]['horasiniestro'] }}">
            <input type="hidden" name="descripcionsiniestro" id="descripcionsiniestro" value="{{ $reclamos[0]['descripcionsiniestro'] }}">
            <input type="hidden" name="textobackend"        id="textobackend"       value="{{ $reclamos[0]['textobackend'] }}">
            <input type="hidden" name="observaciones"       id="observaciones"      value="{{ $reclamos[0]['observaciones'] }}">
            <input type="hidden" name="motivobaja"          id="motivobaja"         value="{{ $reclamos[0]['motivobaja'] }}">
            <input type="hidden" name="cliente"             id="cliente"            value="{{ $reclamos[0]['cliente']['id'] }}">
            <input type="hidden" name="estados_poliza"      id="estados_poliza"     value="{{ $reclamos[0]['estados_poliza']['id'] }}">
            <input type="hidden" name="producto"            id="producto"           value="{{ $reclamos[0]['producto']['id'] }}">
            <input type="hidden" name="ciudadsiniestro"     id="ciudadsiniestro"    value="{{ $reclamos[0]['ciudadsiniestro']['id'] }}">
        </div>

    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<style type="text/css">
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    display: inline-block;
    color: #207ce5;
}

.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #207ce5;
    color: white;
}
.inputfile + label {
    cursor: pointer; /* "hand" cursor */
}
.inputfile:focus + label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>

<script type="text/javascript">
var reclamo = new Reclamo();


function eliminar(urls){
    var r = confirm("Desea eliminar ?");
    if (r == true) {
        $.ajax({
            url: urls, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }   
}


$('#newobservaciones').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#observaciones').val($('#newobservaciones').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});

$('#codigointerno').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#textobackend').val($('#codigointerno').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});


$('#newestado').on('change', function() {
    var r = confirm("Desea actualizar la información?");
    if (r == true) {
        //actualizar informacion
        $('#estados_poliza').val($('#newestado').val());
        reclamo.updload();
    } else {
        console.log('Sera para un proximo cambio');
    }
});


function Reclamo(){
    this.updload = function(){
        var reclamoid = this.validate('reclamoid');
        var cedula = this.validate('cedula');
        var nombreasegurado = this.validate('nombreasegurado');
        var apellidoasegurado = this.validate('apellidoasegurado');
        var fechanacimientoasse = this.validate('fechanacimientoasse');
        var ciudadrecidencia = this.validate('ciudadrecidencia');
        var telefonofijo = this.validate('telefonofijo');
        var celular = this.validate('celular');
        var direccion = this.validate('direccion');
        var email = this.validate('email');
        var telefonolaboral = this.validate('telefonolaboral');
        var jsonrespuestaform = this.validate('jsonrespuestaform');
        var fechasiniestro = this.validate('fechasiniestro');
        var horasiniestro = this.validate('horasiniestro');
        var descripcionsiniestro = this.validate('descripcionsiniestro');
        var textobackend = this.validate('textobackend');
        var observaciones = this.validate('observaciones');
        var motivobaja = this.validate('motivobaja');
        var cliente = this.validate('cliente');
        var estados_poliza = this.validate('estados_poliza');
        var producto = this.validate('producto');
        var ciudadsiniestro = this.validate('ciudadsiniestro');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'reclamoid' : reclamoid,
            'cedula' : cedula,
            'nombreasegurado' : nombreasegurado,
            'apellidoasegurado' : apellidoasegurado,
            'fechanacimientoasse' : fechanacimientoasse,
            'ciudadrecidencia' : ciudadrecidencia,
            'telefonofijo' : telefonofijo,
            'celular' : celular,
            'direccion' : direccion,
            'email' : email,
            'telefonolaboral' : telefonolaboral,
            'jsonrespuestaform' : jsonrespuestaform,
            'fechasiniestro' : fechasiniestro,
            'horasiniestro' : horasiniestro,
            'descripcionsiniestro' : descripcionsiniestro,
            'textobackend' : textobackend,
            'observaciones' : observaciones,
            'motivobaja' : motivobaja,
            'cliente' : cliente,
            'estados_poliza' : estados_poliza,
            'producto' : producto,
            'ciudadsiniestro' : ciudadsiniestro
        };

        console.log(parametrer);

        $.ajax({
            url: '/reclamos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.listchekings = function(){
        var nombre = this.validate('nombre');
        var img = this.validate('img');
        var tipo = this.validate('tipo');
        var titulo = this.validate('titulo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var subdescripcion = this.validate('subdescripcion');
        var ayuda = this.validate('ayuda');
        var tipo_polizas = this.validate('tipo_polizas');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 
        var reclamo = this.validate('reclamo');

        var parametrer = {
            'nombre' : nombre,
            'img' : img,
            'tipo': tipo,
            'titulo': titulo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'subdescripcion' : subdescripcion,
            'ayuda' : ayuda,
            'tipo_polizas' : tipo_polizas,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado,
            'reclamo' : reclamo
        };

        $.ajax({
            url: '/listchekings/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }
}
</script>

@endsection