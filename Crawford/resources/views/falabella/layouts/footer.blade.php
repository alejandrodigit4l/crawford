<footer class="footer" data-color="falabella">
    <div class="container-fluid" >
        
        <p class="copyright pull-right" style="color: white;">
            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://digit4l.co/" >DIGIT4L </a>® Todos los derechos reservados   |  Digit4l  Agencia |  DES16N  
        </p>
        <p class="copyright pull-right" style="font-size: 11px;color: white;">La información contenida en ésta página y en todos sus archivos anexos, es confidencial y/o privilegiada y sólo puede ser utilizada por la(s) persona(s) a la(s) cual(es) está dirigida. Si usted no es el destinatario autorizado, cualquier modificación, retención, difusión, distribución o copia total o parcial de éste mensaje y/o de la información contenida en el mismo y/o en sus archivos anexos está prohibida y son sancionadas por la ley. Si por error recibe éste mensaje, le ofrecemos disculpas, sírvase borrarlo de inmediato, notificarle de su error a la persona que lo envió y abstenerse de divulgar su contenido y anexos.</p>
    </div>
</footer>
<style type="text/css">
    body{
        color: #9a9a9a;
    }
</style>