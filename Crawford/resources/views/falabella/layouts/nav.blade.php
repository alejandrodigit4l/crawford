<div class="sidebar" data-color="falabella" data-image="/digit4l_sass/pasto.jpg" >

    <!--
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
    -->
    <div class="sidebar-wrapper">
	    <div class="logo">
	        <a href="/falabella/index" class="simple-text">
	            <img style="width: 80%;left: -10px;position: relative;opacity: 0.4;" src="/digit4l_sass/falabella_icon.jpg">
	        </a>
	    </div>
	    
	    <ul class="nav" >
	        <li class="active" id="estadistica">
	            <a href="/falabella/index" class="dropdown-toggle" data-color="falabella" > <p>Estadistícas</p></a>
	        </li>
	        <li id="reclamos">
				<a href="/falabella/reclamos" class="dropdown-toggle" data-color="falabella" ><p>Reclamos </p></a>
			</li>
			<li id="clientes">
				<a class="dropdown-toggle"  href="/falabella/clientes" data-color="falabella"><p>Clientes </p></a>
			</li>
			<!--li id="polizas">
				<a class="dropdown-toggle"  href="#navDatabase" data-color="falabella"><p>Pólizas </p></a>
			</li-->
			<!--<li id="chat">
				<a class="dropdown-toggle"  href="/falabella/chat" data-color="falabella"><p>Chat </p></a>
			</li>-->
			<!--li id="ajustes">
				<a class="dropdown-toggle"  href="/falabella/ajustes" data-color="falabella"><p>Ajustes </p></a>
			</li-->
			<!--li id="database">
				<a class="dropdown-toggle"  href="https://admin.crawfordaffinitycolombia.com/database" data-color="falabella"><p>DataBase </p></a>
			</li-->
			
	    </ul>

	    <div id="cerrar" class="active" style="position: absolute;left:5%;bottom: 5%;width: 90%;height:50px;background: black;border-radius: 5px 5px 5px 5px;
-moz-border-radius: 5px 5px 5px 5px;
-webkit-border-radius: 5px 5px 5px 5px;
border: 0px solid #000000;">
			<!-- <a class="dropdown-toggle" href="javascript:;" onclick="menu.cerrar();" data-color="falabella"><p style="text-align: center;margin-top: 10px;color:white;">Cerrar Sesion </p></a> -->
			<a class="dropdown-toggle" href="javascript:;" onclick="event.preventDefault();document.getElementById('logout-form').submit();" data-color="crawford"><p style="text-align: center;margin-top: 10px;color:white;">Cerrar Sesion </p></a>
			<form id="logout-form" action="{{ route('logout.falabella') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
				<input type="hidden" id="token_session" name="token_session" value="{{Auth::user()->token_session}}">
				<input type="hidden" id="email" name="email" value="{{Auth::user()->email}}">
				<input type="hidden" id="password" name="password" value="{{Auth::user()->password}}">
			</form>
		</div>
	</div>
</div>

<style type="text/css">
div a:hover{
	text-decoration: none;
}
</style>

<script type="text/javascript">

$( document ).ready(function() {
	console.log( "ready!" );
	
	 
	 setTimeout(eliminaToken, 3600000);

	function eliminaToken() {

		var token = document.getElementById("token_session").value;
		var email = document.getElementById("email").value;
		var password = document.getElementById("password").value;

		data = {
			'token_session': token,
			'password': password,
			'email': email
		}
		
		$.ajax({
			url: '/eliminaTokenSession',
			type: 'POST',
			data: data,
			success: function(r){
				if(r){
					console.log(r);					
				}
			}
		 });
	}
});


var menu = new Menu();

menu.verifySession();
// menu.init();

function Menu(){
	this.init = function(){

		$('.nav li').hide();
		var mainMenu = localStorage.getItem('rol').split(",");
		for (var i = mainMenu.length - 1; i >= 0; i--) {
			if(mainMenu[i]=='AJUSTES'){
				$('#ajustes').show();
			}
			if(mainMenu[i]=='POLIZAS'){
				$('#polizas').show();
				$('#chat').show();
			}
			if(mainMenu[i]=='CLIENTES'){
				$('#clientes').show();
			}
			if(mainMenu[i]=='RECLAMOS'){
				$('#reclamos').show();
			}
			if(mainMenu[i]=='ESTADISTICAS'){
				$('#estadistica').show();
			}
			if(mainMenu[i]=='DATABASE'){
				$('#database').show();
			}
			if(mainMenu[i]=='CHAT'){
				$('#chat').show();
			}
		}
		$('#cerrar').show();
	};

	/*
	*	Para que pueda funcionar el superadministrador indicando rutas desde la url, debera loguearse con un usuario cualquiera. y navegar entre vaiables
	*/
	this.verifySession = function(){
        if(localStorage.getItem('cliente')==''){ //no esta vacio
            location.href = '/falabella';
        }
    };

	this.cerrar = function(){
		//eliminar sesiones en localstorage
		localStorage.setItem('cliente','');
		localStorage.setItem('producto','');
		localStorage.setItem('reclamacion','');
		localStorage.setItem('rol','');
		localStorage.setItem('url','');
		//go to index
		location.href = '/falabella';		
	};
}
</script>
