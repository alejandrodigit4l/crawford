@extends('falabella.layout')

@section('content')

	<h4>Reclamaciones </h4>

    <?php $cont=0; ?>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-10">
            	<div class="card" data-color="falabella">
                    <label>Buscar:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
            	</div>
            </div>

            <!--div class="col-md-2">
                <div class="card" data-color="falabella">
                    <center>
                    <a href="/falabella/export/reclamos" class="btn btn-success" ><img width="28" src="http://www.iconarchive.com/download/i86104/graphicloads/filetype/excel-xls.ico"><br>Descargar Formato XLS</a></center>
                </div>
            </div-->

            <div class="col-md-12">
                <div class="card" data-color="falabella">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " id="tabla" data-color="falabella">
                            <thead>
                                <tr >
                                    <th>ID</th>
                                    <th>reclamoid</th>
                                    <th>cedula</th>
                                    <th>nombreasegurado</th>
                                    <th>apellidoasegurado</th>
                                    <!--th>Cliente</th-->
                                    <th>estados_poliza</th>
                                    <th>producto</th>
                                    <th>Actualizado</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $reclamos as $data )
                            <?php $id = $data['id']; ?>
                                <tr onclick="reclamos_crawford.link('/falabella/idreclamo/{{ $id }}')">
                                    <td>{{ $data['id'] }}</td>
                                    <td><a href="/falabella/idreclamo/{{ $data['id'] }}">{{ $data['reclamoid'] }}</a></td>
                                    <td><a href="/falabella/cliente/{{ $data['cliente']['id'] }}">{{ $data['cliente']['cedula'] }}</a></td>
                                    <td>{{ $data['nombreasegurado'] }}</td>
                                    <td>{{ $data['apellidoasegurado'] }}</td>
                                    <!--td>{{ $data['cliente']['aseguradora']['nombre'] }}</a></td-->
                                    <td><a href="/falabella/estado/{{ $data['estados_poliza']['id'] }}">{{ $data['estados_poliza']['nombre'] }}</a></td>
                                    <td>{{ $data['producto']['titulo'] }}</td>
                                    <td>{{ $data['updated_at'] }}</td>
                                </tr> 
                                <?php $cont++; ?>
                            @endforeach                                                    
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
function prueba(r,id){
    $(id).html('');
    console.log(r);
    console.log('Cantidad'+r.length);
    var obj = JSON.parse(r);
    console.log(obj.pasos);
    for (var i = 0; i < obj.pasos.length; i++) {
        console.log('//'+obj.pasos[i].name);
        $(id).append(obj.pasos[i].name+':'+obj.pasos[i].value+'<br>');
    }
}

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#reclamos').addClass('active');
    $('#search').focus();
    //recorrer tabla para sacar las respuestas
    var nFilas = $("#tabla tr").length;
    for (var i = 0; i < nFilas; i++) {
        prueba( $('#input'+i).val() , '#reclamo'+i);
    }

});


$('#search').keypress(function(e) {
    if(e.which == 13) {
        console.log('buscando /.....');
        if($('#search').val()==''){
            location.href = '/falabella/search/ALL';
        }
        else{
            location.href = '/falabella/search/'+$('#search').val();
        }
        
    }
});


var reclamos_crawford = new Reclamos_crawford;

$('#filtromarca').on('change', function() {
    reclamos_crawford.marca($('#filtromarca').val());
});

function Reclamos_crawford(){

    this.init = function(){
        //
    };

    this.marca = function(MARCA){
        //redireccionar a MARCA
        window.location.href = "/falabella/filtro_marca/"+MARCA;
    }

    this.link = function(url){
        //alert(url);
        window.location.href = url;
    }

}

</script>

@endsection