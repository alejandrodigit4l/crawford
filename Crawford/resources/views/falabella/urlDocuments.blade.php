<!DOCTYPE html>
<html style="height:676px;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Falabella-app</title>
    <link rel="stylesheet" href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/css/Footer-Basic.css">
    <link rel="stylesheet" href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/css/Header-Blue.css">
    <link rel="stylesheet" href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/css/Navigation-Clean1.css">
    <link rel="stylesheet" href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/js/config.js"></script>
    <script src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/js/md5.pack.js"></script>
</head>

<body>

    <style type="text/css">
        .footer {
            padding-bottom: 10px;
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: rgb(40, 175, 65);
            opacity: 0.8;
            color: white;
            text-align: center;
        }
    </style>

    <div>
        <nav class="navbar navbar-inverse navbar-static-top navigation-clean" data-color="falabella" id="navegador">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand navbar-link" href="#" style="height:100px;margin:0px;padding:0px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/logo-crawford.jpg" style="height:50px;margin:0px;margin-top:13px;margin-right:0px;margin-bottom:0px;margin-left:10px;"></a>
                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active" role="presentation" style="width:70px;">
                            
                        </li>
                        <li role="presentation">
                            <a href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/html/panel.html" style="width:80px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/inicio-blanco.svg" style="width:30px;margin-right:10px;margin-left:14px;margin-bottom:2px;">
                                <h6 class="text-center" style="margin:0px;width:58px;color:#5eab4d">Inicio</h6></a>
                        </li>
                        <li role="presentation">
                            <a href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/html/miperfil.html" style="width:80px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/perfil-blanco.svg" style="width:30px;margin-right:10px;margin-left:14px;margin-bottom:2px;">
                                <h6 class="text-center" style="margin:0px;width:58px;color:#5eab4d;">Mi Perfil</h6></a>
                        </li>
                        <li role="presentation">
                            <a href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/html/reclamos.html" style="width:70px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/reclamos-blanco.svg" style="width:30px;margin-left:11px;margin-right:7px;margin-bottom:2px;">
                                <h6 class="text-center" style="margin:0px;width:58px;color:#5eab4d;">Reclamar </h6></a>
                        </li>
                        <li role="presentation">
                            <a href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/html/ayuda.html" style="width:69px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/ayuda-blanco.svg" style="width:30px;margin-right:0px;margin-left:14px;margin-bottom:2px;">
                                <h6 class="text-center" style="margin:0px;height:12px;width:58px;color:#5eab4d;">Ayuda</h6></a>
                        </li>
                        <li role="presentation">
                            <a href="https://admin.crawfordaffinitycolombia.com/webapp/falabella/html/cerrar.html" style="width:69px;"><img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/img/cerrar-blanco.svg" style="width:30px;margin-right:0px;margin-left:14px;margin-bottom:2px;">
                                <h6 class="text-center" style="margin:0px;height:12px;width:58px;color:#5eab4d;">Cerrar</h6></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div>
        <div class="container" style="margin-top:30px;">
            <div class="content table-full-width table-responsive">
            	<table class="table table-striped table-hover">
            		<thead>
            			<tr>
            				<th>ID</th>
            				<th>Imagen</th>
            				<th>Fecha Subida</th>
            				<!-- <th>Eliminar</th> -->
            			</tr>
            		</thead>
            		<tbody>
            		@foreach( $response as $documento )
            			<tr>
            				<td>{{ $documento['id'] }}</td>
            				<td><img width="100" src="{{ $documento['url'] }}"></td>
            				<td>{{ $documento['created_at'] }}</td>
            				<!-- <td><a onclick="panel.eliminar({{$documento['id']}})" href="javascript:;" class="btn btn-danger">Eliminar</a></td> -->
            			</tr>
            		@endforeach 
            		</tbody>
            	</table>
            <div class="footer"><!-- react-text: 162 --><!-- /react-text --><div class="stats"><i></i><!-- react-text: 165 --> <!-- /react-text --></div></div></div>
        </div>

        <!-- <center><button class="btn btn-success" onclick="window.history.go(-1)">Volver al checklist</button></center> -->
        
    </div>

    <br><br><br><br><br><br><br><br>
    <!--<div class="footer-basic" data-color="falabella">
        <footer style="margin-top:0px;margin-bottom:0px;">
            <br><br><p class="text-center copyright" style="margin-top:150px;">Crawford Affinity Colombia 2017 inspired by Digit4l © Todos los derechos reservados</p>
        </footer>
    </div>-->
    <div class="footer">
        <br><strong> Horario de atención:</strong> lunes a sábado de 8:00 a.m a 5:00 p.m
        <br><strong>Telefonos:</strong> Bogotá: 7561951 Línea nacional: 01 8000 510 311.
        <br>Crawford Affinity Colombia 2017 inspired by Digit4l © Todos los derechos reservados
    </div>

    <script src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/js/jquery.min.js"></script>
    <script src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>


<script type="text/javascript">
var panel = new Panel();

panel.init();

function Panel(){
	this.init = function(){
		$('.container').hide();
		$('.container').slideDown(1000);
		console.log(JSON.parse(sessionStorage.getItem('cliente_')));
		var nombre = JSON.parse(sessionStorage.getItem('cliente_')).nombre;
		var apellido = JSON.parse(sessionStorage.getItem('cliente_')).lastname;
		var correo = JSON.parse(sessionStorage.getItem('cliente_')).email;

		//escribir en html el nombre
		$('#correo').html(correo);
		$('#nombre').html(nombre+" "+apellido);
		$('#miperfil').html(nombre[0]+apellido[0]);
	};
	this.eliminar = function(id){
		$.ajax({
            url: '/documentsmodel/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
	};
}
</script>