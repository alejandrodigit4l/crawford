@extends('crawford.layout')

@section('content')

	<h4>Homologos </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 250px;">
            		<p>Formulario Homologos</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-6">
                            <label>Escribe el nombre del estado BACK</label><br>
                            <input type="text" name="nombre" class="form-control" id="nombre" style="color: black;" placeholder="Nombre Estado (BACK)" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe la descripción del estado</label><br>
                            <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripcion" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="homologos.save();">Guardar</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="homologos.sendUpdate();">Actualizar</div>
                        </div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $homologos as $homologo )
                                <tr>
                                    <td>{{ $homologo['id'] }}</td>
                                    <td>{{ $homologo['nombre'] }}</td>
                                    <td>{{ $homologo['descripcion'] }}</td>
                                    <td><a href="javascript:;" onclick="homologos.update({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="homologos.delete({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var homologos = new Homologos();

$('#actualizar').hide();
$('#guardar').show();

function Homologos(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');       

        var parametrer = {
            'nombre' : nombre,
            'descripcion' : descripcion
        };

        $.ajax({
            url: 'homologos/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'homologos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'descripcion' : descripcion
        };

        $.ajax({
            url: 'homologos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'homologos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'homologos/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'homologos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'homologos/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#descripcion').val(response.body[0].descripcion);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'homologos/'+DATA;
    }
}


</script>
	
@endsection