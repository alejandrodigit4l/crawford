@extends('crawford.layout')

@section('content')

	<h4>listchekings </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="listchekings/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario listchekings</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            @foreach( $marcas as $marca)
                            <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                            @endforeach
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <select name="aseguradora" id="aseguradora" class="form-control" >
                            <option value="">Aseguradora</option>
                        </select>
                        <select name="campana" id="campana" class="form-control" >
                            <option value="">Campaña</option>
                        </select>
                        <select name="tienda" id="tienda" class="form-control" >
                            <option value="">Tienda</option>
                        </select>
                        <select name="asegurado" id="asegurado" class="form-control" >
                            <option value="">Asegurado</option>
                        </select>
                        <select name="tipo_polizas" id="tipo_polizas" class="form-control" >
                            <option value="">Tipo poliza</option>
                        </select>
                        <input type="text" name="img" id="img" class="form-control" placeholder="img">
                        <input type="text" name="tipo" id="tipo" class="form-control" placeholder="tipo">
                        <input type="text" name="titulo" id="titulo" class="form-control" placeholder="titulo">
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="subdescripcion" class="form-control" id="subdescripcion" placeholder="subdescripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="ayuda" class="form-control" id="ayuda" placeholder="ayuda" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="number" name="reclamo" id="reclamo" placeholder="reclamo SI existe de lo contrario es un 0" value="0">
                        <br>
                        <div class="btn btn-success" onclick="listchekings.save();">Guardar</div>
                        <div class="btn btn-success" onclick="listchekings.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>id</th>
	                                <th>nombre</th>
									<th>img</th>
									<th>titulo</th>
									<th>descripcion</th>
									<th>tipo</th>
									<th>subdescripcion</th>
									<th>ayuda</th>
									<th>tipo_polizas</th>
									<th>asegurado</th>
									<th>marca</th>
									<th>tipo_seguro</th>
									<th>aseguradora</th>
									<th>campana</th>
									<th>tienda</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $listchekings as $listcheking )
                                <tr>
                                    <td>{{ $listcheking['id'] }}</td>
                                    <td>{{ $listcheking['nombre'] }}</td>
									<td><img width="50" src="{{ $listcheking['img'] }}"></td>
									<td>{{ $listcheking['titulo'] }}</td>
									<td>{{ $listcheking['descripcion'] }}</td>
									<td>{{ $listcheking['tipo'] }}</td>
									<td>{{ $listcheking['subdescripcion'] }}</td>
									<td>{{ $listcheking['ayuda'] }}</td>
									<td>{{ $listcheking['tipo_polizas'] }}</td>
									<td>{{ $listcheking['asegurado']['nombre'] }}</td>
									<td>{{ $listcheking['marca']['nombre'] }}</td>
									<td>{{ $listcheking['tipo_seguro']['nombre'] }}</td>
									<td>{{ $listcheking['aseguradora']['nombre'] }}</td>
									<td>{{ $listcheking['campana']['nombre'] }}</td>
									<td>{{ $listcheking['tienda']['nombre'] }}</td>
                                    <td>{{ $listcheking['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="listchekings.update({{ $listcheking['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="listchekings.delete({{ $listcheking['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var listchekings = new Listchekings();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        listchekings.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    listchekings.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    listchekings.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});
$('#aseguradora').on('change', function() {
    listchekings.campana($('#aseguradora').val(),'#campana','Campañas');
});
$('#campana').on('change', function() {
    listchekings.tienda($('#campana').val(),'#tienda','Tiendas');
});
$('#tienda').on('change', function() {
    listchekings.asegurado($('#tienda').val(),'#asegurado','Asegurados');
});
$('#asegurado').on('change', function() {
    listchekings.tipo_poliza($('#asegurado').val(),'#tipo_polizas','Tipo poliza');
});


$('#searchestado').on('change', function() {
    listchekings.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

function Listchekings(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var img = this.validate('img');
        var tipo = this.validate('tipo');
        var titulo = this.validate('titulo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var subdescripcion = this.validate('subdescripcion');
        var ayuda = this.validate('ayuda');
        var tipo_polizas = this.validate('tipo_polizas');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 
        var reclamo = this.validate('reclamo');

        var parametrer = {
            'nombre' : nombre,
            'img' : img,
            'tipo': tipo,
            'titulo': titulo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'subdescripcion' : subdescripcion,
            'ayuda' : ayuda,
            'tipo_polizas' : tipo_polizas,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado,
            'reclamo' : reclamo
        };

        $.ajax({
            url: 'listchekings/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'listchekings';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var nombre = this.validate('nombre');
        var img = this.validate('img');
        var tipo = this.validate('tipo');
        var titulo = this.validate('titulo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var subdescripcion = this.validate('subdescripcion');
        var ayuda = this.validate('ayuda');
        var tipo_polizas = this.validate('tipo_polizas');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 
        var reclamo = this.validate('reclamo');

        var parametrer = {
            'id'    : id,
            'nombre' : nombre,
            'img' : img,
            'tipo': tipo,
            'titulo': titulo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'subdescripcion' : subdescripcion,
            'ayuda' : ayuda,
            'tipo_polizas' : tipo_polizas,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado,
            'reclamo' : reclamo
        };

        $.ajax({
            url: 'listchekings/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'listchekings';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'listchekings/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'listchekings';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'listchekings/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#tipo').val(response.body[0].tipo);
                    $('#img').val(response.body[0].img);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                    $('#subdescripcion').val(response.body[0].subdescripcion);
                    $('#ayuda').val(response.body[0].ayuda);
                    $('#tipo_polizas').val(response.body[0].tipo_polizas);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#campana').val(response.body[0].campana);
                    $('#aseguradora').val(response.body[0].aseguradora);
                    $('#tienda').val(response.body[0].tienda);
                    $('#asegurado').val(response.body[0].asegurado);
                    $('reclamo').val(response.body[0].asegurado);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'listchekings/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA CAMPANAS EN ESPECIAL */
    this.campana = function(CAMPANA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'campanas/search/'+CAMPANA, //This is the CAMPANAS
            type: "GET",
            success: function(response){
                console.log('TIENDAS CAMPANA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIENDA EN ESPECIAL */
    this.tienda = function(TIENDA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tiendas/search/'+TIENDA, //This is the TIENDA
            type: "GET",
            success: function(response){
                console.log('TIPO_AEGURADOs TIENDA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA ASEGURADO EN ESPECIAL */
    this.asegurado = function(ASEGURADO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_asegurados/search/'+ASEGURADO, //This is the ASEGURADO
            type: "GET",
            success: function(response){
                console.log('TIENDAS ASEGURADO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_POLIZA EN ESPECIAL */
    this.tipo_poliza = function(TIPO_POLIZA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_asegurados/search/'+TIPO_POLIZA, //This is the TIPO_POLIZA
            type: "GET",
            success: function(response){
                console.log('ASEGURADO TIPO_POLIZA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
@endsection