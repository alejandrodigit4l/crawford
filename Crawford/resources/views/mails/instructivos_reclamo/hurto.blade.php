<div style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;" class="container">

	<br>Reciba un Cordial Saludo:<br>

	<p>Para efectos de realizar la reclamación, solicitamos que por favor envíe los documentos relacionados a continuación al correo electrónico
	@if($sponsor == 'exito')
		garantiaextendidaexito@crawfordaffinity.com
	@elseif($sponsor == 'ibg')
		garantiaextendidaibg@crawfordaffinity.com
	@elseif($sponsor == 'soelco')
		garantiaextendidasoelco@crawfordaffinity.com
	@endif
	e indicando en el asunto, nombre y cédula del asegurado y número de caso.</p><br>

	<p>Adjuntar los siguientes documentos al correo:</p>

	<ul>
		<li>
			1.	Diligenciar en su totalidad el Formato de Reclamación adjunto a este correo electrónico.
		</li>
		<li>
			2.	Copia de la  Factura de Compra del articulo (En caso de no tenerla, se debe solicitar con la tienda)
		</li>
		<li>
			3.  Fotocopia de Cedula de Ciudadanía (en caso de ser persona natural). Cuando se trate de personas jurídicas se debe anexar  Certificado de cámara de comercio no mayor a 30 días y Fotocopia de la cedula del representante legal, quien debe firmar el Formulario de Reclamación.
		</li>
		<li>
			4.	Original o copia del denuncio por hurto ante Fiscalía, Policía o Uri, el cual debe contener, fecha y lugar de ocurrencia de los hechos, marca, modelo y numero del IMEI/SERIAL de su equipo celular y una descripción de los hechos. Se aceptarán denuncios vía web que se hayan tramitado en el siguiente link bajo la modalidad Hurto a Personas, en estos casos deberá enviar el denuncio, no el soporte o constancia de denuncio, también debe contener la información solicitada.
		</li>
	</ul><br>

	<p><a href="https://adenunciar.policia.gov.co/adenunciar/Login.aspx?ReturnUrl=%2fadenunciar%2ffrm_denuncia_hp.aspx">https://adenunciar.policia.gov.co/adenunciar/Login.aspx?ReturnUrl=%2fadenunciar%2ffrm_denuncia_hp.aspx </a></p>

	<p>
		Nota: En casos de que el artículo hurtado sea un celular debe presentar el Bloqueo del IMEI de su equipo ante su operador, lo puede validar en <a href="www.imeicolombia.com.co">www.imeicolombia.com.co</a>
	</p>

	<p>En caso de consulta o información adicional, estamos a su disposición de lunes a viernes de 8:00 a.m. a  6:00 p.m. y los sábados de 8 a.m. a 1:00 p.m. a través de nuestra línea de servicio al cliente 018000423628 a nivel nacional.</p>

	<p>Cordialmente,</p>

	<p><b>Servicio al Cliente Garantia Extendida</b></p>

</div>