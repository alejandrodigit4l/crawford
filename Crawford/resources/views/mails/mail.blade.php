<img style="width: 70%" src="https://admin.crawfordaffinitycolombia.com/img/email_falabella.png">

<p></p>

<strong style="color: green; font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;">{{ $titulo }}</strong><br><br>

<!-- <p style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;">{{ $mensaje }}.</p><br> -->

	<pre style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;" contenteditable="true">{{ $mensaje }}</pre>


<p style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;">En caso de consulta o información adicional, estamos a tu disposición de lunes a sábado de 9:00 a.m. a  8:00 p.m. a través <br> de nuestra línea de servicio al cliente 7561951 en Bogotá y 018000 510 311 a nivel nacional.
</p>

<p style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;">Cordialmente,</p>

<strong style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif; color: green;">Agencia de Seguros Falabella</strong>
<p></p>
<img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/img/vigilado_negro.png" style="width: 1%;">
<img src="https://admin.crawfordaffinitycolombia.com/webapp/falabella/img/seguros_mundial.png" style="width: 10%;">