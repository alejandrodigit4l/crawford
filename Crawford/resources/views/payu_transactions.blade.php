@extends('crawford.layout')

@section('content')

	<h4>payu_transactions </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario payu_transactions</p>      
                    <form data-color="crawford">
                    <input type="hidden" name="id" id="id" value="">
                    <input type="text" name="nombre" class="form-control" id="nombre" style="color: black;" placeholder="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="email" class="form-control" id="email" style="color: black;" placeholder="email" value="" >
                    <input type="text" name="cedula" class="form-control" id="cedula" style="color: black;" placeholder="cedula" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <select name="marca" class="form-control" id="marca">
                        <option value="">Seleccione marca</option>
                        @foreach( $marcas as $marca)
                        <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                        @endforeach
                    </select>
                    <select name="producto" class="form-control" id="producto">
                        <option value="">Seleccione Producto</option>
                    </select>
                    <input type="text" name="titulo" class="form-control" id="titulo" style="color: black;" placeholder="titulo" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="number" name="valor" class="form-control" id="valor" style="color: black;" placeholder="valor" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="solicitud_payu" class="form-control" id="solicitud_payu" style="color: black;" placeholder="solicitud_payu" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="number" name="valor_payu" class="form-control" id="valor_payu" style="color: black;" placeholder="valor_payu" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="payer_name" class="form-control" id="payer_name" style="color: black;" placeholder="payer_name" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="email" name="payer_email" class="form-control" id="payer_email" style="color: black;" placeholder="payer_email" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="payer_dni" class="form-control" id="payer_dni" style="color: black;" placeholder="payer_dni" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="payerment_method" class="form-control" id="payerment_method" style="color: black;" placeholder="payerment_method" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="code_payu" class="form-control" id="code_payu" style="color: black;" placeholder="code_payu" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="order_id" class="form-control" id="order_id" style="color: black;" placeholder="order_id" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="transactionid" class="form-control" id="transactionid" style="color: black;" placeholder="transactionid" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="state" class="form-control" id="state" style="color: black;" placeholder="state" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="responsecode" class="form-control" id="responsecode" style="color: black;" placeholder="responsecode" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="date" name="fecha_creacion" class="form-control" id="fecha_creacion" style="color: black;" placeholder="fecha_creacion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="date" name="fecha_vencimiento" class="form-control" id="fecha_vencimiento" style="color: black;" placeholder="fecha_vencimiento" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="serial" class="form-control" id="serial" style="color: black;" placeholder="serial" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="poliza" class="form-control" id="poliza" style="color: black;" placeholder="poliza" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="enviado" class="form-control" id="enviado" style="color: black;" placeholder="enviado" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <input type="text" name="url" class="form-control" id="url" style="color: black;" placeholder="url" value="" >
                    <select name="clientes_id" class="form-control" id="clientes_id" >
                        <option value="">Seleccione cliente</option>
                    </select>
                    <br>
                        <div class="btn btn-success" onclick="payu_transactions.save();">Guardar</div>
                        <div class="btn btn-success" onclick="payu_transactions.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>email</th>
                                    <th>cedula</th>
                                    <th>marca</th>
                                    <th>producto</th>
                                    <th>titulo</th>
                                    <th>valor</th>
                                    <th>solicitud_payu</th>
                                    <th>valor_payu</th>
                                    <th>payer_name</th>
                                    <th>payer_email</th>
                                    <th>payer_dni</th>
                                    <th>payerment_method</th>
                                    <th>code_payu</th>
                                    <th>order_id</th>
                                    <th>transactionid</th>
                                    <th>state</th>
                                    <th>responsecode</th>
                                    <th>fecha_creacion</th>
                                    <th>fecha_vencimiento</th>
                                    <th>serial</th>
                                    <th>poliza</th>
                                    <th>enviado</th>
                                    <th>url</th>
                                    <th>clientes_id</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $payu_transactions as $data )
                                <tr>
                                    <td>{{ $data['id'] }}</td>
                                    <td>{{ $data['nombre'] }}</td>
                                    <td>{{ $data['email'] }}</td>
                                    <td>{{ $data['cedula'] }}</td>
                                    <td>{{ $data['marca'] }}</td>
                                    <td>{{ $data['producto'] }}</td>
                                    <td>{{ $data['titulo'] }}</td>
                                    <td>{{ $data['valor'] }}</td>
                                    <td>{{ $data['solicitud_payu'] }}</td>
                                    <td>{{ $data['valor_payu'] }}</td>
                                    <td>{{ $data['payer_name'] }}</td>
                                    <td>{{ $data['payer_email'] }}</td>
                                    <td>{{ $data['payer_dni'] }}</td>
                                    <td>{{ $data['payerment_method'] }}</td>
                                    <td>{{ $data['code_payu'] }}</td>
                                    <td>{{ $data['order_id'] }}</td>
                                    <td>{{ $data['transactionid'] }}</td>
                                    <td>{{ $data['state'] }}</td>
                                    <td>{{ $data['responsecode'] }}</td>
                                    <td>{{ $data['fecha_creacion'] }}</td>
                                    <td>{{ $data['fecha_vencimiento'] }}</td>
                                    <td>{{ $data['serial'] }}</td>
                                    <td>{{ $data['poliza'] }}</td>
                                    <td>{{ $data['enviado'] }}</td>
                                    <td>{{ $data['url'] }}</td>
                                    <td>{{ $data['clientes_id'] }}</td>
                                    <td><a href="javascript:;" onclick="payu_transactions.update({{ $data['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="payu_transactions.delete({{ $data['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var payu_transactions = new Payu_transactions();


$('#marca').on('change', function() {
    payu_transactions.productos($('#marca').val(),'#producto','Producto');
});

$('#marca').on('change', function() {
    payu_transactions.clientes($('#marca').val(),'#clientes_id','clientes_id');
});

function Payu_transactions(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var email = this.validate('email');
        var cedula = this.validate('cedula');
        var marca = this.validate('marca');
        var producto = this.validate('producto');
        var titulo = this.validate('titulo');
        var valor = this.validate('valor');
        var solicitud_payu = this.validate('solicitud_payu');
        var valor_payu = this.validate('valor_payu');
        var payer_name = this.validate('payer_name');
        var payer_email = this.validate('payer_email');
        var payer_dni = this.validate('payer_dni');
        var payerment_method = this.validate('payerment_method');
        var code_payu = this.validate('code_payu');
        var order_id = this.validate('order_id');
        var transactionid = this.validate('transactionid');
        var state = this.validate('state');
        var responsecode = this.validate('responsecode');
        var fecha_creacion = this.validate('fecha_creacion');
        var fecha_vencimiento = this.validate('fecha_vencimiento');
        var serial = this.validate('serial');
        var poliza = this.validate('poliza');
        var enviado = this.validate('enviado');
        var url = this.validate('url');
        var clientes_id = this.validate('clientes_id');
    

        var parametrer = {
            'nombre' : nombre,
            'email' : email,
            'cedula' : cedula,
            'marca' : marca,
            'producto' : producto,
            'titulo' : titulo,
            'valor' : valor,
            'solicitud_payu' : solicitud_payu,
            'valor_payu' : valor_payu,
            'payer_name' : payer_name,
            'payer_email' : payer_email,
            'payer_dni' : payer_dni,
            'payerment_method' : payerment_method,
            'code_payu' : code_payu,
            'order_id' : order_id,
            'transactionid' : transactionid,
            'state' : state,
            'responsecode' : responsecode,
            'fecha_creacion' : fecha_creacion,
            'fecha_vencimiento' : fecha_vencimiento,
            'serial' : serial,
            'poliza' : poliza,
            'enviado' : enviado,
            'url' : url,
            'clientes_id' : clientes_id
        };

        $.ajax({
            url: 'payu_transactions/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'payu_transactions';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var email = this.validate('email');
        var cedula = this.validate('cedula');
        var marca = this.validate('marca');
        var producto = this.validate('producto');
        var titulo = this.validate('titulo');
        var valor = this.validate('valor');
        var solicitud_payu = this.validate('solicitud_payu');
        var valor_payu = this.validate('valor_payu');
        var payer_name = this.validate('payer_name');
        var payer_email = this.validate('payer_email');
        var payer_dni = this.validate('payer_dni');
        var payerment_method = this.validate('payerment_method');
        var code_payu = this.validate('code_payu');
        var order_id = this.validate('order_id');
        var transactionid = this.validate('transactionid');
        var state = this.validate('state');
        var responsecode = this.validate('responsecode');
        var fecha_creacion = this.validate('fecha_creacion');
        var fecha_vencimiento = this.validate('fecha_vencimiento');
        var serial = this.validate('serial');
        var poliza = this.validate('poliza');
        var enviado = this.validate('enviado');
        var url = this.validate('url');
        var clientes_id = this.validate('clientes_id');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'email' : email,
            'cedula' : cedula,
            'marca' : marca,
            'producto' : producto,
            'titulo' : titulo,
            'valor' : valor,
            'solicitud_payu' : solicitud_payu,
            'valor_payu' : valor_payu,
            'payer_name' : payer_name,
            'payer_email' : payer_email,
            'payer_dni' : payer_dni,
            'payerment_method' : payerment_method,
            'code_payu' : code_payu,
            'order_id' : order_id,
            'transactionid' : transactionid,
            'state' : state,
            'responsecode' : responsecode,
            'fecha_creacion' : fecha_creacion,
            'fecha_vencimiento' : fecha_vencimiento,
            'serial' : serial,
            'poliza' : poliza,
            'enviado' : enviado,
            'url' : url,
            'clientes_id' : clientes_id
        };

        $.ajax({
            url: 'payu_transactions/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'payu_transactions';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'payu_transactions/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'payu_transactions';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'payu_transactions/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#email').val(response.body[0].email);
                    $('#cedula').val(response.body[0].cedula);
                    $('#marca').val(response.body[0].marca);
                    $('#producto').val(response.body[0].producto);
                    $('#titulo').val(response.body[0].titulo);
                    $('#valor').val(response.body[0].valor);
                    $('#solicitud_payu').val(response.body[0].solicitud_payu);
                    $('#valor_payu').val(response.body[0].valor_payu);
                    $('#payer_name').val(response.body[0].payer_name);
                    $('#payer_email').val(response.body[0].payer_email);
                    $('#payer_dni').val(response.body[0].payer_dni);
                    $('#payerment_method').val(response.body[0].payerment_method);
                    $('#code_payu').val(response.body[0].code_payu);
                    $('#order_id').val(response.body[0].order_id);
                    $('#transactionid').val(response.body[0].transactionid);
                    $('#state').val(response.body[0].state);
                    $('#responsecode').val(response.body[0].responsecode);
                    $('#fecha_creacion').val(response.body[0].fecha_creacion);
                    $('#fecha_vencimiento').val(response.body[0].fecha_vencimiento);
                    $('#serial').val(response.body[0].serial);
                    $('#poliza').val(response.body[0].poliza);
                    $('#enviado').val(response.body[0].enviado);
                    $('#url').val(response.body[0].url);
                    $('#clientes_id').val(response.body[0].clientes_id);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'payu_transactions/'+DATA;
    };

    /* TOMAR TODOS LOS PRODUCTOS DE UN PRODUCTO */
    this.productos = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'productos/search/marca/'+MARCA, //This is the MARCA
            type: "GET",
            success: function(response){
                console.log('PRODUCTOS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };

    /* TOMAR TODOS LOS CLIENTES DE UN PRODUCTO */
    this.clientes = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'clientes/search/marca/'+MARCA, //This is the MARCA
            type: "GET",
            success: function(response){
                console.log('CLIENTES MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };

}


</script>
	
@endsection