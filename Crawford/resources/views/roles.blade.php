@extends('crawford.layout')

@section('content')

	<h4>Roles </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 500px;">
            		<p>Formulario Roles</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-6">
                            <br><label>Escoge una marca</label>
                            <select name="marca" id="marca" class="form-control" >
                                <option value="">Selecciona marca</option>
                                @foreach( $marcas as $marca)
                                <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <br><label>Escribe el nombre del rol/perfil</label>
                            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-12">
                            <label>Escoge los menús que deseas ver en el rol/perfil</label><br>
                            <div class="col-sm-6">
                                <input type="checkbox" class="menusss" id="ajustes1"  value="AJUSTES"> <label> AJUSTES</label><br>
                                <input type="checkbox" class="menusss" id="polizas1"  value="POLIZAS"> <label> POLIZAS</label><br>
                                <input type="checkbox" class="menusss" id="clientes1"  value="CLIENTES"> <label> CLIENTES</label><br>
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" class="menusss" id="reclamos1"  value="RECLAMOS"> <label> RECLAMOS</label><br>
                                <input type="checkbox" class="menusss" id="estadisticas1" value="ESTADISTICAS"> <label> ESTADISTICAS</label><br>
                                <input type="checkbox" class="menusss" id="chat1" value="CHAT"> <label> CHAT</label><br><br><br>

                            </div>
                            <div class="col-sm-12">
                            *Confirme todos los menús que tendrá este usuario, usando este botón.
                            </div>
                            <div class="col-sm-6">
                                <div onclick="roles.concat();" class="btn btn-warning form-control">Confirmar menús</div>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" name="menu" class="form-control" id="menu" placeholder="menu" value="" disabled="">
                            </div>
                        </div>
                                                
                        
                        <input type="hidden" name="submenu" class="form-control" id="submenu" placeholder="submenu" value="ALL" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="roles.save();">Guardar</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="roles.sendUpdate();">Actualizar</div>
                        </div>
                    </form>          		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                	<th>id</th>
	                                <th>marca</th>
									<th>nombre</th>
									<th>menu</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $roles as $rol )
                                <tr>
                                	<td>{{ $rol['id'] }}</td>
                                    <td>{{ $rol['marca']['nombre'] }}</td>
									<td>{{ $rol['nombre'] }}</td>
									<td>{{ $rol['menu'] }}</td>
                                    <td>{{ $rol['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="roles.update({{ $rol['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="roles.delete({{ $rol['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var roles = new Roles();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        roles.search($('#searchnombre').val());
    }
});

$('#actualizar').hide();
$('#guardar').show();

function Roles(){

    var selectmenus = [];

    this.save = function(){
		var marca = this.validate('marca');
		var nombre = this.validate('nombre');
		var menu = this.validate('menu');
		var submenu = this.validate('submenu');

        var parametrer = {
			'marca' : marca,
			'nombre' : nombre,
			'menu' : menu,
			'submenu' : submenu
        };

        $.ajax({
            url: '/roles/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
		var marca = this.validate('marca');
		var nombre = this.validate('nombre');
		var menu = this.validate('menu');
		var submenu = this.validate('submenu');

        var parametrer = {
            'id'    : id,
			'marca' : marca,
			'nombre' : nombre,
			'menu' : menu,
			'submenu' : submenu
        };

        $.ajax({
            url: 'roles/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'roles/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'roles/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
					$('#marca').val(response.body[0].marca.id);
                    //$("#marca option[value=1]").attr('selected', 'selected');
					$('#nombre').val(response.body[0].nombre);
					$('#menu').val(response.body[0].menu);
					$('#submenu').val(response.body[0].submenu);         
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'roles/'+DATA;
    };

    this.concat = function(){
        var checkedValue = null; 
        var texto = 'inicio';
        var inputElements = document.getElementsByClassName('menusss');
        for(var i=0; inputElements[i]; ++i){
              if(inputElements[i].checked){
                   checkedValue = inputElements[i].value;
                   texto = texto + ',' + checkedValue;
              }
        }
        texto = texto.replace('inicio','');
        $('#menu').val(texto);
    };

}


</script>
	
@endsection