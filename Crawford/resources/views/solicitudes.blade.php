@extends('crawford.layout')

@section('content')

	<h4>solicitudes </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario solicitudes</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
						<input type="text" name="mensaje" id="mensaje" class="form-control" placeholder="mensaje" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="telefono" id="telefono" class="form-control" placeholder="telefono" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="franja" id="franja" class="form-control" placeholder="franja" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="diasemana" id="diasemana" class="form-control" placeholder="diasemana" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="estado" id="estado" class="form-control" placeholder="estado" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="cliente" id="cliente" class="form-control" placeholder="cliente" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="solicitudes.save();">Guardar</div>
                        <div class="btn btn-success" onclick="solicitudes.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>mensaje</th>
									<th>telefono</th>
									<th>franja</th>
									<th>diasemana</th>
									<th>estado</th>
									<th>cliente</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $solicitudes as $homologo )
                                <tr>
                                    <td>{{ $homologo['id'] }}</td>
                                    <td>{{ $homologo['mensaje'] }}</td>
									<td>{{ $homologo['telefono'] }}</td>
									<td>{{ $homologo['franja'] }}</td>
									<td>{{ $homologo['diasemana'] }}</td>
									<td>{{ $homologo['estado'] }}</td>
									<td>{{ $homologo['cliente'] }}</td>
                                    <td><a href="javascript:;" onclick="solicitudes.update({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="solicitudes.delete({{ $homologo['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var solicitudes = new Solicitudes();

function Solicitudes(){

    this.save = function(){
        var mensaje = this.validate('mensaje');
		var telefono = this.validate('telefono');
		var franja = this.validate('franja');
		var diasemana = this.validate('diasemana');
		var estado = this.validate('estado');
		var cliente = this.validate('cliente');  

        var parametrer = {
            'mensaje' : mensaje,
			'telefono' : telefono,
			'franja' : franja,
			'diasemana' : diasemana,
			'estado' : estado,
			'cliente' : cliente
        };

        $.ajax({
            url: 'solicitudes/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'solicitudes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var mensaje = this.validate('mensaje');
		var telefono = this.validate('telefono');
		var franja = this.validate('franja');
		var diasemana = this.validate('diasemana');
		var estado = this.validate('estado');
		var cliente = this.validate('cliente'); 
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'mensaje' : mensaje,
			'telefono' : telefono,
			'franja' : franja,
			'diasemana' : diasemana,
			'estado' : estado,
			'cliente' : cliente
        };

        $.ajax({
            url: 'solicitudes/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'solicitudes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'solicitudes/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'solicitudes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'solicitudes/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#mensaje').val(response.body[0].mensaje);
					$('#telefono').val(response.body[0].telefono);
					$('#franja').val(response.body[0].franja);
					$('#diasemana').val(response.body[0].diasemana);
					$('#estado').val(response.body[0].estado);
					$('#cliente').val(response.body[0].cliente);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'solicitudes/'+DATA;
    }
}


</script>
	
@endsection