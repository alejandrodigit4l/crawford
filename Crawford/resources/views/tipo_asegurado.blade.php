@extends('crawford.layout')

@section('content')

	<h4>Tipo_asegurados </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="tipo_asegurados/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Tipo_asegurados</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            @foreach( $marcas as $marca)
                            <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                            @endforeach
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <select name="aseguradora" id="aseguradora" class="form-control" >
                            <option value="">Aseguradora</option>
                        </select>
                        <select name="campana" id="campana" class="form-control" >
                            <option value="">Campaña</option>
                        </select>
                        <select name="tienda" id="tienda" class="form-control" >
                            <option value="">Tienda</option>
                        </select>
                        <input name="descripcion" id="descripcion" class="form-control" placeholder="Descripcion" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="logo" id="logo" class="form-control" placeholder="URL logo">
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="tipo_asegurados.save();">Guardar</div>
                        <div class="btn btn-success" onclick="tipo_asegurados.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>
                                    <th>campana</th>
                                    <th>tienda</th>
                                    <th>logo</th> 
                                    <th>descripcion</th>                                    
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $tipo_asegurados as $tipo_asegurado )
                                <tr>
                                    <td>{{ $tipo_asegurado['id'] }}</td>
                                    <td>{{ $tipo_asegurado['nombre'] }}</td>
                                    <td>{{ $tipo_asegurado['marca']['nombre'] }}</td>
                                    <td>{{ $tipo_asegurado['tipo_seguro']['nombre'] }}</td>
                                    <td>{{ $tipo_asegurado['aseguradora']['nombre'] }}</td>
                                    <td>{{ $tipo_asegurado['campana']['nombre'] }}</td>
                                    <td>{{ $tipo_asegurado['tienda']['nombre'] }}</td>
                                    <td><img width="50" src="{{ $tipo_asegurado['logo'] }}"></td>
                                    <td>{{ $tipo_asegurado['descripcion'] }}</td>
                                    <td>{{ $tipo_asegurado['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="tipo_asegurados.update({{ $tipo_asegurado['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="tipo_asegurados.delete({{ $tipo_asegurado['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var tipo_asegurados = new Tipo_asegurados();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        tipo_asegurados.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    tipo_asegurados.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    tipo_asegurados.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});
$('#aseguradora').on('change', function() {
    tipo_asegurados.campana($('#aseguradora').val(),'#campana','Campañas');
});
$('#campana').on('change', function() {
    tipo_asegurados.tienda($('#campana').val(),'#tienda','Campañas');
});

$('#searchestado').on('change', function() {
    tipo_asegurados.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

function Tipo_asegurados(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var logo = this.validate('logo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 

        var parametrer = {
            'nombre' : nombre,
            'logo' : logo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda
        };

        $.ajax({
            url: 'tipo_asegurados/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_asegurados';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var nombre = this.validate('nombre');
        var logo = this.validate('logo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');  
        var tienda = this.validate('tienda');     

        var parametrer = {
            'id'    : id,
            'nombre' : nombre,
            'logo' : logo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda
        };

        $.ajax({
            url: 'tipo_asegurados/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_asegurados';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'tipo_asegurados/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_asegurados';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'tipo_asegurados/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#logo').val(response.body[0].logo);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#campana').val(response.body[0].campana);
                    $('#aseguradora').val(response.body[0].aseguradora);
                    $('#tienda').val(response.body[0].tienda);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'tipo_asegurados/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA CAMPANAS EN ESPECIAL */
    this.campana = function(CAMPANA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'campanas/search/'+CAMPANA, //This is the CAMPANAS
            type: "GET",
            success: function(response){
                console.log('TIENDAS CAMPANA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIENDA EN ESPECIAL */
    this.tienda = function(TIENDA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tiendas/search/'+TIENDA, //This is the TIENDA
            type: "GET",
            success: function(response){
                console.log('TIPO_AEGURADOs TIENDA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
@endsection