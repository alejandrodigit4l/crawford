@extends('crawford.layout')

@section('content')

	<h4>tipo_polizas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 550px;">
            		<p>Formulario tipo_poliza</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="marca" id="marca" class="form-control" >
                                <option value="">Estado de la marca</option>
                                @foreach( $marcas as $marca)
                                <option value="{{ $marca['id'] }}">{{ $marca['nombre'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                                <option value="">Tipo seguro</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="aseguradora" id="aseguradora" class="form-control" >
                                <option value="">Aseguradora</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="campana" id="campana" class="form-control" >
                                <option value="">Campaña</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="tienda" id="tienda" class="form-control" >
                                <option value="">Tienda</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <select name="asegurado" id="asegurado" class="form-control" >
                                <option value="">Asegurado</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <input type="text" name="formulario" id="formulario" class="form-control" placeholder="formulario JSON">
                        </div>
                        <div class="col-sm-6">
                            <label>Escribe los nombres del usuario</label><br>
                            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-12">
                            <label>Escribe los nombres del usuario</label><br>
                            <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="tipo_polizas.save();">Guardar</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="tipo_polizas.sendUpdate();">Actualizar</div>
                        </div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>formulario</th>
                                    <th>descripcion</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>
                                    <th>campana</th>
                                    <th>tienda</th>
                                    <th>asegurado</th>                                 
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $tipo_polizas as $tipo_poliza )
                                <tr>
                                    <td>{{ $tipo_poliza['id'] }}</td>
                                    <td>{{ $tipo_poliza['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['formulario'] }}</td>
                                    <td>{{ $tipo_poliza['descripcion'] }}</td>
                                    <td>{{ $tipo_poliza['marca']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['tipo_seguro']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['aseguradora']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['campana']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['tienda']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['asegurado']['nombre'] }}</td>
                                    <td>{{ $tipo_poliza['created_at'] }}</td>
                                    <td><a href="javascript:;" onclick="tipo_polizas.update({{ $tipo_poliza['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="tipo_polizas.delete({{ $tipo_poliza['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var tipo_polizas = new Tipo_polizas();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        tipo_polizas.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    tipo_polizas.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    tipo_polizas.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});
$('#aseguradora').on('change', function() {
    tipo_polizas.campana($('#aseguradora').val(),'#campana','Campañas');
});
$('#campana').on('change', function() {
    tipo_polizas.tienda($('#campana').val(),'#tienda','Tiendas');
});
$('#tienda').on('change', function() {
    tipo_polizas.asegurado($('#tienda').val(),'#asegurado','Asegurados');
});

$('#searchestado').on('change', function() {
    tipo_polizas.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

$('#actualizar').hide();
$('#guardar').show();

function Tipo_polizas(){



    this.save = function(){
        var nombre = this.validate('nombre');
        var formulario = this.validate('formulario');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 

        var parametrer = {
            'nombre' : nombre,
            'formulario' : formulario,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado
        };

        $.ajax({
            url: 'tipo_polizas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var nombre = this.validate('nombre');
        var formulario = this.validate('formulario');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');  
        var tienda = this.validate('tienda');     
        var asegurado = this.validate('asegurado'); 

        var parametrer = {
            'id'    : id,
            'nombre' : nombre,
            'formulario' : formulario,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado
        };

        $.ajax({
            url: 'tipo_polizas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'tipo_polizas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'tipo_polizas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#formulario').val(response.body[0].formulario);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#campana').val(response.body[0].campana);
                    $('#aseguradora').val(response.body[0].aseguradora);
                    $('#tienda').val(response.body[0].tienda);
                    $('#asegurado').val(response.body[0].asegurado);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'tipo_polizas/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA CAMPANAS EN ESPECIAL */
    this.campana = function(CAMPANA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'campanas/search/'+CAMPANA, //This is the CAMPANAS
            type: "GET",
            success: function(response){
                console.log('TIENDAS CAMPANA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIENDA EN ESPECIAL */
    this.tienda = function(TIENDA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tiendas/search/'+TIENDA, //This is the TIENDA
            type: "GET",
            success: function(response){
                console.log('TIPO_AEGURADOs TIENDA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA ASEGURADO EN ESPECIAL */
    this.asegurado = function(ASEGURADO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_asegurados/search/'+ASEGURADO, //This is the ASEGURADO
            type: "GET",
            success: function(response){
                console.log('TIENDAS ASEGURADO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
@endsection