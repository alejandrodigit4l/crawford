@extends('crawford.layout')

@section('content')

	<h4>Usuarios </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford" style="height: 300px;">
            		<p>Formulario Usuarios</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="col-sm-5">
                            <label>Nombre completo del usuario</label><br>
                            <input type="text" name="nombre_usuario" class="form-control" id="nombre_usuario" placeholder="Nombres" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-sm-4">
                            <label>Correo electronico del usuario</label><br>
                            <input type="text" name="email_cliente" class="form-control" id="email_cliente" placeholder="email" value="">
                        </div>
                        <div class="col-sm-3">
                            <label>Celular/telefono del usuario</label><br>
                            <input type="text" name="telefono" class="form-control" id="telefono" placeholder="telefono" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        {{-- <div class="col-sm-4">
                        <label>Escribe los apellidos del usuario</label><br>
                            <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Apellidos" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div> --}}
                        <div class="col-sm-3">
                            <label>rol/perfil de usuario</label><br>
                            <select name="roles" id="roles" class="form-control" >
                                <option value=""></option>
                                @foreach( $roles as $role)
                                <option value="{{ $role['id'] }}">{{ $role['nombre'] }} </option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="col-sm-3">
                            <label>Escribe el nombre del usuario (USERNAME)</label><br>
                            <input type="text" name="username" class="form-control" id="username" placeholder="username" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div> --}}
                        <div class="col-sm-3">
                            <label>marca del usuario</label><br>
                            <select class="form-control" id="marca" name="marca">
                                <option value=""></option>
                                <option value="1">CRAWFORD</option>
                                <option value="2">FALABELLA</option>
                                <option value="5">FALABELLA CHUBB</option>
                                <option value="3"> SEGUROS MUNDIAL</option>
                                <option value="4"> SAMSUNG</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>Estado</label><br>
                            <select class="form-control" id="estado" name="estado">
                                <option value="1">ACTIVADO</option>
                                <option value="0">DESACTIVADO</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>Contraseña</label><br>
                            <input type="password" name="password_cliente" class="form-control" id="password_cliente" placeholder="password">
                        </div>
                        <input type="hidden" name="remember_token" class="form-control" id="remember_token" placeholder="remember_token" value="123" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="col-sm-6">
                            <div class="btn btn-success" style="width: 100%;" id="guardar" onclick="usuarios.save();">Guardar</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-warning" style="width: 100%;" id="actualizar" onclick="usuarios.sendUpdate();">Actualizar</div>
                        </div>
                        
                        
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                	<th>id</th>
                                	<th>roles</th>
									<th>Nombre</th>
									<th>email</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $usuarios as $usuario )
                                <tr>
                                	<td>{{ $usuario['id'] }}</td>
                                    <td>{{ $usuario['rol']['nombre'] }}</td>
									<td>{{ $usuario['name'] }}</td>
									<td>{{ $usuario['email'] }}</td>
                                    <td><a href="javascript:;" onclick="usuarios.update({{ $usuario['id'] }});" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="usuarios.delete({{ $usuario['id'] }});" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            @endforeach                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var usuarios = new Usuarios();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        usuarios.search($('#searchnombre').val());
    }
});

$('#actualizar').hide();
$('#guardar').show();

function Usuarios(){

    this.save = function(){
		var nombre_usuario = this.validate('nombre_usuario');
		// var firstname = this.validate('firstname');
		// var lastname = this.validate('lastname');
        var email = this.validate('email_cliente');
        var telefono = this.validate('telefono');
        var roles = this.validate('roles');
        var marca = this.validate('marca');
        var estado = this.validate('estado');
		var password = this.validate('password_cliente');
		// var remember_token = this.validate('remember_token');
		

        var parametrer = {
			'nombre_usuario' : nombre_usuario,
            'email' : email,
			'telefono' : telefono,
            'rol' : roles,
            'marca' : marca,
            'estado' : estado,
			'password' : password,
			// 'remember_token' : remember_token
        };

        $.ajax({
            url: 'usuarios/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
    	var nombre_usuario = this.validate('nombre_usuario');
		var email = this.validate('email_cliente');
        var telefono = this.validate('telefono');
        var roles = this.validate('roles');
        var marca = this.validate('marca');
        var estado = this.validate('estado');
        var password = this.validate('password_cliente');
		// var remember_token = this.validate('remember_token');

        var parametrer = {
            'id'    : id,
            'nombre_usuario' : nombre_usuario,
            'email' : email,
            'telefono' : telefono,
            'rol' : roles,
            'marca' : marca,
            'estado' : estado,
            'password' : password
			// 'remember_token' : remember_token
        };

        $.ajax({
            url: 'usuarios/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'usuarios/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'usuarios/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
					$('#nombre_usuario').val(response.body[0].name);
					$('#email_cliente').val(response.body[0].email);
					$('#telefono').val(response.body[0].telefono);
                    $('#roles').val(response.body[0].rol.id);
                    $('#marca').val(response.body[0].marca);
                    $('#estado').val(response.body[0].active);
					$('#password_cliente').val(response.body[0].password);
					// $('#remember_token').val(response.body[0].remember_token);  
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
                $('#actualizar').show();
                $('#guardar').hide();
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'usuarios/'+DATA;
    }

}


</script>
	
@endsection