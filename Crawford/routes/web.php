<?php

use Illuminate\Support\Facades\Storage;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('laravel', function () {
//     return view('home1');
// });

// Route::get('/', function () {
//     // return view('choise');
//     return '';
// });

// Route::get('falabella', function () {
//     // return view('login-falabella');
//     return '';
// });
// Route::get('/falabella/olvido', function(){
// 	return view('falabella.olvido');
// });
// Route::post('/falabella/olvido', 'UsuariosController@olvido');

// Route::get('crawford', function () {
//     return view('auth.login');
// });
// Route::get('/crawford/olvido', function(){
// 	return view('crawford.olvido');
// });

Route::get('/actualizar_pass','ClientesController@updatePass');

Route::get('laravel', function () {
    return view('home1');
});

Route::get('/', function () {
    // return view('choise');
    return view('sitio_en_construccion');
});

Route::get('/falabella', 'Auth\LoginfalabellaController@showLoginForm');

Route::get('/falabella/olvido', function(){
	return view('falabella.olvido');
});
Route::post('/falabella/olvido', 'UsuariosController@olvido');


Route::get('/crawford', function(){
	return view('auth.login');
});

Route::post('logincrawford', 'Auth\LoginController@login')->name('login.crawford');
Route::post('logoutcrawford', 'Auth\LoginController@logout')->name('logout.crawford');
Route::post('loginfalabella', 'Auth\LoginfalabellaController@login')->name('login.falabella');
Route::post('logoutfalabella', 'Auth\LoginfalabellaController@logout')->name('logout.falabella');


Route::get('/crawford/olvido', function(){
	return view('crawford.olvido');
});


Route::post('sendmessage', 'ChatsController@sendMessage');

/**
*  ROUTE MARCAS
*
*  
*/
Route::get('/marcas', 'marcasController@index');
Route::post('/marcas/create', 'marcasController@save');
Route::post('/marcas/update', 'marcasController@update');
Route::get('/marcas/delete/{id}', 'marcasController@delete');
Route::get('/marcas/get/{id}', 'marcasController@get');
Route::get('/marcas/all', 'marcasController@all');
Route::get('/marcas/download/all', 'marcasController@downloadExcel');
Route::get('/marcas/{filtro}', 'marcasController@filtros');

/**
*  ROUTE TIPO_SEGUROS
*
*  
*/
Route::get('/tipo_seguros', 'Tipo_segurosController@index');
Route::post('/tipo_seguros/create', 'Tipo_segurosController@save');
Route::post('/tipo_seguros/update', 'Tipo_segurosController@update');
Route::get('/tipo_seguros/delete/{id}', 'Tipo_segurosController@delete');
Route::get('/tipo_seguros/get/{id}', 'Tipo_segurosController@get');
Route::get('/tipo_seguros/all', 'Tipo_segurosController@all');
Route::get('/tipo_seguros/download/all', 'Tipo_segurosController@downloadExcel');
Route::get('/tipo_seguros/{filtro}', 'Tipo_segurosController@filtros');
Route::get('/tipo_seguros/search/{marca}','Tipo_segurosController@marca');

/**
*  ROUTE ASEGURADORAS
*
*  
*/
Route::get('/aseguradoras', 'AseguradorasController@index');
Route::post('/aseguradoras/create', 'AseguradorasController@save');
Route::post('/aseguradoras/update', 'AseguradorasController@update');
Route::get('/aseguradoras/delete/{id}', 'AseguradorasController@delete');
Route::get('/aseguradoras/get/{id}', 'AseguradorasController@get');
Route::get('/aseguradoras/all', 'AseguradorasController@all');
Route::get('/aseguradoras/download/all', 'AseguradorasController@downloadExcel');
Route::get('/aseguradoras/{filtro}', 'AseguradorasController@filtros');
Route::get('/aseguradoras/search/{tipo_seguro}','AseguradorasController@tipo_seguro');

/**
*  ROUTE CAPAÑAS
*
*  
*/
Route::get('/campanas', 'CampanasController@index');
Route::post('/campanas/create', 'CampanasController@save');
Route::post('/campanas/update', 'CampanasController@update');
Route::get('/campanas/delete/{id}', 'CampanasController@delete');
Route::get('/campanas/get/{id}', 'CampanasController@get');
Route::get('/campanas/all', 'CampanasController@all');
Route::get('/campanas/download/all', 'CampanasController@downloadExcel');
Route::get('/campanas/{filtro}', 'CampanasController@filtros');
Route::get('/campanas/search/{aseguradora}','CampanasController@aseguradora');

/**
*  ROUTE TIENDAS
*
*  
*/
Route::get('/tiendas', 'TiendasController@index');
Route::post('/tiendas/create', 'TiendasController@save');
Route::post('/tiendas/update', 'TiendasController@update');
Route::get('/tiendas/delete/{id}', 'TiendasController@delete');
Route::get('/tiendas/get/{id}', 'TiendasController@get');
Route::get('/tiendas/all', 'TiendasController@all');
Route::get('/tiendas/download/all', 'TiendasController@downloadExcel');
Route::get('/tiendas/{filtro}', 'TiendasController@filtros');
Route::get('/tiendas/search/{campana}','TiendasController@campana');

/**
*  ROUTE TIPO_ASEGURADO
*
*  
*/
Route::get('/tipo_asegurados', 'Tipo_aseguradosController@index');
Route::post('/tipo_asegurados/create', 'Tipo_aseguradosController@save');
Route::post('/tipo_asegurados/update', 'Tipo_aseguradosController@update');
Route::get('/tipo_asegurados/delete/{id}', 'Tipo_aseguradosController@delete');
Route::get('/tipo_asegurados/get/{id}', 'Tipo_aseguradosController@get');
Route::get('/tipo_asegurados/all', 'Tipo_aseguradosController@all');
Route::get('/tipo_asegurados/download/all', 'Tipo_aseguradosController@downloadExcel');
Route::get('/tipo_asegurados/{filtro}', 'Tipo_aseguradosController@filtros');
Route::get('/tipo_asegurados/search/{tienda}','Tipo_aseguradosController@tienda');

/**
*  ROUTE TIPO_POLIZAS
*
*  
*/
Route::get('/tipo_polizas', 'Tipo_polizasController@index');
Route::post('/tipo_polizas/create', 'Tipo_polizasController@save');
Route::post('/tipo_polizas/update', 'Tipo_polizasController@update');
Route::get('/tipo_polizas/delete/{id}', 'Tipo_polizasController@delete');
Route::get('/tipo_polizas/get/{id}', 'Tipo_polizasController@get');
Route::get('/tipo_polizas/all', 'Tipo_polizasController@all');
Route::get('/tipo_polizas/download/all', 'Tipo_polizasController@downloadExcel');
Route::get('/tipo_polizas/{filtro}', 'Tipo_polizasController@filtros');
Route::get('/tipo_polizas/search/{asegurado}','Tipo_polizasController@asegurado');

/**
*  ROUTE LISTCHEKINGS
*
*  
*/
Route::get('/listchekings', 'ListchekingsController@index');
Route::post('/listchekings/create', 'ListchekingsController@save');
Route::post('/listchekings/update', 'ListchekingsController@update');
Route::get('/listchekings/delete/{id}', 'ListchekingsController@delete');
Route::get('/listchekings/get/{id}', 'ListchekingsController@get');
Route::get('/listchekings/all', 'ListchekingsController@all');
Route::get('/listchekings/download/all', 'ListchekingsController@downloadExcel');
Route::get('/listchekings/{filtro}', 'ListchekingsController@filtros');
Route::get('/listchekings/search/{tipo_poliza}','ListchekingsController@tipo_poliza');
Route::get('/listchekings/search/{tipo_poliza}/{reclamo}','ListchekingsController@tipo_polizareclamo');
Route::get('/listchekings/traer_lista/{reclamo}','ListchekingsController@traer_lista');

/**
*  ROUTE PRODUCTOS
*
*  
*/
Route::get('/productos', 'ProductosController@index');
Route::post('/productos/create', 'ProductosController@save');
Route::post('/productos/update', 'ProductosController@update');
Route::get('/productos/delete/{id}', 'ProductosController@delete');
Route::get('/productos/get/{id}', 'ProductosController@get');
Route::get('/productos/all', 'ProductosController@all');
Route::get('/productos/download/all', 'ProductosController@downloadExcel');
Route::get('/productos/{filtro}', 'ProductosController@filtros');
Route::get('/productos/search/{asegurado}','ProductosController@asegurado');
Route::get('/productos/search/marca/{marca}','ProductosController@marca');






/**
*  ROUTE ROLES
*
*  
*/
Route::get('/roles', 'RolesController@index');
Route::post('/roles/create', 'RolesController@save');
Route::post('/roles/update', 'RolesController@update');
Route::get('/roles/delete/{id}', 'RolesController@delete');
Route::get('/roles/get/{id}', 'RolesController@get');
Route::get('/roles/all', 'RolesController@all');
Route::get('/roles/download/all', 'RolesController@downloadExcel');
Route::get('/roles/{filtro}', 'RolesController@filtros');

/**
*  ROUTE USUARIOS
*
*  
*/
Route::get('/usuarios', 'UsuariosController@index');
Route::post('/usuarios/create', 'UsuariosController@save');
Route::post('/usuarios/update', 'UsuariosController@update');
Route::get('/usuarios/delete/{id}', 'UsuariosController@delete');
Route::get('/usuarios/get/{id}', 'UsuariosController@get');
Route::get('/usuarios/all', 'UsuariosController@all');
Route::get('/usuarios/download/all', 'UsuariosController@downloadExcel');


/**
*  ROUTE ARBOL_PRODUCTOS
*
*  
*/
Route::get('/arbol_productos', 'Arbol_productosController@index');
Route::post('/arbol_productos/create', 'Arbol_productosController@save');
Route::post('/arbol_productos/update', 'Arbol_productosController@update');
Route::get('/arbol_productos/delete/{id}', 'Arbol_productosController@delete');
Route::get('/arbol_productos/get/{id}', 'Arbol_productosController@get');
Route::get('/arbol_productos/all', 'Arbol_productosController@all');
Route::get('/arbol_productos/download/all', 'Arbol_productosController@downloadExcel');
Route::get('/arbol_productos/{filtro}', 'Arbol_productosController@filtros');






/**
*  ROUTE HOMOLOGOS
*
*  
*/
Route::get('/homologos', 'HomologosController@index');
Route::post('/homologos/create', 'HomologosController@save');
Route::post('/homologos/update', 'HomologosController@update');
Route::get('/homologos/delete/{id}', 'HomologosController@delete');
Route::get('/homologos/get/{id}', 'HomologosController@get');
Route::get('/homologos/all', 'HomologosController@all');
Route::get('/homologos/download/all', 'HomologosController@downloadExcel');
Route::get('/homologos/{filtro}', 'HomologosController@filtros');

/**
*  ROUTE ESTADOS_POLIZAS
*
*  
*/
Route::get('/estados_polizas', 'Estados_polizasController@index');
Route::post('/estados_polizas/create', 'Estados_polizasController@save');
Route::post('/estados_polizas/update', 'Estados_polizasController@update');
Route::get('/estados_polizas/delete/{id}', 'Estados_polizasController@delete');
Route::get('/estados_polizas/get/{id}', 'Estados_polizasController@get');
Route::get('/estados_polizas/all', 'Estados_polizasController@all');
Route::get('/estados_polizas/download/all', 'Estados_polizasController@downloadExcel');
Route::get('/estados_polizas/{filtro}', 'Estados_polizasController@filtros');






/**
*  ROUTE CLIENTES
*
*  
*/
Route::get('/clientes', 'ClientesController@index');
Route::post('/clientes/create', 'ClientesController@save');
Route::post('/clientes/update', 'ClientesController@update');
Route::post('/clientes/update_admin', 'ClientesController@update_admin');
Route::get('/clientes/delete/{id}', 'ClientesController@delete');
Route::get('/clientes/get/{id}', 'ClientesController@get');
Route::get('/clientes/all', 'ClientesController@all');
Route::get('/clientes/download/all', 'ClientesController@downloadExcel');
Route::get('/clientes/{filtro}', 'ClientesController@filtros');
Route::get('/clientes/search/marca/{marca}','ClientesController@marca');
Route::post('/clientes/destruir_token','ClientesController@destruir_token');

/**
*  ROUTE CIUDADES
*
*  
*/
Route::get('/ciudades', 'CiudadesController@index');
Route::post('/ciudades/create', 'CiudadesController@save');
Route::post('/ciudades/update', 'CiudadesController@update');
Route::get('/ciudades/delete/{id}', 'CiudadesController@delete');
Route::get('/ciudades/get/{id}', 'CiudadesController@get');
Route::get('/ciudades/all', 'CiudadesController@all');
Route::get('/ciudades/show_ciudades/{id_departamento}/{marca}', 'CiudadesController@show_ciudades');
Route::get('/ciudades/download/all', 'CiudadesController@downloadExcel');
Route::get('/ciudades/{filtro}', 'CiudadesController@filtros');

/**
*  ROUTE RECLAMOS
*
*  
*/
Route::get('/reclamos', 'ReclamosController@index');
Route::post('/reclamos/create', 'ReclamosController@save');
Route::post('/reclamos/create_admin', 'ReclamosController@save_admin');
Route::post('/reclamos/create_admin_zurich', 'ReclamosController@save_zurich');
Route::post('/reclamos/update', 'ReclamosController@update');
Route::get('/reclamos/delete/{id}', 'ReclamosController@delete');
Route::get('/reclamos/get/{id}', 'ReclamosController@get');
Route::get('/reclamos/all', 'ReclamosController@all');
Route::get('/reclamos/download/all', 'ReclamosController@downloadExcel');
Route::get('/reclamos/{filtro}', 'ReclamosController@filtros');
Route::get('/reclamos/cliente/{cliente}', 'ReclamosController@getcliente');
Route::get('/reclamos/cliente_app/{cliente}', 'ReclamosController@getcliente_app');
Route::post('/reclamos/updateToEnEstudio' , 'ReclamosController@updateToEnEstudio');
Route::get('/reclamos/download/{file}' , 'ReclamosController@downloadFile');

/**
*  ROUTE DOCUMENTS
*
*  
*/
Route::get('/documentsmodel', 'DocumentsController@index');
Route::post('/documentsmodel/create', 'DocumentsController@save');
Route::post('/documentsmodel/uploadSubmit', 'DocumentsController@uploadSubmit');
Route::post('/documentsmodel/uploadSubmitAPP', 'DocumentsController@uploadSubmitAPP');
Route::post('/documentsmodel/uploadSubmitBackend', 'DocumentsController@uploadSubmitBackend');
Route::post('/documentsmodel/update', 'DocumentsController@update');
Route::get('/documentsmodel/delete/{id}', 'DocumentsController@delete');
Route::get('/documentsmodel/get/{id}', 'DocumentsController@get');
Route::get('/documentsmodel/all', 'DocumentsController@all');
Route::get('/documentsmodel/download/all', 'DocumentsController@downloadExcel');
Route::get('/documentsmodel/{filtro}', 'DocumentsController@filtros');
Route::get('/documentsmodel/reclamo/{reclamo}', 'DocumentsController@reclamo');
Route::get('/documentsmodel/subirdocumento/{reclamo}/{listcheking}' ,'DocumentsController@subirdocumento');
Route::get('/documentsmodel/observardocumentos/{reclamo}/{listcheking}', 'DocumentsController@urlDocuments');
Route::get('/documentsmodel/traer_numero/{reclamo}/{checklist}','DocumentsController@traer_numero');

/**
*  ROUTE PAYU_TRANSACTIONS
*
*  
*/
Route::get('/payu_transactions', 'Payu_transactionsController@index');
Route::post('/payu_transactions/create', 'Payu_transactionsController@save');
Route::post('/payu_transactions/update', 'Payu_transactionsController@update');
Route::get('/payu_transactions/delete/{id}', 'Payu_transactionsController@delete');
Route::get('/payu_transactions/get/{id}', 'Payu_transactionsController@get');
Route::get('/payu_transactions/all', 'Payu_transactionsController@all');
Route::get('/payu_transactions/download/all', 'Payu_transactionsController@downloadExcel');
Route::get('/payu_transactions/{filtro}', 'Payu_transactionsController@filtros');

/**
*  ROUTE PETICION_COMPRAs
*
*  
*/
Route::get('/peticion_compras', 'Peticion_comprasController@index');
Route::post('/peticion_compras/create', 'Peticion_comprasController@save');
Route::post('/peticion_compras/update', 'Peticion_comprasController@update');
Route::get('/peticion_compras/delete/{id}', 'Peticion_comprasController@delete');
Route::get('/peticion_compras/get/{id}', 'Peticion_comprasController@get');
Route::get('/peticion_compras/all', 'Peticion_comprasController@all');
Route::get('/peticion_compras/download/all', 'Peticion_comprasController@downloadExcel');
Route::get('/peticion_compras/{filtro}', 'Peticion_comprasController@filtros');

/**
*  ROUTE NOTIFICACIONES
*
*  
*/
Route::get('/notificaciones', 'notificacionesController@index');
Route::post('/notificaciones/create', 'notificacionesController@save');
Route::post('/notificaciones/sendBackend', 'notificacionesController@sendBackend');
Route::post('/notificaciones/update', 'notificacionesController@update');
Route::get('/notificaciones/delete/{id}', 'notificacionesController@delete');
Route::get('/notificaciones/get/{id}', 'notificacionesController@get');
Route::get('/notificaciones/reclamo/{reclamo}', 'notificacionesController@reclamo');
Route::get('/notificaciones/all', 'notificacionesController@all');
Route::get('/notificaciones/download/all', 'notificacionesController@downloadExcel');
Route::get('/notificaciones/{filtro}', 'notificacionesController@filtros');

/**
*  ROUTE SOLICITUDES
*
*  
*/
Route::get('/solicitudes', 'SolicitudesController@index');
Route::post('/solicitudes/create', 'SolicitudesController@save');
Route::post('/solicitudes/create_app', 'SolicitudesController@save_app');
Route::post('/solicitudes/update', 'SolicitudesController@update');
Route::get('/solicitudes/delete/{id}', 'SolicitudesController@delete');
Route::get('/solicitudes/get/{id}', 'SolicitudesController@get');
Route::get('/solicitudes/all', 'SolicitudesController@all');
Route::get('/solicitudes/download/all', 'SolicitudesController@downloadExcel');
Route::get('/solicitudes/{filtro}', 'SolicitudesController@filtros');

/**
*  ROUTE POLIZAS
*
*  
*/
Route::get('/polizas', 'PolizasController@index');
Route::post('/polizas/create', 'PolizasController@save');
Route::post('/polizas/update', 'PolizasController@update');
Route::get('/polizas/delete/{id}', 'PolizasController@delete');
Route::get('/polizas/get/{id}', 'PolizasController@get');
Route::get('/polizas/all', 'PolizasController@all');
Route::get('/polizas/download/all', 'PolizasController@downloadExcel');
Route::get('/polizas/{filtro}', 'PolizasController@filtros');


/**
*  ROUTE CHATS
*
*  
*/
Route::get('/chats', 'ChatsController@index');
Route::post('/chats/create', 'ChatsController@save');
Route::post('/chats/update', 'ChatsController@update');
Route::get('/chats/delete/{id}', 'ChatsController@delete');
Route::get('/chats/get/{id}', 'ChatsController@get');
Route::get('/chats/all', 'ChatsController@all');
Route::get('/chats/download/all', 'ChatsController@downloadExcel');
Route::get('/chats/historial/{sender}/{receptor}', 'ChatsController@historial');
Route::get('/chats/backend/{sender}', 'ChatsController@backend');
Route::get('/chats/cambio_estado/{id_cliente}','ChatsController@cambio_estado');
Route::get('/chats/todos/','ChatsController@todos');
/**
*  ROUTE UtilchatsController
*
*  
*/
Route::get('/utilchats', 'UtilchatsController@index');
Route::post('/utilchats/create', 'UtilchatsController@save');
Route::post('/utilchats/update', 'UtilchatsController@update');
Route::get('/utilchats/delete/{id}', 'UtilchatsController@delete');
Route::get('/utilchats/get/{id}', 'UtilchatsController@get');
Route::get('/utilchats/all', 'UtilchatsController@all');
Route::get('/utilchats/download/all', 'UtilchatsController@downloadExcel');
Route::get('/utilchats/search' , 'UtilchatsController@ultimedata');

/**
*  ROUTE DEVICES
*
*  
*/
Route::get('/devices', 'DevicesController@index');
Route::post('/devices/create', 'DevicesController@save');
Route::post('/devices/update', 'DevicesController@update');
Route::get('/devices/delete/{id}', 'DevicesController@delete');
Route::get('/devices/get/{id}', 'DevicesController@get');
Route::get('/devices/all', 'DevicesController@all');
Route::get('/devices/download/all', 'DevicesController@downloadExcel');


/**
*
* 	Estadisticas CRAWFORD
*
*
*/
Route::get('/reclamacionesxfecha/{aseguradora}', 'EstadisticasController@reclamacionesxfecha');
Route::get('/clientesaseguradoras', 'EstadisticasController@clientesaseguradoras');
Route::get('/reclamosaseguradora', 'EstadisticasController@reclamosaseguradora');


/**
*  ROUTE Crawford backend
*
*  
*/

// Route::group(['middleware' => 'auth'], function(){

// 	Route::get('/crawford/reclamos', 'Reclamos_crawford@viewReclamos');

// });

// Route::get('/crawford/index', 'Reclamos_crawford@dashboard');
// Route::get('/crawford/ciudad/{ciudad}', 'Reclamos_crawford@ciudad');
// Route::get('/crawford/cliente/{cliente}', 'Reclamos_crawford@cliente');
// Route::get('/crawford/estado/{estado}', 'Reclamos_crawford@estado');
// Route::get('/crawford/idreclamo/{id}', 'Reclamos_crawford@idreclamo');
// Route::get('/crawford/reclamos', 'Reclamos_crawford@viewReclamos')->middleware('auth');
// Route::get('/crawford/filtro_marca/{marca}', 'Reclamos_crawford@filtro_marca');
// Route::get('/crawford/clientes','Reclamos_crawford@clientes');
// Route::get('/crawford/create_reclamo/{cliente}','Reclamos_crawford@create_reclamo');
// Route::get('/crawford/chat','ChatsController@chatcrawford');
// Route::get('/crawford/clientes/search/{search}','Reclamos_crawford@cliente_search');
// Route::get('/crawford/search/{search}','Reclamos_crawford@search');
// Route::get('/crawford/ajustes', 'Reclamos_crawford@ajustes');
// Route::get('/crawford/polizas', 'PolizasController@polizasPendientes');
// Route::get('/crawford/datos_poliza/{id}', 'PolizasController@datos_poliza');
// Route::post('/crawford/actualizar_estado_poliza/', 'PolizasController@actualizar_estado_poliza');
// Route::get('/crawford/download_poliza/{file}', 'PolizasController@download_poliza');
// Route::get('/crawford/filtro_estado/{estado_poliza}', 'PolizasController@filtro_estado');

// Route::get('/crawford/export/zip/{reclamo}','ExportController@zip');
// Route::get('/crawford/export/excel/{reclamo}','ExportController@reclamos_one');
// Route::get('/crawford/export/clientes','ExportController@clientes_All');
// Route::get('/crawford/export/reclamos','ExportController@reclamos_All');


// Route::get('/crawford/filtro_campaña','Reclamos_crawford@filtro_campaña');

// Route::get('/crawford/polizasbeneficio', 'PolizasController@vistaPoliza');
// Route::post('/crawford/subirimeis', 'PolizasController@subirimeis');


/**
*  ROUTE Falabella backend
*
*  
*/
// Route::get('/falabella/index', 'Reclamos_falabella@dashboard');
// Route::get('/falabella/ciudad/{ciudad}', 'Reclamos_falabella@ciudad');
// Route::get('/falabella/cliente/{cliente}', 'Reclamos_falabella@cliente');
// Route::get('/falabella/estado/{estado}', 'Reclamos_falabella@estado');
// Route::get('/falabella/idreclamo/{id}', 'Reclamos_falabella@idreclamo');
// Route::get('/falabella/reclamos', 'Reclamos_falabella@viewReclamos');
// Route::get('/falabella/filtro_marca/{marca}', 'Reclamos_falabella@filtro_marca');
// Route::get('/falabella/clientes','Reclamos_falabella@clientes');
// Route::get('/falabella/create_reclamo/{cliente}','Reclamos_falabella@create_reclamo');
// Route::get('/falabella/chat','ChatsController@chatcrawford');
// Route::get('/falabella/clientes/search/{search}','Reclamos_falabella@cliente_search');
// Route::get('/falabella/search/{search}','Reclamos_falabella@search');
// Route::get('/falabella/ajustes', 'Reclamos_falabella@ajustes');

// Route::get('/falabella/export/zip/{reclamo}','ExportController@zip');
// Route::get('/falabella/export/excel/{reclamo}','ExportController@reclamos_one');
// Route::get('/falabella/export/clientes','ExportController@clientes_All');
// Route::get('/falabella/export/reclamos','ExportController@reclamos_All');


/**
*  ROUTE Crawford backend
*
*  
*/

Route::group(['middleware' => 'crawfordMiddleware'], function(){

	Route::get('/crawford/index', 'Reclamos_crawford@dashboard');
	Route::get('/crawford/cliente/{cliente}', 'Reclamos_crawford@cliente');
	Route::get('/crawford/reclamos', 'Reclamos_crawford@viewReclamos');
	Route::get('/crawford/clientes','Reclamos_crawford@clientes');
	Route::get('/crawford/chat','ChatsController@chatcrawford');
	Route::get('/crawford/export/clientes','ExportController@clientes_All');
	Route::get('/crawford/ajustes', 'Reclamos_crawford@ajustes');
	Route::get('/crawford/idreclamo/{id}', 'Reclamos_crawford@idreclamo');
	Route::get('/crawford/idreclamo_zurich/{id}', 'Reclamos_crawford@idreclamo_zurich');
	Route::post('/crawford/actualizar_informacion_reclamo_zurich', 'Reclamos_crawford@actualizar_informacion_reclamo_zurich');
	Route::post('/crawford/actualizar_informacion_siniestro_zurich', 'Reclamos_crawford@actualizar_informacion_siniestro_zurich');
	Route::post('/crawford/nueva_observacion_zurich', 'Reclamos_crawford@nueva_observacion_zurich');
	Route::post('/crawford/enviar_notificacion_zurich', 'notificacionesController@enviar_notificacion_zurich');
	Route::post('/crawford/adjunto_checklist_zurich', 'Reclamos_crawford@adjunto_checklist_zurich');
	Route::post('/crawford/adjunto_adicional_zurich', 'Reclamos_crawford@adjunto_adicional_zurich');
	Route::post('/crawford/nuevo_estado_zurich', 'Reclamos_crawford@nuevo_estado_zurich');
	Route::post('/crawford/eliminar_documento_zurich/{id_documento}', 'Reclamos_crawford@eliminar_documento_zurich');
	Route::get('/crawford/url_archivos_checklist_zurich/{archivo}', 'Reclamos_crawford@url_archivos_checklist_zurich');
	Route::post('/crawford/checkbox_checklist_zurich','Reclamos_crawford@checkbox_checklist_zurich');
	Route::get('/crawford/polizas', 'PolizasController@polizasPendientes');
	Route::get('/crawford/polizasbeneficio', 'PolizasController@vistaPoliza');
	Route::get('/crawford/filtro_estado/{estado_poliza}', 'PolizasController@filtro_estado');
	Route::get('/crawford/export/excel/{reclamo}','ExportController@reclamos_one');
	Route::get('/crawford/export_excel_zurich/{reclamo}','ExportController@reclamo_zurich');
	Route::get('/crawford/filtro_campaña','Reclamos_crawford@filtro_campaña');
	Route::get('/img/{imagen}','ClientesController@imagen');
	Route::get('/archivos_js/{archivo}','ClientesController@archivos_js');
	Route::get('database', 'Reclamos_crawford@view_database');
	
});

Route::get('/crawford/datos_poliza/{id}', 'PolizasController@datos_poliza');
Route::get('/crawford/ciudad/{ciudad}', 'Reclamos_crawford@ciudad');
Route::get('/crawford/estado/{estado}', 'Reclamos_crawford@estado');
Route::get('/crawford/filtro_marca/{marca}', 'Reclamos_crawford@filtro_marca');
Route::get('/crawford/create_reclamo/{cliente}','Reclamos_crawford@create_reclamo');
Route::get('/crawford/clientes/search','Reclamos_crawford@cliente_search');
Route::get('/crawford/search','Reclamos_crawford@search');
Route::post('/crawford/actualizar_estado_poliza/', 'PolizasController@actualizar_estado_poliza');
Route::post('/crawford/notificacion_poliza/', 'PolizasController@notificacion_poliza');
Route::get('/crawford/download_poliza/{file}', 'PolizasController@download_poliza');
Route::get('/crawford/prueba_meses', 'PolizasController@prueba_meses');

Route::get('/crawford/export/zip/{reclamo}/{marca_cliente}','ExportController@zip');
Route::get('/crawford/export/reclamos','ExportController@reclamos_All');
Route::post('/crawford/subirimeis', 'PolizasController@subirimeis');



Route::post('/eliminaTokenSession', 'Auth\LoginController@eliminaToken');


/**
*  ROUTE Falabella backend
*
*  
*/

Route::group(['middleware' => 'falabellaMiddleware'], function(){

	Route::get('/falabella/index', 'Reclamos_falabella@dashboard');
	Route::get('/falabella/reclamos', 'Reclamos_falabella@viewReclamos');
	Route::get('/falabella/idreclamo/{id}', 'Reclamos_falabella@idreclamo');
	Route::get('/falabella/clientes','Reclamos_falabella@clientes');
	Route::get('/falabella/cliente/{cliente}', 'Reclamos_falabella@cliente');
	Route::get('/falabella/export/clientes','ExportController@clientes_All');
	Route::get('/falabella/export/excel/{reclamo}','ExportController@reclamos_one');
	
});

Route::get('/falabella/ciudad/{ciudad}', 'Reclamos_falabella@ciudad');
Route::get('/falabella/estado/{estado}', 'Reclamos_falabella@estado');

Route::get('/falabella/filtro_marca/{marca}', 'Reclamos_falabella@filtro_marca');
Route::get('/falabella/create_reclamo/{cliente}','Reclamos_falabella@create_reclamo');
// Route::get('/falabella/chat','ChatsController@chatcrawford');
Route::get('/falabella/clientes/search/{search}','Reclamos_falabella@cliente_search');
Route::get('/falabella/search/{search}','Reclamos_falabella@search');
Route::get('/falabella/ajustes', 'Reclamos_falabella@ajustes');

Route::get('/falabella/export/zip/{reclamo}','ExportController@zip');
Route::get('/falabella/export/reclamos','ExportController@reclamos_All');




/**
*  ROUTE Crawford App Raynier
*
*  
*/

Route::post( 'login-samsung', 'ClientesController@loginSamsung' );
Route::get( 'clientesamsung/{imei}/{serial}/{modelo}','ClientesController@clientesSamsung' );
Route::get( 'listreclamossamsung/{imei}', 'ReclamosController@listarReclamosSamsung' );
Route::post( '/documentsmodel/uploadimagesamsung/', 'DocumentsController@cargarImagenSamsung' );
Route::get( 'getChekingAmpliacion/{id_reclamo}/{marca}','ListchekingsController@getCheklistReclamo' );
// Route::get( 'getProductoSamsung/{modelo}','ProductosController@obtenerPolizasSamsung' );
// Route::post( '/documentsmodel/uploadimagepoliza/', 'PolizasController@subirDocumentosPoliza' );
Route::get( 'getProductoSamsung/{modelo}/{imei}/{email}','ProductosController@obtenerPolizasSamsung' );
Route::post( '/documentsmodel/uploadimagepoliza/', 'PolizasController@subirDocumentosPoliza' );
Route::post('pagossamsung', 'PolizasController@pagos');
Route::get('pdf/generar/{id}','PolizasController@getGenerar');
Route::get('/probarcookie', 'PolizasController@probarCookie');
Route::get('/polizasamsung', 'PolizasController@verpoliza');
Route::get('/verificapago/{id}', 'PolizasController@verificarpago');
Route::post('/actualizatoken', 'ClientesController@updateTokenSamsung');
Route::get('/ip', 'PolizasController@getIp');

/*******************************************************************************************************************************************************************/

/*WEB APPS*/

Route::post('/clientes/webapp/login', 'ClientesController@login');
Route::post('/usuarios/web/login', 'UsuariosController@login');
Route::post('/clientes/webapp/olvido', 'ClientesController@olvido');

Route::post('/documentsmodel/verificarcompletos','DocumentsController@verificarcompletos');

//Route::get('send','ClientesController@send');
Route::get('/send','notificacionesController@send');
Route::get('/send_push', 'notificacionesController@sendPush');

Route::post('/test/uploadaudio', 'TestController@uploadAudio');

Route::get('vista_pdf',function(){
	return view('crawford.pdf');
});


Auth::routes();


Route::get('/crawford/{file}',function($file){
	return Storage::response('public/$file');
});
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', 'Reclamos_crawford@dashboard')->name('home');
Route::get('/test/getinfo', 'TestController@getPhpInfo');
Route::post('/crsf', 'ClientesController@crsf');


Route::get('ruta_poliza', function(){
	$vista = array(
					'titulo' => 'x',
                    'iva' => '19',
                    'precioTotal' => '0',
                    'precio' => '0',
                    'imei' => '312121212121212',
                    'cubrimiento'=> '12',
                    'modelo'=> 'note 9',
                    'consecutivo' => '$consecutivo',
                    'descripcion' => 'xxx'
                );
	return view('crawford.polizaSamsung',array('data' => $vista));
});


Route::get('encriptar','ClientesController@encriptar');
Route::get('desencriptar','ClientesController@desencriptar');


Route::get('cliente/crear_cliente', 'ClientesController@vista_creacion_cliente');
Route::post('cliente/registrar_cliente', 'ClientesController@save_admin')->name('registrar_cliente');
Route::get('cliente/crear_reclamo/{id_cliente}', 'ClientesController@vista_creacion_reclamo');
Route::get('cliente/verificar_factura/{factura}', 'ReclamosController@verificar_factura');
Route::get('cliente/validar_correo_registro/{correo}', 'ClientesController@validar_correo_registro');


Route::post('prueba_request_compratido','ReclamosController@prueba_request_compratido');

Route::post('subir_excel_clientes','ClientesController@subir_excel_clientes');
Route::post('subir_excel_reclamos','ClientesController@subir_excel_reclamos');

