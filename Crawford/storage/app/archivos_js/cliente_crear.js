let cliente = new Cliente();

let flag = 1;

function Cliente(){
	this.offset = function(el){
		var rect = el.getBoundingClientRect(),
	    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
	    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
	    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
	};
	this.mostrar_datepicker = function(){
		let input = cliente.offset(document.querySelector('#txtTest'));
		flag += 1;
		if (flag%2 == 0) {
			pureJSCalendar.open('yyyy-MM-dd', input.left, input.top, 1, '2018-5-5', '2019-8-20', 'txtTest', 20)
		}else{
			pureJSCalendar.close()
		}
	};
	this.crear = function(){

		let aseguradora = '';

		if ($('#marca').val() == '2') {
			aseguradora = 2;
		}else if($('#marca').val() == '3'){
			aseguradora = 4;
		}else if($('#marca').val() == '4'){
			aseguradora = 5;
		}else if($('#marca').val() == '5'){
			aseguradora = 8;
		}

		let clientid = "CRCLI"+Math.floor((Math.random() * 1000000000) + 1);
		console.log('funcion creado');
		$.ajax({
			data: {
				'clientid': clientid,
				'nombres': $('#nombres').val(),
				'apellidos': $('#apellidos').val(),
				'email_cliente': $('#email_cliente').val(),
				'cedula': $('#cedula').val(),
				'password_cliente': $('#password_cliente').val(),
				'password_cliente_validate': $('#password_cliente_validate').val(),
				'celular': $('#celular').val(),
				'fechanacimiento': $('#txtTest').val(),
				'marca': $('#marca').val(),
				'aseguradora': aseguradora
			},
			url: '/cliente/registrar_cliente',
			type: 'post',
			success: function(response){
				console.log(response);
				swal({
				  title: 'Se ha creado el cliente correctamente!',
				  text: 'Será redireccionado a la creación de reclamación del cliente.',
				  type: "success",
				  showLoaderOnConfirm: true,
				},function(){
					location.reload();
					location.href = `/cliente/crear_reclamo/${response.id_cliente}`;
				});
			},
			error: function(e){
				if (e.status == 422) {
					for(var i in e.responseJSON){
						$.notify({
							icon: "pe-7s-look",
							message: e.responseJSON[i]
						},{
							type: 'danger',
							timer: 200,
							placement: {
								from: 'top',
								align: 'right'
							}
						});
					}
				}
			}
		});
	};
}