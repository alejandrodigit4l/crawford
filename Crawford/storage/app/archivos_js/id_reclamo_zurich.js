
var reclamo = new Reclamo();

function Reclamo(){

    this.actualizar_informacion_reclamo = function(id_reclamo){
        $.ajax({
            data: {
                'id_reclamo': id_reclamo,
                'numero_factura': $('#numero_factura').val(),
                'tienda': $('#tienda').val(),
                'fecha_asignacion_cst': $('#fecha_asignacion_cst').val(),
                'nombre_cst': $('#nombre_cst').val(),
                'fecha_ingreso_cst': $('#fecha_ingreso_cst').val(),
                'fecha_diagnostico': $('#fecha_diagnostico').val(),
                'tiempo_reparacion': $('#tiempo_reparacion').val(),
                'causal': $('#causal').val(),
                'valor_indemnizado': $('#valor_indemnizado').val(),
                'fecha_notificacion': $('#fecha_notificacion').val(),
                'certificado': $('#certificado').val(),
                'radicado_zurich': $('#radicado_zurich').val(),
                'fecha_envio_instructivo': $('#fecha_envio_instructivo').val(),
                'fecha_aviso': $('#fecha_aviso').val(),
                'fecha_recepcion_documentos': $('#fecha_recepcion_documentos').val(),
                'fecha_recep_doc_completos': $('#fecha_recep_doc_completos').val(),
                'hora_recepcion_documentos': $('#hora_recepcion_documentos').val(),
                'fecha_entrega_ajuste': $('#fecha_entrega_ajuste').val(),
                'fecha_ajuste': $('#fecha_ajuste').val()
            },
            url: '/crawford/actualizar_informacion_reclamo_zurich',
            type: 'put',
            success: function(res){
                swal({
                    title: 'Se ha actualizado correctamente',
                    type: 'success'
                },function(){
                    location.reload();
                });
            }
        });
    };

    this.añadir_observacion = function(id_reclamo){
        if ($('#observacion').val() == '') {
            swal("", "Debe insertar la observación.", "error");
        }else{
            $.ajax({
                data: {
                    'id_reclamo': id_reclamo,
                    'observacion': $('#observacion').val()
                },
                url: '/crawford/nueva_observacion_zurich',
                type: 'post',
                success: function(){
                    location.reload();
                },
                error: function(e){
                    console.log(e);
                }
            });
        }
    };

    this.cambiar_estado_reclamo = function(id_reclamo){
        $.ajax({
            data: {
                'id_reclamo': id_reclamo,
                'estado_nuevo': $('#estado_reclamo').val()
            },
            url: '/crawford/nuevo_estado_zurich',
            type: 'post',
            success: function(){
                location.reload();  
            },
            error: function(e){
                console.log(e);
            }
        });
    };

    this.subir_adjunto_checklist = function(nombre_checklist, id_checklist, id_reclamo){
        swal({
          title: `<h4>CARGAR ${nombre_checklist}:</h4>`,
          text: `<div class="input-group">
                    <label class="input-group-btn">
                        <span class="btn btn-default btn-fill">
                            Buscar&hellip; <input type="file" id="adjunto" style="display: none;" multiple>
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly>
                </div>`,
          html: true,
          showCancelButton: true,
          closeOnConfirm: false,
        }, function () {
            let formdata = new FormData();
            formdata.append('adjunto', $('#adjunto')[0].files[0]);
            formdata.append('id_checklist', id_checklist);
            formdata.append('id_reclamo', id_reclamo);
            if ($('#adjunto').val() == '') {
                swal({
                    title: 'Primero debe seleccionar el documento.',
                    type: 'error'
                });
            }else{
                $.ajax({
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    data: formdata,
                    url: '/crawford/adjunto_checklist_zurich',
                    type: 'post',
                    success: function(res){
                        swal({
                            title: 'Se ha cargado el documento correctamente',
                            type: 'success'
                        },function(){
                            location.reload();
                        });
                    }
                });
            }
        });
    };

    this.ver_documentos_checklist = function(nombre_checklist, documentos){

        let tbody = '<tbody>';
        for( let i  in documentos ){
            tbody += `<tr>
                        <td style="font-size: 12px;"><a href="/crawford/url_archivos_checklist_zurich/${documentos[i].nombre_documento}" target="_blank" style="color: black;">${documentos[i].nombre_documento}</a></td>
                        <td><a class="btn btn-danger btn-fill" onclick="reclamo.eliminar_documentos(${documentos[i].id})">Eliminar</a></td>
                      </tr>`;
        }
        tbody += '</tbody>';
        swal({
            title: `<h5>DOCUMENTOS ${nombre_checklist}</h5>`,
            text: `<table class="table table-hover table-striped">
                    <thead>
                     <th>Documento</th>
                     <th>Eliminar</th>
                    </thead>
                    ${tbody}
                   </table>`,
            showConfirmButton: false,
            allowOutsideClick: true,
            html: true
        });

    };

    this.eliminar_documentos = function(id_documento){
        $.ajax({
            url: `/crawford/eliminar_documento_zurich/${id_documento}`,
            type: 'post',
            success: function(res){
                location.reload();
            }
        });
    };

    this.cambio_checkbox = function(id_checklist, id_reclamo){
        $.ajax({
            data: {
                'id_checklist': id_checklist,
                'id_reclamo': id_reclamo,
                'valor': $('#checkbox')[0].checked
            },
            url: '/crawford/checkbox_checklist_zurich',
            type: 'post',
            success: function(res){
                console.log(res);
            }
        });
    }

}



// MODIFICACIÓN INPUT DE ARCHIVOS
$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {
          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;
          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
  });
});
// MODIFICACIÓN INPUT DE ARCHIVOS