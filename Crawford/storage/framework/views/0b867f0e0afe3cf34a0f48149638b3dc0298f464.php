<?php $__env->startSection('content'); ?>

	<h4>Data Base </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Escoje la base de datos a modificar</p>            		
            	</div>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="marcas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>1. marcas</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="tipo_seguros" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>2. tipo seguros</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="aseguradoras" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>3. aseguradoras</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="campanas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>4. campanas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="tiendas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>5. tiendas</p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="tipo_asegurados" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>6. Tipo asegurados</p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="tipo_polizas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>7. tipo_polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="listchekings" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>8. listchekings</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="productos" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>9. productos</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="roles" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>10.roles</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="usuarios" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>11. usuarios</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="arbol_productos" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
                    <p>12. arbol productos</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="homologos" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>13.homologos </p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="estados_polizas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>14.estados polizas</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="clientes" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>15. clientes</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="ciudades" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>16. ciudades</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="reclamos" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>17. reclamos</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="documentsmodel" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>18. documents</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="payu_transactions" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>19. payu transactions</p>                    
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="peticion_compras" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>20. peticion compras</p>                 
                </div></a>
            </div>
            <div class="col-md-2" id="reclamo">
                <a href="notificaciones" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>21. notificaciones</p>                   
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="solicitudes" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>22. solicitudes</p>                  
                </div></a>
            </div>
            <div class="col-md-2" id="compra">
                <a href="polizas" class="data"><div class="card" data-color="crawford">
                    <center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
                    <p>23. polizas</p>                  
                </div></a>
            </div>

            
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apiconfirm note8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card " data-color="crawford" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apiconfirm s8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apirecives note8s</p>            		
            	</div></a>
            </div>
           <div class="col-md-2" id="api">
            	<a href="#" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>apirecives s8s</p>            		
            	</div></a>
            </div>           
           <div class="col-md-2" id="reclamo">
            	<a href="#" class="data"><div class="card" data-color="crawford" style="background: black;color: white;opacity: 0.5;">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138937.svg"></center>
            		<p>ben. campanas</p>            		
            	</div></a>
            </div>
           
           <div class="col-md-2" id="reclamo">
            	<a href="#" class="data"><div class="card" data-color="crawford">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
            		<p>chats</p>            		
            	</div></a>
            </div>
           
           
           <div class="col-md-2" id="reclamo">
            	<a href="#" class="data"><div class="card" data-color="crawford">
            		<center><img width="50" src="https://image.flaticon.com/icons/svg/138/138935.svg"></center>
            		<p>devices</p>            		
            	</div></a>
            </div>
           
           

        </div>
    </div>
</div>

<style type="text/css">
	.data{
		color: gray;
	}
	.data:hover{
		color: black;
	}
</style>

<script type="text/javascript">

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#database').addClass('active');
});

</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>