<div class="sidebar" data-color="crawford" data-image="http://digit4lcore.com/Crawford/Crawford/public/digit4l_sass/imagen-menu-crawford.jpg" >

    <!--
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
    -->
    <div class="sidebar-wrapper">
	    <div class="logo">
	        <a href="http://www.creative-tim.com" class="simple-text">
	            <img style="width: 100%;left: -10px;position: relative;" src="https://www.crawfordaffinitycolombia.com/images/logo.png">
	        </a>
	    </div>

	    <ul class="nav" >
	        <li class="active" id="estadistica">
	            <a href="https://backendv2.crawfordaffinitycolombia.com/crawford/index" class="dropdown-toggle" data-color="crawford" > <p>Estadistícas</p></a>
	        </li>
	        <li id="reclamos">
				<a href="/crawford/reclamos" class="dropdown-toggle" data-color="crawford" ><p>Reclamos </p></a>
			</li>
			<li id="clientes">
				<a class="dropdown-toggle"  href="/crawford/clientes" data-color="crawford"><p>Clientes </p></a>
			</li>
			<li id="polizas">
				<a class="dropdown-toggle"  href="#navDatabase" data-color="crawford"><p>Pólizas </p></a>
			</li>
			<li id="chat">
				<a class="dropdown-toggle"  href="/crawford/chat" data-color="crawford"><p>Chat </p></a>
			</li>
			<li id="ajustes">
				<a class="dropdown-toggle"  href="/crawford/ajustes" data-color="crawford"><p>Ajustes </p></a>
			</li>
			<li id="database">
				<a class="dropdown-toggle"  href="https://backendv2.crawfordaffinitycolombia.com/database" data-color="crawford"><p>DataBase </p></a>
			</li>
			
	    </ul>
	</div>
</div>

<script type="text/javascript">
var menu = new Menu();

menu.init();

function Menu(){
	this.init = function(){
		$('.nav li').hide();
		var mainMenu = localStorage.getItem('rol').split(",");
		for (var i = mainMenu.length - 1; i >= 0; i--) {
			if(mainMenu[i]=='AJUSTES'){
				$('#ajustes').show();
			}
			if(mainMenu[i]=='POLIZAS'){
				$('#polizas').show();
				$('#chat').show();
			}
			if(mainMenu[i]=='CLIENTES'){
				$('#clientes').show();
			}
			if(mainMenu[i]=='RECLAMOS'){
				$('#reclamos').show();
			}
			if(mainMenu[i]=='ESTADISTICAS'){
				$('#estadistica').show();
			}
			if(mainMenu[i]=='DATABASE'){
				$('#database').show();
			}
		}
	};
}
</script>
