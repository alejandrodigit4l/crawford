<?php $__env->startSection('content'); ?>

	<h4>tipo_seguro </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="tipo_seguros/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <select  name="searchestado" id="searchestado"  class="form-control">
                        <option value="">BUSCAR POR MARCA</option>
                        <option value="">TODOS</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario tipo_seguro</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre Tipo Seguro" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion Tipo" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="tipo_seguro.save();">Guardar</div>
                        <div class="btn btn-success" onclick="tipo_seguro.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th>marca</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tipo_seguros; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipo_seguro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($tipo_seguro['id']); ?></td>
                                    <td><?php echo e($tipo_seguro['nombre']); ?></td>
                                    <td><?php echo e($tipo_seguro['descripcion']); ?></td>
                                    <td><?php echo e($tipo_seguro['marca']['nombre']); ?></td>
                                    <td><?php echo e($tipo_seguro['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="tipo_seguro.update(<?php echo e($tipo_seguro['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="tipo_seguro.delete(<?php echo e($tipo_seguro['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var tipo_seguro = new Tipo_seguro();

$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        tipo_seguro.search($('#searchnombre').val());
    }
});

$('#searchestado').on('change', function() {
    tipo_seguro.search($('#searchestado').val());
});

function Tipo_seguro(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var estado = this.validate('descripcion');
        var marca   = this.validate('marca');        

        var parametrer = {
            'nombre' : nombre,
            'descripcion' : estado,
            'marca'   : marca
        };

        $.ajax({
            url: 'tipo_seguros/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_seguros';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');   
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca
        };

        $.ajax({
            url: 'tipo_seguros/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_seguros';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'tipo_seguros/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tipo_seguros';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'tipo_seguros/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#marca').val(response.body[0].marca);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'tipo_seguros/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>