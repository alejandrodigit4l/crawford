<?php $__env->startSection('content'); ?>

	<h4>Devices </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Devices</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="deviceid" class="form-control" id="deviceid" style="color: black;" placeholder="deviceid" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="registrationid" class="form-control" id="registrationid" style="color: black;" placeholder="registrationid" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="clientid" class="form-control" id="clientid" style="color: black;" placeholder="clientid" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="platform" class="form-control" id="platform" style="color: black;" placeholder="platform" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="cliente" class="form-control" id="cliente" style="color: black;" placeholder="cliente" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripcion" onkeyup="javascript:this.value=this.value.toUpperCase();"><br>
                        <div class="btn btn-success" onclick="devices.save();">Guardar</div>
                        <div class="btn btn-success" onclick="devices.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>deviceid</th>
                                    <th>registrationid</th>
                                    <th>clientid</th>
                                    <th>platform</th>
                                    <th>cliente</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homologo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($homologo['id']); ?></td>
                                    <td><?php echo e($homologo['deviceid']); ?></td>
									<td><?php echo e($homologo['registrationid']); ?></td>
									<td><?php echo e($homologo['clientid']); ?></td>
									<td><?php echo e($homologo['platform']); ?></td>
									<td><?php echo e($homologo['cliente']); ?></td>
                                    <td><a href="javascript:;" onclick="devices.update(<?php echo e($homologo['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="devices.delete(<?php echo e($homologo['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var devices = new Devices();


function Devices(){

    this.save = function(){
        var deviceid = this.validate('deviceid');
		var registrationid = this.validate('registrationid');
		var clientid = this.validate('clientid');
		var platform = this.validate('platform');
		var cliente = this.validate('cliente');  

        var parametrer = {
            'deviceid' : deviceid,
			'registrationid' : registrationid,
			'clientid' : clientid,
			'platform' : platform,
			'cliente' : cliente
        };

        $.ajax({
            url: 'devices/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'devices';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var deviceid = this.validate('deviceid');
		var registrationid = this.validate('registrationid');
		var clientid = this.validate('clientid');
		var platform = this.validate('platform');
		var cliente = this.validate('cliente'); 
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'deviceid' : deviceid,
			'registrationid' : registrationid,
			'clientid' : clientid,
			'platform' : platform,
			'cliente' : cliente
        };

        $.ajax({
            url: 'devices/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'devices';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'devices/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'devices';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'devices/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#deviceid').val(response.body[0].deviceid);
					$('#registrationid').val(response.body[0].registrationid);
					$('#clientid').val(response.body[0].clientid);
					$('#platform').val(response.body[0].platform);
					$('#cliente').val(response.body[0].cliente);                    
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'devices/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>