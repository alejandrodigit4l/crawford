<?php $__env->startSection('content'); ?>

	<h4>Polizas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Polizas</p>      
                    <form data-color="crawford">
                    <input type="hidden" name="id" id="id" value="">
					<input type="date" name="date_start" class="form-control" id="date_start" style="color: black;" placeholder="date_start" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="date" name="date_end" class="form-control" id="date_end" style="color: black;" placeholder="date_end" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="number" name="days_on_period" class="form-control" id="days_on_period" style="color: black;" placeholder="days_on_period" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="date" name="date_certificate_start" class="form-control" id="date_certificate_start" style="color: black;" placeholder="date_certificate_start" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="date" name="date_certificate_end" class="form-control" id="date_certificate_end" style="color: black;" placeholder="date_certificate_end" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="name_user" class="form-control" id="name_user" style="color: black;" placeholder="name_user" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="city" class="form-control" id="city" style="color: black;" placeholder="city" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="name_taker" class="form-control" id="name_taker" style="color: black;" placeholder="name_taker" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="city_taker" class="form-control" id="city_taker" style="color: black;" placeholder="city_taker" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="id_taker" class="form-control" id="id_taker" style="color: black;" placeholder="id_taker" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="price" class="form-control" id="price" style="color: black;" placeholder="price" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="takes" class="form-control" id="takes" style="color: black;" placeholder="takes" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="price_and_takes" class="form-control" id="price_and_takes" style="color: black;" placeholder="price_and_takes" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="email" name="email" class="form-control" id="email" style="color: black;" placeholder="email" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="name_product" class="form-control" id="name_product" style="color: black;" placeholder="name_product" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="url" class="form-control" id="url" style="color: black;" placeholder="url" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="serie" class="form-control" id="serie" style="color: black;" placeholder="serie" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="consecutivo" class="form-control" id="consecutivo" style="color: black;" placeholder="consecutivo" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="cobertura" class="form-control" id="cobertura" style="color: black;" placeholder="cobertura" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="deducible" class="form-control" id="deducible" style="color: black;" placeholder="deducible" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="producto" class="form-control" id="producto" style="color: black;" placeholder="producto" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
					<input type="text" name="cliente" class="form-control" id="cliente" style="color: black;" placeholder="cliente" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                        
                    <br>
                        <div class="btn btn-success" onclick="polizas.save();">Guardar</div>
                        <div class="btn btn-success" onclick="polizas.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>date_start</th>
						            <th>date_end</th>
						            <th>days_on_period</th>
						            <th>date_certificate_start</th>
						            <th>date_certificate_end</th>
						            <th>name_user</th>
						            <th>city</th>
						            <th>name_taker</th>
						            <th>city_taker</th>
						            <th>id_taker</th>
						            <th>price</th>
						            <th>takes</th>
						            <th>price_and_takes</th>
						            <th>email</th>
						            <th>name_product</th>
						            <th>url</th>
						            <th>serie</th>
						            <th>consecutivo</th>
						            <th>cobertura</th>
						            <th>deducible</th>
						            <th>producto</th>
						            <th>cliente</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $polizas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data['id']); ?></td>
                                    <td><?php echo e($data['date_start']); ?></td>
						            <td><?php echo e($data['date_end']); ?></td>
						            <td><?php echo e($data['days_on_period']); ?></td>
						            <td><?php echo e($data['date_certificate_start']); ?></td>
						            <td><?php echo e($data['date_certificate_end']); ?></td>
						            <td><?php echo e($data['name_user']); ?></td>
						            <td><?php echo e($data['city']); ?></td>
						            <td><?php echo e($data['name_taker']); ?></td>
						            <td><?php echo e($data['city_taker']); ?></td>
						            <td><?php echo e($data['id_taker']); ?></td>
						            <td><?php echo e($data['price']); ?></td>
						            <td><?php echo e($data['takes']); ?></td>
						            <td><?php echo e($data['price_and_takes']); ?></td>
						            <td><?php echo e($data['email']); ?></td>
						            <td><?php echo e($data['name_product']); ?></td>
						            <td><?php echo e($data['url']); ?></td>
						            <td><?php echo e($data['serie']); ?></td>
						            <td><?php echo e($data['consecutivo']); ?></td>
						            <td><?php echo e($data['cobertura']); ?></td>
						            <td><?php echo e($data['deducible']); ?></td>
						            <td><?php echo e($data['producto']); ?></td>
						            <td><?php echo e($data['cliente']); ?></td>
                                    <td><a href="javascript:;" onclick="polizas.update(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="polizas.delete(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var polizas = new Polizas();

function Polizas(){

    this.save = function(){
    	var date_start = this.validate('date_start');
        var date_end = this.validate('date_end');
        var days_on_period = this.validate('days_on_period');
        var date_certificate_start = this.validate('date_certificate_start');
        var date_certificate_end = this.validate('date_certificate_end');
        var name_user = this.validate('name_user');
        var city = this.validate('city');
        var name_taker = this.validate('name_taker');
        var city_taker = this.validate('city_taker');
        var id_taker = this.validate('id_taker');
        var price = this.validate('price');
        var takes = this.validate('takes');
        var price_and_takes = this.validate('price_and_takes');
        var email = this.validate('email');
        var name_product = this.validate('name_product');
        var url = this.validate('url');
        var serie = this.validate('serie');
        var consecutivo = this.validate('consecutivo');
        var cobertura = this.validate('cobertura');
        var deducible = this.validate('deducible');
        var producto = this.validate('producto');
        var cliente = this.validate('cliente');
    

        var parametrer = {
        	'date_start' : date_start,
            'date_end' : date_end,
            'days_on_period' : days_on_period,
            'date_certificate_start' : date_certificate_start,
            'date_certificate_end' : date_certificate_end,
            'name_user' : name_user,
            'city' : city,
            'name_taker' : name_taker,
            'city_taker' : city_taker,
            'id_taker' : id_taker,
            'price' : price,
            'takes' : takes,
            'price_and_takes' : price_and_takes,
            'email' : email,
            'name_product' : name_product,
            'url' : url,
            'serie' : serie,
            'consecutivo' : consecutivo,
            'cobertura' : cobertura,
            'deducible' : deducible,
            'producto' : producto,
            'cliente' : cliente
        };

        $.ajax({
            url: '/polizas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var date_start = this.validate('date_start');
        var date_end = this.validate('date_end');
        var days_on_period = this.validate('days_on_period');
        var date_certificate_start = this.validate('date_certificate_start');
        var date_certificate_end = this.validate('date_certificate_end');
        var name_user = this.validate('name_user');
        var city = this.validate('city');
        var name_taker = this.validate('name_taker');
        var city_taker = this.validate('city_taker');
        var id_taker = this.validate('id_taker');
        var price = this.validate('price');
        var takes = this.validate('takes');
        var price_and_takes = this.validate('price_and_takes');
        var email = this.validate('email');
        var name_product = this.validate('name_product');
        var url = this.validate('url');
        var serie = this.validate('serie');
        var consecutivo = this.validate('consecutivo');
        var cobertura = this.validate('cobertura');
        var deducible = this.validate('deducible');
        var producto = this.validate('producto');
        var cliente = this.validate('cliente');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'date_start' : date_start,
            'date_end' : date_end,
            'days_on_period' : days_on_period,
            'date_certificate_start' : date_certificate_start,
            'date_certificate_end' : date_certificate_end,
            'name_user' : name_user,
            'city' : city,
            'name_taker' : name_taker,
            'city_taker' : city_taker,
            'id_taker' : id_taker,
            'price' : price,
            'takes' : takes,
            'price_and_takes' : price_and_takes,
            'email' : email,
            'name_product' : name_product,
            'url' : url,
            'serie' : serie,
            'consecutivo' : consecutivo,
            'cobertura' : cobertura,
            'deducible' : deducible,
            'producto' : producto,
            'cliente' : cliente
        };

        $.ajax({
            url: '/polizas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: '/polizas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/polizas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: '/polizas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id
                    $('#date_start').val(response.body[0].date_start);
		            $('#date_end').val(response.body[0].date_end);
		            $('#days_on_period').val(response.body[0].days_on_period);
		            $('#date_certificate_start').val(response.body[0].date_certificate_start);
		            $('#date_certificate_end').val(response.body[0].date_certificate_end);
		            $('#name_user').val(response.body[0].name_user);
		            $('#city').val(response.body[0].city);
		            $('#name_taker').val(response.body[0].name_taker);
		            $('#city_taker').val(response.body[0].city_taker);
		            $('#id_taker').val(response.body[0].id_taker);
		            $('#price').val(response.body[0].price);
		            $('#takes').val(response.body[0].takes);
		            $('#price_and_takes').val(response.body[0].price_and_takes);
		            $('#email').val(response.body[0].email);
		            $('#name_product').val(response.body[0].name_product);
		            $('#url').val(response.body[0].url);
		            $('#serie').val(response.body[0].serie);
		            $('#consecutivo').val(response.body[0].consecutivo);
		            $('#cobertura').val(response.body[0].cobertura);
		            $('#deducible').val(response.body[0].deducible);
		            $('#producto').val(response.body[0].producto);
		            $('#cliente').val(response.body[0].cliente);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = '/polizas/'+DATA;
    };

}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>