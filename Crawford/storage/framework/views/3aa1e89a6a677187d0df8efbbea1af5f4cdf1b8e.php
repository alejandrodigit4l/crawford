<?php $__env->startSection('content'); ?>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-3">
            	<div class="card" data-color="crawford" style="overflow-y: scroll;height: 750px;">
                    <h4>Contactos</h4>
                    <?php $__currentLoopData = $chats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="/chats/backend/<?php echo e($usuario['sender']['id']); ?>"><label class="btn-success form-control "><?php echo e($usuario['sender']['email']); ?><strong > <?php echo e($usuario['cnt']); ?></strong></label></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            	</div>
            </div>

            <div class="col-md-9">
                <div class="card" id="respuestascroll" data-color="crawford" style="overflow-y: scroll;height: 650px;">
                    <h4>Conversación</h4>
                    <div class="card" id="response">
                        ...
                    </div>
                </div>
                <input type="hidden" name="receptortext" id="receptortext" value="<?php echo e($desde['id']); ?>">
                <input id="message" class="form-control" style="height: 90px;width: 100%; position: relative;top: -20px;" placeholder="Escribe el mensaje">
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#chat').addClass('active');

});


var chats = new Chats();

chats.init();

$('#message').keypress(function(e) {
    if(e.which == 13) {
        chats.send();
    }
});


function Chats(){

    this.init = function(){       
        chats.historial();
        setInterval( "chats.historial()" , 9000);
    }

    this.send = function(){
        
        var message = this.validate('message');
        var state = 'ENVIADO';
        var sender = '1'; //administrador
        var receptor = this.validate('receptortext'); //id cliente

        var parametrer = {
            'message': message,
            'state': state,
            'sender': sender,
            'receptor': receptor
        };

        $.ajax({
            url: '/chats/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    //location.href = 'chats';
                    $('#message').val('');
                    chats.historial();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    /* HISTOIRAL DE CONVERSACIONES */
    this.historial = function(){
        $('#response').html('');
        $.ajax({
            url: '/chats/historial/'+$('#receptortext').val()+'/1', //This is the marca
            type: "GET",
            success: function(response){
                console.log('historial:');
                console.log(response);
                for (var i = response.body.length - 1; i >= 0; i--) {
                    response.body[i];
                    if(response.body[i].id=='1'){//sender ya que es administrador
                        $('#response').append('<div class="card" style="background: #f6f6f6;padding: 10px;text-align: left;color:#4a4a4a">'+response.body[i].message+'<br><small style="font-size:9px">'+response.body[i].created_at+'</small></div>');
                    }
                    else{
                        $('#response').append('<div class="card" style="background: #d1e1eb;padding: 10px;text-align: right;color: #2b2b2b;">'+response.body[i].message+'<br><small style="font-size:9px">'+response.body[i].created_at+'</small></div>');
                    }                    
                }
            }
        }); 
        var altura = $('#respuestascroll').height(); 
        $("#respuestascroll").animate({scrollTop:altura+"px"});
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>