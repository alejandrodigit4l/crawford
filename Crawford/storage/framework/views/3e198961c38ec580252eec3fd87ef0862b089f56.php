<?php $__env->startSection('content'); ?>

	<h4>Tiendas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="tiendas/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Tiendas</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <select name="aseguradora" id="aseguradora" class="form-control" >
                            <option value="">Aseguradora</option>
                        </select>
                        <select name="campana" id="campana" class="form-control" >
                            <option value="">Campaña</option>
                        </select>
                        <select name="estado" id="estado" class="form-control"><option value="">Estado tienda</option><option value="ACTIVO">ACTIVO</option><option value="INACTIVO">INACTIVO</option></select>
                        <input type="text" name="icono" id="icono" class="form-control" placeholder="URL Icono">
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="tiendas.save();">Guardar</div>
                        <div class="btn btn-success" onclick="tiendas.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>
                                    <th>campana</th>
                                    <th>icono</th> 
                                    <th>estado</th>                                    
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tiendas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tienda): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($tienda['id']); ?></td>
                                    <td><?php echo e($tienda['nombre']); ?></td>
                                    <td><?php echo e($tienda['marca']['nombre']); ?></td>
                                    <td><?php echo e($tienda['tipo_seguro']['nombre']); ?></td>
                                    <td><?php echo e($tienda['aseguradora']['nombre']); ?></td>
                                    <td><?php echo e($tienda['campana']['nombre']); ?></td>
                                    <td><img width="50" src="<?php echo e($tienda['icono']); ?>"></td>
                                    <td><?php echo e($tienda['estado']); ?></td>
                                    <td><?php echo e($tienda['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="tiendas.update(<?php echo e($tienda['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="tiendas.delete(<?php echo e($tienda['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var tiendas = new Tiendas();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        tiendas.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    tiendas.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    tiendas.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});
$('#aseguradora').on('change', function() {
    tiendas.campana($('#aseguradora').val(),'#campana','Campañas');
});


$('#searchestado').on('change', function() {
    tiendas.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

function Tiendas(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var icono = this.validate('icono');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var estado = this.validate('estado');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      

        var parametrer = {
            'nombre' : nombre,
            'icono' : icono,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'estado' : estado,
            'campana' : campana,
            'aseguradora' : aseguradora
        };

        $.ajax({
            url: 'tiendas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tiendas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var nombre = this.validate('nombre');
        var icono = this.validate('icono');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var estado = this.validate('estado');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      

        var parametrer = {
            'id'    : id,
            'nombre' : nombre,
            'icono' : icono,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'estado' : estado,
            'campana' : campana,
            'aseguradora' : aseguradora
        };

        $.ajax({
            url: 'tiendas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tiendas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'tiendas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'tiendas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'tiendas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#icono').val(response.body[0].icono);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                    $('#estado').val(response.body[0].estado);
                    $('#campana').val(response.body[0].campana);
                    $('#aseguradora').val(response.body[0].aseguradora);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'tiendas/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA CAMPANAS EN ESPECIAL */
    this.campana = function(CAMPANA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'campanas/search/'+CAMPANA, //This is the CAMPANAS
            type: "GET",
            success: function(response){
                console.log('TIENDAS CAMPANA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>