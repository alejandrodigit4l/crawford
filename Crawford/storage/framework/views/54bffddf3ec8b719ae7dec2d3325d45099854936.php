<?php $__env->startSection('content'); ?>

	<h4>aseguradoras </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="aseguradoras/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <select  name="searchestado" id="searchestado"  class="form-control">
                        <option value="">BUSCAR POR MARCA</option>
                        <option value="">TODOS</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <select name="searchtipo_seguro" id="searchtipo_seguro" class="form-control" >
                        <option value="">Buscar Tipo seguro</option>
                    </select>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario aseguradoras</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="aseguradora.save();">Guardar</div>
                        <div class="btn btn-success" onclick="aseguradora.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $aseguradoras; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aseguradora): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($aseguradora['id']); ?></td>
                                    <td><?php echo e($aseguradora['nombre']); ?></td>
                                    <td><?php echo e($aseguradora['descripcion']); ?></td>
                                    <td><?php echo e($aseguradora['marca']['nombre']); ?></td>
                                    <td><?php echo e($aseguradora['tipo_seguro']['nombre']); ?></td>
                                    <td><?php echo e($aseguradora['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="aseguradora.update(<?php echo e($aseguradora['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="aseguradora.delete(<?php echo e($aseguradora['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var aseguradora = new Aseguradoras();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        aseguradora.search($('#searchnombre').val());
    }
});
$('#marca').on('change', function() {
    aseguradora.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#searchestado').on('change', function() {
    aseguradora.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});
$('#searchtipo_seguro').on('change', function() {
    aseguradora.search($('#searchtipo_seguro').val());
});

function Aseguradoras(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');      

        var parametrer = {
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro
        };

        $.ajax({
            url: 'aseguradoras/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'aseguradoras';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');   
        var id     = this.validate('id');   
        var tipo_seguro = this.validate('tipo_seguro');         

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro
        };

        $.ajax({
            url: 'aseguradoras/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'aseguradoras';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'aseguradoras/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'aseguradoras';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'aseguradoras/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'aseguradoras/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    }
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>