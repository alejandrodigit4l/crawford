<!DOCTYPE html>
<html lang="en">
<head>
  <title>crawford-Choise</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {background-color: #1e1e1e;height: 550px}
    	
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #1e1e1e;
      height: 100%;
    }
        
    /* On small screens, set height to 'auto' for the grid */
    @media  screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style="background: #1e1e1e;color: white;">

<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="https://crawfordaffinitycolombia.com/images/logo.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Bienvenido</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid" >
  <div class="row content">
    <div class="col-sm-3 sidenav hidden-xs">
      <h2><img src="https://crawfordaffinitycolombia.com/images/logo.png"></h2>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="#section1">Bienvenido</a></li>
      </ul><br>
    </div>
    <br>
    
    <div class="col-sm-9">
      <div class="well" style="background: #1e1e1e;border: 0px;color: white;">
        <h4>Crawford posee grandes tecnologias en aplicaciones. </h4>
        <p>Estamos en la nueva era. Algunos de nuestros clientes son: </p>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <a href="crawford"><div class="well" style="background: #1e1e1e;color: white;">
            <img src="https://crawfordaffinitycolombia.com/images/logo.png" style="padding: 10px;width: 100%;"><br><br>
            <button class="form-control btn btn-success">Ingresar al Backend Crawofrd</button> 
          </div></a>
        </div>
        <div class="col-sm-3">
          <div class="well" style="background: #1e1e1e;color: white;"> 
            <img src="http://www.segurosmundial.com.co/media/logoMUNDIAL.png" style="padding: 10px;width: 100%;"><br><br>
            <button class="form-control btn btn-success">Ingresar al Backend SM</button> 
          </div>
        </div>
        <div class="col-sm-3">
          <div class="well" style="background: #1e1e1e;color: white;">
            <img src="http://tusimagenesde.com/wp-content/uploads/2017/07/samsung-logo-6.png" style="padding: 10px;width: 100%;"><br><br>
            <button class="form-control btn btn-success">Ingresar al Backend Samsung</button> 
          </div>
        </div>
        <div class="col-sm-3">
          <a href="falabella"><div class="well" style="background: #1e1e1e;color: white;">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Banco_Falabella_Logo.svg/3000px-Banco_Falabella_Logo.svg.png" style="padding: 10px;width: 100%;"><br><br>
            <button class="form-control btn btn-success">Ingresar al Backend Falabella</button> 
          </div></a>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="well" style="background: #1e1e1e;color: white;">
            <p>API Samsung Documentación</p> 
            <img src="https://image.flaticon.com/icons/svg/139/139094.svg" style="padding: 10px;width: 20%;">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well" style="background: #1e1e1e;color: white;">
            <p>API Falabella Documentación</p> 
            <img src="https://image.flaticon.com/icons/svg/139/139094.svg" style="padding: 10px;width: 20%;">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
