<?php $__env->startSection('content'); ?>

	<h4>Documents id[<?php echo e($reclamo); ?>]</h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Documents</p>      
                    <form action="/documentsmodel/uploadSubmit" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="estado" id="estado" class="form-control" value="Aceptado">
						<input type="hidden" name="validacion" id="validacion" class="form-control" placeholder="validacion" value="SIN VALIDACION">
						<input type="hidden" name="respuestaback" id="respuestaback" class="form-control" placeholder="respuestaback" value="SIN RESPUESTA">
						<input type="file" name="file" id="file" class="inputfile" />
                        <label class="btn btn-succes form-control" for="file">Sube un archivo</label>
                        <input type="hidden" name="FileType" value="camara">
                        <input type="hidden" name="redirect" value="/crawford/idreclamo/<?php echo e($reclamo); ?>">
						<input type="hidden" name="reclamo" id="reclamo" class="form-control" value="<?php echo e($reclamo); ?>">
						<input type="hidden" name="listcheking" id="listcheking" class="form-control" value="<?php echo e($listcheking); ?>">
                        <input type="hidden" name="cliente" id="cliente" class="form-control" value="">
                        <input type="submit" value="Subir" class="btn btn-success form-control" />
                    </form>      		
            	</div>
            </div>
           
           
        </div>
    </div>
</div>

<style type="text/css">
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    display: inline-block;
    color: #207ce5;
}

.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #207ce5;
    color: white;
}
.inputfile + label {
    cursor: pointer; /* "hand" cursor */
}
.inputfile:focus + label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>
<script type="text/javascript">
var documents = new Documents();

documents.init();

function Documents(){

    this.init = function(){
        $('#cliente').val(JSON.parse(localStorage.getItem('cliente')).id);
    };

    this.save = function(){
    	var estado = this.validate('estado');
		var validacion = this.validate('validacion');
		var respuestaback = this.validate('respuestaback');
		var url = this.validate('url');
		var reclamo = this.validate('reclamo');
		var listcheking = this.validate('listcheking');

        var parametrer = {
        	'estado' : estado,
			'validacion' : validacion,
			'respuestaback' : respuestaback,
			'url' : url,
			'reclamo' : reclamo,
			'listcheking' : listcheking
        };

        $.ajax({
            url: 'documentsmodel/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var estado = this.validate('estado');
		var validacion = this.validate('validacion');
		var respuestaback = this.validate('respuestaback');
		var url = this.validate('url');
		var reclamo = this.validate('reclamo');
		var listcheking = this.validate('listcheking');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'estado' : estado,
			'validacion' : validacion,
			'respuestaback' : respuestaback,
			'url' : url,
			'reclamo' : reclamo,
			'listcheking' : listcheking
        };

        $.ajax({
            url: 'documentsmodel/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'documentsmodel/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'documentsmodel/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#estado').val(response.body[0].estado);
					$('#validacion').val(response.body[0].validacion);
					$('#respuestaback').val(response.body[0].respuestaback);
					$('#url').val(response.body[0].url);
					$('#reclamo').val(response.body[0].reclamo);
					$('#listcheking').val(response.body[0].listcheking);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'documentsmodel/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>