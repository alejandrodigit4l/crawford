<?php $__env->startSection('content'); ?>

	<h4>notificaciones </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario notificaciones</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control">
                            <option value="">Seleccione marca</option>
                        	<?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        	<option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
						<input type="text" name="notyid" id="notyid" class="form-control" placeholder="notyid" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="fecha" id="fecha" class="form-control" placeholder="fecha" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="texto" id="texto" class="form-control" placeholder="texto" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="estado" id="estado" class="form-control" placeholder="estado" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="cliente" id="cliente" class="form-control" placeholder="cliente" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="reclamo" id="reclamo" class="form-control" placeholder="reclamo" onkeyup="javascript:this.value=this.value.toUpperCase();">						
						<br>
                        <div class="btn btn-success" onclick="notificaciones.save();">Guardar</div>
                        <div class="btn btn-success" onclick="notificaciones.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>notyid</th>
									<th>fecha</th>
									<th>texto</th>
									<th>estado</th>
									<th>cliente</th>
									<th>reclamo</th>
									<th>marca</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $notificaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data['id']); ?></td>
                                    <td><?php echo e($data['notyid']); ?></td>
									<td><?php echo e($data['fecha']); ?></td>
									<td><?php echo e($data['texto']); ?></td>
									<td><?php echo e($data['estado']); ?></td>
									<td><?php echo e($data['cliente']); ?></td>
									<td><?php echo e($data['reclamo']); ?></td>
									<td><?php echo e($data['marca']); ?></td>
                                    <td><a href="javascript:;" onclick="notificaciones.update(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="notificaciones.delete(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var notificaciones = new Notificaciones();
                                    

function Notificaciones(){

    this.save = function(){
        var notyid = this.validate('notyid');
		var fecha = this.validate('fecha');
		var texto = this.validate('texto');
		var estado = this.validate('estado');
		var cliente = this.validate('cliente');
		var reclamo = this.validate('reclamo');
		var marca = this.validate('marca'); 

        var parametrer = {
            'notyid' : notyid,
			'fecha' : fecha,
			'texto' : texto,
			'estado' : estado,
			'cliente' : cliente,
			'reclamo' : reclamo,
			'marca' : marca
        };

        $.ajax({
            url: 'notificaciones/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'notificaciones';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var notyid = this.validate('notyid');
		var fecha = this.validate('fecha');
		var texto = this.validate('texto');
		var estado = this.validate('estado');
		var cliente = this.validate('cliente');
		var reclamo = this.validate('reclamo');
		var marca = this.validate('marca'); 
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'notyid' : notyid,
			'fecha' : fecha,
			'texto' : texto,
			'estado' : estado,
			'cliente' : cliente,
			'reclamo' : reclamo,
			'marca' : marca
        };

        $.ajax({
            url: 'notificaciones/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'notificaciones';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'notificaciones/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'notificaciones';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'notificaciones/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#notyid').val(response.body[0].notyid);
					$('#fecha').val(response.body[0].fecha);
					$('#texto').val(response.body[0].texto);
					$('#estado').val(response.body[0].estado);
					$('#cliente').val(response.body[0].cliente);
					$('#reclamo').val(response.body[0].reclamo);
					$('#marca').val(response.body[0].marca);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'notificaciones/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>