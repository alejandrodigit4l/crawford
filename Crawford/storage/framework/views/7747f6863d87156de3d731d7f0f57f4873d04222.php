<?php $__env->startSection('content'); ?>

<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<h4>Reclamo #  <?php echo e($reclamos[0]['reclamoid']); ?></h4><br>
            		<p>Seguros Mundial </p>
            	</div>
            </div>

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <h4>Cliente</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nombre</th>
                                <th>cedula</th>
                                <th>fecha cc</th>
                                <th>fecha nacimiento</th>
                                <th>email</th>
                                <th>celular</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="/crawford/cliente/<?php echo e($reclamos[0]['cliente']['id']); ?>"><?php echo e($reclamos[0]['cliente']['id']); ?></a></td>
                                <td><?php echo e($reclamos[0]['cliente']['nombre'].' '.$reclamos[0]['cliente']['lastname']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['cedula']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['fechaCC']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['fechanacimiento']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['email']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['celular']); ?></td>
                                <td><?php echo e($reclamos[0]['cliente']['created_at']); ?></td>
                            </tr>                                                
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-9">
            	<div class="card" data-color="crawford">
            		<h4>Datos del Reclamo</h4>
            		<div class="row">

            			<div class="col-md-3">
            				<label for="nombreasegurado">Nombre Asegurado</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['nombreasegurado']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad Recidencia</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['ciudadrecidencia']['nombre']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Email</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['email']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Celular</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['celular']); ?>" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Cedula</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['cedula']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha nacimiento</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['cliente']['fechanacimiento']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono fijo</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['telefonofijo']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Telefono laboral</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['telefonolaboral']); ?>" disabled>
            			</div>

            			<div class="col-md-3">
            				<label for="nombreasegurado">Direccion Laboral</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['direccion']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Tipo Producto</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($arrayTipo_polizas[0]['nombre']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Producto</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['producto']['titulo']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Tienda</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($arrayTiendas[0]['nombre']); ?>" disabled>
            			</div>
            		</div>
            	</div>
            </div>

            <div class="col-md-3">
                <div class="card" data-color="crawford">
                    <h4>Exportar Reclamo</h4>
                    <a href="/crawford/export/zip/<?php echo e($reclamos[0]['id']); ?>" class="btn btn-primary" style="width: 100%">Exportar zip </a> <br><br>
                    <a href="/crawford/export/excel/<?php echo e($reclamos[0]['id']); ?>" class="btn btn-success" style="width: 100%">Exportar Excel </a>
                </div>
            </div>

            <div class="col-md-9">
            	<div class="card" data-color="crawford">
            		<h4>Datos del Siniestro</h4>
            		<div class="row">
            			<div class="col-md-3">
            				<label for="nombreasegurado">Estado</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['estados_poliza']['nombre']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Fecha</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['fechasiniestro']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Hora</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['horasiniestro']); ?>" disabled>
            			</div>
            			<div class="col-md-3">
            				<label for="nombreasegurado">Ciudad</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['ciudadsiniestro']['nombre']); ?>" disabled>
            			</div>

            			<div class="col-md-12">
            				<label for="nombreasegurado">Descripcion</label>
            				<input type="text" class="form-control" name="" value="<?php echo e($reclamos[0]['descripcionsiniestro']); ?>" disabled>
            			</div>
            			<div class="col-md-12">
            				<label for="nombreasegurado">Observaciones</label>
            				<input type="text" class="form-control" name="newobservaciones" id="newobservaciones" value="<?php echo e($reclamos[0]['observaciones']); ?>" >
            			</div>
            		</div>
            	</div>
            </div>

            <div class="col-md-3">
                <div class="card" data-color="crawford">
                    <h4>Cambiar Estado/código interno</h4>
                    <label>Estado</label>
                    <select class="form-control" id="newestado">
                    	<option value="">Seleccione estado</option>
                    	<?php $__currentLoopData = $homologos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homologo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    	<option value="<?php echo e($homologo['id']); ?>"><?php echo e($homologo['nombre']); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <label>Codigo Interno</label>
                    <input type="text" class="form-control" name="codigointerno" id="codigointerno" value="<?php echo e($reclamos[0]['textobackend']); ?>">
                </div>
            </div>

            <div class="col-md-9">
                <div class="card" data-color="crawford" style="overflow-x: scroll;">
                    <h4>Documentos</h4>
                    <table class="table table " data-color="crawford">
                        <thead>
                            <tr>
                                <th><div style="width: 70px;text-align: center;">Titulo</div></th>
                                <th><div style="width: 120px;text-align: center;">Ayuda</div></th>
                                <th>Imagenes</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $listchekings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if( $data['reclamo']==0 ): ?>
                            <tr>
                                <td><div style="width: 70px;"><?php echo e($data['titulo']); ?></div></td>
                                <td><div style="width: 120px;"><?php echo e($data['ayuda']); ?></div></td>
                                <td>
                                    
                                    <table class="table " data-color="crawford">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                            <?php if( $document['listcheking'] == $data['id'] ): ?>
                                                
                                                <td><div style="width: 100px;"><a href="http://localhost:8000<?php echo e($document['url']); ?>"  target="_blank"><img style="width: 100%;" src="http://localhost:8000<?php echo e($document['url']); ?>" ></a></div></td>
                                                <td><div style="width: 180px;"><?php echo e($document['created_at']); ?></div></td>
                                                <td><a onclick="eliminar(`/documentsmodel/delete/<?php echo e($document['id']); ?>`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td>
                                                 
                                            <?php endif; ?>
                                            </tr> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                              
                                        </tbody>
                                    </table>

                                </td>
                                <td><a href="/documentsmodel/subirdocumento/<?php echo e($reclamos[0]['id']); ?>/<?php echo e($data['id']); ?>" class="btn btn-success ">Subir</a></td>
                            </tr> 
                            <?php endif; ?>

                            <?php if( $data['reclamo']== $reclamos[0]['id'] ): ?>
                            <tr>
                                <td><div style="width: 70px;"><?php echo e($data['titulo']); ?></div></td>
                                <td><div style="width: 120px;"><?php echo e($data['ayuda']); ?></div></td>
                                <td>
                                    
                                    <table class="table " data-color="crawford">
                                        <thead>
                                            <tr>
                                                <th><div style="width: 100px;">URL</div></th>
                                                <th><div style="width: 180px;">creado</div></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                            <?php if( $document['listcheking'] == $data['id'] ): ?>
                                                
                                                <td><div style="width: 100px;"><a href="http://localhost:8000<?php echo e($document['url']); ?>"  target="_blank"><img style="width: 100%;" src="http://localhost:8000<?php echo e($document['url']); ?>" ></a></div></td>
                                                <td><div style="width: 180px;"><?php echo e($document['created_at']); ?></div></td>
                                                <td><a onclick="eliminar(`/documentsmodel/delete/<?php echo e($document['id']); ?>`)" href="javascript:;" class="btn btn-danger ">Eliminar</a></td>
                                                 
                                            <?php endif; ?>
                                            </tr> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                              
                                        </tbody>
                                    </table>

                                </td>
                                <td><a href="/documentsmodel/subirdocumento/<?php echo e($reclamos[0]['id']); ?>/<?php echo e($data['id']); ?>" class="btn btn-success ">Subir</a></td>
                            </tr> 
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                              
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" data-color="crawford">
                        <input type="hidden" name="marca" id="marca" class="form-control" value="<?php echo e($reclamos[0]['producto']['marca']); ?>">
                        <input type="hidden" name="tipo_seguro" id="tipo_seguro" class="form-control" value="<?php echo e($reclamos[0]['producto']['tipo_seguro']); ?>">
                        <input type="hidden" name="aseguradora" id="aseguradora" class="form-control" value="<?php echo e($reclamos[0]['producto']['aseguradora']); ?>">
                        <input type="hidden" name="campana" id="campana" class="form-control" value="<?php echo e($reclamos[0]['producto']['campana']); ?>">
                        <input type="hidden" name="tienda" id="tienda" class="form-control" value="<?php echo e($reclamos[0]['producto']['campana']); ?>">
                        <input type="hidden" name="asegurado" id="asegurado" class="form-control" value="<?php echo e($reclamos[0]['producto']['asegurado']); ?>">
                        <input type="hidden" name="tipo_polizas" id="tipo_polizas" class="form-control" value="<?php echo e($reclamos[0]['producto']['tipo_poliza']); ?>">
                        <input type="hidden" name="img" id="img" class="form-control" placeholder="img" value="https://image.flaticon.com/icons/svg/148/148769.svg">
                        <label>Título</label>
                        <input type="text" name="titulo" id="titulo" class="form-control"  onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Descripción</label>
                        <input type="text" name="descripcion" class="form-control" id="descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Sub-descripción</label>
                        <input type="text" name="subdescripcion" class="form-control" id="subdescripcion"  value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Ayuda</label>
                        <input type="text" name="ayuda" class="form-control" id="ayuda" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <label>Tipo de formato</label>
                        <select name="tipo" id="tipo" class="form-control" placeholder="tipo">
                            <option value="">Seleccione el tipo de formato</option>
                            <option value="camara">Camara</option>
                            <option value="audio">Audio</option>
                        </select>
                        <input type="hidden" name="reclamo" id="reclamo" placeholder="reclamo SI existe de lo contrario es un 0" value="<?php echo e($reclamos[0]['id']); ?>"><br>
                    <button class="btn btn-success form-control" onclick="reclamo.listchekings();">Solicitar</button>
                </div>
            </div>

            <div class="col-md-9">
                <div class="card" data-color="crawford">
                    <h4>Notificaciones</h4>
                    <table class="table table-hover " data-color="crawford">
                        <thead>
                            <tr>
                                <th>fecha</th>
                                <th>mensaje</th>
                                <th>adjunto</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $notificaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notificacion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($notificacion['fecha']); ?></td>
                                <td><?php echo e($notificacion['texto']); ?></td>
                                <td><a href="https://backendv2.crawfordaffinitycolombia.com/<?php echo e($notificacion['notyid']); ?>"><?php echo e($notificacion['notyid']); ?></a></td>
                            </tr>  
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                              
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" data-color="crawford">
                    <form action="/notificaciones/sendBackend" method="POST" enctype="multipart/form-data">
                        <h4>Enviar notificación</h4>
                        <input type="hidden" name="id" id="id" value="<?php echo e($reclamos[0]['id']); ?>">
                        <label>Texto</label>
                        <input type="text" class="form-control" name="textonotificacion">
                        <label>Adjuntar</label>
                        <input type="file" name="file" id="file" class="inputfile" />
                        <label class="btn btn-succes form-control" for="file">Sube un archivo</label><br>
                        <input type="submit" value="Enviar" class="btn btn-success form-control" name="submit">
                    </form>
                </div>
            </div>

        </div>

        <div >
            <input type="hidden" name="id"                  id="id"                 value="<?php echo e($reclamos[0]['id']); ?>">
            <input type="hidden" name="reclamoid"           id="reclamoid"          value="<?php echo e($reclamos[0]['reclamoid']); ?>">
            <input type="hidden" name="cedula"              id="cedula"             value="<?php echo e($reclamos[0]['cedula']); ?>">
            <input type="hidden" name="nombreasegurado"     id="nombreasegurado"    value="<?php echo e($reclamos[0]['nombreasegurado']); ?>">
            <input type="hidden" name="apellidoasegurado"   id="apellidoasegurado"  value="<?php echo e($reclamos[0]['apellidoasegurado']); ?>">
            <input type="hidden" name="fechanacimientoasse" id="fechanacimientoasse" value="<?php echo e($reclamos[0]['fechanacimientoasse']); ?>">
            <input type="hidden" name="ciudadrecidencia"    id="ciudadrecidencia"   value="<?php echo e($reclamos[0]['ciudadrecidencia']['id']); ?>">
            <input type="hidden" name="telefonofijo"        id="telefonofijo"       value="<?php echo e($reclamos[0]['telefonofijo']); ?>">
            <input type="hidden" name="celular"             id="celular"            value="<?php echo e($reclamos[0]['celular']); ?>">
            <input type="hidden" name="direccion"           id="direccion"          value="<?php echo e($reclamos[0]['direccion']); ?>">
            <input type="hidden" name="email"               id="email"              value="<?php echo e($reclamos[0]['email']); ?>">
            <input type="hidden" name="telefonolaboral"     id="telefonolaboral"    value="<?php echo e($reclamos[0]['telefonolaboral']); ?>">
            <input type="hidden" name="jsonrespuestaform"   id="jsonrespuestaform"  value="<?php echo e($reclamos[0]['jsonrespuestaform']); ?>">
            <input type="hidden" name="fechasiniestro"      id="fechasiniestro"     value="<?php echo e($reclamos[0]['fechasiniestro']); ?>">
            <input type="hidden" name="horasiniestro"       id="horasiniestro"      value="<?php echo e($reclamos[0]['horasiniestro']); ?>">
            <input type="hidden" name="descripcionsiniestro" id="descripcionsiniestro" value="<?php echo e($reclamos[0]['descripcionsiniestro']); ?>">
            <input type="hidden" name="textobackend"        id="textobackend"       value="<?php echo e($reclamos[0]['textobackend']); ?>">
            <input type="hidden" name="observaciones"       id="observaciones"      value="<?php echo e($reclamos[0]['observaciones']); ?>">
            <input type="hidden" name="motivobaja"          id="motivobaja"         value="<?php echo e($reclamos[0]['motivobaja']); ?>">
            <input type="hidden" name="cliente"             id="cliente"            value="<?php echo e($reclamos[0]['cliente']['id']); ?>">
            <input type="hidden" name="estados_poliza"      id="estados_poliza"     value="<?php echo e($reclamos[0]['estados_poliza']['id']); ?>">
            <input type="hidden" name="producto"            id="producto"           value="<?php echo e($reclamos[0]['producto']['id']); ?>">
            <input type="hidden" name="ciudadsiniestro"     id="ciudadsiniestro"    value="<?php echo e($reclamos[0]['ciudadsiniestro']['id']); ?>">
        </div>

    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<style type="text/css">
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    display: inline-block;
    color: #207ce5;
}

.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #207ce5;
    color: white;
}
.inputfile + label {
    cursor: pointer; /* "hand" cursor */
}
.inputfile:focus + label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>

<script type="text/javascript">
var reclamo = new Reclamo();


function eliminar(urls){
    var r = confirm("Desea eliminar ?");
    if (r == true) {
        $.ajax({
            url: urls, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }   
}


$('#newobservaciones').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#observaciones').val($('#newobservaciones').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});

$('#codigointerno').keypress(function(e) {
    if(e.which == 13) {
        var r = confirm("Desea actualizar la información?");
        if (r == true) {
            //actualizar informacion
            $('#textobackend').val($('#codigointerno').val());
            reclamo.updload();
        } else {
            console.log('Sera para un proximo cambio');
        }
    }
});


$('#newestado').on('change', function() {
    var r = confirm("Desea actualizar la información?");
    if (r == true) {
        //actualizar informacion
        $('#estados_poliza').val($('#newestado').val());
        reclamo.updload();
    } else {
        console.log('Sera para un proximo cambio');
    }
});


function Reclamo(){
    this.updload = function(){
        var reclamoid = this.validate('reclamoid');
        var cedula = this.validate('cedula');
        var nombreasegurado = this.validate('nombreasegurado');
        var apellidoasegurado = this.validate('apellidoasegurado');
        var fechanacimientoasse = this.validate('fechanacimientoasse');
        var ciudadrecidencia = this.validate('ciudadrecidencia');
        var telefonofijo = this.validate('telefonofijo');
        var celular = this.validate('celular');
        var direccion = this.validate('direccion');
        var email = this.validate('email');
        var telefonolaboral = this.validate('telefonolaboral');
        var jsonrespuestaform = this.validate('jsonrespuestaform');
        var fechasiniestro = this.validate('fechasiniestro');
        var horasiniestro = this.validate('horasiniestro');
        var descripcionsiniestro = this.validate('descripcionsiniestro');
        var textobackend = this.validate('textobackend');
        var observaciones = this.validate('observaciones');
        var motivobaja = this.validate('motivobaja');
        var cliente = this.validate('cliente');
        var estados_poliza = this.validate('estados_poliza');
        var producto = this.validate('producto');
        var ciudadsiniestro = this.validate('ciudadsiniestro');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'reclamoid' : reclamoid,
            'cedula' : cedula,
            'nombreasegurado' : nombreasegurado,
            'apellidoasegurado' : apellidoasegurado,
            'fechanacimientoasse' : fechanacimientoasse,
            'ciudadrecidencia' : ciudadrecidencia,
            'telefonofijo' : telefonofijo,
            'celular' : celular,
            'direccion' : direccion,
            'email' : email,
            'telefonolaboral' : telefonolaboral,
            'jsonrespuestaform' : jsonrespuestaform,
            'fechasiniestro' : fechasiniestro,
            'horasiniestro' : horasiniestro,
            'descripcionsiniestro' : descripcionsiniestro,
            'textobackend' : textobackend,
            'observaciones' : observaciones,
            'motivobaja' : motivobaja,
            'cliente' : cliente,
            'estados_poliza' : estados_poliza,
            'producto' : producto,
            'ciudadsiniestro' : ciudadsiniestro
        };

        console.log(parametrer);

        $.ajax({
            url: '/reclamos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.listchekings = function(){
        var nombre = this.validate('nombre');
        var img = this.validate('img');
        var tipo = this.validate('tipo');
        var titulo = this.validate('titulo');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var descripcion = this.validate('descripcion');
        var subdescripcion = this.validate('subdescripcion');
        var ayuda = this.validate('ayuda');
        var tipo_polizas = this.validate('tipo_polizas');
        var campana = this.validate('campana');
        var aseguradora = this.validate('aseguradora');      
        var tienda = this.validate('tienda'); 
        var asegurado = this.validate('asegurado'); 
        var reclamo = this.validate('reclamo');

        var parametrer = {
            'nombre' : nombre,
            'img' : img,
            'tipo': tipo,
            'titulo': titulo,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'descripcion' : descripcion,
            'subdescripcion' : subdescripcion,
            'ayuda' : ayuda,
            'tipo_polizas' : tipo_polizas,
            'campana' : campana,
            'aseguradora' : aseguradora,
            'tienda' : tienda,
            'asegurado' : asegurado,
            'reclamo' : reclamo
        };

        $.ajax({
            url: '/listchekings/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.reload();
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    }
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>