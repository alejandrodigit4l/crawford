<?php $__env->startSection('content'); ?>

	<h4>Reclamos </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Reclamos</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="reclamoid" id="reclamoid" class="form-control" placeholder="reclamoid" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="cedula" id="cedula" class="form-control" placeholder="cedula" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="nombreasegurado" id="nombreasegurado" class="form-control" placeholder="nombreasegurado" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="apellidoasegurado" id="apellidoasegurado" class="form-control" placeholder="apellidoasegurado" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="date" name="fechanacimientoasse" id="fechanacimientoasse" class="form-control" placeholder="fechanacimientoasse" >
						<select name="ciudadrecidencia" id="ciudadrecidencia" class="form-control">
							<option value="">Seleccione ciudad de recidencia</option>
							<?php $__currentLoopData = $ciudades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciudad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($ciudad['id']); ?>"><?php echo e($ciudad['nombre']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<input type="text" name="telefonofijo" id="telefonofijo" class="form-control" placeholder="telefonofijo" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="celular" id="celular" class="form-control" placeholder="celular" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="direccion" id="direccion" class="form-control" placeholder="direccion" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="email" name="email" id="email" class="form-control" placeholder="email" >
						<input type="text" name="telefonolaboral" id="telefonolaboral" class="form-control" placeholder="telefonolaboral" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="jsonrespuestaform" id="jsonrespuestaform" class="form-control" placeholder="jsonrespuestaform" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="date" name="fechasiniestro" id="fechasiniestro" class="form-control" placeholder="fechasiniestro" >
						<input type="text" name="horasiniestro" id="horasiniestro" class="form-control" placeholder="horasiniestro" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="descripcionsiniestro" id="descripcionsiniestro" class="form-control" placeholder="descripcionsiniestro" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="textobackend" id="textobackend" class="form-control" placeholder="textobackend" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="observaciones" id="observaciones" class="form-control" placeholder="observaciones" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="motivobaja" id="motivobaja" class="form-control" placeholder="motivobaja" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<select name="cliente" id="cliente" class="form-control" >
							<option value="">Seleccione Cliente</option>
							<?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cliente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($cliente['id']); ?>"><?php echo e($cliente['cedula']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<select name="estados_poliza" id="estados_poliza" class="form-control" >
							<option value="">Seleccione Estado Poliza</option>
							<?php $__currentLoopData = $estados_polizas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $estados_poliza): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($estados_poliza['id']); ?>"><?php echo e($estados_poliza['nombre']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<select name="producto" id="producto" class="form-control" >
							<option value="">Seleccione producto</option>
							<?php $__currentLoopData = $productos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $producto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($producto['id']); ?>"><?php echo e($producto['titulo']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<select name="ciudadsiniestro" id="ciudadsiniestro" class="form-control" >
							<option value="">Seleccione Ciudad Siniestro</option>
							<?php $__currentLoopData = $ciudades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciudad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($ciudad['id']); ?>"><?php echo e($ciudad['nombre']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<br>
                        <div class="btn btn-success" onclick="reclamos.save();">Guardar</div>
                        <div class="btn btn-success" onclick="reclamos.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>reclamoid</th>
									<th>cedula</th>
									<th>nombreasegurado</th>
									<th>apellidoasegurado</th>
									<th>fechanacimientoasse</th>
									<th>ciudadrecidencia</th>
									<th>telefonofijo</th>
									<th>celular</th>
									<th>direccion</th>
									<th>email</th>
									<th>telefonolaboral</th>
									<th>jsonrespuestaform</th>
									<th>fechasiniestro</th>
									<th>horasiniestro</th>
									<th>descripcionsiniestro</th>
									<th>textobackend</th>
									<th>observaciones</th>
									<th>motivobaja</th>
									<th>cliente</th>
									<th>estados_poliza</th>
									<th>producto</th>
									<th>ciudadsiniestro</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $reclamos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data['id']); ?></td>
                                    <td><?php echo e($data['reclamoid']); ?></td>
									<td><?php echo e($data['cedula']); ?></td>
									<td><?php echo e($data['nombreasegurado']); ?></td>
									<td><?php echo e($data['apellidoasegurado']); ?></td>
									<td><?php echo e($data['fechanacimientoasse']); ?></td>
									<td><?php echo e($data['ciudadrecidencia']['nombre']); ?></td>
									<td><?php echo e($data['telefonofijo']); ?></td>
									<td><?php echo e($data['celular']); ?></td>
									<td><?php echo e($data['direccion']); ?></td>
									<td><?php echo e($data['email']); ?></td>
									<td><?php echo e($data['telefonolaboral']); ?></td>
									<td><?php echo e($data['jsonrespuestaform']); ?></td>
									<td><?php echo e($data['fechasiniestro']); ?></td>
									<td><?php echo e($data['horasiniestro']); ?></td>
									<td><?php echo e($data['descripcionsiniestro']); ?></td>
									<td><?php echo e($data['textobackend']); ?></td>
									<td><?php echo e($data['observaciones']); ?></td>
									<td><?php echo e($data['motivobaja']); ?></td>
									<td><?php echo e($data['cliente']['cedula']); ?></td>
									<td><?php echo e($data['estados_poliza']['nombre']); ?></td>
									<td><?php echo e($data['producto']['titulo']); ?></td>
									<td><?php echo e($data['ciudadsiniestro']['nombre']); ?></td>
                                    <td><a href="javascript:;" onclick="reclamos.update(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="reclamos.delete(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var reclamos = new Reclamos();

function Reclamos(){

    this.save = function(){
    	var reclamoid = this.validate('reclamoid');
		var cedula = this.validate('cedula');
		var nombreasegurado = this.validate('nombreasegurado');
		var apellidoasegurado = this.validate('apellidoasegurado');
		var fechanacimientoasse = this.validate('fechanacimientoasse');
		var ciudadrecidencia = this.validate('ciudadrecidencia');
		var telefonofijo = this.validate('telefonofijo');
		var celular = this.validate('celular');
		var direccion = this.validate('direccion');
		var email = this.validate('email');
		var telefonolaboral = this.validate('telefonolaboral');
		var jsonrespuestaform = this.validate('jsonrespuestaform');
		var fechasiniestro = this.validate('fechasiniestro');
		var horasiniestro = this.validate('horasiniestro');
		var descripcionsiniestro = this.validate('descripcionsiniestro');
		var textobackend = this.validate('textobackend');
		var observaciones = this.validate('observaciones');
		var motivobaja = this.validate('motivobaja');
		var cliente = this.validate('cliente');
		var estados_poliza = this.validate('estados_poliza');
		var producto = this.validate('producto');
		var ciudadsiniestro = this.validate('ciudadsiniestro');

        var parametrer = {
        	'reclamoid' : reclamoid,
			'cedula' : cedula,
			'nombreasegurado' : nombreasegurado,
			'apellidoasegurado' : apellidoasegurado,
			'fechanacimientoasse' : fechanacimientoasse,
			'ciudadrecidencia' : ciudadrecidencia,
			'telefonofijo' : telefonofijo,
			'celular' : celular,
			'direccion' : direccion,
			'email' : email,
			'telefonolaboral' : telefonolaboral,
			'jsonrespuestaform' : jsonrespuestaform,
			'fechasiniestro' : fechasiniestro,
			'horasiniestro' : horasiniestro,
			'descripcionsiniestro' : descripcionsiniestro,
			'textobackend' : textobackend,
			'observaciones' : observaciones,
			'motivobaja' : motivobaja,
			'cliente' : cliente,
			'estados_poliza' : estados_poliza,
			'producto' : producto,
			'ciudadsiniestro' : ciudadsiniestro
        };

        $.ajax({
            url: 'reclamos/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'reclamos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var reclamoid = this.validate('reclamoid');
		var cedula = this.validate('cedula');
		var nombreasegurado = this.validate('nombreasegurado');
		var apellidoasegurado = this.validate('apellidoasegurado');
		var fechanacimientoasse = this.validate('fechanacimientoasse');
		var ciudadrecidencia = this.validate('ciudadrecidencia');
		var telefonofijo = this.validate('telefonofijo');
		var celular = this.validate('celular');
		var direccion = this.validate('direccion');
		var email = this.validate('email');
		var telefonolaboral = this.validate('telefonolaboral');
		var jsonrespuestaform = this.validate('jsonrespuestaform');
		var fechasiniestro = this.validate('fechasiniestro');
		var horasiniestro = this.validate('horasiniestro');
		var descripcionsiniestro = this.validate('descripcionsiniestro');
		var textobackend = this.validate('textobackend');
		var observaciones = this.validate('observaciones');
		var motivobaja = this.validate('motivobaja');
		var cliente = this.validate('cliente');
		var estados_poliza = this.validate('estados_poliza');
		var producto = this.validate('producto');
		var ciudadsiniestro = this.validate('ciudadsiniestro');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'reclamoid' : reclamoid,
			'cedula' : cedula,
			'nombreasegurado' : nombreasegurado,
			'apellidoasegurado' : apellidoasegurado,
			'fechanacimientoasse' : fechanacimientoasse,
			'ciudadrecidencia' : ciudadrecidencia,
			'telefonofijo' : telefonofijo,
			'celular' : celular,
			'direccion' : direccion,
			'email' : email,
			'telefonolaboral' : telefonolaboral,
			'jsonrespuestaform' : jsonrespuestaform,
			'fechasiniestro' : fechasiniestro,
			'horasiniestro' : horasiniestro,
			'descripcionsiniestro' : descripcionsiniestro,
			'textobackend' : textobackend,
			'observaciones' : observaciones,
			'motivobaja' : motivobaja,
			'cliente' : cliente,
			'estados_poliza' : estados_poliza,
			'producto' : producto,
			'ciudadsiniestro' : ciudadsiniestro
        };

        $.ajax({
            url: 'reclamos/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'reclamos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'reclamos/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'reclamos';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'reclamos/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#reclamoid').val(response.body[0].reclamoid);
					$('#cedula').val(response.body[0].cedula);
					$('#nombreasegurado').val(response.body[0].nombreasegurado);
					$('#apellidoasegurado').val(response.body[0].apellidoasegurado);
					$('#fechanacimientoasse').val(response.body[0].fechanacimientoasse);
					$('#ciudadrecidencia').val(response.body[0].ciudadrecidencia);
					$('#telefonofijo').val(response.body[0].telefonofijo);
					$('#celular').val(response.body[0].celular);
					$('#direccion').val(response.body[0].direccion);
					$('#email').val(response.body[0].email);
					$('#telefonolaboral').val(response.body[0].telefonolaboral);
					$('#jsonrespuestaform').val(response.body[0].jsonrespuestaform);
					$('#fechasiniestro').val(response.body[0].fechasiniestro);
					$('#horasiniestro').val(response.body[0].horasiniestro);
					$('#descripcionsiniestro').val(response.body[0].descripcionsiniestro);
					$('#textobackend').val(response.body[0].textobackend);
					$('#observaciones').val(response.body[0].observaciones);
					$('#motivobaja').val(response.body[0].motivobaja);
					$('#cliente').val(response.body[0].cliente);
					$('#estados_poliza').val(response.body[0].estados_poliza);
					$('#producto').val(response.body[0].producto);
					$('#ciudadsiniestro').val(response.body[0].ciudadsiniestro);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'reclamos/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>