<?php $__env->startSection('content'); ?>

	<h4>Marcas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="/marcas/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <select  name="searchestado" id="searchestado"  class="form-control">
                        <option value="">BUSCAR POR ESTADO</option>
                        <option value="">TODOS</option>
                        <option value="ACTIVO">ACTIVO</option>s
                        <option value="INACTIVO">INACTIVO</option>
                    </select>
                    <input type="text" name="searchnombre" id="searchnombre" class="form-control" placeholder="Buscar Por Nombre" onkeyup="javascript:this.value=this.value.toUpperCase();">                    
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Marcas</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="nombre" class="form-control" id="nombre" style="color: black;" placeholder="Nombre Marca" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <select name="estado" id="estado" class="form-control" style="color: black;">
                            <option value="">Estado de la marca</option>
                            <option value="ACTIVO">Activo</option>
                            <option value="INACTIVO">Inactivo</option>
                        </select>
                        <input type="text" name="logo" id="logo" class="form-control" style="color: black;" placeholder="URL Imagen" value="" ><br>
                        <div class="btn btn-success" onclick="marcas.save();">Guardar</div>
                        <div class="btn btn-success" onclick="marcas.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>estado</th>
                                    <th>logo</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($marca['id']); ?></td>
                                    <td><?php echo e($marca['nombre']); ?></td>
                                    <td><?php echo e($marca['estado']); ?></td>
                                    <td><img width="50" src="<?php echo e($marca['logo']); ?>"></td>
                                    <td><a href="javascript:;" onclick="marcas.update(<?php echo e($marca['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="marcas.delete(<?php echo e($marca['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var marcas = new Marcas();

$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        marcas.search($('#searchnombre').val());
    }
});

$('#searchestado').on('change', function() {
    marcas.search($('#searchestado').val());
});

function Marcas(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var estado = this.validate('estado');
        var logo   = this.validate('logo');        

        var parametrer = {
            'nombre' : nombre,
            'estado' : estado,
            'logo'   : logo
        };

        $.ajax({
            url: '/marcas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/marcas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var nombre = this.validate('nombre');
        var estado = this.validate('estado');
        var logo   = this.validate('logo');   
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'nombre' : nombre,
            'estado' : estado,
            'logo'   : logo
        };

        $.ajax({
            url: '/marcas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/marcas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: '/marcas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/marcas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: '/marcas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#estado').val(response.body[0].estado);
                    $('#logo').val(response.body[0].logo);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = '/marcas/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>