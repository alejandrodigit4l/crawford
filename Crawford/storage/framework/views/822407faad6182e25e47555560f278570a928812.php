<?php $__env->startSection('content'); ?>

	<h4>Todos los clientes</h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
                    <a href="/crawford/export/clientes" class="btn btn-success" style="padding: 10px;margin-top: 10px;">Exportar en Excel</a>
            	</div>
            </div>

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>clientid</th>
                                    <th>nombre</th>
                                    <th>lastname</th>
                                    <th>email</th>
                                    <th>cedula</th>
                                    <th>password</th>
                                    <th>tokenAPI</th>
                                    <th>celular</th>
                                    <th>fechanacimiento</th>
                                    <th>fechaCC</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>                              
                                    <th><div style="width: 140px;text-align: center;">fecha_creado</div></th>
                                    <!--th>Crear</th-->
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($usuario['id']); ?></td>
                                    <td><?php echo e($usuario['clientid']); ?></td>
                                    <td><?php echo e($usuario['nombre']); ?></td>
                                    <td><?php echo e($usuario['lastname']); ?></td>
                                    <td><?php echo e($usuario['email']); ?></td>
                                    <td><?php echo e($usuario['cedula']); ?></td>
                                    <td><?php echo e($usuario['password']); ?></td>
                                    <td><?php echo e($usuario['tokenAPI']); ?></td>
                                    <td><?php echo e($usuario['celular']); ?></td>
                                    <td><?php echo e($usuario['fechanacimiento']); ?></td>
                                    <td><?php echo e($usuario['fechaCC']); ?></td>
                                    <td><?php echo e($usuario['marca']['nombre']); ?></td>
                                    <td><?php echo e($usuario['tipo_seguro']['nombre']); ?></td>
                                    <td><?php echo e($usuario['aseguradora']['nombre']); ?></td>
                                    <td><?php echo e($usuario['created_at']); ?></td>
                                    <!--td><a href="/crawford/create_reclamo/<?php echo e($usuario['id']); ?>"><button class="btn btn-success form-control">Crear reclamo</button></a></td-->
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#clientes').addClass('active');
    $('#search').focus();
});


var reclamos_crawford = new Reclamos_crawford;

$('#search').keypress(function(e) {
    if(e.which == 13) {
        console.log('buscando /.....');
        if($('#search').val()==''){
            location.href = '/crawford/clientes/search/ALL';
        }
        else{
            location.href = '/crawford/clientes/search/'+$('#search').val();
        }
        
    }
});


function Reclamos_crawford(){

    this.init = function(){
        //
    };

    this.marca = function(MARCA){
        //redireccionar a MARCA
        window.location.href = "/crawford/filtro_marca/"+MARCA;
    }

}

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>