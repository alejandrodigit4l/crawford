<?php $__env->startSection('content'); ?>

	<h4>Roles </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="roles/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Roles</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="menu" class="form-control" id="menu" placeholder="menu" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="submenu" class="form-control" id="submenu" placeholder="submenu" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="roles.save();">Guardar</div>
                        <div class="btn btn-success" onclick="roles.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                	<th>id</th>
	                                <th>marca</th>
									<th>nombre</th>
									<th>menu</th>
									<th>submenu</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rol): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                	<td><?php echo e($rol['id']); ?></td>
                                    <td><?php echo e($rol['marca']['nombre']); ?></td>
									<td><?php echo e($rol['nombre']); ?></td>
									<td><?php echo e($rol['menu']); ?></td>
									<td><?php echo e($rol['submenu']); ?></td>
                                    <td><?php echo e($rol['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="roles.update(<?php echo e($rol['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="roles.delete(<?php echo e($rol['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var roles = new Roles();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        roles.search($('#searchnombre').val());
    }
});

function Roles(){

    this.save = function(){
		var marca = this.validate('marca');
		var nombre = this.validate('nombre');
		var menu = this.validate('menu');
		var submenu = this.validate('submenu');

        var parametrer = {
			'marca' : marca,
			'nombre' : nombre,
			'menu' : menu,
			'submenu' : submenu
        };

        $.ajax({
            url: '/roles/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
		var marca = this.validate('marca');
		var nombre = this.validate('nombre');
		var menu = this.validate('menu');
		var submenu = this.validate('submenu');

        var parametrer = {
            'id'    : id,
			'marca' : marca,
			'nombre' : nombre,
			'menu' : menu,
			'submenu' : submenu
        };

        $.ajax({
            url: 'roles/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'roles/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'roles';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'roles/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
					$('#marca').val(response.body[0].marca);
					$('#nombre').val(response.body[0].nombre);
					$('#menu').val(response.body[0].menu);
					$('#submenu').val(response.body[0].submenu);         
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'roles/'+DATA;
    }

}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>