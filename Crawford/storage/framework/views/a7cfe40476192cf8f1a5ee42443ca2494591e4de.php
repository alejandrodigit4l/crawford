<?php $__env->startSection('content'); ?>

	<h4>Usuarios </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="usuarios/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Usuarios</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="roles" id="roles" class="form-control" >
                            <option value="">Roles</option>
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($role['id']); ?>"><?php echo e($role['nombre'].' '.$role['marca']); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <input type="text" name="username" class="form-control" id="username" placeholder="username" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="firstname" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="lastname" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="email" class="form-control" id="email" placeholder="email" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="password" name="password" class="form-control" id="password" placeholder="password" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="remember_token" class="form-control" id="remember_token" placeholder="remember_token" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="usuarios.save();">Guardar</div>
                        <div class="btn btn-success" onclick="usuarios.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                	<th>id</th>
                                	<th>roles</th>
									<th>username</th>
									<th>firstname</th>
									<th>lastname</th>
									<th>email</th>
									<th>password</th>
									<th>remember_token</th>
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                	<td><?php echo e($usuario['id']); ?></td>
                                    <td><?php echo e($usuario['roles']['nombre']); ?></td>
									<td><?php echo e($usuario['username']); ?></td>
									<td><?php echo e($usuario['firstname']); ?></td>
									<td><?php echo e($usuario['lastname']); ?></td>
									<td><?php echo e($usuario['email']); ?></td>
									<td>************<center><img width="20" src="https://image.flaticon.com/icons/svg/14/14343.svg"></center><div style="display: none"><?php echo e($usuario['password']); ?></div></td>
									<td><?php echo e($usuario['remember_token']); ?></td>
                                    <td><?php echo e($usuario['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="usuarios.update(<?php echo e($usuario['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="usuarios.delete(<?php echo e($usuario['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var usuarios = new Usuarios();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        usuarios.search($('#searchnombre').val());
    }
});

function Usuarios(){

    this.save = function(){
    	var roles = this.validate('roles');
		var username = this.validate('username');
		var firstname = this.validate('firstname');
		var lastname = this.validate('lastname');
		var email = this.validate('email');
		var password = this.validate('password');
		var remember_token = this.validate('remember_token');
		

        var parametrer = {
            'rol' : roles,
			'username' : username,
			'firstname' : firstname,
			'lastname' : lastname,
			'email' : email,
			'password' : password,
			'remember_token' : remember_token
        };

        $.ajax({
            url: 'usuarios/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
    	var roles = this.validate('roles');
		var username = this.validate('username');
		var firstname = this.validate('firstname');
		var lastname = this.validate('lastname');
		var email = this.validate('email');
		var password = this.validate('password');
		var remember_token = this.validate('remember_token');

        var parametrer = {
            'id'    : id,
            'rol' : roles,
			'username' : username,
			'firstname' : firstname,
			'lastname' : lastname,
			'email' : email,
			'password' : password,
			'remember_token' : remember_token
        };

        $.ajax({
            url: 'usuarios/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'usuarios/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'usuarios';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'usuarios/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#roles').val(response.body[0].roles);
					$('#username').val(response.body[0].username);
					$('#firstname').val(response.body[0].firstname);
					$('#lastname').val(response.body[0].lastname);
					$('#email').val(response.body[0].email);
					$('#password').val(response.body[0].password);
					$('#remember_token').val(response.body[0].remember_token);  
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'usuarios/'+DATA;
    }

}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>