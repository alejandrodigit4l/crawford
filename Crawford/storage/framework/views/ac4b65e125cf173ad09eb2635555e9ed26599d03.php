<?php $__env->startSection('content'); ?>

	<h4>Reclamaciones por ciudades</h4>

    <?php $cont=0; ?>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Palabra a buscar en la tabla">
                </div>
            </div>

            <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " id="tabla" data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>reclamoid</th>
                                    <th>cedula</th>
                                    <th>nombreasegurado</th>
                                    <th>apellidoasegurado</th>
                                    <th>fechanacimientoasse</th>
                                    <th>ciudadrecidencia</th>
                                    <th>telefonofijo</th>
                                    <th>celular</th>
                                    <th>direccion</th>
                                    <th>email</th>
                                    <th>telefonolaboral</th>
                                    <th>jsonrespuestaform</th>
                                    <th>fechasiniestro</th>
                                    <th>horasiniestro</th>
                                    <th>descripcionsiniestro</th>
                                    <th>textobackend</th>
                                    <th>observaciones</th>
                                    <th>motivobaja</th>
                                    <th>cliente</th>
                                    <th>estados_poliza</th>
                                    <th>producto</th>
                                    <th>ciudadsiniestro</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $reclamos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data['id']); ?></td>
                                    <td><a href="/crawford/idreclamo/<?php echo e($data['id']); ?>"><?php echo e($data['reclamoid']); ?></a></td>
                                    <td><?php echo e($data['cedula']); ?></td>
                                    <td><?php echo e($data['nombreasegurado']); ?></td>
                                    <td><?php echo e($data['apellidoasegurado']); ?></td>
                                    <td><?php echo e($data['fechanacimientoasse']); ?></td>
                                    <td><?php echo e($data['ciudadrecidencia']['nombre']); ?></td>
                                    <td><?php echo e($data['telefonofijo']); ?></td>
                                    <td><?php echo e($data['celular']); ?></td>
                                    <td><?php echo e($data['direccion']); ?></td>
                                    <td><?php echo e($data['email']); ?></td>
                                    <td><?php echo e($data['telefonolaboral']); ?></td>
                                    <td id="reclamo<?php echo e($cont); ?>"><input type="hidden" id="input<?php echo e($cont); ?>" value="<?php echo e($data['jsonrespuestaform']); ?>"></td>
                                    <td><?php echo e($data['fechasiniestro']); ?></td>
                                    <td><?php echo e($data['horasiniestro']); ?></td>
                                    <td><?php echo e($data['descripcionsiniestro']); ?></td>
                                    <td><?php echo e($data['textobackend']); ?></td>
                                    <td><?php echo e($data['observaciones']); ?></td>
                                    <td><?php echo e($data['motivobaja']); ?></td>
                                    <td><?php echo e($data['cliente']['cedula']); ?></td>
                                    <td><?php echo e($data['estados_poliza']['nombre']); ?></td>
                                    <td><?php echo e($data['producto']['titulo']); ?></td>
                                    <td><?php echo e($data['ciudadsiniestro']['nombre']); ?></td>
                                </tr> 
                                <?php $cont++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    a{
        color: black;
    }
    a:hover{
        color: blue;
    }
</style>

<script type="text/javascript">
function prueba(r,id){
    $(id).html('');
    console.log(r);
    console.log('Cantidad'+r.length);
    var obj = JSON.parse(r);
    console.log(obj.pasos);
    for (var i = 0; i < obj.pasos.length; i++) {
        console.log('//'+obj.pasos[i].name);
        $(id).append(obj.pasos[i].name+':'+obj.pasos[i].value+'<br>');
    }
}

$(document).ready(function(){
    $('ul li').removeClass('active');
    $('#reclamos').addClass('active');
    $('#search').focus();
    //recorrer tabla para sacar las respuestas
    var nFilas = $("#tabla tr").length;
    for (var i = 0; i < nFilas; i++) {
        prueba( $('#input'+i).val() , '#reclamo'+i);
    }
});


$('#search').keypress(function(e) {
    if(e.which == 13) {
        console.log('buscando /.....');
        if($('#search').val()==''){
            location.href = '/crawford/search/ALL';
        }
        else{
            location.href = '/crawford/search/'+$('#search').val();
        }
        
    }
});

var reclamos_crawford = new Reclamos_crawford;

$('#filtromarca').on('change', function() {
    reclamos_crawford.marca($('#filtromarca').val());
});

function Reclamos_crawford(){

    this.init = function(){
        //
    };

    this.marca = function(MARCA){
        //redireccionar a MARCA
        window.location.href = "/crawford/filtro_marca/"+MARCA;
    }

}

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>