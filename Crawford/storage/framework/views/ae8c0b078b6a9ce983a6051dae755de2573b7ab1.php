<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="https://reqlut2.s3.amazonaws.com/uploads/logos/53a0991f0ed0653a0991f0ed7d45945.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Crawford-backend</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
   <!-- Bootstrap core CSS     -->
   <link href="http://localhost:8000/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="http://localhost:8000/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8000/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8000/css/demo.css" rel="stylesheet" />
    <link href="http://localhost:8000/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="http://localhost:8000/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8000/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8000/css/demo.css" rel="stylesheet" />
    <!--charts-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- JQuery -->
        <script type="text/javascript" src="http://localhost:8000/jscharts/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="http://localhost:8000/jscharts/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="http://localhost:8000/jscharts/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="http://localhost:8000/jscharts/mdb.min.js"></script>
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8000/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<div class="wrapper">
    
    <?php echo $__env->make('crawford.layouts.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="main-panel">
        
        <?php echo $__env->make('crawford.layouts.navbar-fixed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="content" data-color="crawford">
            

        	<?php echo $__env->yieldContent('content'); ?>


        </div>

        <?php echo $__env->make('crawford.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </div>
</div>


</body>
    <!--   Core JS Files   -->
    <script src="http://localhost:8000/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://localhost:8000/js/bootstrap.min.js" type="text/javascript"></script>
    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="http://localhost:8000/js/bootstrap-checkbox-radio-switch.js"></script>
    <!--  Charts Plugin -->
    <script src="http://localhost:8000/js/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="http://localhost:8000/js/bootstrap-notify.js"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="http://localhost:8000/js/light-bootstrap-dashboard.js"></script>
    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="http://localhost:8000/js/demo.js"></script>
    <script src="http://localhost:8000/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="http://localhost:8000/js/bootstrap.min.js" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="http://localhost:8000/js/bootstrap-checkbox-radio-switch.js"></script>
	<!--  Charts Plugin -->
	<script src="http://localhost:8000/js/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="http://localhost:8000/js/bootstrap-notify.js"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="http://localhost:8000/js/light-bootstrap-dashboard.js"></script>
	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="http://localhost:8000/js/demo.js"></script>
	<script type="text/javascript">
    	/*$(document).ready(function(){
        	demo.initChartist();
        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Bienvenido<b> Asesor Juridico</b> - La mejor experiencia de un abogado."
            },{
                type: 'info',
                timer: 4000
            });
    	});*/
	</script>
</html>