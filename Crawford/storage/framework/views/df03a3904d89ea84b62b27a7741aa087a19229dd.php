<?php $__env->startSection('content'); ?>

	<h4>Campañas </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="/campanas/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Campañas</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <select name="aseguradora" id="aseguradora" class="form-control" >
                            <option value="">Aseguradora</option>
                        </select>
                        <select id="ifcomprapoliza" name="ifcomprapoliza" class="form-control"><option value="">Campaña con compra de polizas?</option><option value="1">SI</option><option value="0">NO</option></select>
                        <select id="ifreclamos" name="ifreclamos" class="form-control"><option value="">Campaña con reclamos?</option><option value="1">SI</option><option value="0">NO</option></select>
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <br>
                        <div class="btn btn-success" onclick="campanas.save();">Guardar</div>
                        <div class="btn btn-success" onclick="campanas.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th>marca</th>
                                    <th>tipo_seguro</th>
                                    <th>aseguradora</th>
                                    <th>ifreclamos</th>
                                    <th>ifcomprapoliza</th>                                    
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $campanas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campana): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($campana['id']); ?></td>
                                    <td><?php echo e($campana['nombre']); ?></td>
                                    <td><?php echo e($campana['descripcion']); ?></td>
                                    <td><?php echo e($campana['marca']['nombre']); ?></td>
                                    <td><?php echo e($campana['tipo_seguro']['nombre']); ?></td>
                                    <td><?php echo e($campana['aseguradora']['nombre']); ?></td>
                                    <td><?php echo e($campana['ifreclamos']); ?></td>
                                    <td><?php echo e($campana['ifcomprapoliza']); ?></td>
                                    <td><?php echo e($campana['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="campanas.update(<?php echo e($campana['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="campanas.delete(<?php echo e($campana['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var campanas = new Campanas();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        campanas.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    campanas.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    campanas.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});


$('#searchestado').on('change', function() {
    campanas.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

function Campanas(){

    this.save = function(){
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var ifreclamos = this.validate('ifreclamos');
        var ifcomprapoliza = this.validate('ifcomprapoliza');
        var aseguradora = this.validate('aseguradora');      

        var parametrer = {
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'ifreclamos' : ifreclamos,
            'ifcomprapoliza' : ifcomprapoliza,
            'aseguradora' : aseguradora
        };

        $.ajax({
            url: '/campanas/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/campanas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var nombre = this.validate('nombre');
        var descripcion = this.validate('descripcion');
        var marca   = this.validate('marca');  
        var tipo_seguro = this.validate('tipo_seguro');
        var ifreclamos = this.validate('ifreclamos');
        var ifcomprapoliza = this.validate('ifcomprapoliza');
        var aseguradora = this.validate('aseguradora');      

        var parametrer = {
            'id'    : id,
            'nombre' : nombre,
            'descripcion' : descripcion,
            'marca'   : marca,
            'tipo_seguro' : tipo_seguro,
            'ifreclamos' : ifreclamos,
            'ifcomprapoliza' : ifcomprapoliza,
            'aseguradora' : aseguradora
        };

        $.ajax({
            url: '/campanas/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/campanas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: '/campanas/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = '/campanas';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: '/campanas/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#nombre').val(response.body[0].nombre);
                    $('#descripcion').val(response.body[0].descripcion);
                    $('#marca').val(response.body[0].marca);
                    $('#tipo_seguro').val(response.body[0].tipo_seguro);
                    $('#ifreclamos').val(response.body[0].ifreclamos);
                    $('#ifcomprapoliza').val(response.body[0].ifcomprapoliza);
                    $('#aseguradora').val(response.body[0].aseguradora);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = '/campanas/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: '/tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: '/aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>