<?php $__env->startSection('content'); ?>

	<h4>Documents </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Documents</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="estado" id="estado" class="form-control">
							<option value="">Seleccione un Estado Documentos</option>
							<option value="RECHAZADO">Rechazado</option>
							<option value="ACEPTADO">Aceptado</option>
							<option value="PENDIENTE">Pendiente</option>
						</select>
						<input type="text" name="validacion" id="validacion" class="form-control" placeholder="validacion" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="respuestaback" id="respuestaback" class="form-control" placeholder="respuestaback" onkeyup="javascript:this.value=this.value.toUpperCase();">
						<input type="text" name="url" id="url" class="form-control" placeholder="url" >
						<select name="reclamo" id="reclamo" class="form-control">
							<option value="">Seleccione un reclamo</option>
							<?php $__currentLoopData = $reclamos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reclamo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($reclamo['id']); ?>"><?php echo e($reclamo['reclamoid']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<select name="listcheking" id="listcheking" class="form-control">
							<option value="">Seleccione un listcheking</option>
							<?php $__currentLoopData = $listchekings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listcheking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($listcheking['id']); ?>"><?php echo e($listcheking['nombre']); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
                        <br>
                        <div class="btn btn-success" onclick="documents.save();">Guardar</div>
                        <div class="btn btn-success" onclick="documents.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>estado</th>
									<th>validacion</th>
									<th>respuestaback</th>
									<th>url</th>
									<th>reclamo</th>
									<th>listcheking</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data['id']); ?></td>
                                    <td><?php echo e($data['estado']); ?></td>
									<td><?php echo e($data['validacion']); ?></td>
									<td><?php echo e($data['respuestaback']); ?></td>
									<td><?php echo e($data['url']); ?></td>
									<td><?php echo e($data['reclamo']); ?></td>
									<td><?php echo e($data['listcheking']); ?></td>
                                    <td><a href="javascript:;" onclick="documents.update(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="documents.delete(<?php echo e($data['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var documents = new Documents();


function Documents(){

    this.save = function(){
    	var estado = this.validate('estado');
		var validacion = this.validate('validacion');
		var respuestaback = this.validate('respuestaback');
		var url = this.validate('url');
		var reclamo = this.validate('reclamo');
		var listcheking = this.validate('listcheking');

        var parametrer = {
        	'estado' : estado,
			'validacion' : validacion,
			'respuestaback' : respuestaback,
			'url' : url,
			'reclamo' : reclamo,
			'listcheking' : listcheking
        };

        $.ajax({
            url: 'documentsmodel/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var estado = this.validate('estado');
		var validacion = this.validate('validacion');
		var respuestaback = this.validate('respuestaback');
		var url = this.validate('url');
		var reclamo = this.validate('reclamo');
		var listcheking = this.validate('listcheking');
        var id     = this.validate('id');        

        var parametrer = {
            'id'     : id,
            'estado' : estado,
			'validacion' : validacion,
			'respuestaback' : respuestaback,
			'url' : url,
			'reclamo' : reclamo,
			'listcheking' : listcheking
        };

        $.ajax({
            url: 'documentsmodel/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'documentsmodel/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'documentsmodel';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'documentsmodel/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                    $('#estado').val(response.body[0].estado);
					$('#validacion').val(response.body[0].validacion);
					$('#respuestaback').val(response.body[0].respuestaback);
					$('#url').val(response.body[0].url);
					$('#reclamo').val(response.body[0].reclamo);
					$('#listcheking').val(response.body[0].listcheking);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'documentsmodel/'+DATA;
    }
}


</script>
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>