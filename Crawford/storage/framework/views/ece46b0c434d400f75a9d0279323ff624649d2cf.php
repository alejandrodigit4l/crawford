<?php $__env->startSection('content'); ?>

	<h4>Clientes </h4>


<div class="main-content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card" data-color="crawford" style="background: #f6f6f6;">
                    <p>Herramientas de Busqueda</p>
                    <a href="clientes/download/all" style="width: 100%;" ><img width="50" src="https://image.flaticon.com/icons/png/512/303/303850.png"></a><br><br>
                    <input type="text" name="searchnombre" class="form-control" id="searchnombre" placeholder="Buscar por Palabra" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </div>

            <div class="col-md-12">
            	<div class="card" data-color="crawford">
            		<p>Formulario Usuarios</p>      
                    <form data-color="crawford">
                        <input type="hidden" name="id" id="id" value="">
                        <select name="marca" id="marca" class="form-control" >
                            <option value="">Estado de la marca</option>
                            <?php $__currentLoopData = $marcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $marca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($marca['id']); ?>"><?php echo e($marca['nombre']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <select name="tipo_seguro" id="tipo_seguro" class="form-control" >
                            <option value="">Tipo seguro</option>
                        </select>
                        <select name="aseguradora" id="aseguradora" class="form-control" >
                            <option value="">Aseguradora</option>
                        </select>
                        <input type="text" name="clientid" class="form-control" id="clientid" placeholder="clientid" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="nombre" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="lastname" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="email" name="email" class="form-control" id="email" placeholder="email" value="" >
                        <input type="text" name="cedula" class="form-control" id="cedula" placeholder="cedula" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="password" name="password" class="form-control" id="password" placeholder="password" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="tokenAPI" class="form-control" id="tokenAPI" placeholder="tokenAPI" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="text" name="celular" class="form-control" id="celular" placeholder="celular" value="" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <input type="date" name="fechanacimiento" class="form-control" id="fechanacimiento" placeholder="fechanacimiento" value="" >
                        <input type="date" name="fechaCC" class="form-control" id="fechaCC" placeholder="fechaCC" value="" >
                        <br>
                        <div class="btn btn-success" onclick="clientes.save();">Guardar</div>
                        <div class="btn btn-success" onclick="clientes.sendUpdate();">Actualizar</div>
                    </form>      		
            	</div>
            </div>
           
           <div class="col-md-12">
                <div class="card" data-color="crawford">
                    <div class="content table-responsive table-full-width" style="overflow-x: scroll;overflow-y: scroll;height: 600px;">
                        <table class="table table-hover " data-color="crawford">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>clientid</th>
									<th>nombre</th>
									<th>lastname</th>
									<th>email</th>
									<th>cedula</th>
									<th>password</th>
									<th>tokenAPI</th>
									<th>celular</th>
									<th>fechanacimiento</th>
									<th>fechaCC</th>
									<th>marca</th>
									<th>tipo_seguro</th>
									<th>aseguradora</th>                              
                                    <th>fecha creado</th>
                                    <th>Actualizar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($usuario['id']); ?></td>
                                    <td><?php echo e($usuario['clientid']); ?></td>
									<td><?php echo e($usuario['nombre']); ?></td>
									<td><?php echo e($usuario['lastname']); ?></td>
									<td><?php echo e($usuario['email']); ?></td>
									<td><?php echo e($usuario['cedula']); ?></td>
									<td><?php echo e($usuario['password']); ?></td>
									<td><?php echo e($usuario['tokenAPI']); ?></td>
									<td><?php echo e($usuario['celular']); ?></td>
									<td><?php echo e($usuario['fechanacimiento']); ?></td>
									<td><?php echo e($usuario['fechaCC']); ?></td>
									<td><?php echo e($usuario['marca']['nombre']); ?></td>
									<td><?php echo e($usuario['tipo_seguro']['nombre']); ?></td>
									<td><?php echo e($usuario['aseguradora']['nombre']); ?></td>
                                    <td><?php echo e($usuario['created_at']); ?></td>
                                    <td><a href="javascript:;" onclick="clientes.update(<?php echo e($usuario['id']); ?>);" style="color: red;"><button class="btn btn-warning">></button></a></td>
                                    <td><a href="javascript:;" onclick="clientes.delete(<?php echo e($usuario['id']); ?>);" style="color: red;"><button class="btn btn-danger">x</button></a></td>
                                </tr> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                    
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var clientes = new Clientes();


$('#searchnombre').keypress(function(e) {
    if(e.which == 13) {
        clientes.search($('#searchnombre').val());
    }
});


$('#marca').on('change', function() {
    clientes.tipo_seguro($('#marca').val(),'#tipo_seguro','Tipo seguro');
});
$('#tipo_seguro').on('change', function() {
    clientes.aseguradora($('#tipo_seguro').val(),'#aseguradora','Aseguradora');
});


$('#searchestado').on('change', function() {
    clientes.tipo_seguro($('#searchestado').val(),'#searchtipo_seguro','Buscar Tipo seguro');
});

function Clientes(){

    this.save = function(){
    	var clientid = this.validate('clientid');
		var nombre = this.validate('nombre');
		var lastname = this.validate('lastname');
		var email = this.validate('email');
		var cedula = this.validate('cedula');
		var password = this.validate('password');
		var tokenAPI = this.validate('tokenAPI');
		var celular = this.validate('celular');
		var fechanacimiento = this.validate('fechanacimiento');
		var fechaCC = this.validate('fechaCC');
		var marca = this.validate('marca');
		var tipo_seguro = this.validate('tipo_seguro');
		var aseguradora = this.validate('aseguradora');

        var parametrer = {
        	'clientid' : clientid,
			'nombre' : nombre,
			'lastname' : lastname,
			'email' : email,
			'cedula' : cedula,
			'password' : password,
			'tokenAPI' : tokenAPI,
			'celular' : celular,
			'fechanacimiento' : fechanacimiento,
			'fechaCC' : fechaCC,
			'marca' : marca,
			'tipo_seguro' : tipo_seguro,
			'aseguradora' : aseguradora
        };

        $.ajax({
            url: 'clientes/create', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SAVE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.sendUpdate = function(){
        var id = this.validate('id');
        var clientid = this.validate('clientid');
		var nombre = this.validate('nombre');
		var lastname = this.validate('lastname');
		var email = this.validate('email');
		var cedula = this.validate('cedula');
		var password = this.validate('password');
		var tokenAPI = this.validate('tokenAPI');
		var celular = this.validate('celular');
		var fechanacimiento = this.validate('fechanacimiento');
		var fechaCC = this.validate('fechaCC');
		var marca = this.validate('marca');
		var tipo_seguro = this.validate('tipo_seguro');
		var aseguradora = this.validate('aseguradora');  

        var parametrer = {
            'id'    : id,
            'clientid' : clientid,
			'nombre' : nombre,
			'lastname' : lastname,
			'email' : email,
			'cedula' : cedula,
			'password' : password,
			'tokenAPI' : tokenAPI,
			'celular' : celular,
			'fechanacimiento' : fechanacimiento,
			'fechaCC' : fechaCC,
			'marca' : marca,
			'tipo_seguro' : tipo_seguro,
			'aseguradora' : aseguradora
        };

        $.ajax({
            url: 'clientes/update', //This is the current doc
            type: "POST",
            data: parametrer,
            success: function(response){
                console.log('SENDUPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 

    };

    this.delete = function(id){
        $.ajax({
            url: 'clientes/delete/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('DELETE:');
                console.log(response);
                if(response.Status == 'successful'){
                    location.href = 'clientes';
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.update = function(id){
        $.ajax({
            url: 'clientes/get/'+id, //This is the current doc
            type: "GET",
            success: function(response){
                console.log('UPDATE:');
                console.log(response);
                if(response.Status == 'successful'){
                    $('#id').val(response.body[0].id);
                	$('#clientid').val(response.body[0].clientid);
					$('#nombre').val(response.body[0].nombre);
					$('#lastname').val(response.body[0].lastname);
					$('#email').val(response.body[0].email);
					$('#cedula').val(response.body[0].cedula);
					$('#password').val(response.body[0].password);
					$('#tokenAPI').val(response.body[0].tokenAPI);
					$('#celular').val(response.body[0].celular);
					$('#fechanacimiento').val(response.body[0].fechanacimiento);
					$('#fechaCC').val(response.body[0].fechaCC);
					$('#marca').val(response.body[0].marca);
					$('#tipo_seguro').val(response.body[0].tipo_seguro);
					$('#aseguradora').val(response.body[0].aseguradora);
                }  
                else{
                    alert('Algo salio mal, intenta nuevamente');
                }
            }
        }); 
    };

    this.validate = function(DATA){
        if($('#'+DATA).val() != ''){
            $('#'+DATA).css('border','1px solid green');
            return $('#'+DATA).val();
        }
        else{
            alert('Algo anda mal, verifica');
            $('#'+DATA).css('border','1px solid red');
            exit();
        }
    };

    this.search = function(DATA){
        location.href = 'clientes/'+DATA;
    }


    /* TOMAR TODOS LOS SEGUROS DE UNA MARCA EN ESPECIAL */
    this.tipo_seguro = function(MARCA,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'tipo_seguros/search/'+MARCA, //This is the marca
            type: "GET",
            success: function(response){
                console.log('TIPOS_SEGUROS MARCA:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
    /* TOMAR TODOS LOS SEGUROS DE UNA TIPO_SEGURO EN ESPECIAL */
    this.aseguradora = function(TIPO_SEGURO,nombre,vacio){
        $(nombre).empty().append('<option value="">'+vacio+'</option>');
        $.ajax({
            url: 'aseguradoras/search/'+TIPO_SEGURO, //This is the TIPO_SEGURO
            type: "GET",
            success: function(response){
                console.log('ASEGURADORA TIPO_SEGURO:');
                console.log(response);
                jQuery.each( response.body, function( i, val ) {
                    $(nombre).append('<option value="'+val.id+'">'+val.nombre+'</option>');
                });                
            }
        }); 
    };
}


</script>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('crawford.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>